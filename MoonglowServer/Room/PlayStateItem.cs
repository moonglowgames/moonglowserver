﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Room
{
    public interface IRuleAble
    {
        Rule rule { get; set; }
    }

    public abstract class PSBase : IRuleAble, IStateItem<PlayState>
    {
        public Rule rule { get; set; }

        abstract public void ExecuteOnEnter( PlayState beforeState);
        abstract public void ExecuteOnUpdate();
        abstract public void ExecuteOnExit();

        void IStateItem<PlayState>.OnEnter(PlayState beforeState)
        {
            ExecuteOnEnter(beforeState);
        }

        void IStateItem<PlayState>.OnExit()
        {
            ExecuteOnExit();
        }

        void IStateItem<PlayState>.OnUpdate()
        {
            ExecuteOnUpdate();
        }
    }

    public class PSIUnkown : PSBase
    {
        public override void ExecuteOnEnter(PlayState beforeState) { }
        public override void ExecuteOnUpdate() { }
        public override void ExecuteOnExit() { }

    }
 
    class PSIReady: PSBase
    {
        public override void ExecuteOnEnter(PlayState beforeState) { }
        public override void ExecuteOnUpdate() { }
        public override void ExecuteOnExit() { }
    }

    class PSIStart : PSBase
    {
        public override void ExecuteOnEnter(PlayState beforeState) {

            Console.WriteLine("PSIStart");

            //send SCNotiStartGame 
            rule.SendSCNotiStartGame();

            //stamina -10
            rule.DecreaseStamina();
            rule.DecreaseStake();

            // init cards...
            rule.InitRoomCard();

            //send deck to hand each other...
            rule.SendSCNotiDeckToHandEachOther();
        }
        public override void ExecuteOnUpdate() { }
        public override void ExecuteOnExit() { }
    }

    class PSITurnUseCard : PSBase
    {
        public override void ExecuteOnEnter(PlayState beforeState)
        {
            Console.WriteLine("PSITurnUseCard");

            PlayerTurn();
            // refill resource...
            rule.RefillPriceResource();
        }

        public override void ExecuteOnUpdate() { }
        public override void ExecuteOnExit() { }

        void PlayerTurn()
        {
            //send SCNotiPlayerFocus 
            rule.SendSCNotiPlayerFocus();
        }
    }

    class PSITurnRecieveCard : PSBase
    {
        public override void ExecuteOnEnter(PlayState beforeState)
        {
            Console.WriteLine("PSITurnRecieveCard");

            // receive card from deck, that could not be joker card
            rule.GetOneCardFromAll();

            // send SCNotihandList in the rule GetOneCardFromAll();
        }

        public override void ExecuteOnUpdate() { }
        public override void ExecuteOnExit() { }

    }

    class PSIEnd : PSBase
    {
        public override void ExecuteOnEnter(PlayState beforeState)
        {
            Console.WriteLine("PSIEnd");
           
            //SCNotiGameEnd
            rule.SendSCNotiGameEnd();
        }

        public override void ExecuteOnUpdate() { }
        public override void ExecuteOnExit() { }
    }

    class PSIResult : PSBase
    {
        public override void ExecuteOnEnter(PlayState beforeState)
        {
            Console.WriteLine("PSIResult");

           //SCNotiGameResult 
            rule.SendSCNotiGameResult();
        }

        public override void ExecuteOnUpdate() { }
        public override void ExecuteOnExit() { }
    }

    class PSIDelete : PSBase
    {
        public override void ExecuteOnEnter(PlayState beforeState)
        {
            Console.WriteLine("PSIDelete");

            // later becase room remain empty or delete ?
            // 1. make delete room packet internally
            // 2. send packet to RoomManager...
            rule.WantRemoveThisRoom = true;
        }

        public override void ExecuteOnUpdate() { }
        public override void ExecuteOnExit() { }
    }
}
