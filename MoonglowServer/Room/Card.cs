﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Room
{
    public class Card
    {
        public bool isTryCard = false;
        private int _index = 0;
        public int Index
        {
            get { return _index; }
            set {
                // get cardinfo
                CardInfo info = CardManager.Instance.Get(value);
                if (info != null)
                {
                    _cardInfo = info;
                    _index = value;
                }
                else
                {
                    //Console.WriteLine("ERROR!!!: cardInfo is null: "+ "Card Index is: "+value );
                    if (GameServer._logger.IsErrorEnabled)
                    {
                        string log = string.Format("CardInfo is null, cardIdx: {0}", value);
                        GameServer._logger.Error(log);
                    }
                }
            }
        }
        
        private CardInfo _cardInfo = null;
        public  CardInfo Info
        {
            get
            {
                return _cardInfo;
            }
            
            set 
            {
                _cardInfo = value;
            }
        }


        public virtual CardAbility GetCardAbility()
        {
            return Info.ability;
        }
    }


    public class JokerCard : Card {
        private  MoonGlow.CardAbility  _cardAbility;
    
        public void SetAbility( MoonGlow.CardAbility cardAbility_)
        {
            _cardAbility = cardAbility_;
        }

        public override CardAbility GetCardAbility()
        {
            return _cardAbility;
        }

        //later we make joker and we will finish this games..
        public void SetAbilityInk( int InkIndex_ )
        {

        }
    }
}
