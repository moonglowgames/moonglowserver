﻿using MoonGlow.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Room
{
    public class RoomCardManager
    {
        private List<int> _gameCards = new List<int>();
        private List<List<int>> _allPlayersHandCard = null;
        public Dictionary<int,int> _itemCardDic = null;//test firstly...
        public bool isFirstInitial = false;

        public void InitGameCard()
        {
            // 1. new _allCards
            if(_gameCards == null) _gameCards = new List<int>();

            // 2. Set Full Cards ( 1... 98 of CardManager.GetAllCard() except joker )
            foreach (var key in CardManager.Instance.GetItemKeys)
            {
                //get all cards except joker and petcards
                if ((key != CardManager.JOKER_INDEX) && (key / 1000 != 1))
                {
                    _gameCards.Add(key);
                }
            }
            
            // get handcards...
            if (_allPlayersHandCard != null)
            {
                List<int> removeCardIdxList = new List<int>();
            
                for (int i = 0; i < _allPlayersHandCard.Count; i++)
                {
                    foreach (var handCardIdx in _allPlayersHandCard[i])
                    {
                        if (handCardIdx / 1000 == 1)
                        {
                            removeCardIdxList.Add(handCardIdx - 1000);
                        }
                        else
                        {
                            removeCardIdxList.Add(handCardIdx);
                        }
                    }
                }
            
                // remove handcard
                for (int n = (_gameCards.Count-1 ) ; n >= 0; n--)
                {
                    foreach (var removeCardIdx in removeCardIdxList)
                    {
                        if (_gameCards[n] == removeCardIdx)
                        {
                            _gameCards.RemoveAt(n);
                            break;
                        }
                    }
                }
            }
            // 3. Shuffle
            Random rand = new Random();
            List<int> tempGameCards = new List<int>();
            int randIdx = 0;

            int count = _gameCards.Count;
            for (int i = 0; i < count; i++)
            {
                randIdx = rand.Next(0, _gameCards.Count);
                tempGameCards.Add( _gameCards[randIdx] );
                Console.Write("{0}:{1} ", i, _gameCards[randIdx]);
                _gameCards.RemoveAt(randIdx);
               
            }
            _gameCards = tempGameCards;

            //set attach item idx.. test firstly..
            if (isFirstInitial == true)
            {
                isFirstInitial = false;
                List<int> tmpGameCards = new List<int>();
                foreach (var cardIdx in _gameCards)
                {
                    tmpGameCards.Add(cardIdx);
                }

                _itemCardDic = new Dictionary<int, int>();

                Random rand1 = new Random();
                int randIdx2 = 0;
                while (_itemCardDic.Count < 5)
                {
                    randIdx2 = rand1.Next(0, tmpGameCards.Count);

                    if (!_itemCardDic.ContainsKey(tmpGameCards[randIdx2]))
                    {
                        _itemCardDic.Add(tmpGameCards[randIdx2], 0);
                        tmpGameCards.RemoveAt(randIdx2);
                    }
                }
            }
        }

        public void InitPlayerHandCard(PlayerManager playerManager)
        {
            // new Deck +  PlayerNum_ cards
            // 0 = deck , 1~ 4 = player

            // init all player hand card...
            _allPlayersHandCard = new List<List<int>>();


            // init deck card... default is 0
            List<int> deckCard = new List<int>();
            for (int k = 0; k < 4; k++)
            {
                // defalt is 0... set during play
                deckCard.Add(0);
            }
            _allPlayersHandCard.Add(deckCard);//_allPlayersHandCard[0]

            // playerCount handcard
            List<int> playerHandCard = null;
            int cardIdx = 0;

            UserInfo playerInfo = null;
            foreach (Player player in playerManager)
            {
                playerHandCard = new List<int>();
                int maxCardCount = 6;

                List<int> playerPetList = null;
                playerInfo = GameServer.GetUserInfo(player.session);
                if (playerInfo !=null)
                {
                    if (playerInfo.userDataBase.userJoker.spellIdxList != null)
                    {
                        if (playerInfo.userDataBase.userJoker.spellIdxList.Count > 0 && playerInfo.userDataBase.userJoker.faceIdx != 0)
                        {
                            maxCardCount = 5;
                            playerHandCard.Add(CardManager.JOKER_INDEX);//joker card index = 999
                        }
                    }
                    if(playerInfo._playerItemInfo != null) playerPetList = playerInfo._playerItemInfo.petList;
                }

                for (int i = 0; i < maxCardCount; i++)
                {
                    if (GetGameCard(out cardIdx, playerPetList/*playerInfo._playerItemInfo.petList*/))
                    {
                        playerHandCard.Add(cardIdx);
                    }
                    else { /*Oops! ... card is none...*/ }
                }

                _allPlayersHandCard.Add(playerHandCard); //_allPlayersHandCard[1~4]
            }
        }
        public bool GetGameCard( out int cardIdx_, List<int> itemInfoPetList)
        {
            bool retvalue = false;
            cardIdx_ = 0;

            if (_gameCards.Count == 0)
            {
                InitGameCard();
            }

            if( _gameCards.Count > 0 )
            {
                cardIdx_ = _gameCards[0];

                int petIdx = 0;
                if (ItemManager.Instance.GetPetIdxWithCardIdx(cardIdx_, out petIdx))
                {
                    if (itemInfoPetList != null)
                    {
                        if (itemInfoPetList.Contains(petIdx))
                        {
                            cardIdx_ = 1000 + cardIdx_;//change base card index to pet card index...
                        }
                    }
                }

                _gameCards.RemoveAt(0);

                retvalue = true;
            }

            return retvalue;
        }

        // just test
        //private bool BottomInsertGameCardForReuse(int cardIdx_)
        //{
        //    bool retvalue = false;
        //    if( cardIdx_ > 0)
        //    {
        //        Random rand = new Random();

        //        // random insert about all 70 ... insert at ( 35 ~ 70 ) for prevent sequenced get
        //        if (cardIdx_ / 1000 == 1)
        //        {
        //            cardIdx_ = cardIdx_ - 1000;
        //        }
        //        _gameCards.Insert(rand.Next( _gameCards.Count /2  , _gameCards.Count), cardIdx_);

        //       retvalue = true;
        //    }

        //    return retvalue;
        //}

        public bool GetHandCard(int playerNum_, int handPosition_ , out Card card_)
        {
            bool retvalue = false;
            card_ = null;

            // deck don't hanve hand card so is not playerNum == 0
            if (playerNum_ < 1 || playerNum_ > _allPlayersHandCard.Count)
            {
                retvalue = false;
            }


            // 0 = deck , 1 = player1 , 2 = player2 ... 4= player 4
            List<int> playerHandCard = _allPlayersHandCard[playerNum_];


            // check hand position index enable
            if( handPosition_ < 1 || handPosition_ > playerHandCard.Count )
            {
                retvalue = false;
            }
            else
            {

                Card card = new Card();
                card.Index = playerHandCard[ ( handPosition_ - 1 )];

                card_ = card;
                retvalue = true;
            }

            return retvalue;
        }


        //showType: 0 = show card, 1 = getting animation from deck
        internal bool GetCardList(int playerNumber, string playerSession, ShowAnimationType showAnimationType, out CardList cardList_)
        {
            bool retvalue = false;
            cardList_ = null;

            // 0 = deck player 2 = (..count == 3)
            if( _allPlayersHandCard.Count > playerNumber )
            {
                List<CardListInfo> infoList = new List<CardListInfo>();

                int attachItemIdx = 0;//test first...
                int cardIdx = 0;
                for (int i = 0; i < _allPlayersHandCard[playerNumber].Count; i++)
                {
                    cardIdx = _allPlayersHandCard[playerNumber][i];
                    //; itemIdx: Gold = 2  MoonRuby = 3
                    //; Items: EtherealInk = 101 SaqqaraTablet = 102   RosemaryOil = 103 SLElixir = 104     EPElixir = 105   SOElixir = 106
                    //; Badge: 711 712 713 721 722 723

                    //test first...
                    if (_itemCardDic.ContainsKey(cardIdx)) { _itemCardDic[cardIdx] = 3; attachItemIdx = 3; }
                    else { attachItemIdx = 0; }

                    infoList.Add(new CardListInfo(cardIdx, attachItemIdx, i + 1));
                }

                int cardSkinIdx = 900;
                int jokerFaceIdx = 0;
                List<int> spellIdxList = new List<int>();
                UserInfo userInfo = GameServer.GetUserInfo(playerSession);
                if (userInfo != null)
                {
                    jokerFaceIdx = userInfo.userDataBase.userJoker.faceIdx;
                    if (userInfo.userDataBase.userJoker.spellIdxList != null)
                    {
                        foreach (var spellIdx in userInfo.userDataBase.userJoker.spellIdxList)
                        {
                            spellIdxList.Add(spellIdx);
                        }
                    }

                    if (userInfo.userDataBase.userItem.cardSkinList != null)
                    {
                        foreach (var cardSkin in userInfo.userDataBase.userItem.cardSkinList)
                        {
                            if (cardSkin.isSelect == true)
                            {
                                cardSkinIdx = cardSkin.skinIdx;
                            }
                        }
                    }
                }
                cardList_ = new CardList(PacketTypeList.SCNotiDeckToHand.ToString(), playerNumber, showAnimationType, infoList, cardSkinIdx, jokerFaceIdx, spellIdxList);
                retvalue = true;
            }

            return retvalue;
            
        }

        internal bool GetCardListOne(Player currentPlayer_, ShowAnimationType showAnimationType_, int handPosition_, out CardList cardList_)
        {
            bool retvalue = false;
            cardList_ = null;

            int playerNumber_ = currentPlayer_.playerNumber;
            // 0 = deck player 2 = (..count == 3)
            if (_allPlayersHandCard.Count > playerNumber_)
            {
                List<CardListInfo> infoList = new List<CardListInfo>();

                if (_allPlayersHandCard[playerNumber_].Count >= handPosition_)
                {
                    int cardIdx = _allPlayersHandCard[playerNumber_][handPosition_ - 1];
                    //test first...
                    int attachItemIdx = 0;
                    if (_itemCardDic.ContainsKey(cardIdx)) { _itemCardDic[cardIdx] = 2; attachItemIdx = 2; }
                    else { attachItemIdx = 0; }
                    infoList.Add(new CardListInfo(cardIdx, attachItemIdx, handPosition_));

                    int cardSkinIdx = 900;
                    int jokerFaceIdx = 0;
                    List<int> spellIdxList = null;
                    
                    UserInfo userInfo = GameServer.GetUserInfo(currentPlayer_.session);
                    if (userInfo != null)
                    {
                        if (userInfo.userDataBase.userJoker.spellIdxList != null)
                        {
                            jokerFaceIdx = userInfo.userDataBase.userJoker.faceIdx;
                            spellIdxList = userInfo.userDataBase.userJoker.spellIdxList;
                        }
                        if (userInfo.userDataBase.userItem.cardSkinList != null)
                        {
                            foreach (var cardSkin in userInfo.userDataBase.userItem.cardSkinList)
                            {
                                if (cardSkin.isSelect == true)
                                {
                                    cardSkinIdx = cardSkin.skinIdx;
                                    break;
                                }
                            }
                        }
                    }
                    
                    cardList_ = new CardList(PacketTypeList.SCNotiDeckToHand.ToString(), playerNumber_, showAnimationType_, infoList, cardSkinIdx, jokerFaceIdx, spellIdxList);
                    retvalue = true;
                }

            }

            return retvalue;

        }

        internal bool EmptyHandCard(int playerNumber, int handPosition_)
        {
            bool retvalue = false;

            // 0 = deck player 2 = (..count == 3)
            if (_allPlayersHandCard.Count > playerNumber)
            {
                if ( _allPlayersHandCard[playerNumber].Count >= handPosition_)
                {
                    _allPlayersHandCard[playerNumber][handPosition_ - 1] = 0;
                    retvalue = true;
                }
            }

            return retvalue;
        }

        internal bool FindEmptySlot(int playerNumber, out int emptySlotNumber)
        {
            // find cardindex is 0
            bool retvalue = false;
            emptySlotNumber = 0;

            // 0 = deck player 2 = (..count == 3)
            if (_allPlayersHandCard.Count > playerNumber)
            {
                for (int i = 0; i < _allPlayersHandCard[playerNumber].Count; i++)
                {
                    if (_allPlayersHandCard[playerNumber][i] == 0)
                    {
                        emptySlotNumber = i + 1;
                        retvalue = true;
                        break;
                    }
                }                    
            }
            return retvalue;
        }


        // deck's Exchange is true because deck's hand card info is always exist
        // player's Exchange is false
        internal bool SetHandCard(int playerNumber, int emptySlotNumber, int cardIndex, bool wantExchange_)
        {
            // if emptyposition cardindex is 0 then save to cardindex
            // if emptyposition cardindex is not 0 then return false
            bool retvalue = false;

            // 0 = deck player 2 = (..count == 3)
            if (_allPlayersHandCard.Count > playerNumber)
            {
                if (_allPlayersHandCard[playerNumber].Count >= emptySlotNumber)
                {
                    if (_allPlayersHandCard[playerNumber][emptySlotNumber - 1] == 0 || wantExchange_)
                    {
                        _allPlayersHandCard[playerNumber][emptySlotNumber - 1] = cardIndex;
                        retvalue = true;
                    }
                    else
                    {
                        //error no empty slot...
                    }
                }
            }

            return retvalue;
        }
    }
}
