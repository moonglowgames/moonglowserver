﻿using MoonGlow.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Room
{
    public class Room : CommandTransfer
    {
        public string roomID;
        public GameType gameType = GameType.Unknown;
        public PlayMode playMode = PlayMode.Unknown;
        public PlayTime playTime = PlayTime.Unknown;
        public int stake = 0;
        public bool disClose;
        public bool isLock;

        public int goalHP = 0;
        public int goalResource = 0;

        private static int roomCount = 0;
        private PlayerManager _playerManager;
        public List<string> _earlyOutPlayerSessionList = null;
        public List<string> _pausePlayerSessionList = null;

        public Rule _rule;
        private InspectorManager _inspectorManager;
        private RoomCardManager _roomCardManager;

        public PlayStateManager _playStateManager;

        public List<DeckInfo> _deckInfolist;

        public int MaxPlayer { get; private set; }
        public bool onlyDiscard = false;
        public bool isPlaying = false;
        internal int playCount = 0;
        public bool unNormal = false;
        internal DateTime startTime;
        public bool isRequested = false;
        internal bool singlePlayer = false;

        private Room()
        {
            // Init();
            // MaxPlayer = 0;
        }

        private void Init()
        {
            this.myTag = CommandTag.Room;
            this.WantRestart = false;

            _roomCardManager = new RoomCardManager();
            _inspectorManager = new InspectorManager();
            _playerManager = new PlayerManager(_roomCardManager);
            _playStateManager = new PlayStateManager();

            _deckInfolist = new List<DeckInfo>();

            _rule = new Rule(this, _roomCardManager, _playerManager, _playStateManager);
            _rule.SetParent(this);

            // set _rule to statemanager and stateitems...
            _playStateManager.InitStateItem(_rule);
        }

        internal void SetDisconnectPlayer(string userSession)
        {
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            Room room = this;
            if (GetPlayers().Count == 1 && room._playerManager.creator.session == userSession)//me, of course not playing
            {
                if (userInfo != null)
                {
                    //save tmp roominfo
                    userInfo.tmpRoomInfo = new Room();
                    userInfo.tmpRoomInfo.gameType = room.gameType;
                    userInfo.tmpRoomInfo.playTime = room.playTime;
                    userInfo.tmpRoomInfo.playMode = room.playMode;
                    userInfo.tmpRoomInfo.stake = room.stake;
                    userInfo.tmpRoomInfo.disClose = room.disClose;
                    userInfo.tmpRoomInfo.isLock = room.isLock;
                }
                //remove room
                if (RoomManager.Instance.TryRemove(userInfo.roomID, out room))
                {
                    // ok room is deleted...
                    if (userInfo != null) userInfo.roomID = null;
                }
            }
            else
            {
                if (!this.isPlaying)
                {
                    //out of room
                    ExitPlayer(userSession);

                    //SendPlayerList...again..
                    SendPlayerList();
                    // player status 
                    SendPlayerStatus();
                    //send playeriteminfo
                    SendPlayerItemInfo();

                    _playStateManager.SetState(PlayState.Unknown);
                }
                else
                {
                    SetDisconnectPlayerStatus(userSession);
                }
            }
        }

        internal void PlayerWantEnd(string serverSession, string userSession)
        {
            ExitPlayer(userSession);
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo == null) { GlobalManager.Instance.NullUserInfo(serverSession); return; }

            // 1. i'm exit
            userInfo.SetHome();
            PacketsHandlerServer.SendSCNotiGotoScene(serverSession, SceneType.Home);

            if (GetPlayers().Count == 0)
            {
                //remove room
                Room room = this;
                if (RoomManager.Instance.TryRemove(roomID, out room)) { /* ok room is deleted...*/ }
            }
            else
            {
                // 2. send playerlist to others
                SendPlayerList();
                SendPlayerStatus();
                SendPlayerItemInfo();
            }
        }

        internal void PlayerWantExit(string serverSession, string userSession)
        {
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo == null) { GlobalManager.Instance.NullUserInfo(serverSession); return; }
            
            if (this.isPlaying == false)
            {
                if (GetPlayers().Count == 0)
                {
                    //remove room
                    Room room = this;
                    if (RoomManager.Instance.TryRemove(this.roomID, out room)) { /* ok room is deleted...*/ }
                    unNormal = true;
                }
                else
                {
                    //out of room
                    ExitPlayer(userSession);

                    //SendPlayerList...again..
                    SendPlayerList();
                    // player status 
                    SendPlayerStatus();
                    //send playeriteminfo
                    SendPlayerItemInfo();

                    _playStateManager.SetState(PlayState.Unknown);
                }

                userInfo.roomID = null;
                userInfo.SetHome();
                PacketsHandlerServer.SendSCNotiGotoScene(serverSession, SceneType.Home);
                return;
            }

            foreach (var player in _playerManager)
            {
                if (player.session == userSession)
                {
                    player.wantForceExit = true;
                    break;
                }
            }

            ExitPlayer(userSession);
           
            /*
             * I am already out of room in fact
             */
            
            //SendPlayerList...again..
            SendPlayerList();
            // player status 
            SendPlayerStatus();
            //send playeriteminfo
            SendPlayerItemInfo();
            this.unNormal = true;

            Result result = (playCount > 10) ? Result.Lose : Result.Draw;
            int addGold = 0;
            int downLevelExp = 0;
            int upLevelExp = 0;
            int addRankPoint = 0;
            int addRuby = 0;
            int addStake = -this.stake;
            bool levelUp = false;
            bool levelDown = false;
            ItemInfo itemInfo = new ItemInfo(0, 0);
            TimeSpan gameTime = DateTime.Now - startTime;
            string gameTimeStr = string.Format("{0:00}:{1:00}", gameTime.Minutes, gameTime.Seconds);
            //check adOK
            bool adOK = userInfo.IsAdOK();
            GameEndReason reason = GameEndReason.Unknown;
            int reasonValue = 0;
            
            //no reward
            downLevelExp = LevelExpManager.Instance.GetDownLevelExp(userInfo.userDataBase.Level);
            upLevelExp = LevelExpManager.Instance.GetUpLevelExp(userInfo.userDataBase.Level);
            //check profile...
            if (!singlePlayer)
            {
                if (result == Result.Lose)
                {
                    if (userInfo.userDataBase.Level > 1 || (userInfo.userDataBase.Level == 1 && userInfo.userDataBase.Exp > downLevelExp)) userInfo.userDataBase.Exp -= 1;
                    //check level
                    if (userInfo.userDataBase.Level > 1 && userInfo.userDataBase.Exp <= downLevelExp)
                    {
                        userInfo.userDataBase.Exp = 0;
                        int lastLevel = userInfo.userDataBase.Level;
                        userInfo.userDataBase.Level--;
                        levelDown = true;
                        userInfo.SetJokerLine(LogItemType.LevelDown);

                        //confiscate one ink
                        //check joker...
                        if (userInfo.userDataBase.Ink > 0) userInfo.userDataBase.Ink--;
                        userInfo.CheckUserJoker();
                        userInfo.CheckUserPet();
                        userInfo.CheckUserBadge();

                        if (GameServer._isCloud == true)
                        {
                            //item log
                            GlobalManager.Instance.LogItemInsert(serverSession, userInfo.userID, userInfo.userDataBase.nickName, (int)ElementType.Ink, ElementType.Ink.ToString(), -1, LogItemType.LevelDown);

                            //level log
                            GlobalManager.Instance.LogLevelInsert(serverSession, userInfo.userID, userInfo.userDataBase.nickName, lastLevel, userInfo.userDataBase.Level);
                        }
                    }

                    userInfo.userDataBase.userProfile.loseCount++;
                    if (userInfo.userDataBase.userProfile.chanelIdx != 0)
                    {
                       userInfo.userDataBase.userProfile.rankLose++;
                    }
                }//lose
                else //draw
                {
                    userInfo.userDataBase.userProfile.drawCount++;
                    if (userInfo.userDataBase.userProfile.chanelIdx != 0)
                    {
                        userInfo.userDataBase.userProfile.rankDraw++;
                    }
                }
            }
            //check reason
            if (result == Result.Lose)//lose
            {
                reason = GameEndReason.IExit;
            }
            else//draw
            {
                reason = GameEndReason.IExitWithinTenPlay;
            }

            List<PlayFriend> playFriendList = null;
            _rule.SetPlayFriend(userSession, result, out playFriendList);//no return definitely

            SCNotiGameResult pck = new SCNotiGameResult();
            pck.gameResult = new GameResult(PacketTypeList.SCNotiGameResult.ToString(), userInfo.userDataBase.Exp, downLevelExp, upLevelExp, addRankPoint, addGold, addRuby, addStake, levelUp, levelDown, itemInfo, /*isLevelUp*/userInfo.userDataBase.Level, gameTimeStr, adOK, reason, reasonValue, result, playFriendList);

            //CheckFriend();
            if (GameServer._isCloud == true)
            {
                //play log
                GlobalManager.Instance.LogPlayInsert(serverSession, userInfo.userID, userInfo.userDataBase.nickName, gameType, playMode, playTime, this.stake, disClose, isLock, LogPlayType.End, gameTimeStr, result.ToString());
                //log profile
                GlobalManager.Instance.LogProfileInsert(serverSession, userInfo.userID, userInfo.userDataBase.nickName, userInfo.userDataBase.userProfile.winCount, userInfo.userDataBase.userProfile.loseCount, userInfo.userDataBase.userProfile.drawCount, userInfo.userDataBase.userProfile.rankWin, userInfo.userDataBase.userProfile.rankLose, userInfo.userDataBase.userProfile.rankDraw);
            }

            List<string> userSessionList = new List<string>();
            userSessionList.Add(userInfo.session);

            if (userSessionList.Count != 0/* && userInfo.sceneType == SceneType.Room*/)
            {
                // make command
                Command cmd = new Command();
                cmd.pck = pck;
                cmd.receiver = CommandTag.Client;
                cmd.sender = CommandTag.Rule;
                cmd.executeTime = 100;
                cmd.sendUserSession = userSessionList;
                cmd.sendType = CommandSendType.Single;   // send each other CommandSendType.Single
                //send to parent
                SendToParent(cmd);

                //save userinfo
                //level down, matbe, not now
                //userInfo.SaveUserInfo();
            }

            if (GetPlayers().Count == 0)
            {
                //remove room
                Room room = this;
                if (RoomManager.Instance.TryRemove(this.roomID, out room)) { /* ok room is deleted...*/ }
                unNormal = true;
            }
            else
            {
                //fail, No Reward...
                SetEarlyOutPlayerStatus(userSession);
            }
            //not logout yet, so make roomID to null for rejoin another game...
            userInfo.roomID = null;


            userInfo.SaveUserInfo();

            if (!GameServer._enable)
            {
                UserType userType = UserType.User;
                GlobalManager.Instance.GetUserType(userInfo.userDataBase.account, out userType);
                if (userType == UserType.User)
                {
                    PacketsHandlerServer.SendSCNotiGotoScene(serverSession, SceneType.Home);
                    PacketsHandlerInner.SMReqRemoveRedis(serverSession, userInfo.userID, userSession);
                }

            }
        }

        internal bool IsCreator(string userSession)
        {
            bool retvalue = false;
            if (_playerManager.creator != null)
            {
                if (_playerManager.creator.session == userSession) retvalue = true;
            }
            return retvalue;
        }

        internal void AskCreator(string serverSession, string askerSession)
        {
            if (_playerManager.creator == null) return;
            if (_playerManager.creator.session == askerSession) return;

            UserInfo askerInfo = GameServer.GetUserInfo(askerSession);
            if (askerInfo == null)
            {
                GlobalManager.Instance.NullUserInfo(serverSession);
                return;
            }

            //send message to asker
            PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.SendJoinOk);

            UserInfo creatorInfo = GameServer.GetUserInfo(_playerManager.creator.session);
            if (creatorInfo != null)
            {
                //int winCount = askerInfo.userDataBase.userProfile.winCount;
                //int loseCount = askerInfo.userDataBase.userProfile.loseCount;
                //int totalCount = winCount + loseCount;
                //decimal winRate = (totalCount != 0) ? ((decimal)winCount / loseCount) : 0;
                //string winRateStr = winRate.ToString("#0.##%", CultureInfo.InvariantCulture);

                PacketsHandlerServer.SendSCNotiPlayerJoin(creatorInfo.serverSession, askerInfo.userID, askerInfo.userDataBase.FaceIdx, askerInfo.userDataBase.nickName, askerInfo.userDataBase.Level/*, winCount, loseCount, winRateStr*/);

                this.isRequested = true;
                askerInfo.creatorSession = creatorInfo.session;

            }
        }

        private int GetMaxPlayNum(PlayMode playMode_)
        {
            int retvalue = 0;

            switch (playMode_)
            {
                case PlayMode.Unknown:
                    break;
                case PlayMode.Team:
                case PlayMode.Single4:
                    retvalue = 4;
                    break;
                case PlayMode.Single3:
                    retvalue = 3;
                    break;
                case PlayMode.Single2:
                    retvalue = 2;
                    break;
                case PlayMode.All:
                    break;
                default:
                    break;
            }
            return retvalue;
        }

        internal void CancelPausePlayer(string userSession)
        {
            if (/*_playerManager.*/_pausePlayerSessionList != null)
            {
                foreach (var pausePlayerSession in /*_playerManager.*/_pausePlayerSessionList)
                {
                    if (pausePlayerSession == userSession)
                    {
                        /*_playerManager.*/
                        _pausePlayerSessionList.Remove(pausePlayerSession);
                        break;
                    }
                }

            }
        }

        internal void CancelEarlyOutPlayer(string userSession)
        {
            if (/*_playerManager.*/_earlyOutPlayerSessionList != null)
            {
                foreach (var outPlayerSession in /*_playerManager.*/_earlyOutPlayerSessionList)
                {
                    if (outPlayerSession == userSession)
                    {
                        /*_playerManager.*/
                        _earlyOutPlayerSessionList.Remove(outPlayerSession);
                        break;
                    }
                }
            }
        }

        public Room(ref string roomName_, GameType gameType_, PlayMode playMode_, PlayTime playTime_, int stake_, bool disClose_, bool isLock_)
        {
            Init();

            if (string.IsNullOrEmpty(roomName_))
            {
                roomName_ = "ROOM" + (++roomCount).ToString();
            }
            // TODO: Complete member initialization
            this.roomID = roomName_;
            this.gameType = gameType_;
            this.playMode = playMode_;
            this.playTime = playTime_;
            this.stake = stake_;
            this.disClose = disClose_;
            this.isLock = isLock_;

            this.MaxPlayer = GetMaxPlayNum(playMode_);

            ConditionInfo conditionInfo = ConditionManager.Instance.Get(((int)gameType_ - 1));
            if (conditionInfo != null)
            {
                this.goalHP = conditionInfo.goalHP;
                this.goalResource = conditionInfo.goalResource;
            }
        }

        internal void SetPausePlayer(string userSession)
        {
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            Room room = this;
            if (room.GetPlayers().Count == 1 && room._playerManager.creator.session == userSession)//me, of course not playing
            {
                if (userInfo != null)
                {
                    //save tmp roominfo
                    userInfo.tmpRoomInfo = new Room();
                    userInfo.tmpRoomInfo.gameType = room.gameType;
                    userInfo.tmpRoomInfo.playTime = room.playTime;
                    userInfo.tmpRoomInfo.playMode = room.playMode;
                    userInfo.tmpRoomInfo.stake = room.stake;
                    userInfo.tmpRoomInfo.disClose = room.disClose;
                    userInfo.tmpRoomInfo.isLock = room.isLock;
                }

                //remove room
                if (RoomManager.Instance.TryRemove(userInfo.roomID, out room))
                {
                    // ok room is deleted...
                    if (userInfo != null) userInfo.roomID = null;
                }
            }
            else
            {
                SetPausePlayerStatus(userSession);
            }
        }

        public void SetPausePlayerStatus(string userSession)
        {
            this.unNormal = true;

            foreach (var player in _playerManager)
            {
                if (userSession == player.session)
                {
                    if (_pausePlayerSessionList != null)
                    {
                        if (_pausePlayerSessionList.Contains(player.session))
                        {
                            return;
                        }
                    }

                    player.isUser = false;
                    player.noReward = true;
                    //send playerList again
                    SendPlayerList();

                    if (_pausePlayerSessionList == null) _pausePlayerSessionList = new List<string>();
                    _pausePlayerSessionList.Add(player.session);

                    break;
                }
            }
        }

        public void SetDisconnectPlayerStatus(string userSession)
        {
            this.unNormal = true;

            foreach (var player in _playerManager)
            {
                if (userSession == player.session)
                {
                    if (_earlyOutPlayerSessionList != null)
                    {
                        if (_earlyOutPlayerSessionList.Contains(player.session))
                        {
                            return;
                        }
                    }

                    player.isUser = false;
                    player.noReward = true;
                    //send playerList again
                    SendPlayerList();//since change isUser to false

                    if (_pausePlayerSessionList != null)
                    {
                        foreach (var pausePlayerSession in _pausePlayerSessionList)
                        {
                            if (pausePlayerSession == player.session)
                            {
                                /*_playerManager.*/
                                _pausePlayerSessionList.Remove(pausePlayerSession);

                                break;
                            }
                        }
                    }

                    if (_earlyOutPlayerSessionList == null) _earlyOutPlayerSessionList = new List<string>();
                    _earlyOutPlayerSessionList.Add(player.session);

                    break;
                }
            }
        }

        internal void ReConnectSCNotiDeckToHand(string userSession)
        {
            CardList cardlist = null;
            //UserInfo playerInfo = null;
            foreach (var player in _playerManager)
            {
                //playerInfo = GameServer.GetUserInfo(player.session);
                // if (playerInfo != null)
                {
                    if (player.session == userSession)
                    {
                        if (_roomCardManager.GetCardList(player.playerNumber, userSession, ShowAnimationType.ShowCard, out cardlist))
                        {
                            _rule.SendSCNotiDeckToHand(cardlist, userSession, CommandSendType.Single);
                        }
                        break;
                    }
                }
            }
        }

        public void SetEarlyOutPlayerStatus(string userSession)
        {
            this.unNormal = true;
            foreach (var player in _playerManager)
            {
                if (userSession == player.session)
                {
                    if (_earlyOutPlayerSessionList == null) _earlyOutPlayerSessionList = new List<string>();
                    _earlyOutPlayerSessionList.Add(player.session);

                    if (!player.IsLoseCondition)
                    {
                        //lose! No reward...
                        player.forceLose = true;
                        UserInfo userInfo = GameServer.GetUserInfo(userSession);
                        if (userInfo != null) { userInfo.roomID = null; }

                        if (player == _playerManager.currentPlayer)
                        {
                            player.IsRecieveCard = true;
                            player.playAgain = false;
                            _playStateManager.SetState(PlayState.TurnRecieveCard);
                            //choose next player..
                        }
                        else
                        {
                            //if only remained me and another player
                            List<int> remainPlayerNumbers = new List<int>();
                            foreach (var player2 in _playerManager)
                            {
                                if (player2.IsLoseCondition == false) { remainPlayerNumbers.Add(player2.playerNumber); }
                            }
                            if (remainPlayerNumbers.Count == 1)
                            {
                                _playerManager.winnerPlayer = _playerManager[remainPlayerNumbers[0] - 1];

                                _playStateManager.SetState(PlayState.End);
                            }
                            //forceLose
                        }
                    }
                    player.noReward = true;

                    break;
                }
            }
        }

        public override void SendToParent(ICommand cmd_)
        {
            if (cmd_.receiver == CommandTag.Client)
            {
                // send to server ->  client

                switch (cmd_.sendType)
                {
                    case CommandSendType.Single:
                        // GameServer.SendMsg(serversession, cmd_.pck);
                        GameServer.SendMsgWithUserSession(cmd_.sendUserSession, cmd_.pck);
                        break;
                    case CommandSendType.Players:
                        GameServer.SendMsgWithUserSession(cmd_.sendUserSession, cmd_.pck);
                        break;
                    case CommandSendType.PlayersExceptMe:
                        GameServer.SendMsgWithUserSession(cmd_.sendUserSession, cmd_.pck);
                        break;
                    case CommandSendType.Inspectors:
                        // later implement
                        break;
                    case CommandSendType.AllExceptMe:
                        // later implement
                        break;
                    case CommandSendType.All:
                        GameServer.SendBC(cmd_.pck);
                        break;
                    case CommandSendType.Unknown:
                    default:
                        break;
                }
            }
            else
            {
                base.SendToParent(cmd_);
            }
        }

        public bool isRequirePlayer
        {
            get
            {
                if (isPlaying == true || this._playStateManager.State != PlayState.Ready) { return false; }
                return (_playerManager.Count < MaxPlayer) ? true : false;
            }
        }

        public bool Join(string userSession, bool isCreator)
        {
            bool retvalue = false;
            UserInfo userinfo = GameServer.GetUserInfo(userSession);

            if (userinfo != null)
            {
                if (_playerManager.IsUserExist(userSession))
                {
                    // exit
                }
                else
                {
                    Player player = new Player(_roomCardManager);
                    Random rand = new Random();
                    //;iDx, GameType, GoalHP, GoalResource, MoonDust, MoonStone, Mana, ZenStone, RodEnergy, MoonCrystal, StartHP, StartSP
                    // 1,  Mercury,   50,     150,         5,         2,         5,    2,        5,         2,           20,       5
                    player.InitResources(this.gameType, userinfo.userDataBase.FaceIdx, userinfo.userDataBase.nickName, userinfo.session, userinfo.userDataBase.Level);
                    player.playerNumber = _playerManager.Count + 1;
                    userinfo.SetRoom(this.roomID);
                    _playerManager.Add(player);

                    if (isCreator) _playerManager.creator = player;
                }

                retvalue = true;
            }

            return retvalue;
        }

        public bool AIJoin(string AISession)
        {
            bool retvalue = false;
            //UserInfo userinfo = GameServer.GetUserInfo(userSession);

            //if (userinfo != null)
            {
                if (_playerManager.IsUserExist(AISession))
                {
                    // exit
                }
                else
                {
                    Player player = new Player(_roomCardManager);
                    Random rand = new Random();
                    //;iDx, GameType, GoalHP, GoalResource, MoonDust, MoonStone, Mana, ZenStone, RodEnergy, MoonCrystal, StartHP, StartSP
                    // 1,  Mercury,   50,     150,         5,         2,         5,    2,        5,         2,           20,       5
                    int AIFace = GlobalManager.AI_FACE_IDX;
                    string AINick = "Eve";
                    int AILevel = 0;
                    player.InitResources(this.gameType, AIFace, AINick, AISession, AILevel);
                    player.playerNumber = _playerManager.Count + 1;
                    player.isUser = false;
                    player.setAI = true;
                    player.IsRoomEnterReady = true;
                    //userinfo.SetRoom(this.roomID);
                    _playerManager.Add(player);
                }

                retvalue = true;
            }

            return retvalue;
        }

        public void ExitPlayer(string userSession)
        {
            // 1. find user
            // 2. find player in playermanager
            // 3. Remove player
            // 4. Reset playernumber all player

            //1.
            UserInfo userInfo = GameServer.GetUserInfo(userSession);//null;
            if (userInfo != null) { userInfo.roomID = null; }

            // 2.
            Player selectPlayer = null;

            foreach (var player in _playerManager)
            {
                if (player.session == userSession)
                {
                    selectPlayer = player;
                    break;
                }
            }

            // 3.
            if (selectPlayer != null)
            {
                _playerManager.Remove(selectPlayer);
            }

            // 4.
            for (int i = 0; i < _playerManager.Count; i++)
            {
                _playerManager[i].playerNumber = i + 1;
            }

        }

        public PlayerList GetPlayerList(string sender_)
        {
            PlayerList playerList = null;

            // make playerlist...
            playerList = _playerManager.GetPlayerList(sender_);

            if (playerList.playerList != null)
            {
                if (playerList.playerList.Count < this.MaxPlayer)
                {
                    int currentCount = playerList.playerList.Count;
                    for (int i = MaxPlayer; i > currentCount; i--)
                    {
                        playerList.playerList.Add(new PlayerInfo(i, GlobalManager.DEFAULT_FACE_IDX, string.Empty, 0, false, false));
                    }
                }
            }
            return playerList;
        }

        public void GetUserSessionList(out List<string> userSessionList)
        {
            userSessionList = _playerManager.GetUserSessionList(this);
        }

        public void GetUserIdList(string exceptUserId, out List<string> useridlist)
        {
            useridlist = _playerManager.GetUserIDList(exceptUserId);
        }

        public List<Player> GetPlayers()
        {
            //throw new NotImplementedException();
            //List<Player> players = null;

            //if (_playerManager != null)
            //{
            //    players = _playerManager;
            //}

            //return players;
            return (_playerManager != null) ? _playerManager : null;
        }

        public void Run(int timeCount)
        {
            _rule.Run(timeCount);
        }

        // player want restart game within same room
        public bool WantRestart { get; set; }

        internal void UseCard(string session_, int targetNumber_, int handPosition_)
        {
            //check losed player(cannot attact losed player)
            Player target = null;
            if (targetNumber_ != 0)
            {
                if (_playerManager.GetPlayer(targetNumber_, out target))
                {
                    if (target.IsLoseCondition)
                    {
                        return;
                    }
                }
            }
            //1. check userid_ == current player's id , and prevent using card double,
            if (session_ != _playerManager.currentPlayer.session || _playerManager.currentPlayer.IsUseCard == true)
            {
                return;
            }

            UseCardToTarget(session_, targetNumber_, handPosition_);
        }
        // usercard

        void UseCardToTarget(string session_, int targetNumber_, int handPosition_)
        {
            //2. get card from handposition
            PlayCardInfo playCardInfo = null;
            Card card = null;
            int currentPlayerNumber = _playerManager.currentPlayer.playerNumber;
            int myEffectIdx = 0;
            int targetEffectIdx = 0;
            int globalEffectIdx = 0;
            CardUseType cardUseType = CardUseType.Unknown;

            if (_roomCardManager.GetHandCard(currentPlayerNumber, handPosition_, out card))
            {
                int attachItemIdx = 0;//test first...
                int cardSkinIdx = 900;
                int jokerFaceIdx = 0;
                List<int> spellIdxList = new List<int>();

                if (card.Index == 0)
                {
                    //resend hand cards
                    CardList cardlist = null;
                    if (_roomCardManager.GetCardList(currentPlayerNumber, _playerManager.currentPlayer.session, ShowAnimationType.ShowCard, out cardlist))
                    {
                        _rule.SendSCNotiDeckToHand(cardlist, _playerManager.currentPlayer.session, CommandSendType.Single);
                    }

                    return;
                }
                if (!_playerManager.currentPlayer.IsCardUseable(card, targetNumber_))
                {
                    playCardInfo = new PlayCardInfo(PacketTypeList.SCNotiUseCard.ToString(), false, currentPlayerNumber, targetNumber_, card.Index, 0, jokerFaceIdx, spellIdxList);
                    _rule.SendSCNotiUseCard(playCardInfo);

                    return;
                }

                UserInfo userInfo = GameServer.GetUserInfo(session_);
                if (userInfo != null)
                {
                    if (userInfo.userDataBase.userItem.cardSkinList != null)
                    {
                        foreach (var cardSkin in userInfo.userDataBase.userItem.cardSkinList)
                        {
                            if (cardSkin.isSelect == true)
                            {
                                cardSkinIdx = cardSkin.skinIdx;
                                break;
                            }
                        }
                    }

                    if (card.Index == CardManager.JOKER_INDEX)
                    {
                        jokerFaceIdx = userInfo.userDataBase.userJoker.faceIdx;
                        if (userInfo.userDataBase.userJoker.spellIdxList != null)
                        {
                            foreach (var spellIdx in userInfo.userDataBase.userJoker.spellIdxList)
                            {
                                spellIdxList.Add(spellIdx);
                            }
                        }
                    }
                }

                cardUseType = card.Info.cardUseType;

                myEffectIdx = card.Info.myEffectIdx;
                targetEffectIdx = card.Info.targetEffectIdx;
                globalEffectIdx = card.Info.globalEffectIdx;

                if (card.Index == CardManager.JOKER_INDEX)
                {
                    GetJokerCardUseType(_playerManager.currentPlayer, out cardUseType);
                }

                if ((cardUseType == CardUseType.Enemy && targetNumber_ == currentPlayerNumber) ||
                    (cardUseType == CardUseType.EnemyAndAll && targetNumber_ == currentPlayerNumber) ||
                    (cardUseType == CardUseType.Me && (targetNumber_ != currentPlayerNumber && targetNumber_ != 0)) ||
                    (cardUseType == CardUseType.MeBasedOnEnemy && targetNumber_ == currentPlayerNumber))
                {
                    playCardInfo = new PlayCardInfo(PacketTypeList.SCNotiUseCard.ToString(), false, currentPlayerNumber, targetNumber_, card.Index, 0, jokerFaceIdx, spellIdxList);
                    _rule.SendSCNotiUseCard(playCardInfo);

                    return;
                }

                if (onlyDiscard == true)
                {
                    if (targetNumber_ == 0)
                    {
                        onlyDiscard = false;
                        _playerManager.currentPlayer.playAgain = true;
                    }
                    else
                    {
                        int effectIdx = 0;//test first...
                        playCardInfo = new PlayCardInfo(PacketTypeList.SCNotiUseCard.ToString(), false, currentPlayerNumber, targetNumber_, card.Index, effectIdx, jokerFaceIdx, spellIdxList);
                        _rule.SendSCNotiUseCard(playCardInfo);
                        return;
                    }
                }

                Player target = null;
                List<Player> targetList = null;
                if (targetNumber_ != 0)
                {
                    //1. card affect to target  ( target number = 0 is deck )
                    // targetNumber == 0 is deck...
                    if (_playerManager.GetPlayer(targetNumber_, out target))
                    {
                        if (_roomCardManager.EmptyHandCard(currentPlayerNumber, handPosition_))
                        {
                            if (card.Index == CardManager.JOKER_INDEX)
                            {
                                JokerAppliaction(target);

                                //decrease resource price...
                                ChargeJokerPrice();
                            }
                            else
                            {
                                //pet card
                                if (card.Index / 1000 == 1)
                                {
                                    SetPlayerItemInfoPet(card.Index);
                                }

                                switch (card.Info.special)
                                {
                                    case 1: Special1(card); break;
                                    case 2: Special2(card, target); break;
                                    case 3: Special3(card, out onlyDiscard); break;
                                    case 4: Special4(card); break;
                                    case 5: Special5(card, out targetList); break;
                                    case 6: Special6(card, target, out cardUseType); break;
                                    case 7: Special7(card); break;
                                    case 8: Special8(card); break;
                                    case 9: Special9(card); break;
                                    case 10: Special10(card); break;
                                    case 11: Special11(card, target); break;
                                    case 12: Special12(card, target); break;
                                    case 13: Special13(card, out targetList); break;
                                    case 14: Special14(card); break;
                                    case 15: Special15(card, target); break;
                                    case 16: Special16(card, target); break;
                                    case 17: Special17(card); break;
                                    case 18: Special18(card); break;
                                    case 19: Special19(card, target); break;
                                    case 20: Special20(card, out onlyDiscard); break;
                                    case 21: Special21(card, target); break;
                                    case 22: Special22(card, target); break;
                                    case 23: Special23(card, target); break;
                                    case 24: Special24(card, target); break;
                                    case 25: Special25(card, target); break;
                                    case 26: Special26(card, target); break;
                                    case 27: Special27(card); break;
                                    default:
                                        // if target is PLAYER, attack to target...
                                        target.TargetApplyCard(card.GetCardAbility(), _playerManager.currentPlayer);
                                        break;
                                }

                                //resource currentplayer's price decrease, play again and isUseCard check
                                _playerManager.currentPlayer.moonDustRed += (card.GetCardAbility().moonDustRed - card.GetCardAbility().priceMoonDustRed);
                                if (_playerManager.currentPlayer.moonDustRed < 0) _playerManager.currentPlayer.moonDustRed = 0;
                                _playerManager.currentPlayer.manaBlue += (card.GetCardAbility().manaBlue - card.GetCardAbility().priceManaBlue);
                                if (_playerManager.currentPlayer.manaBlue < 0) _playerManager.currentPlayer.manaBlue = 0;
                                _playerManager.currentPlayer.rodEnergyGreen += (card.GetCardAbility().rodEnergyGreen - card.GetCardAbility().priceRodEnergyGreen);
                                if (_playerManager.currentPlayer.rodEnergyGreen < 0) _playerManager.currentPlayer.rodEnergyGreen = 0;
                                _playerManager.currentPlayer.playAgain = (card.GetCardAbility().playAgain == 0) ? false : true;
                            }

                            _playerManager.currentPlayer.IsUseCard = true;

                            //2. deck's hand view affect card's index update...
                            //_roomCardManager.SetHandCard( 0 /* 0 is Deck*/, targetNumber_, card.Index, true);
                            if (_roomCardManager._itemCardDic.ContainsKey(card.Index))
                            {
                                attachItemIdx = _roomCardManager._itemCardDic[card.Index];
                                _roomCardManager._itemCardDic.Remove(card.Index);
                                if (userInfo != null)
                                {
                                    userInfo.AddUserItem(attachItemIdx, 1);
                                    //save items..gold and ruby
                                    //userInfo.SaveUserInfo();
                                    //add game get item count: ruby and gold
                                    //...
                                }
                            }
                            _deckInfolist.Add(new DeckInfo(card.Index, attachItemIdx, /*currentPlayerNumber, targetNumber_,*/ targetNumber_));

                            DeckList deckList = new DeckList(PacketTypeList.SCNotiDeckInfo.ToString(), _deckInfolist, cardSkinIdx, jokerFaceIdx, spellIdxList);
                            if (deckList != null)
                            {
                                _rule.SendSCNotiDeckInfo(deckList);
                            }
                        }

                        //3. SCNotiHandList currentuser's send currentuser
                        CardList cardlist = null;
                        if (_roomCardManager.GetCardList(currentPlayerNumber, _playerManager.currentPlayer.session, ShowAnimationType.ShowCard, out cardlist))
                        {
                            _rule.SendSCNotiDeckToHand(cardlist, _playerManager.currentPlayer.session, CommandSendType.Single);
                        }
                        else
                        {
                            // error cardlist is null
                        }

                        //4. SCNotiHandList deck's send all 
                        // 0 = deck , 
                        //not work in practice, because id is empty, userInfo is null
                        if (_roomCardManager.GetCardList(0, "", ShowAnimationType.ShowCard, out cardlist))
                        {
                            _rule.SendSCNotiDeckToHand(cardlist, "", CommandSendType.Players);
                        }
                        else
                        {
                            // Oops! card list is null
                        }

                        //4. send use card to room all
                        //SCNotiUseCard 
                        //int jokerFaceIdx2 = 0;
                        //List<int> spellIdxList2 = null;
                        //if (card.Index == CardManager.JOKER_INDEX)
                        //{
                        //    _playerManager.currentPlayer.SetUserJokerInfo(out jokerFaceIdx2, out spellIdxList2);//for SCNotiUseCard
                        //}

                        int effectIdx = 0;//test first...
                        playCardInfo = new PlayCardInfo(PacketTypeList.SCNotiUseCard.ToString(), true, currentPlayerNumber, targetNumber_, card.Index, effectIdx, jokerFaceIdx, spellIdxList);
                        _rule.SendSCNotiUseCard(playCardInfo);

                        //5. send noti player state ( HP, SP, Mana...) to room all 
                        //Send owner's SCNotiPlayerStatus to all
                        //unconditionally have to send current player's status because of resource consume...^^!!!
                        //CardUseType.Me and CardUseType.MeBasedOnEnemy are included in
                        _rule.SendSCNotiPlayerStatus(_playerManager.currentPlayer, myEffectIdx, globalEffectIdx);

                        switch (cardUseType)
                        {
                            case CardUseType.Enemy://enemy + me and enemy
                                _rule.SendSCNotiPlayerStatus(target, targetEffectIdx, 0);
                                break;
                            case CardUseType.Others:
                                if (targetList != null && targetList.Count > 0)
                                {
                                    foreach (Player player in targetList)
                                    {
                                        if (player.HP > 0)
                                        //if (!player.IsLoseCondition)
                                        {
                                            _rule.SendSCNotiPlayerStatus(player, targetEffectIdx, 0);
                                        }
                                    }
                                }
                                break;
                            case CardUseType.EnemyAndAll:
                            case CardUseType.All://me and all + all
                                foreach (Player player in _playerManager)
                                {
                                    if (/*!player.IsLoseCondition*/ player.HP > 0 && player != _playerManager.currentPlayer)
                                    {
                                        _rule.SendSCNotiPlayerStatus(player, targetEffectIdx, 0);
                                    }
                                }
                                break;
                            default: break;
                        }
                    }
                }
                //  discard card
                else if (targetNumber_ == 0)
                {
                    bool discardOk = false;
                    if (_roomCardManager.EmptyHandCard(currentPlayerNumber, handPosition_))
                    {
                        CardList cardlist = null;
                        if (_roomCardManager.GetCardList(currentPlayerNumber, _playerManager.currentPlayer.session, 0, out cardlist))
                        {
                            _rule.SendSCNotiDeckToHand(cardlist, _playerManager.currentPlayer.session, CommandSendType.Single);
                            discardOk = true;
                        }

                        _playerManager.currentPlayer.IsUseCard = true;

                        if (_roomCardManager._itemCardDic.ContainsKey(card.Index))
                        {
                            attachItemIdx = _roomCardManager._itemCardDic[card.Index];
                            _roomCardManager._itemCardDic.Remove(card.Index);
                            //when apply to target, eat item else not eat ?
                        }
                        _deckInfolist.Add(new DeckInfo(card.Index, attachItemIdx, /*currentPlayerNumber, currentPlayerNumber,*/ targetNumber_/*currentPlayerNumber*/));

                        DeckList deckList = new DeckList(PacketTypeList.SCNotiDeckInfo.ToString(), _deckInfolist, cardSkinIdx, jokerFaceIdx, spellIdxList);
                        if (deckList != null)
                        {
                            _rule.SendSCNotiDeckInfo(deckList);
                            discardOk = true;
                        }

                        //pet card
                        if (card.Index / 1000 == 1)
                        {
                            SetPlayerItemInfoPet(card.Index);
                        }
                    }

                    if (!discardOk)
                    {
                        playCardInfo = new PlayCardInfo(PacketTypeList.SCNotiUseCard.ToString(), false, currentPlayerNumber, targetNumber_, card.Index, 0, jokerFaceIdx, spellIdxList);
                        _rule.SendSCNotiUseCard(playCardInfo);
                    }
                }

            }// if ... getHandCard

        }

        private void SetPlayerItemInfoPet(int cardIdx)
        {
            UserInfo userInfo = GameServer.GetUserInfo(_playerManager.currentPlayer.session);
            if (userInfo != null)
            {
                if (userInfo._playerItemInfo != null)
                {
                    int petIdx = 0;
                    int removePosition = 0;
                    if (ItemManager.Instance.GetPetIdxWithCardIdx(cardIdx - 1000, out petIdx))
                    {
                        if (userInfo._playerItemInfo.petList != null)
                        {
                            for (int i = 0; i < userInfo._playerItemInfo.petList.Count; i++)
                            {
                                if (userInfo._playerItemInfo.petList[i] == petIdx)
                                {
                                    removePosition = i;
                                    break;
                                }
                            }

                            userInfo._playerItemInfo.petList.RemoveAt(removePosition);
                        }

                        //send playerItemInfo again
                        _rule.SendSCNotiPlayerItemInfo(/*_playerManager.currentPlayer.session,*/ userInfo._playerItemInfo);
                    }
                }
            }
        }

        private void ChargeJokerPrice()
        {
            UserInfo userInfo = GameServer.GetUserInfo(_playerManager.currentPlayer.session);
            if (userInfo != null)
            {
                if (userInfo.userDataBase.userJoker.spellIdxList != null)
                {
                    int priceMoonDustRed = 0;
                    int priceManaBlue = 0;
                    int priceRodEnergyGreen = 0;
                    foreach (var spellIdx in userInfo.userDataBase.userJoker.spellIdxList)
                    {
                        SpellInfo spellInfo = SpellManager.Instance.GetSpellInfo(spellIdx);
                        if (spellInfo != null)
                        {
                            priceMoonDustRed += spellInfo.ability.priceMD;
                            priceManaBlue += spellInfo.ability.priceM;
                            priceRodEnergyGreen += spellInfo.ability.priceRE;
                        }
                    }

                    _playerManager.currentPlayer.moonDustRed -= priceMoonDustRed;
                    if (_playerManager.currentPlayer.moonDustRed < 0) _playerManager.currentPlayer.moonDustRed = 0;
                    _playerManager.currentPlayer.manaBlue -= priceManaBlue;
                    if (_playerManager.currentPlayer.manaBlue < 0) _playerManager.currentPlayer.manaBlue = 0;
                    _playerManager.currentPlayer.rodEnergyGreen -= priceRodEnergyGreen;
                    if (_playerManager.currentPlayer.rodEnergyGreen < 0) _playerManager.currentPlayer.rodEnergyGreen = 0;

                }
            }
        }

        private void GetJokerCardUseType(Player currentPlayer, out CardUseType cardUseType)
        {
            cardUseType = CardUseType.Unknown;
            UserInfo userInfo = GameServer.GetUserInfo(currentPlayer.session);
            if (userInfo != null)
            {
                if (userInfo.userDataBase.userJoker.spellIdxList != null)
                {
                    if (userInfo.userDataBase.userJoker.spellIdxList.Count > 0)
                    {
                        cardUseType = CardUseType.Me;
                        foreach (var spellIdx in userInfo.userDataBase.userJoker.spellIdxList)
                        {
                            SpellInfo spellInfo = SpellManager.Instance.GetSpellInfo(spellIdx);
                            if (spellInfo != null)
                            {
                                //if one spell's cardUseType is enemy, then all joker card's cardUseType is enemy
                                if (spellInfo.target == CardUseType.Enemy)
                                {
                                    cardUseType = CardUseType.Enemy;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void JokerAppliaction(Player target)
        {
            UserInfo userInfo = GameServer.GetUserInfo(_playerManager.currentPlayer.session);
            if (userInfo != null)
            {
                if (userInfo.userDataBase.userJoker.spellIdxList != null)
                {
                    foreach (var spellIdx in userInfo.userDataBase.userJoker.spellIdxList)
                    {
                        SpellInfo spellInfo = null;
                        spellInfo = SpellManager.Instance.GetSpellInfo(spellIdx);
                        switch (spellInfo.special)
                        {
                            case 1:
                                //switch shield point
                                int tmpSP = _playerManager.currentPlayer.SP;
                                _playerManager.currentPlayer.SP = target.SP;
                                target.SP = tmpSP;
                                break;
                            case 2:
                                //play again
                                _playerManager.currentPlayer.playAgain = true;
                                break;
                            default:
                                TargetApplySpell(target, spellInfo.ability);
                                break;
                        }
                    }
                }
            }
        }

        private void TargetApplySpell(Player target, SpellAbility spellAbility)
        {
            //OWNER
            Player owner = _playerManager.currentPlayer;

            owner.manaBlue += spellAbility.manaBlue;
            if (owner.manaBlue < 0) owner.manaBlue = 0;
            owner.zenStoneBluePlus += spellAbility.zenstoneBlue;
            if (owner.zenStoneBluePlus < 0) owner.zenStoneBluePlus = 0;
            owner.moonDustRed += spellAbility.moonDustRed;
            if (owner.moonDustRed < 0) owner.moonDustRed = 0;
            owner.moonStoneRedPlus += spellAbility.moonstoneRed;
            if (owner.moonStoneRedPlus < 0) owner.moonStoneRedPlus = 0;
            owner.rodEnergyGreen += spellAbility.rodEnergyGreen;
            if (owner.rodEnergyGreen < 0) owner.rodEnergyGreen = 0;
            owner.moonCrystalGreenPlus += spellAbility.moonCrystalGreen;
            if (owner.moonCrystalGreenPlus < 0) owner.moonCrystalGreenPlus = 0;

            //TARGET
            target.HP += spellAbility.hpEnemy;
            if (target.HP < 0) target.HP = 0;
            target.manaBlue += spellAbility.manaEnemyBlue;
            if (target.manaBlue < 0) target.manaBlue = 0;
            target.zenStoneBluePlus += spellAbility.zenstoneEnemyBlue;
            if (target.zenStoneBluePlus < 0) target.zenStoneBluePlus = 0;
            target.moonDustRed += spellAbility.moonDustEnemyRed;
            if (target.moonDustRed < 0) target.moonDustRed = 0;
            target.moonStoneRedPlus += spellAbility.moonstoneEnemyRed;
            if (target.moonStoneRedPlus < 0) target.moonStoneRedPlus = 0;
            target.rodEnergyGreen += spellAbility.rodEnergyEnemyGreen;
            if (target.rodEnergyGreen < 0) target.rodEnergyGreen = 0;
            target.moonCrystalGreenPlus += spellAbility.moonCrystalEnemyGreen;
            if (target.moonCrystalGreenPlus < 0) target.moonCrystalGreenPlus = 0;

            if (spellAbility.damage > 0)
            {
                int remaindamage = spellAbility.damage;

                if (target.SP >= remaindamage)
                {
                    target.SP -= remaindamage;
                }
                else
                {
                    remaindamage -= target.SP;
                    target.SP = 0;
                    target.HP -= remaindamage;
                    if (target.HP < 0) target.HP = 0;
                }
            }
        }

        private void Special27(Card card)
        {
            CardAbility cardAbility = card.GetCardAbility();
            if (card.isTryCard)
            {
                int remaindamage = 0;
                foreach (var player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        remaindamage = cardAbility.damage;

                        if (player.trySP >= remaindamage)
                        {
                            player.trySP -= remaindamage;
                        }
                        else
                        {
                            remaindamage -= player.trySP;
                            player.trySP = 0;
                            player.tryHP -= remaindamage;
                            if (player.tryHP < 0) player.tryHP = 0;
                        }
                    }
                }
            }
            else
            {
                int remaindamage = 0;
                foreach (var player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        remaindamage = cardAbility.damage;

                        if (player.SP >= remaindamage)
                        {
                            player.SP -= remaindamage;
                        }
                        else
                        {
                            remaindamage -= player.SP;
                            player.SP = 0;
                            player.HP -= remaindamage;
                            if (player.HP < 0) player.HP = 0;
                        }
                    }
                }
            }

        }

        private void Special26(Card card, Player target)
        {
            Player owner = _playerManager.currentPlayer;
            CardAbility cardAbility = card.GetCardAbility();
            if (card.isTryCard)
            {
                int ownerManaPlus = 0;
                int ownerMoonDustPlus = 0;

                if (target.tryManaBlue < Math.Abs(cardAbility.manaBlueEnemy))
                {
                    ownerManaPlus = target.tryManaBlue;
                    target.tryManaBlue = 0;
                }
                else
                {
                    target.tryManaBlue += cardAbility.manaBlueEnemy;
                    ownerManaPlus = Math.Abs(cardAbility.manaBlueEnemy);
                }

                if (target.tryMoonDustRed < Math.Abs(cardAbility.moonDustRedEnemy))
                {
                    ownerMoonDustPlus = target.tryMoonDustRed;
                    target.tryMoonDustRed = 0;
                }
                else
                {
                    target.tryMoonDustRed += cardAbility.moonDustRedEnemy;
                    ownerMoonDustPlus = Math.Abs(cardAbility.moonDustRedEnemy);
                }

                owner.tryManaBlue += Convert.ToInt32((ownerManaPlus + 1) / 2);
                owner.tryMoonDustRed += Convert.ToInt32((ownerMoonDustPlus + 1) / 2);
            }
            else
            {
                int ownerManaPlus = 0;
                int ownerMoonDustPlus = 0;

                if (target.manaBlue < Math.Abs(cardAbility.manaBlueEnemy))
                {
                    ownerManaPlus = target.manaBlue;
                    target.manaBlue = 0;
                }
                else
                {
                    target.manaBlue += cardAbility.manaBlueEnemy;
                    ownerManaPlus = Math.Abs(cardAbility.manaBlueEnemy);
                }

                if (target.moonDustRed < Math.Abs(cardAbility.moonDustRedEnemy))
                {
                    ownerMoonDustPlus = target.moonDustRed;
                    target.moonDustRed = 0;
                }
                else
                {
                    target.moonDustRed += cardAbility.moonDustRedEnemy;
                    ownerMoonDustPlus = Math.Abs(cardAbility.moonDustRedEnemy);
                }

                owner.manaBlue += Convert.ToInt32((ownerManaPlus + 1) / 2);
                owner.moonDustRed += Convert.ToInt32((ownerMoonDustPlus + 1) / 2);
            }

        }

        private void Special25(Card card, Player target)
        {
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                int remaindamage = 0;
                if (target.trySP > 10) remaindamage = cardAbility.damage;
                else remaindamage = 7;

                if (target.trySP >= remaindamage)
                {
                    target.trySP -= remaindamage;
                }
                else
                {
                    remaindamage -= target.trySP;
                    target.trySP = 0;
                    target.tryHP -= remaindamage;
                    if (target.tryHP < 0) target.tryHP = 0;
                }
            }
            else
            {
                int remaindamage = 0;
                if (target.SP > 10) remaindamage = cardAbility.damage;
                else remaindamage = 7;

                if (target.SP >= remaindamage)
                {
                    target.SP -= remaindamage;
                }
                else
                {
                    remaindamage -= target.SP;
                    target.SP = 0;
                    target.HP -= remaindamage;
                    if (target.HP < 0) target.HP = 0;
                }
            }

        }

        private void Special24(Card card, Player target)
        {
            Player owner = _playerManager.currentPlayer;
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                if (owner.trySP > target.trySP)
                {
                    target.tryHP += cardAbility.HPEnemy;
                    if (target.tryHP < 0) target.tryHP = 0;
                }
                else
                {
                    int remaindamage = cardAbility.damage;
                    if (target.trySP >= remaindamage)
                    {
                        target.trySP -= remaindamage;
                    }
                    else
                    {
                        remaindamage -= target.trySP;
                        target.trySP = 0;
                        target.tryHP -= remaindamage;
                        if (target.tryHP < 0) target.tryHP = 0;
                    }
                }
            }
            else
            {
                if (owner.SP > target.SP)
                {
                    target.HP += cardAbility.HPEnemy;
                    if (target.HP < 0) target.HP = 0;
                }
                else
                {
                    int remaindamage = cardAbility.damage;
                    if (target.SP >= remaindamage)
                    {
                        target.SP -= remaindamage;
                    }
                    else
                    {
                        remaindamage -= target.SP;
                        target.SP = 0;
                        target.HP -= remaindamage;
                        if (target.HP < 0) target.HP = 0;
                    }
                }
            }

        }

        private void Special23(Card card, Player target)
        {
            Player owner = _playerManager.currentPlayer;
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                int remaindamage = 0;
                if (owner.tryZenStoneBluePlus > target.tryZenStoneBluePlus) remaindamage = cardAbility.damage;
                else remaindamage = 8;

                if (target.trySP >= remaindamage)
                {
                    target.trySP -= remaindamage;
                }
                else
                {
                    remaindamage -= target.trySP;
                    target.trySP = 0;
                    target.tryHP -= remaindamage;
                    if (target.tryHP < 0) target.tryHP = 0;
                }
            }
            else
            {
                int remaindamage = 0;
                if (owner.zenStoneBluePlus > target.zenStoneBluePlus) remaindamage = cardAbility.damage;
                else remaindamage = 8;

                if (target.SP >= remaindamage)
                {
                    target.SP -= remaindamage;
                }
                else
                {
                    remaindamage -= target.SP;
                    target.SP = 0;
                    target.HP -= remaindamage;
                    if (target.HP < 0) target.HP = 0;
                }
            }

        }

        private void Special22(Card card, Player target)
        {
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                int remaindamage = 0;
                if (target.trySP == 0) remaindamage = cardAbility.damage;
                else remaindamage = 6;

                if (target.trySP >= remaindamage)
                {
                    target.trySP -= remaindamage;
                }
                else
                {
                    remaindamage -= target.trySP;
                    target.trySP = 0;
                    target.tryHP -= remaindamage;
                    if (target.tryHP < 0) target.tryHP = 0;
                }
            }
            else
            {
                int remaindamage = 0;
                if (target.SP == 0) remaindamage = cardAbility.damage;
                else remaindamage = 6;

                if (target.SP >= remaindamage)
                {
                    target.SP -= remaindamage;
                }
                else
                {
                    remaindamage -= target.SP;
                    target.SP = 0;
                    target.HP -= remaindamage;
                    if (target.HP < 0) target.HP = 0;
                }
            }

        }

        private void Special21(Card card, Player target)
        {
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                int remaindamage = cardAbility.damage;
                if (target.trySP >= remaindamage)
                {
                    target.trySP -= remaindamage;
                }
                else
                {
                    remaindamage -= target.trySP;
                    target.trySP = 0;
                    target.tryHP -= remaindamage;
                    if (target.tryHP < 0) target.tryHP = 0;
                }

                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.tryMoonDustRed += cardAbility.moonDustRedEnemy;
                        if (player.tryMoonDustRed < 0) player.tryMoonDustRed = 0;
                        player.tryManaBlue += cardAbility.manaBlueEnemy;
                        if (player.tryManaBlue < 0) player.tryManaBlue = 0;
                        player.tryRodEnergyGreen += cardAbility.rodEnergyGreenEnemy;
                        if (player.tryRodEnergyGreen < 0) player.tryRodEnergyGreen = 0;
                    }
                }
            }
            else
            {
                int remaindamage = cardAbility.damage;
                if (target.SP >= remaindamage)
                {
                    target.SP -= remaindamage;
                }
                else
                {
                    remaindamage -= target.SP;
                    target.SP = 0;
                    target.HP -= remaindamage;
                    if (target.HP < 0) target.HP = 0;
                }

                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.moonDustRed += cardAbility.moonDustRedEnemy;
                        if (player.moonDustRed < 0) player.moonDustRed = 0;
                        player.manaBlue += cardAbility.manaBlueEnemy;
                        if (player.manaBlue < 0) player.manaBlue = 0;
                        player.rodEnergyGreen += cardAbility.rodEnergyGreenEnemy;
                        if (player.rodEnergyGreen < 0) player.rodEnergyGreen = 0;
                    }
                }
            }

        }

        private void Special20(Card card, out bool onlyDiscard)
        {
            //...Draw a card, discard a card, play again
            onlyDiscard = true;
        }

        private void Special19(Card card, Player target)
        {
            Player owner = _playerManager.currentPlayer;
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                int remaindamage = 0;
                if (owner.trySP > target.trySP) remaindamage = cardAbility.damage;
                else remaindamage = 2;

                if (target.trySP >= remaindamage)
                {
                    target.trySP -= remaindamage;
                }
                else
                {
                    remaindamage -= target.trySP;
                    target.trySP = 0;
                    target.tryHP -= remaindamage;
                    if (target.tryHP < 0) target.tryHP = 0;
                }
            }
            else
            {
                int remaindamage = 0;
                if (owner.SP > target.SP) remaindamage = cardAbility.damage;
                else remaindamage = 2;

                if (target.SP >= remaindamage)
                {
                    target.SP -= remaindamage;
                }
                else
                {
                    remaindamage -= target.SP;
                    target.SP = 0;
                    target.HP -= remaindamage;
                    if (target.HP < 0) target.HP = 0;
                }
            }

        }

        private void Special18(Card card)
        {
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.tryRodEnergyGreen += cardAbility.rodEnergyGreenEnemy;
                        if (player.tryRodEnergyGreen < 0) player.tryRodEnergyGreen = 0;
                    }
                }
            }
            else
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.rodEnergyGreen += cardAbility.rodEnergyGreenEnemy;
                        if (player.rodEnergyGreen < 0) player.rodEnergyGreen = 0;
                    }
                }
            }

        }

        private void Special17(Card card)
        {
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.tryMoonCrystalGreenPlus += cardAbility.moonCrystalGreenplusEnemy;
                        if (player.tryMoonCrystalGreenPlus < 0) player.tryMoonCrystalGreenPlus = 0;
                    }
                }
            }
            else
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.moonCrystalGreenPlus += cardAbility.moonCrystalGreenplusEnemy;
                        if (player.moonCrystalGreenPlus < 0) player.moonCrystalGreenPlus = 0;
                    }
                }
            }
        }

        private void Special16(Card card, Player target)
        {
            Player owner = _playerManager.currentPlayer;

            if (card.isTryCard)
            {
                int temp = owner.trySP;
                owner.trySP = target.trySP;
                target.trySP = temp;
            }
            else
            {
                int temp = owner.SP;
                owner.SP = target.SP;
                target.SP = temp;
            }
        }

        private void Special15(Card card, Player target)
        {
            Player owner = _playerManager.currentPlayer;
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                owner.trySP += cardAbility.SP;
                if (owner.tryMoonCrystalGreenPlus < target.tryMoonCrystalGreenPlus)
                {
                    owner.tryMoonCrystalGreenPlus += cardAbility.moonCrystalGreenplus;
                }
            }
            else
            {
                owner.SP += cardAbility.SP;
                if (owner.moonCrystalGreenPlus < target.moonCrystalGreenPlus)
                {
                    owner.moonCrystalGreenPlus += cardAbility.moonCrystalGreenplus;
                }
            }
        }

        private void Special14(Card card)
        {
            CardAbility cardAbility = card.GetCardAbility();
            if (card.isTryCard)
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.trySP += cardAbility.SPEnemy;
                        if (player.trySP < 0) player.trySP = 0;
                    }
                }
            }
            else
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.SP += cardAbility.SPEnemy;
                        if (player.SP < 0) player.SP = 0;
                    }
                }
            }

        }

        private void Special13(Card card, out List<Player> targetList)
        {
            Player owner = _playerManager.currentPlayer;
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                int lowestSP = owner.trySP;
                targetList = new List<Player>();
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        if (lowestSP > player.trySP) lowestSP = player.trySP;
                    }
                }
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        if (lowestSP == player.trySP) targetList.Add(player);
                    }
                }

                if (targetList.Count > 0)
                {
                    foreach (Player target in targetList)
                    {
                        target.tryMoonCrystalGreenPlus += cardAbility.moonCrystalGreenplusEnemy;
                        if (target.tryMoonCrystalGreenPlus < 0) target.tryMoonCrystalGreenPlus = 0;
                        target.tryHP += cardAbility.HPEnemy;
                        if (target.tryHP < 0) target.tryHP = 0;
                    }
                }
            }
            else
            {
                int lowestSP = owner.SP;
                targetList = new List<Player>();
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        if (lowestSP > player.SP) lowestSP = player.SP;
                    }
                }
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        if (lowestSP == player.SP) targetList.Add(player);
                    }
                }

                if (targetList.Count > 0)
                {
                    foreach (Player target in targetList)
                    {
                        target.moonCrystalGreenPlus += cardAbility.moonCrystalGreenplusEnemy;
                        if (target.moonCrystalGreenPlus < 0) target.moonCrystalGreenPlus = 0;
                        target.HP += cardAbility.HPEnemy;
                        if (target.HP < 0) target.HP = 0;
                    }
                }
            }

        }

        private void Special12(Card card, Player target)
        {
            Player owner = _playerManager.currentPlayer;

            if (card.isTryCard)
            {
                if (owner.trymoonStoneRedPlus < target.trymoonStoneRedPlus) owner.trymoonStoneRedPlus = target.trymoonStoneRedPlus;
            }
            else
            {
                if (owner.moonStoneRedPlus < target.moonStoneRedPlus) owner.moonStoneRedPlus = target.moonStoneRedPlus;
            }
        }

        private void Special11(Card card, Player target)
        {
            Player owner = _playerManager.currentPlayer;
            CardAbility cardAbility = card.GetCardAbility();
            if (card.isTryCard)
            {
                if (owner.trymoonStoneRedPlus < target.trymoonStoneRedPlus) owner.trymoonStoneRedPlus += cardAbility.moonStoneRedplus;
                else owner.trymoonStoneRedPlus += 1;
            }
            else
            {
                if (owner.moonStoneRedPlus < target.moonStoneRedPlus) owner.moonStoneRedPlus += cardAbility.moonStoneRedplus;
                else owner.moonStoneRedPlus += 1;
            }

        }

        private void Special10(Card card)
        {
            Player owner = _playerManager.currentPlayer;
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                if (owner.trySP == 0) owner.trySP += cardAbility.SP;
                else owner.trySP += 3;
            }
            else
            {
                if (owner.SP == 0) owner.SP += cardAbility.SP;
                else owner.SP += 3;
            }

        }

        private void Special9(Card card)
        {
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.trymoonStoneRedPlus += cardAbility.moonStoneRedplusEnemy;
                        if (player.trymoonStoneRedPlus < 0) player.trymoonStoneRedPlus = 0;
                    }
                }
            }
            else
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.moonStoneRedPlus += cardAbility.moonStoneRedplusEnemy;
                        if (player.moonStoneRedPlus < 0) player.moonStoneRedPlus = 0;
                    }
                }
            }
        }

        private void Special8(Card card)
        {
            CardAbility cardAbility = card.GetCardAbility();
            if (card.isTryCard)
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.trymoonStoneRedPlus += cardAbility.moonStoneRedplusEnemy;
                        if (player.trymoonStoneRedPlus < 0) player.trymoonStoneRedPlus = 0;
                    }
                }
            }
            else
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.moonStoneRedPlus += cardAbility.moonStoneRedplusEnemy;
                        if (player.moonStoneRedPlus < 0) player.moonStoneRedPlus = 0;
                    }
                }
            }

        }

        private void Special7(Card card)
        {
            CardAbility cardAbility = card.GetCardAbility();
            if (card.isTryCard)
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.tryMoonDustRed += cardAbility.moonDustRedEnemy;
                        if (player.tryMoonDustRed < 0) player.tryMoonDustRed = 0;
                    }
                }
            }
            else
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.moonDustRed += cardAbility.moonDustRedEnemy;
                        if (player.moonDustRed < 0) player.moonDustRed = 0;
                    }
                }
            }

        }

        private void Special6(Card card, Player target, out CardUseType cardUseType)
        {
            CardAbility cardAbility = card.GetCardAbility();

            if (card.isTryCard)
            {
                if (_playerManager.currentPlayer.tryHP > target.trySP)
                {
                    target.tryHP += cardAbility.HPEnemy;
                    if (target.tryHP < 0) target.tryHP = 0;
                    cardUseType = CardUseType.Enemy;
                }
                else
                {
                    foreach (Player player in _playerManager)
                    {
                        if (!player.IsLoseCondition)
                        {
                            int remaindamage = cardAbility.damage;
                            if (player.trySP >= remaindamage)
                            {
                                player.trySP -= remaindamage;
                            }
                            else
                            {
                                remaindamage -= player.trySP;
                                player.trySP = 0;
                                player.tryHP -= remaindamage;
                                if (player.tryHP < 0) player.tryHP = 0;
                            }
                        }
                    }
                    cardUseType = CardUseType.All;
                }
            }
            else
            {
                if (_playerManager.currentPlayer.HP > target.SP)
                {
                    target.HP += cardAbility.HPEnemy;
                    if (target.HP < 0) target.HP = 0;
                    cardUseType = CardUseType.Enemy;
                }
                else
                {
                    foreach (Player player in _playerManager)
                    {
                        if (!player.IsLoseCondition)
                        {
                            int remaindamage = cardAbility.damage;
                            if (player.SP >= remaindamage)
                            {
                                player.SP -= remaindamage;
                            }
                            else
                            {
                                remaindamage -= player.SP;
                                player.SP = 0;
                                player.HP -= remaindamage;
                                if (player.HP < 0) player.HP = 0;
                            }
                        }
                    }
                    cardUseType = CardUseType.All;
                }
            }
        }

        private void Special5(Card card, out List<Player> targetList)
        {
            targetList = new List<Player>();
            int highestZenstonePlus = 0;
            if (card.isTryCard)
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        if (highestZenstonePlus < player.tryZenStoneBluePlus) highestZenstonePlus = player.tryZenStoneBluePlus;
                    }
                }

                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        if (player.tryZenStoneBluePlus < highestZenstonePlus)
                        {
                            player.tryZenStoneBluePlus = highestZenstonePlus;
                            targetList.Add(player);
                        }
                    }
                }
            }
            else
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        if (highestZenstonePlus < player.zenStoneBluePlus) highestZenstonePlus = player.zenStoneBluePlus;
                    }
                }

                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        if (player.zenStoneBluePlus < highestZenstonePlus)
                        {
                            player.zenStoneBluePlus = highestZenstonePlus;
                            targetList.Add(player);
                        }
                    }
                }
            }
        }

        private void Special4(Card card)
        {
            CardAbility cardAbility = card.GetCardAbility();
            if (card.isTryCard)
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.tryHP += cardAbility.HPEnemy;
                        player.tryZenStoneBluePlus += cardAbility.zenStoneBlueplusEnemy;

                        if (player.tryHP < 0) player.tryHP = 0;
                        if (player.tryZenStoneBluePlus < 0) player.tryZenStoneBluePlus = 0;
                    }
                }
            }
            else
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.HP += cardAbility.HPEnemy;
                        player.zenStoneBluePlus += cardAbility.zenStoneBlueplusEnemy;

                        if (player.HP < 0) player.HP = 0;
                        if (player.zenStoneBluePlus < 0) player.zenStoneBluePlus = 0;
                    }
                }
            }

        }

        private void Special3(Card card, out bool onlyDiscard)
        {
            //...Draw a card, discard a card, play again
            onlyDiscard = true;
        }

        private void Special2(Card card, Player target)
        {
            Player owner = _playerManager.currentPlayer;
            CardAbility cardAbility = card.GetCardAbility();
            if (card.isTryCard)
            {
                if (owner.tryHP < target.tryHP) owner.tryHP += cardAbility.myHP;
                else owner.tryHP += 1;
            }
            else
            {
                if (owner.HP < target.HP) owner.HP += cardAbility.myHP;
                else owner.HP += 1;
            }

        }

        private void Special1(Card card)
        {
            CardAbility cardAbility = card.GetCardAbility();
            if (card.isTryCard)
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.tryHP += cardAbility.HPEnemy;
                        if (player.tryHP < 0) player.tryHP = 0;
                    }
                }
            }
            else
            {
                foreach (Player player in _playerManager)
                {
                    if (!player.IsLoseCondition)
                    {
                        player.HP += cardAbility.HPEnemy;
                        if (player.HP < 0) player.HP = 0;
                    }
                }
            }
        }

        internal List<RoomUserInfo> GetUserInfoList(string userSession, out bool isMine)
        {
            isMine = false;
            List<RoomUserInfo> userInfoList = new List<RoomUserInfo>();

            foreach (Player player in _playerManager)
            {
                userInfoList.Add(new RoomUserInfo(player.playerName, player.level));
                if (player.session == userSession && player == _playerManager.creator) isMine = true;
            }


            return userInfoList;
        }

        internal void PlayerEnterReady(string userSession_)
        {
            foreach (Player player in _playerManager)
            {
                if (player.session == userSession_)
                {
                    player.IsRoomEnterReady = true;
                    break;
                }

            }

        }

        internal bool WantRemoveThisRoom
        {
            get { return _rule.WantRemoveThisRoom; }
        }

        public bool EnableRoomList
        {
            get
            {
                bool retvalue = true;
                //if (_playStateManager.State != PlayState.Ready) retvalue = false;
                if (WantRemoveThisRoom) retvalue = false;
                //if (GetPlayers().Count >= this.MaxPlayer) retvalue = false;
                //if (isPlaying == true) retvalue = false;
                //new 
                if (_playStateManager.State != PlayState.Ready && isPlaying == false) retvalue = false;
                if (_playerManager.Count >= MaxPlayer && isPlaying == false) retvalue = false;

                if (isRequested) retvalue = false;

                return retvalue;
            }
        }

        public void SendPlayerList()
        {
            SCNotiPlayerList pck = new SCNotiPlayerList();
            // send playserlist
            List<Player> players = GetPlayers();
            foreach (var player in players)
            {
                pck.playerList = GetPlayerList(player.session);
                pck.playerList.name = pck.name;
                GameServer.SendMsgWithUserSession(player.session, pck);
            }

        }

        internal void ReConnectPlayerStatus(string serverSession)
        {
            List<Player> players = GetPlayers();
            foreach (var player in players)
            {
                //UserInfo playerInfo = GameServer.GetUserInfo(player.session);
                //if (playerInfo != null)
                {
                    int HP = player.HP/* + playerInfo.userDataBase.addHp*/;//addHp = 0 or 1
                    int SP = player.SP/* + playerInfo.userDataBase.addSp*/;//addSp = 0 or 1
                    int MD = player.moonDustRed;
                    int MA = player.manaBlue;
                    int RE = player.rodEnergyGreen;
                    int MS = player.moonStoneRedPlus;
                    int ZS = player.zenStoneBluePlus;
                    int MC = player.moonCrystalGreenPlus;
                    int effectIdx = 0;
                    int globalEffectIdx = 0;
                    PacketsHandlerServer.SendSCNotiPlayerStatus(serverSession, player.playerNumber, HP, SP, MD, MA, RE, MS, ZS, MC, effectIdx, globalEffectIdx);
                }
            }
        }

        internal void SendPlayerStatus()
        {
            List<Player> players = GetPlayers();
            int effectIdx = 0;//test first...
            int globalEffectIdx = 0;//test first...
            foreach (var player in players)
            {
                if (player.addHP == true)
                {
                    player.HP += 1;
                    player.addHP = false;
                }
                if (player.addSP == true)
                {
                    player.SP += 1;
                    player.addSP = false;
                }

                _rule.SendSCNotiPlayerStatus(player, effectIdx, globalEffectIdx);
            }

            if (players.Count < this.MaxPlayer)
            {
                Player tmpPlayer = null;
                int currentCount = players.Count;
                for (int i = MaxPlayer; i > currentCount; i--)
                {
                    tmpPlayer = new Player();
                    tmpPlayer.playerNumber = i;
                    tmpPlayer.HP = 0;
                    tmpPlayer.SP = 0;
                    tmpPlayer.moonDustRed = 0;
                    tmpPlayer.manaBlue = 0;
                    tmpPlayer.rodEnergyGreen = 0;
                    tmpPlayer.moonStoneRedPlus = 0;
                    tmpPlayer.zenStoneBluePlus = 0;
                    tmpPlayer.moonCrystalGreenPlus = 0;

                    _rule.SendSCNotiPlayerStatus(tmpPlayer, effectIdx, globalEffectIdx);

                    //playerList.playerList.Add(new PlayerInfo(i, OperateManager.DEFAULT_FACE_IDX, string.Empty, 0, false, false));
                }
            }
        }

        internal void ReConnectPlayerItemInfo(string serverSession)
        {
            List<Player> players = GetPlayers();
            UserInfo playerInfo = null;
            foreach (var player in players)
            {
                bool hpPlus = false;
                bool spPlus = false;
                List<int> petIdxList = new List<int>();
                int badgeIdx = 0;
                playerInfo = GameServer.GetUserInfo(player.session);
                if (playerInfo != null)
                {
                    if (playerInfo.userDataBase.userProfile.addHP == 1)
                    {
                        hpPlus = true;
                    }
                    if (playerInfo.userDataBase.userProfile.addSP == 1)
                    {
                        spPlus = true;
                    }

                    if (playerInfo.userDataBase.userItem.petList != null)
                    {
                        foreach (var pet in playerInfo.userDataBase.userItem.petList)
                        {
                            if (pet.isSelect)
                            {
                                petIdxList.Add(pet.petIdx);
                            }
                        }
                    }

                    badgeIdx = playerInfo.badgeIdx;

                    playerInfo._playerItemInfo = new PlayerItemInfo(PacketTypeList.SCNotiPlayerItemInfo.ToString(), player.playerNumber, hpPlus, spPlus, petIdxList, badgeIdx);

                    PacketsHandlerServer.SendSCNotiPlayerItemInfo(serverSession, playerInfo._playerItemInfo);
                }

            }
        }

        internal void SendPlayerItemInfo()
        {
            List<Player> players = GetPlayers();
            UserInfo playerInfo = null;
            foreach (var player in players)
            {
                bool hpPlus = false;
                bool spPlus = false;
                List<int> petIdxList = new List<int>();
                int badgeIdx = 0;
                playerInfo = GameServer.GetUserInfo(player.session);
                if (playerInfo != null)
                {
                    if (playerInfo.userDataBase.userProfile.addHP == 1)
                    {
                        hpPlus = true;
                    }
                    if (playerInfo.userDataBase.userProfile.addSP == 1)
                    {
                        spPlus = true;
                    }

                    if (playerInfo.userDataBase.userItem.petList != null)
                    {
                        foreach (var pet in playerInfo.userDataBase.userItem.petList)
                        {
                            if (pet.isSelect)
                            {
                                petIdxList.Add(pet.petIdx);
                            }
                        }
                    }

                    badgeIdx = playerInfo.badgeIdx;

                    playerInfo._playerItemInfo = new PlayerItemInfo(PacketTypeList.SCNotiPlayerItemInfo.ToString(), player.playerNumber, hpPlus, spPlus, petIdxList, badgeIdx);
                    _rule.SendSCNotiPlayerItemInfo(/*playerInfo.session,*/ playerInfo._playerItemInfo);
                }
            }
            if (players.Count < this.MaxPlayer)
            {
                PlayerItemInfo _tmpPlayerItemInfo = null;
                int currentCount = players.Count;
                List<int> tmpPetList = new List<int>();
                for (int i = MaxPlayer; i > currentCount; i--)
                {
                    _tmpPlayerItemInfo = new PlayerItemInfo(PacketTypeList.SCNotiPlayerItemInfo.ToString(), i, false, false, tmpPetList, 0);
                    _rule.SendSCNotiPlayerItemInfo(/*playerInfo.session,*/ _tmpPlayerItemInfo);

                    //playerList.playerList.Add(new PlayerInfo(i, OperateManager.DEFAULT_FACE_IDX, string.Empty, 0, false, false));
                }
            }
        }

        internal void ResetAutoDiscardCount()
        {
            if (_playerManager.currentPlayer.autoDiscardCount != 0)
            {
                _playerManager.currentPlayer.autoDiscardCount = 0;
            }
        }

        internal void GetTryEvaluateFunction(decimal f_current_, out int handPosition_, out int targetNumber_, out Card card_)
        {
            handPosition_ = 0;
            targetNumber_ = 0;
            card_ = null;

            int tryTargetNumber = 0;
            int enemyNumber = 0;
            foreach (var player in _playerManager)
            {
                if (player != _playerManager.currentPlayer)
                {
                    enemyNumber = player.playerNumber;
                }
            }

            //Card card = null;
            decimal[] f_try = new decimal[6];

            for (int pos = 1; pos <= 6; pos++)
            {
                foreach (Player player in _playerManager)
                {
                    player.tryHP = player.HP;
                    player.trySP = player.SP;
                    player.tryMoonDustRed = player.moonDustRed;
                    player.tryManaBlue = player.manaBlue;
                    player.tryRodEnergyGreen = player.rodEnergyGreen;
                    player.trymoonStoneRedPlus = player.moonStoneRedPlus;
                    player.tryZenStoneBluePlus = player.zenStoneBluePlus;
                    player.tryMoonCrystalGreenPlus = player.moonCrystalGreenPlus;
                }

                if (_roomCardManager.GetHandCard(_playerManager.currentPlayer.playerNumber, pos, out card_))
                {
                    switch (card_.Info.cardUseType)
                    {
                        case CardUseType.Deck: tryTargetNumber = 0; break;
                        case CardUseType.Me: tryTargetNumber = _playerManager.currentPlayer.playerNumber; break;
                        default: tryTargetNumber = enemyNumber; break;
                    }

                    if (_playerManager.currentPlayer.IsCardUseable(card_, tryTargetNumber))
                    {
                        Player target = null;

                        if (tryTargetNumber != 0)
                        {
                            if (_playerManager.GetPlayer(tryTargetNumber, out target))
                            {
                                //switch shield card
                                if (card_.Info.special != 16 || (card_.Info.special == 16 && _playerManager.currentPlayer.SP < target.SP))
                                {
                                    TryUseCard(card_, target);
                                }
                            }
                        }
                    }
                }

                f_try[pos - 1] = _playerManager.GetEvaluateFunction(true);
            }

            //decimal f_try_max = f_try[0];
            //int pos_max = 1;
            //decimal f_try_min = f_try[0];
            //int pos_min = 1;

            //for (int i = 2; i <= 6; i++)
            //{
            //    if (f_try_max <= f_try[i - 1])
            //    {
            //        f_try_max = f_try[i - 1];
            //        pos_max = i;
            //    }

            //    else if (f_try_min >= f_try[i - 1])
            //    {
            //        f_try_min = f_try[i - 1];
            //        pos_min = i;
            //    }
            //}

            //initialize
            int pos_max = 1;
            int pos_min = 1;

            decimal[] f_try_tmp = new decimal[6];
            for (int i = 0; i < f_try.Length; i++)
            {
                f_try_tmp[i] = f_try[i];
            }

            //order from big to small 
            decimal tmp = 0;
            for (int i = 0; i < f_try_tmp.Length - 1; i++)
            {
                for (int j = i; j < f_try_tmp.Length; j++)
                {
                    if (f_try_tmp[i] < f_try_tmp[j])
                    {
                        tmp = f_try_tmp[i];
                        f_try_tmp[i] = f_try_tmp[j];
                        f_try_tmp[j] = tmp;
                    }
                }
            }

            //find pos_max
            bool isPosMaxFind = false;
            for (int j = 0; j < f_try_tmp.Length; j++)
            {
                if (isPosMaxFind) break;
                for (int i = 0; i < f_try.Length; i++)
                {
                    if (f_try_tmp[j] == f_try[i])
                    {
                        if (f_try[i] >= f_current_)
                        {
                            if (_roomCardManager.GetHandCard(_playerManager.currentPlayer.playerNumber, i + 1, out card_))
                            {
                                switch (card_.Info.cardUseType)
                                {
                                    case CardUseType.Deck: tryTargetNumber = 0; break;
                                    case CardUseType.Me: tryTargetNumber = _playerManager.currentPlayer.playerNumber; break;
                                    default: tryTargetNumber = enemyNumber; break;
                                }
                                if (_playerManager.currentPlayer.IsCardUseable(card_, tryTargetNumber))
                                {
                                    pos_max = i + 1;
                                    isPosMaxFind = true;
                                }
                            }
                        }

                        break;
                    }
                }
            }

            //find pos_min
            bool isPosMinFind = false;
            if (onlyDiscard || !isPosMaxFind)
            {
                for (int j = f_try_tmp.Length - 1; j >= 0; j--)
                {
                    if (isPosMinFind) break;
                    for (int i = 0; i < f_try.Length; i++)
                    {
                        if (f_try_tmp[j] == f_try[i])
                        {
                            if (f_try[i] < f_current_)
                            {
                                if (_roomCardManager.GetHandCard(_playerManager.currentPlayer.playerNumber, i + 1, out card_))
                                {
                                    switch (card_.Info.cardUseType)
                                    {
                                        case CardUseType.Deck: tryTargetNumber = 0; break;
                                        case CardUseType.Me: tryTargetNumber = _playerManager.currentPlayer.playerNumber; break;
                                        default: tryTargetNumber = enemyNumber; break;
                                    }
                                    if (_playerManager.currentPlayer.IsCardUseable(card_, tryTargetNumber))
                                    {
                                        pos_min = i + 1;
                                        isPosMinFind = true;
                                    }
                                }
                            }

                            break;
                        }
                    }
                }

                if (!isPosMinFind)
                {
                    //calculate maxResourceGapPos
                    int maxResourceGapPos = 0;
                    int maxResourceGap = 0;
                    int tmp_gap = 0;
                    CardAbility cardAbility;
                    for (int i = 0; i < 6; i++)
                    {
                        if (maxResourceGapPos == 0) maxResourceGapPos = i + 1;
                        if (_roomCardManager.GetHandCard(_playerManager.currentPlayer.playerNumber, i + 1, out card_))
                        {
                            cardAbility = card_.GetCardAbility();
                            //moondust
                            if (card_.Info.priceColor == (int)CardPriceColor.Moondust)
                                tmp_gap = cardAbility.priceMoonDustRed - _playerManager.currentPlayer.moonDustRed;
                            //rod energy
                            if (card_.Info.priceColor == (int)CardPriceColor.RodEnergy)
                                tmp_gap = cardAbility.priceRodEnergyGreen - _playerManager.currentPlayer.rodEnergyGreen;
                            //mana
                            if (card_.Info.priceColor == (int)CardPriceColor.Mana)
                                tmp_gap = cardAbility.priceManaBlue - _playerManager.currentPlayer.manaBlue;

                            if (maxResourceGap < tmp_gap)
                            {
                                maxResourceGap = tmp_gap;
                                maxResourceGapPos = i + 1;
                            }
                        }
                    }

                    pos_min = maxResourceGapPos;
                    isPosMinFind = true;
                }
            }

            if (onlyDiscard) handPosition_ = pos_min;
            else
            {
                if (isPosMaxFind) handPosition_ = pos_max;
                else handPosition_ = pos_min;
            }

            if (_roomCardManager.GetHandCard(_playerManager.currentPlayer.playerNumber, handPosition_, out card_))
            {
                //Console.WriteLine(card_.Info.name + ", PriceBlue:" + card_.Info.ability.priceManaBlue + ", PriceRed: " + card_.Info.ability.priceMoonDustRed + ", PriceGreen: " + card_.Info.ability.priceRodEnergyGreen);
                switch (card_.Info.cardUseType)
                {
                    case CardUseType.Deck: targetNumber_ = 0; break;
                    case CardUseType.Me: targetNumber_ = _playerManager.currentPlayer.playerNumber; break;
                    default: targetNumber_ = enemyNumber; break;
                }

                //not "discard a card, get a card, play again" card
                if ((handPosition_ == pos_min && isPosMinFind && card_.Info.special != 0 && card_.Info.special != 20)
                    || !_playerManager.currentPlayer.IsCardUseable(card_, targetNumber_)
                    || onlyDiscard)
                {
                    targetNumber_ = 0;
                }

            }

        }

        private void TryUseCard(Card tryCard, Player tryTarget)
        {
            tryCard.isTryCard = true;
            if (tryCard.Index == CardManager.JOKER_INDEX)
            {
                TryJokerAppliaction(tryTarget);

                //decrease resource price...
                TryChargeJokerPrice();
            }
            else
            {
                CardUseType tryCardUseType = tryCard.Info.cardUseType;
                List<Player> tryTargetList = null;
                switch (tryCard.Info.special)
                {
                    case 1: Special1(tryCard); break;
                    case 2: Special2(tryCard, tryTarget); break;
                    //case 3: Special3(tryCard, out onlyDiscard); break;
                    case 4: Special4(tryCard); break;
                    case 5: Special5(tryCard, out tryTargetList); break;
                    case 6: Special6(tryCard, tryTarget, out tryCardUseType); break;
                    case 7: Special7(tryCard); break;
                    case 8: Special8(tryCard); break;
                    case 9: Special9(tryCard); break;
                    case 10: Special10(tryCard); break;
                    case 11: Special11(tryCard, tryTarget); break;
                    case 12: Special12(tryCard, tryTarget); break;
                    case 13: Special13(tryCard, out tryTargetList); break;
                    case 14: Special14(tryCard); break;
                    case 15: Special15(tryCard, tryTarget); break;
                    case 16: Special16(tryCard, tryTarget); break;
                    case 17: Special17(tryCard); break;
                    case 18: Special18(tryCard); break;
                    case 19: Special19(tryCard, tryTarget); break;
                    //case 20: Special20(tryCard, out onlyDiscard); break;
                    case 21: Special21(tryCard, tryTarget); break;
                    case 22: Special22(tryCard, tryTarget); break;
                    case 23: Special23(tryCard, tryTarget); break;
                    case 24: Special24(tryCard, tryTarget); break;
                    case 25: Special25(tryCard, tryTarget); break;
                    case 26: Special26(tryCard, tryTarget); break;
                    case 27: Special27(tryCard); break;
                    default:
                        // if target is PLAYER, attack to target...
                        tryTarget.TryTargetApplyCard(tryCard.GetCardAbility(), _playerManager.currentPlayer);
                        break;
                }
            }
            //resource currentplayer's price decrease, play again and isUseCard check
            _playerManager.currentPlayer.tryMoonDustRed += (tryCard.GetCardAbility().moonDustRed - tryCard.GetCardAbility().priceMoonDustRed);
            if (_playerManager.currentPlayer.tryMoonDustRed < 0) _playerManager.currentPlayer.tryMoonDustRed = 0;
            _playerManager.currentPlayer.tryManaBlue += (tryCard.GetCardAbility().manaBlue - tryCard.GetCardAbility().priceManaBlue);
            if (_playerManager.currentPlayer.tryManaBlue < 0) _playerManager.currentPlayer.tryManaBlue = 0;
            _playerManager.currentPlayer.tryRodEnergyGreen += (tryCard.GetCardAbility().rodEnergyGreen - tryCard.GetCardAbility().priceRodEnergyGreen);
            if (_playerManager.currentPlayer.tryRodEnergyGreen < 0) _playerManager.currentPlayer.tryRodEnergyGreen = 0;

            tryCard.isTryCard = false;
        }

        private void TryChargeJokerPrice()
        {
            UserInfo userInfo = GameServer.GetUserInfo(_playerManager.currentPlayer.session);
            if (userInfo != null)
            {
                if (userInfo.userDataBase.userJoker.spellIdxList != null)
                {
                    int priceMoonDustRed = 0;
                    int priceManaBlue = 0;
                    int priceRodEnergyGreen = 0;
                    foreach (var spellIdx in userInfo.userDataBase.userJoker.spellIdxList)
                    {
                        SpellInfo spellInfo = SpellManager.Instance.GetSpellInfo(spellIdx);
                        if (spellInfo != null)
                        {
                            priceMoonDustRed += spellInfo.ability.priceMD;
                            priceManaBlue += spellInfo.ability.priceM;
                            priceRodEnergyGreen += spellInfo.ability.priceRE;
                        }
                    }

                    _playerManager.currentPlayer.tryMoonDustRed -= priceMoonDustRed;
                    if (_playerManager.currentPlayer.tryMoonDustRed < 0) _playerManager.currentPlayer.tryMoonDustRed = 0;
                    _playerManager.currentPlayer.tryManaBlue -= priceManaBlue;
                    if (_playerManager.currentPlayer.tryManaBlue < 0) _playerManager.currentPlayer.tryManaBlue = 0;
                    _playerManager.currentPlayer.tryRodEnergyGreen -= priceRodEnergyGreen;
                    if (_playerManager.currentPlayer.tryRodEnergyGreen < 0) _playerManager.currentPlayer.tryRodEnergyGreen = 0;

                }
            }
        }

        private void TryJokerAppliaction(Player target)
        {
            UserInfo userInfo = GameServer.GetUserInfo(_playerManager.currentPlayer.session);
            if (userInfo != null)
            {
                if (userInfo.userDataBase.userJoker.spellIdxList != null)
                {
                    foreach (var spellIdx in userInfo.userDataBase.userJoker.spellIdxList)
                    {
                        SpellInfo spellInfo = null;
                        spellInfo = SpellManager.Instance.GetSpellInfo(spellIdx);
                        switch (spellInfo.special)
                        {
                            case 1:
                                //switch shield point
                                int tmpSP = _playerManager.currentPlayer.SP;
                                _playerManager.currentPlayer.trySP = target.trySP;
                                target.trySP = tmpSP;

                                break;
                            case 2:
                                //play again
                                break;
                            default:
                                TryTargetApplySpell(target, spellInfo.ability);
                                break;
                        }
                    }
                }
            }
        }

        private void TryTargetApplySpell(Player target, SpellAbility spellAbility)
        {
            //OWNER
            Player owner = _playerManager.currentPlayer;

            owner.tryManaBlue += spellAbility.manaBlue;
            if (owner.tryManaBlue < 0) owner.tryManaBlue = 0;
            owner.tryZenStoneBluePlus += spellAbility.zenstoneBlue;
            if (owner.tryZenStoneBluePlus < 0) owner.tryZenStoneBluePlus = 0;
            owner.tryMoonDustRed += spellAbility.moonDustRed;
            if (owner.tryMoonDustRed < 0) owner.tryMoonDustRed = 0;
            owner.trymoonStoneRedPlus += spellAbility.moonstoneRed;
            if (owner.trymoonStoneRedPlus < 0) owner.trymoonStoneRedPlus = 0;
            owner.tryRodEnergyGreen += spellAbility.rodEnergyGreen;
            if (owner.tryRodEnergyGreen < 0) owner.tryRodEnergyGreen = 0;
            owner.tryMoonCrystalGreenPlus += spellAbility.moonCrystalGreen;
            if (owner.tryMoonCrystalGreenPlus < 0) owner.tryMoonCrystalGreenPlus = 0;

            //TARGET
            target.tryHP += spellAbility.hpEnemy;
            if (target.tryHP < 0) target.tryHP = 0;
            target.tryManaBlue += spellAbility.manaEnemyBlue;
            if (target.tryManaBlue < 0) target.tryManaBlue = 0;
            target.tryZenStoneBluePlus += spellAbility.zenstoneEnemyBlue;
            if (target.tryZenStoneBluePlus < 0) target.tryZenStoneBluePlus = 0;
            target.tryMoonDustRed += spellAbility.moonDustEnemyRed;
            if (target.tryMoonDustRed < 0) target.tryMoonDustRed = 0;
            target.trymoonStoneRedPlus += spellAbility.moonstoneEnemyRed;
            if (target.trymoonStoneRedPlus < 0) target.trymoonStoneRedPlus = 0;
            target.tryRodEnergyGreen += spellAbility.rodEnergyEnemyGreen;
            if (target.tryRodEnergyGreen < 0) target.tryRodEnergyGreen = 0;
            target.tryMoonCrystalGreenPlus += spellAbility.moonCrystalEnemyGreen;
            if (target.tryMoonCrystalGreenPlus < 0) target.tryMoonCrystalGreenPlus = 0;

            if (spellAbility.damage > 0)
            {
                int remaindamage = spellAbility.damage;

                if (target.trySP >= remaindamage)
                {
                    target.trySP -= remaindamage;
                }
                else
                {
                    remaindamage -= target.trySP;
                    target.trySP = 0;
                    target.tryHP -= remaindamage;
                    if (target.tryHP < 0) target.tryHP = 0;
                }
            }
        }

    }
}
