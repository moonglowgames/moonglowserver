﻿using MoonGlow;
using MoonGlow.Room;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class AIPlay
{
    private Room _room = null;
    private PlayerManager _playerManager = null;

    public AIPlay(Room room_, PlayerManager playerManager_)
    {
        _room = room_;
        _playerManager = playerManager_;
    }

    public void Play()
    {
        //=======================================================================
        //2 players only
        if (_playerManager.Count == 2 && _playerManager.currentPlayer.setAI)
        {
            decimal f_current =  _playerManager.GetEvaluateFunction(false);

            int handPosition = 0;
            int targetNumber = 0;
            Card card = null;
            _room.GetTryEvaluateFunction(f_current, out handPosition, out targetNumber, out card);
            _room.UseCard(_playerManager.currentPlayer.session, targetNumber, handPosition);

            //if (!_playerManager.currentPlayer.setAI) _playerManager.currentPlayer.autoPlayCount += 1;
        }
        else
        {
            //auto discard..
            //random discard
            int randHandPosition = 0;
            if (_room._rule.GetRandomHandPosition(out randHandPosition))
            {
                _room.UseCard(_playerManager.currentPlayer.session, 0, randHandPosition);
                _playerManager.currentPlayer.autoPlayCount += 1;
            }
        }
    }
}