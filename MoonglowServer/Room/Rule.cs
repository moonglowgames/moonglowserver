﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoonGlow.Data;
using System.Timers;

namespace MoonGlow.Room
{
    public class Rule:CommandTransfer
    {
        private RoomCardManager _roomCardManager = null;
        private PlayerManager _playerManager = null;
        private PlayStateManager _playStateManager = null;
        private Room _room = null;
        private int autoDiscardTime = 0;
        private int randDiscardTime = 0;
        private int _playDelayTime = 0;

        public bool WantRemoveThisRoom = false;

        public AIPlay _AI = null;

        public Rule( Room room_, RoomCardManager roomCardManager_,PlayerManager playerManager_, PlayStateManager playStateManager_)
        {
            this.myTag = CommandTag.Rule;
            _roomCardManager = roomCardManager_;
            _playerManager = playerManager_;
            _playStateManager = playStateManager_;
            _room = room_;
            _AI = new AIPlay(_room, _playerManager);
        }

        // intervalTickcount = 100 msec
        public void Run(int timeCount)
        {
            // player enter just one
            switch ( _playStateManager.State)
            { 
                case PlayState.Unknown:
                    //player is joined = more one player
                    if (_playerManager.Count > 0)
                    {
                        _playStateManager.SetState(PlayState.Ready);
                    }
                    _playDelayTime = 0;

                    break;
                case PlayState.Ready:
                    // player full in the room and some dealy
                    if (_playDelayTime == 0 && _playerManager.Count == _room.MaxPlayer && _playerManager.IsAllPlayerReady)
                    {
                        _playDelayTime = timeCount + 4000;
                    }

                    if( _playDelayTime != 0 && _playDelayTime < timeCount && _playerManager.Count == _room.MaxPlayer)
                    {
                        _playStateManager.SetState(PlayState.Start);
                    }
                    
                    break;
                case PlayState.Start:
                    // after some delay choice one player and untile end
                    _room.isPlaying = true;
                    //_room.onlyDiscard = false;
                    _playerManager.ChooseFirstPlayer();
                    _playStateManager.SetState(PlayState.TurnUseCard);
                    
                    break;
                case PlayState.TurnUseCard:
                    // used card?
                    // if use card then give one card from gamecard...
                    if (_playerManager.IsReachWinCondition())
                    {
                        _playStateManager.SetState(PlayState.End);
                    }

                    if (autoDiscardTime == 0)
                    {
                        int interval = 0;
                        switch (_room.playTime)
                        {
                            case PlayTime.Sec_30:
                                interval = 30000;
                                break;
                            case PlayTime.Sec_60:
                                interval = 60000;
                                break;
                            default:
                                interval = 30000;
                                break;
                        }

                        autoDiscardTime = timeCount + interval;
                    }

                    if (autoDiscardTime < timeCount)
                    {
                        int randHandPosition = 0;
                        if (GetRandomHandPosition(out randHandPosition))
                        {
                            _room.UseCard(_playerManager.currentPlayer.session, 0, randHandPosition);
                        }

                        _playerManager.currentPlayer.autoDiscardCount += 1;
                        //_playStateManager.SetState(PlayState.TurnRecieveCard);
                    }

                    //AI's job, change rule when trusteeship work, later...
                    else if (_playerManager.currentPlayer.isUser == false)
                    {
                        if (randDiscardTime == 0)
                        {
                            int timeWait = 5000;
                            if (_playerManager.currentPlayer.setAI)
                            {
                                timeWait = 3000;
                            }
                           
                            randDiscardTime = timeCount + timeWait;
                        }

                        if (randDiscardTime < timeCount)
                        {
                            ////random discard
                            //int randHandPosition = 0;
                            //if (GetRandomHandPosition(out randHandPosition))
                            //{
                            //    _room.UseCard(_playerManager.currentPlayer.session, 0, randHandPosition);
                            //    _playerManager.currentPlayer.randDiscardCount += 1;
                            //}

                            //AI is coming...
                            _AI.Play();
                        }
                    }
                    
                    if (_playerManager.currentPlayer.IsUseCard)
                    {
                        _playerManager.currentPlayer.IsUseCard = false;

                        _playStateManager.SetState(PlayState.TurnRecieveCard);

                    }
                    else
                    {
                        return;
                    }
                    // if not use card  return;
                    break;
                    // Turn with UseCard ... Turn with getCard... important...    
                case PlayState.TurnRecieveCard:
                    _room.playCount++;

                    autoDiscardTime = 0;
                    randDiscardTime = 0;
                    
                    if (_playerManager.currentPlayer.IsRecieveCard)
                    {
                        _playerManager.currentPlayer.IsRecieveCard = false;

                        // after some delay choose next player and untill end
                        if (!_playerManager.IsReachWinCondition())
                        {
                            _playerManager.ChooseNextPlayer(_room);
                            _playStateManager.SetState(PlayState.TurnUseCard);
                        }
                        else
                        {
                            // some player reach win condition or others Hp is 0
                            _playStateManager.SetState(PlayState.End);
                        }
                    }
                    break;
                case PlayState.End:
                    // after some delay  and show result window
                    _room.isPlaying = false;
            
                    _playStateManager.SetState(PlayState.Result);
                    break;
                case PlayState.Result:
                    if (_room.WantRestart)
                    {
                        // some player choose play again button ...
                        _playStateManager.SetState(PlayState.Ready);
                    }

                    if (_playerManager.Count == 0)
                    {
                        // all member leave then delete this room
                        _playStateManager.SetState(PlayState.Delete);
                    }
                    break;
                case PlayState.Delete:
                    break;
                default:
                    break;
            }
        }

        internal void DecreaseStake()
        {
            UserInfo userInfo = null;
            foreach (Player player in _playerManager)
            {
                userInfo = GameServer.GetUserInfo(player.session);
                if (userInfo != null)
                {
                    userInfo.userDataBase.Gold -= _room.stake;
                    if (userInfo.userDataBase.Gold < 0)
                    {
                        userInfo.userDataBase.Gold = 0;
                    }

                    //SCNotiUserElement
                    userInfo.SetSendElementInfoList();

                    if (GameServer._isCloud)
                    {
                        //item log
                        //stake use
                        GlobalManager.Instance.LogItemInsert(userInfo.serverSession, userInfo.userID, userInfo.userDataBase.nickName, (int)ElementType.Stake, ElementType.Stake.ToString(), -_room.stake, LogItemType.Use);
                    }
                }
            }
        }

        internal bool GetRandomHandPosition(out int randHandPosition)
        {
            randHandPosition = 0;
            // time end...
            bool discardAble = false;
            Random random = new Random();
            Card card = null;

            while (discardAble == false)
            {
                randHandPosition = random.Next(1, 7);
                if (_roomCardManager.GetHandCard(_playerManager.currentPlayer.playerNumber, randHandPosition, out card))
                {
                    if (card.Index != CardManager.JOKER_INDEX)
                    {
                        discardAble = true;
                    }
                }
            }
            return discardAble;
        }

        internal void InitRoomCard()
        {
            // card initialize...
            _roomCardManager.isFirstInitial = true;
            _roomCardManager.InitGameCard();

            // player hand card initialize...
            _roomCardManager.InitPlayerHandCard(_playerManager);

        }

        internal void GetOneCardFromAll()
        {
            // all -> one card -> currentplayer
            int cardIndex = 0;
            List<int> playerPetList = null;
            UserInfo playerInfo = GameServer.GetUserInfo(_playerManager.currentPlayer.session);
            if (playerInfo != null)
            {
                if (playerInfo._playerItemInfo == null)
                {
                    playerInfo._playerItemInfo = new PlayerItemInfo();
                }

                playerPetList = playerInfo._playerItemInfo.petList;
            }

            if (_roomCardManager.GetGameCard(out cardIndex, playerPetList))
            { 
                int playerNumber = _playerManager.currentPlayer.playerNumber;

                int emptySlotNumber = 0;

                // find empty cardhandslot
                if (_roomCardManager.FindEmptySlot(playerNumber, out emptySlotNumber))
                {
                    // save to empty cardhandslot
                    if (_roomCardManager.SetHandCard(playerNumber, emptySlotNumber, cardIndex , false))
                    { 
                        // SCNotiDeckToHand send me- reset empty hand position
                        SendCurrentPlayerHandCardFromDeck( emptySlotNumber);
                        _playerManager.currentPlayer.IsRecieveCard = true;
                    }
                }
            }
            // currentplayer. receivedcard = true;
        }

        internal void RefillPriceResource()
        {
            int playerNumber_ = _playerManager.currentPlayer.playerNumber;
            // 1. get player
            foreach (Player player in _playerManager)
            {
                if (player.playerNumber == playerNumber_)
                {
                    if (_playerManager.doubleTurn == false)
                    {
                        // 2. player.RefillPriceResource();
                        player.RefillPriceResource();
                    }
                    // 3. SCNotiPlayerStatus send All member
                    int effectIdx = 0;//test first...
                    int globalEffectIdx = 0;//test first...
                    SendSCNotiPlayerStatus(player, effectIdx, globalEffectIdx);

                    break;
                }
            }
        }

        internal void DecreaseStamina()
        {
            if (_room.singlePlayer) return;

            UserInfo userInfo = null;
            foreach (Player player in _playerManager)
            {
                userInfo = GameServer.GetUserInfo(player.session);
                if (userInfo != null)
                {
                    userInfo.userDataBase.Stamina -= 10;
                    if (userInfo.userDataBase.Stamina < 0)
                    {
                        userInfo.userDataBase.Stamina = 0;
                    }
                    //SCNotiUserElement
                    userInfo.SetSendElementInfoList();
                }
            }
        }

        internal void SendSCNotiContinueGame(string userSession)
        {
            // make packet, sendtoParent
            SCNotiContinueGame pck = new SCNotiContinueGame();

            // make command
            Command cmd = new Command();
            cmd.pck = pck;
            cmd.receiver = CommandTag.Client;
            cmd.sender = CommandTag.Rule;
            cmd.executeTime = 100; // 100 msec after
            cmd.sendUserSession = new List<string>();
            cmd.sendUserSession.Add(userSession);
            cmd.sendType = CommandSendType.Players;

            // send to parent
            SendToParent(cmd);
        }

        internal void SendSCNotiStartGame()
        {
            _room.startTime = DateTime.Now;
            // make packet, sendtoParent
            SCNotiStartGame pck = new SCNotiStartGame();
            
            // make command
            Command cmd = new Command();
            cmd.pck = pck;
            cmd.receiver = CommandTag.Client;
            cmd.sender = CommandTag.Rule;
            cmd.executeTime = 100; // 100 msec after
            cmd.sendUserSession = _playerManager.GetUserSessionList(_room);
            cmd.sendType = CommandSendType.Players;
            
            // send to parent
            SendToParent(cmd);
        }

        internal void SendSCNotiPlayerFocus()
        {
            // make packet
            SCNotiPlayerFocus pck = new SCNotiPlayerFocus();
            if (_playerManager.currentPlayer != null)
            {
                pck.playerNumber = _playerManager.currentPlayer.playerNumber;
                pck.onlyDiscard = _room.onlyDiscard;
                // user enable use card one time
                _playerManager.currentPlayer.IsUseCard = false;
            }
            else
            {
                // Logger .. is Error..
                pck.playerNumber = 0;
            }

            // make command
            Command cmd = new Command();
            cmd.pck = pck;
            cmd.receiver = CommandTag.Client;
            cmd.sender = CommandTag.Rule;
            cmd.executeTime = 100;
            cmd.sendUserSession = _playerManager.GetUserSessionList(_room);
            cmd.sendType = CommandSendType.Players;

            // send to parent
            SendToParent(cmd);
        }

        internal void SendSCNotiGameEnd()
        {
            //_playerManager.AddRecommendFriends();
             // make packet
            SCNotiGameEnd pck = new SCNotiGameEnd();
            
            int winPlayerNum = 0;
            if (_playerManager.WinnerPlayerNumber(ref winPlayerNum))
            {
                pck.winnerNumber = winPlayerNum;
            }
            else
            {
                // logger is error...
                pck.winnerNumber = 0;
            }

            // make command
            Command cmd = new Command();
            cmd.pck = pck;
            cmd.receiver = CommandTag.Client;
            cmd.sender = CommandTag.Rule;
            cmd.executeTime = 100;
            cmd.sendUserSession = _playerManager.GetUserSessionList(_room);
            cmd.sendType = CommandSendType.Players;

            // send to parent
            SendToParent(cmd);
        }

        // send player eachother
        internal void SendSCNotiGameResult()
        {
            int addGold = 0;
            //ruby and iteminfo is also gift, not use now...
            int addRuby = 0;
            ItemInfo itemInfo = new ItemInfo(0,0);
            
            //bool isLevelUp = false;
            TimeSpan gameTime = DateTime.Now - _room.startTime;
           
            string gameTimeStr = string.Format("{0:00}:{1:00}", gameTime.Minutes, gameTime.Seconds);
            bool adOK = false;
            GameEndReason reason = GameEndReason.Unknown;
            int reasonValue = 0;
            Result result = Result.Draw;
            int downLevelExp = 0;
            int upLevelExp = 0;

            //level log
            //LogLevelData levelData = null;
            //item log
            //LogItemData itemData = null;

            GameType gameType = _room.gameType;
            PlayMode playMode = _room.playMode;
            PlayTime playTime = _room.playTime;
            int stake = _room.stake;
            bool disClose = _room.disClose;
            bool isLock = _room.isLock;

            UserInfo playerInfo = null;
            //int winCount = 0;
            //int totalCount = 0;
            //decimal winRate = 0;
            int addRankPoint = 0;
            int addStake = 0;
            bool levelUp = false;
            bool levelDown = false;
            List<PlayFriend> playFriendList = new List<PlayFriend>();
            foreach (Player player in _playerManager)
            {
                playerInfo = GameServer.GetUserInfo(player.session);
                if (playerInfo != null && !player.wantForceExit/*&& playerInfo.userServerState != UserServerStatus.DisConnect*/)
                {
                    //if ((_room._pausePlayerSessionList != null && _room._pausePlayerSessionList.Contains(player.session)) ||
                    //    (_room._pausePlayerSessionList != null && !_room._pausePlayerSessionList.Contains(player.session) && player.noReward == false) ||
                    //    (_room._pausePlayerSessionList == null && player.noReward == false))
                    //{
                        addStake = _room.stake;
                        levelDown = false;
                        levelUp = false;
                        addRankPoint = 0;
                        adOK = playerInfo.IsAdOK();
                        reasonValue = 0;

                        //winCount = playerInfo.userDataBase.userProfile.winCount;
                        //totalCount = playerInfo.userDataBase.userProfile.winCount + playerInfo.userDataBase.userProfile.loseCount /*+ playerInfo.userDataBase.userProfile.drawCount*/;
                        //if (totalCount != 0) winRate = winCount*100 / totalCount;

                        downLevelExp = LevelExpManager.Instance.GetDownLevelExp(player.level);
                        upLevelExp = LevelExpManager.Instance.GetUpLevelExp(player.level);
                        if (_room.unNormal == true && _room.playCount < 10)
                        {
                            addGold = 0;
                            reason = GameEndReason.EnemyExitWithinTenPlay;
                            result = Result.Draw;

                            playerInfo.userDataBase.Gold += addStake;

                            if (GameServer._isCloud)
                            {
                                //item log
                                //stake return
                                GlobalManager.Instance.LogItemInsert(playerInfo.serverSession, playerInfo.userID, playerInfo.userDataBase.nickName, (int)ElementType.Stake, ElementType.Stake.ToString(), addStake, LogItemType.Return);
                            }
                        }//draw
                        else
                        {
                            if (player == _playerManager.winnerPlayer)
                            {
                                if (!_room.singlePlayer)
                                {
                                    if (player.noReward == true) { addGold = 0; addStake = -addStake; }
                                    else
                                    {
                                        addGold = RewardManager.Instance.GetWinCoin(_room.gameType, _room.playMode);
                                        //isWin = true;
                                        addStake = addStake * 2;
                                        playerInfo.userDataBase.Gold += addStake;

                                        if (playerInfo.userDataBase.Level < 30) playerInfo.userDataBase.Exp += 1;

                                        if (GameServer._isCloud)
                                        {
                                            //item log
                                            //stake win
                                            GlobalManager.Instance.LogItemInsert(playerInfo.serverSession, playerInfo.userID, playerInfo.userDataBase.nickName, (int)ElementType.Stake, ElementType.Stake.ToString(), addStake, LogItemType.Game);
                                        }
                                    }
                                }
                                
                                //check game end reason
                                if (_room.unNormal)
                                {
                                    reason = GameEndReason.EnemyExit;
                                }
                                else
                                {
                                    if (player.HP >= _room.goalHP)
                                    {
                                        reason = GameEndReason.IHPFull;
                                        reasonValue = _room.goalHP;
                                    }
                                    else if (player.manaBlue >= _room.goalResource && player.moonDustRed >= _room.goalResource && player.rodEnergyGreen >= _room.goalResource)
                                    {
                                        reason = GameEndReason.IResFull;
                                        reasonValue = _room.goalResource;
                                    }
                                    else 
                                    {
                                        //force lose
                                        reason = GameEndReason.EnemyHPZero;
                                        foreach (var player2 in _playerManager)
                                        {
                                            if (player2.autoDiscardCount >= 5 || player.autoPlayCount >= 5)
                                            {
                                                reason = GameEndReason.EnemyExit;
                                                break;
                                            }
                                        }

                                    }
                                }

                                result = Result.Win;
                            }//win
                            else
                            {
                                if (!_room.singlePlayer)
                                {
                                    if (player.noReward == true) { addGold = 0; }
                                    else if (!_room.singlePlayer)
                                    {
                                        if ((playerInfo.userDataBase.Level > 1 || (playerInfo.userDataBase.Level == 1 && playerInfo.userDataBase.Exp > downLevelExp)))
                                            playerInfo.userDataBase.Exp -= 1;

                                        addGold = RewardManager.Instance.GetLoseCoin(_room.gameType, _room.playMode);
                                        //isWin = false;
                                    }

                                    addStake = -addStake;
                                }

                                //check game end reason
                                if (_room.unNormal || player.autoDiscardCount >= 5 || player.autoPlayCount >= 5)
                                {
                                    reason = GameEndReason.IExit;
                                }
                                else
                                {
                                    if (player.HP <= 0)
                                    {
                                        reason = GameEndReason.IHPZero;
                                    }
                                    else
                                    {
                                        //enemyhpfull or enemyresourcefull
                                        foreach (var player2 in _playerManager)
                                        {
                                            if (player2.HP >= _room.goalHP)
                                            {
                                                reason = GameEndReason.EnemyHPFull;
                                                reasonValue = _room.goalHP;
                                                break;
                                            }
                                            if (player2.manaBlue >= _room.goalResource && player2.moonDustRed >= _room.goalResource && player2.rodEnergyGreen >= _room.goalResource)
                                            {
                                                reason = GameEndReason.EnemyResFull;
                                                reasonValue = _room.goalResource;
                                                break;
                                            }
                                        }
                                    }
                                }

                                result = Result.Lose;
                            }//lose
                        }

                        //check profile...
                        if (!_room.singlePlayer)
                        {
                            // get userinfo with userid
                            playerInfo.userDataBase.Gold += addGold;

                            //check missions, level, ink, joker...
                            if (result == Result.Win)
                            {
                                //level up
                                if (player.level < 30 && playerInfo.userDataBase.Exp >= upLevelExp)
                                {
                                    playerInfo.userDataBase.Exp = 0;
                                    int lastLevel = playerInfo.userDataBase.Level;
                                    playerInfo.userDataBase.Level++;
                                    levelUp = true;
                                    player.level = playerInfo.userDataBase.Level;

                                    playerInfo.CheckAchieveValue(GameType.Unknown, AchieveType.Level, playerInfo.userDataBase.Level);
                                    playerInfo.CheckMissionValue(GameType.Unknown, AchieveType.Level, playerInfo.userDataBase.Level);

                                    //give one more ink
                                    playerInfo.userDataBase.Ink++;

                                    playerInfo.SetJokerLine(LogItemType.LevelUp);
                                    //check joker achieve &　mission (Joker)

                                    if (GameServer._isCloud == true)
                                    {
                                        //item log
                                        GlobalManager.Instance.LogItemInsert(playerInfo.serverSession, playerInfo.userID, playerInfo.userDataBase.nickName, (int)ElementType.Ink, ElementType.Ink.ToString(), 1, LogItemType.LevelDown);

                                        //level log
                                        GlobalManager.Instance.LogLevelInsert(playerInfo.serverSession, playerInfo.userID, playerInfo.userDataBase.nickName, lastLevel, playerInfo.userDataBase.Level);
                                    }
                                }
                                //else { isLevelUp = false; }

                                //update userProfile..
                                playerInfo.userDataBase.userProfile.winCount++;
                                playerInfo.userDataBase.userProfile.conWinCount++;
                                switch (_room.gameType)
                                {
                                    case GameType.Armdree:
                                    playerInfo.userDataBase.userProfile.ArConWinCount++;
                                    //check achievement..
                                    playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.ArConWinCount);
                                    break;
                                    case GameType.Cheshire:
                                    playerInfo.userDataBase.userProfile.ChConWinCount++;
                                    //check achievement..
                                    playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.ChConWinCount);
                                    break;
                                    case GameType.Colmaret:
                                    playerInfo.userDataBase.userProfile.CoConWinCount++;
                                    //check achievement..
                                    playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.CoConWinCount);
                                    break;
                                    case GameType.Joha:
                                    playerInfo.userDataBase.userProfile.JoConWinCount++;
                                    //check achievement..
                                    playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.JoConWinCount);
                                    break;
                                    case GameType.Krazan:
                                    playerInfo.userDataBase.userProfile.KrConWinCount++;
                                    //check achievement..
                                    playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.KrConWinCount);
                                    break;
                                    case GameType.Tarassen:
                                    playerInfo.userDataBase.userProfile.TaConWinCount++;
                                    //check achievement..
                                    playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.TaConWinCount);
                                    break;
                                    case GameType.Yanan:
                                    playerInfo.userDataBase.userProfile.YaConWinCount++;
                                    //check achievement..
                                    playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.YaConWinCount);
                                    break;
                                    case GameType.Zatoo:
                                    playerInfo.userDataBase.userProfile.ZaConWinCount++;
                                    //check achievement..
                                    playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.ZaConWinCount);
                                    break;
                                    default:break;
                                }
                                //check achievement..
                                playerInfo.CheckAchieveValue(GameType.Unknown, AchieveType.Win, 1);
                                playerInfo.CheckAchieveValue(_room.gameType, AchieveType.Win, 1);
                                playerInfo.CheckAchieveValue(GameType.Unknown, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.conWinCount);

                                //check mission..
                                //special gameType
                                playerInfo.CheckMissionValue(_room.gameType, AchieveType.Win, 1);

                                if (playerInfo.userDataBase.userProfile.chanelIdx != 0)
                                {
                                    //if(_room.unNormal == false)
                                    //{
                                    //playerInfo.SetRankPoint(rankPoint + gameTime.Minutes - 10 + winRate);
                                    //}
                                    //else
                                    //{
                                        //    playerInfo.SetRankPoint((rankPoint + gameTime.Minutes - 10 + winRate) * 0.8f);
                                    //}

                                    addRankPoint = RewardManager.Instance.GetRankPoint(_room.gameType, _room.playMode);
                                    playerInfo.SetRankPoint(addRankPoint);
                                    playerInfo.userDataBase.userProfile.rankWin++;
                                }
                            }
                            else if (result == Result.Lose)
                            {
                                //level down
                                if (player.level > 1 && playerInfo.userDataBase.Exp <= downLevelExp)
                                {
                                    playerInfo.userDataBase.Exp = 0;
                                    int lastLevel = playerInfo.userDataBase.Level;
                                    playerInfo.userDataBase.Level--;
                                    levelDown = true;
                                    player.level = playerInfo.userDataBase.Level;

                                    playerInfo.SetJokerLine(LogItemType.LevelDown);

                                    //confiscate one ink
                                    //check joker...
                                    if (playerInfo.userDataBase.Ink > 0) playerInfo.userDataBase.Ink--;
                                    playerInfo.CheckUserJoker();
                                    playerInfo.CheckUserPet();
                                    playerInfo.CheckUserBadge();

                                    if (GameServer._isCloud == true)
                                    {
                                        //item log
                                        GlobalManager.Instance.LogItemInsert(playerInfo.serverSession, playerInfo.userID, playerInfo.userDataBase.nickName, (int)ElementType.Ink, ElementType.Ink.ToString(), -1, LogItemType.LevelDown);

                                        //level log
                                        GlobalManager.Instance.LogLevelInsert(playerInfo.serverSession, playerInfo.userID, playerInfo.userDataBase.nickName, lastLevel, playerInfo.userDataBase.Level);
                                    }

                                    //...
                                }
                                //else
                                //{
                                //    if (playerInfo.userDataBase.Exp < downLevelExp) playerInfo.userDataBase.Exp = downLevelExp;
                                //}

                                //update userProfile..
                                playerInfo.userDataBase.userProfile.loseCount++;
                                playerInfo.userDataBase.userProfile.conWinCount = 0;
                                //change mission value room.gametype not now, later maybe...

                                //check achievement..
                                switch (_room.gameType)
                                {
                                    case GameType.Armdree:
                                        playerInfo.userDataBase.userProfile.ArConWinCount = 0;
                                        //check achievement..
                                        playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.ArConWinCount);
                                        break;
                                    case GameType.Cheshire:
                                        playerInfo.userDataBase.userProfile.ChConWinCount = 0;
                                        //check achievement..
                                        playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.ChConWinCount);
                                        break;
                                    case GameType.Colmaret:
                                        playerInfo.userDataBase.userProfile.CoConWinCount = 0;
                                        //check achievement..
                                        playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.CoConWinCount);
                                        break;
                                    case GameType.Joha:
                                        playerInfo.userDataBase.userProfile.JoConWinCount = 0;
                                        //check achievement..
                                        playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.JoConWinCount);
                                        break;
                                    case GameType.Krazan:
                                        playerInfo.userDataBase.userProfile.KrConWinCount = 0;
                                        //check achievement..
                                        playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.KrConWinCount);
                                        break;
                                    case GameType.Tarassen:
                                        playerInfo.userDataBase.userProfile.TaConWinCount = 0;
                                        //check achievement..
                                        playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.TaConWinCount);
                                        break;
                                    case GameType.Yanan:
                                        playerInfo.userDataBase.userProfile.YaConWinCount = 0;
                                        //check achievement..
                                        playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.YaConWinCount);
                                        break;
                                    case GameType.Zatoo:
                                        playerInfo.userDataBase.userProfile.ZaConWinCount = 0;
                                        //check achievement..
                                        playerInfo.CheckAchieveValue(_room.gameType, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.ZaConWinCount);
                                        break;
                                    default: break;
                                }

                                playerInfo.CheckAchieveValue(GameType.Unknown, AchieveType.Lose, 1);
                                playerInfo.CheckAchieveValue(_room.gameType, AchieveType.Lose, 1);
                                playerInfo.CheckAchieveValue(GameType.Unknown, AchieveType.ConsecutiveWin, playerInfo.userDataBase.userProfile.conWinCount);

                                //check mission..
                                playerInfo.CheckMissionValue(_room.gameType, AchieveType.Lose, 1);

                                if (playerInfo.userDataBase.userProfile.chanelIdx != 0)
                                {
                                    //playerInfo.SetRankPoint((rankPoint + gameTime.Minutes - 10 + winRate) * 0.1f);
                                    playerInfo.userDataBase.userProfile.rankLose++;
                                }
                            }
                            else
                            {
                                /*draw...*/
                                playerInfo.userDataBase.userProfile.drawCount++;
                                if (playerInfo.userDataBase.userProfile.chanelIdx != 0)
                                {
                                    //playerInfo.SetRankPoint((rankPoint + gameTime.Minutes - 10 + winRate) * 0.1f);
                                    playerInfo.userDataBase.userProfile.rankDraw++;
                                }
                            }

                            SetPlayFriend(playerInfo.session, result, out playFriendList);
                        }

                        // make packet
                        SCNotiGameResult pck = new SCNotiGameResult();
                        pck.gameResult = new GameResult(PacketTypeList.SCNotiGameResult.ToString(), playerInfo.userDataBase.Exp, downLevelExp, upLevelExp, addRankPoint, addGold, addRuby, addStake, levelUp, levelDown, itemInfo, /*isLevelUp*/playerInfo.userDataBase.Level, gameTimeStr, adOK, reason, reasonValue, result, playFriendList);
                        List<string> userSessionList = new List<string>();
                        userSessionList.Add(player.session);
                        if (userSessionList.Count != 0/* && userInfo.sceneType == SceneType.Room*/)
                        {
                            // make command
                            Command cmd = new Command();
                            cmd.pck = pck;
                            cmd.receiver = CommandTag.Client;
                            cmd.sender = CommandTag.Rule;
                            cmd.executeTime = 100;
                            cmd.sendUserSession = userSessionList;
                            cmd.sendType = CommandSendType.Single;   // send each other CommandSendType.Single
                                                                     // send to parent
                            SendToParent(cmd);

                            // save userinfo
                            //save game result: level(exclude), exp, rankPoint(exclude), items..
                            playerInfo.SaveUserInfo();
                        }

                        if (_room.singlePlayer) playerInfo.roomID = null;
                        //CheckFriend();
                        if (GameServer._isCloud == true)
                        {
                            //play log
                            GlobalManager.Instance.LogPlayInsert(playerInfo.serverSession, playerInfo.userID, playerInfo.userDataBase.nickName, gameType, playMode, playTime, stake, disClose, isLock, LogPlayType.End, gameTimeStr, result.ToString());

                            //log profile
                            GlobalManager.Instance.LogProfileInsert(playerInfo.serverSession, playerInfo.userID, playerInfo.userDataBase.nickName, playerInfo.userDataBase.userProfile.winCount, playerInfo.userDataBase.userProfile.loseCount, playerInfo.userDataBase.userProfile.drawCount, playerInfo.userDataBase.userProfile.rankWin, playerInfo.userDataBase.userProfile.rankLose, playerInfo.userDataBase.userProfile.rankDraw);
                        }
                    //}
                }
            }

            if (!GameServer._enable)
            {
                if (_playerManager != null && _playerManager.Count > 0)
                for (int i = _playerManager.Count-1; i >= 0; i--)
                {
                    playerInfo = GameServer.GetUserInfo(_playerManager[i].session);
                    if (playerInfo != null)
                    {
                        UserType userType = UserType.User;
                        GlobalManager.Instance.GetUserType(playerInfo.userDataBase.account, out userType);
                        if (userType == UserType.User)
                        {
                            PacketsHandlerServer.SendSCNotiGotoScene(playerInfo.serverSession, SceneType.Home);
                            PacketsHandlerInner.SMReqRemoveRedis(playerInfo.serverSession, playerInfo.userID, playerInfo.session);
                        }
                    }
                }

                return;
            }

            //remove players that not logout or exist before game end...
            RemoveEarlyOutPlayers();

            RemovePausePlayers();

           
        }

        public void SetPlayFriend(string userSession, Result result, out List<PlayFriend> playFriendList)
        {
            _playerManager.AddRecommendFriends();

            playFriendList = new List<PlayFriend>();
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo != null)
            {
                UserInfo playerFriendInfo = null;
                foreach (var playFriend in _playerManager)
                {
                    playerFriendInfo = GameServer.GetUserInfo(playFriend.session);
                    if (playerFriendInfo != null)
                    {
                        if (playerFriendInfo.userID != userInfo.userID)
                        {
                            if (userInfo._recommendFriends != null)
                            {
                                //foreach (var recommandFriend in userInfo._recommendFriends)
                                //{
                                //    if (playerFriendInfo.userID == recommandFriend.friendID)
                                //    {
                                //        playFriendList.Add(new PlayFriend(playFriend.faceIndex, playFriend.playerName));
                                //        break;
                                //    }
                                //}
                                for (int i = 0; i < userInfo._recommendFriends.Count; i++)
                                {
                                    if (playerFriendInfo.userID == userInfo._recommendFriends[i].friendID)
                                    {
                                        playFriendList.Add(new PlayFriend(i + 1, playFriend.faceIndex, playFriend.playerName, playFriend.level));
                                        break;
                                    }
                                    
                                    //friendList.Add(new Friend(i + 1, _recommendFriends[i].friendNick, _recommendFriends[i].faceIdx, _recommendFriends[i].level, _recommendFriends[i].friendLastLoginDate, _recommendFriends[i].friendType, false, _recommendFriends[i].enableGetItem));
                                }
                            }

                            if (userInfo._myFriends != null)
                            {
                                foreach (var myFriend in userInfo._myFriends)
                                {
                                    if (playerFriendInfo.userID == myFriend.friendID)
                                    {
                                        PacketsHandlerInner.SDReqUpdateFriendFight(userInfo.serverSession, userInfo. userID, myFriend.friendID, result);
                                    }
                                }
                            }
                            
                        }
                    }

                }
            }
            
        }

        private void RemovePausePlayers()
        {
            if (_room._pausePlayerSessionList != null)
            {
                UserInfo playerInfo = null;

                for (int i = _room._pausePlayerSessionList.Count - 1; i >= 0; i--)
                {
                    playerInfo = GameServer.GetUserInfo(_room._pausePlayerSessionList[i]);
                    //if offline
                    if (playerInfo == null || playerInfo.userServerState == UserServerStatus.DisConnect)
                    {
                        if (_playerManager != null && _playerManager.Count > 0)
                        {
                            foreach (var player in _playerManager)
                            {
                                if (player.session == _room._pausePlayerSessionList[i])
                                {
                                    _playerManager.Remove(player);
                                    _room._pausePlayerSessionList.RemoveAt(i);

                                    break;
                                }
                            }
                        }
                    }

                }

            }
        }

        private void RemoveEarlyOutPlayers()
        {
            if (_room._earlyOutPlayerSessionList != null)
            {
                UserInfo playerInfo = null;
                foreach (var playerSession in _room._earlyOutPlayerSessionList)
                {
                    if (_playerManager != null && _playerManager.Count > 0)
                    {
                        foreach (var player in _playerManager)
                        {
                            if (player.session == playerSession)
                            {
                                _playerManager.Remove(player);
                                break;
                            }
                        }
                    }

                    playerInfo = GameServer.GetUserInfo(playerSession);
                    //if player is online...
                    if (playerInfo != null && playerInfo.userServerState != UserServerStatus.DisConnect)
                    {
                        playerInfo.roomID = null;
                    }

                }

                _room._earlyOutPlayerSessionList = null;
            }
        }

        internal void SendSCNotiUseCard(PlayCardInfo playCardInfo/*, int playerNumber_, int targetNumber_, int cardIndex_, CardUseType cardUseType_*/)
        {
            // make packet
            SCNotiUseCard pck = new SCNotiUseCard();
            pck.playCardInfo = playCardInfo;
            //pck.playerNumber = playerNumber_;       
            //pck.targetNumber = targetNumber_;      
            //pck.cardIndex = cardIndex_;
            //pck.effectType = EffectType.None;

            // make command
            Command cmd = new Command();
            cmd.pck = pck;
            cmd.receiver = CommandTag.Client;
            cmd.sender = CommandTag.Rule;
            cmd.executeTime = 100;
            cmd.sendUserSession = _playerManager.GetUserSessionList(_room);
            cmd.sendType = CommandSendType.Players;   // send each other CommandSendType.Single

            // send to parent
            SendToParent(cmd);
        }

        //internal void SendSCNotiPlayerStatus(Player player_, int effectIdx_, int globalEffectIdx_)
        internal void SendSCNotiPlayerStatus(Player player_, int effectIdx_, int globalEffectIdx_)
        {
            //UserInfo userInfo = GameServer.GetUserInfo(player_.session);
            //int addHP = 0;
            //int addSP = 0;

            //if (userInfo != null)
            //{
            //    addHP = userInfo.userDataBase.addHp;
            //    addSP = userInfo.userDataBase.addSp;
            //}
                // make packet
                SCNotiPlayerStatus pck = new SCNotiPlayerStatus();
                pck.playerNumber = player_.playerNumber;
                pck.HP = player_.HP /*+ addHP*/;//addHp = 0 or 1
                pck.SP = player_.SP /*+ addSP*/;//addSp = 0 or 1
                pck.MD = player_.moonDustRed;
                pck.MA = player_.manaBlue;
                pck.RE = player_.rodEnergyGreen;
                pck.MS = player_.moonStoneRedPlus;
                pck.ZS = player_.zenStoneBluePlus;
                pck.MC = player_.moonCrystalGreenPlus;
                pck.effectIdx = effectIdx_;
                pck.globalEffectIdx = globalEffectIdx_;

                // make command
                Command cmd = new Command();
                cmd.pck = pck;
                cmd.receiver = CommandTag.Client;
                cmd.sender = CommandTag.Rule;
                cmd.executeTime = 100;
                cmd.sendUserSession = _playerManager.GetUserSessionList(_room);
                cmd.sendType = CommandSendType.Players;   // send each other CommandSendType.Single

                // send to parent
                SendToParent(cmd);
        }

        internal void SendSCNotiPlayerItemInfo(/*string session_,*/ PlayerItemInfo playerItemInfo_)
        {
            //UserInfo userInfo = GameServer.GetUserInfo(session_);//null;
            //if (userInfo != null)
            {
                // make packet
                SCNotiPlayerItemInfo pck = new SCNotiPlayerItemInfo();
                pck.playerItemInfo = playerItemInfo_;

                // make command
                Command cmd = new Command();
                cmd.pck = pck;
                cmd.receiver = CommandTag.Client;
                cmd.sender = CommandTag.Rule;
                cmd.executeTime = 100;
                cmd.sendUserSession = _playerManager.GetUserSessionList(_room);
                cmd.sendType = CommandSendType.Players;   // send each other CommandSendType.Single

                // send to parent
                SendToParent(cmd);
            }
        }

        internal void SendSCNotiDeckToHand(CardList pckCardList, string playerSession, CommandSendType commandSendType)
        {
            // make packet
            SCNotiDeckToHand pck = new SCNotiDeckToHand();
            pck.cardList = pckCardList;

            // make command
            Command cmd = new Command();
            cmd.pck = pck;
            cmd.receiver = CommandTag.Client;
            cmd.sender = CommandTag.Rule;
            cmd.executeTime = 100;
            cmd.sendType = commandSendType;   // send each other CommandSendType.Single

            UserInfo playerInfo = GameServer.GetUserInfo(playerSession);
            if (playerInfo != null)
            {
                cmd.sendUserSession = new List<string>();
                cmd.sendUserSession.Add(playerSession);
            }
            
            // send to parent
            SendToParent(cmd);
        }

        internal void SendCurrentPlayerHandCardFromDeck( int handPosition_ )
        {
            // 틀렸음.... 한개만 보내야됨...
            // currentplayer
            CardList cardlist = null;
            
            // if animaition type so  showAnimationType = 1
            if (_roomCardManager.GetCardListOne(_playerManager.currentPlayer, ShowAnimationType.AnimationFromDeck , handPosition_, out cardlist))
            {
                SendSCNotiDeckToHand(cardlist, _playerManager.currentPlayer.session, CommandSendType.Single);
            }
        }

        internal void SendSCNotiDeckToHandEachOther()
        {
            foreach( Player player in _playerManager)
            {
                CardList cardList = null;
                if( _roomCardManager.GetCardList( player.playerNumber, player.session, ShowAnimationType.AnimationFromDeck, out cardList))
                {
                    SendSCNotiDeckToHand(cardList, player.session, CommandSendType.Single);
                }
            }
        }

        internal void SendSCNotiDeckInfo(DeckList deckList)
        {
            // make packet
            SCNotiDeckInfo pck = new SCNotiDeckInfo();
            pck.deckList = deckList;

            // make command
            Command cmd = new Command();
            cmd.pck = pck;
            cmd.receiver = CommandTag.Client;
            cmd.sender = CommandTag.Rule;
            cmd.executeTime = 100;
            cmd.sendUserSession = _playerManager.GetUserSessionList(_room);
            cmd.sendType = CommandSendType.Players; 
          
            // send to parent
            SendToParent(cmd);
        }
    }
}
