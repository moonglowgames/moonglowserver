﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;

namespace MoonGlow.Room
{
    public class RoomManager: ConcurrentDictionary<string, Room>
    {
        public static RoomManager _instance = null;
        private int _currentTickCount = 0;
        private const int tickIntervalRun = 100; // 100 msec 

        public static RoomManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RoomManager();
                }

                return _instance;
            }
        }

        public void QuickStart(string serverSession, string userSession, GameType gameType_ , PlayMode playMode_ , PlayTime playTime_ , int stake_, bool disClose_, bool isLock_ )
        {
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo == null)
            {
                GlobalManager.Instance.NullUserInfo(serverSession);
                return;
            }
            if (userInfo.userDataBase.Stamina < 10)
            {
                PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.LackStamina);
                return; //lack stamina
            }
            
            // 1. find room
            // 2. if not find then create room
            // 3. join to room with myinfo
            // 4. send packet to client: SCNotiEnterRoom

            Room room = null;
            //bool isNewRoom = false;
            if (FindQuickStartRoom(out room))
            { /*find*/
                //isNewRoom = false;
                if(userInfo.userDataBase.Gold < room.stake)
                {
                    PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.LackGold);
                    return; //lack gold
                }
                //ask creator and return;
                //ask creator
                room.AskCreator(serverSession, userInfo.session);
                return;
            }
            else
            {
                if (userInfo.userDataBase.Gold < stake_)
                {
                    PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.LackGold);
                    return; //lack gold
                }

                // create
                if (CreateRoom("", gameType_, playMode_, playTime_, stake_, disClose_, isLock_, out room)) 
                {
                    // sucess
                    //isNewRoom = true;
                }
                else { /* fail*/ }
            }

            if (room != null)
            {
                if (room.Join(userInfo.session, true))
                {
                    //PacketsHandlerServer.SendSCNotiEnterRoom(serverSession, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, room.goalHP, room.goalResource/*conditionInfo.goalHP, conditionInfo.goalResource*/);

                    //create room and send roomlist directly
                    //send room list
                    room._playStateManager.SetState(PlayState.Ready);
                    RoomList roomList = RoomManager.Instance.GetAllRoomList(userSession);
                    if (roomList != null) PacketsHandlerServer.SendSCNotiRoomList(serverSession, roomList);

                    if (GameServer._isCloud == true)
                    {
                        //play log
                        //if (isNewRoom == false)
                        //{
                            GlobalManager.Instance.LogPlayInsert(serverSession, userInfo.userID, userInfo.userDataBase.nickName, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, LogPlayType.Join, string.Empty, string.Empty);
                        //}
                        //else
                        //{
                            GlobalManager.Instance.LogPlayInsert(serverSession, userInfo.userID, userInfo.userDataBase.nickName, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, LogPlayType.Create, string.Empty, string.Empty);
                        //}
                    }
            }
            else
            {
                // fail to join, go to Home
                userInfo.SetHome();
                PacketsHandlerServer.SendSCNotiGotoScene(serverSession, SceneType.Home);
            }
        }
    }

        public bool CreateRoom(string roomName, GameType gameType, PlayMode playMode, PlayTime playTime, int stake, bool disClose, bool isLock, out Room newRoom)
        {
            bool retvalue = false;
            newRoom = null;

            Room tempRoom = new Room(ref roomName, gameType, playMode, playTime, stake, disClose,isLock);
            if (this.TryAdd(roomName, tempRoom))
            {
                newRoom = tempRoom;
                retvalue = true;
            }
            else
            { 
                // fail    
            }

            return retvalue;
        }

        //some problems need be solved later...
        // 1. try to find player is full require is one , more useful
        // 2. need empty room list for fast find
        public bool FindQuickStartRoom(out Room findRoom)
        {
            bool retvalue = false;
            Room room = null;
            findRoom = null;

            foreach (var roomPair in this)
            {
                room = roomPair.Value;
                if (!room.WantRemoveThisRoom ) // 
                {
                    if (room.isRequirePlayer)
                    {
                        findRoom = room;
                        if(!room.isRequested) retvalue = true;
                        break;
                    }
                }
            }
            return retvalue;
        }

        //public bool FindRoom(GameType gameType, PlayMode playMode, PlayTime playTime, bool disClose, bool isLock,out Room findRoom )
        //{
        //    bool retvalue = false;
        //    Room room = null;
        //    findRoom = null;

        //    foreach (var roomPair in this)
        //    { 
        //        room = roomPair.Value;
        //        if (!room.WantRemoveThisRoom)
        //        {
        //            if (room.gameType == gameType && room.playMode == playMode && room.playTime == playTime && room.disClose == disClose && room.isLock == isLock)
        //            {
        //                findRoom = room;
        //                retvalue = true;
        //                break;
        //            }
        //        }
        //    }
        //    return retvalue;
        //}

        public void Run(int timeCount)
        {
            List<string> wantRemoveRoomList = null;
            foreach (var room in this)
            {
                if (room.Value.WantRemoveThisRoom)
                {
                    if(wantRemoveRoomList ==null) wantRemoveRoomList = new List<string>();
                    wantRemoveRoomList.Add(room.Value.roomID);
                }
                else
                {
                    room.Value.Run(timeCount);
                }
            }

            // remove room
            if (wantRemoveRoomList != null)
            {
                foreach (var roomid in wantRemoveRoomList)
                { 
                    Room removedroom = null;
                    if (this.TryRemove(roomid, out removedroom))
                    {
                        // ok room is deleted...
                    }
                    else
                    { 
                        // Opps.. room is not deleted...
                    }
                }
            }
        }

        internal RoomList GetAllRoomList(string userSession)
        {
            RetrieveRoomList();

            List<RoomInfo> roomInfoList = new List<RoomInfo>();
            bool isMine = false;
            List<RoomUserInfo> userInfoList = null;
            foreach (var roompair in this)
            { 
                Room room  = roompair.Value;
                if (room.EnableRoomList)
                {
                    userInfoList = room.GetUserInfoList(userSession, out isMine);
                    roomInfoList.Add(new RoomInfo(room.roomID, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, userInfoList, isMine, room.isPlaying));
                }
            }

            RoomList roomList = new RoomList(PacketTypeList.SCNotiRoomList.ToString(), roomInfoList);
            
            return roomList;
        }

        internal RoomList GetReqRoomList(ReqRoomList reqRoomList_)
        {
            List<RoomInfo> roomInfoList = new List<RoomInfo>();

            Room room = null;
            RetrieveRoomList();

            bool isMine = false;
            foreach (var roompair in this)
            {
                room = roompair.Value;
                if (room.GetPlayers().Count != 0)
                {
                    if (room.EnableRoomList)
                    {
                        List<RoomUserInfo> userInfoList = room.GetUserInfoList(reqRoomList_.session, out isMine);
                        // roomInfoList.Add(new RoomInfo(room.roomID, room.gameType, room.playMode, room.playTime, room.disClose, userInfoList));

                        if (reqRoomList_.gameTypeList.Count == 0)
                        {
                            reqRoomList_.gameTypeList.Add(GameType.Armdree);
                            reqRoomList_.gameTypeList.Add(GameType.Cheshire);
                            reqRoomList_.gameTypeList.Add(GameType.Colmaret);
                            reqRoomList_.gameTypeList.Add(GameType.Joha);
                            reqRoomList_.gameTypeList.Add(GameType.Krazan);
                            reqRoomList_.gameTypeList.Add(GameType.Tarassen);
                            reqRoomList_.gameTypeList.Add(GameType.Yanan);
                            reqRoomList_.gameTypeList.Add(GameType.Zatoo);
                        }
                        if (reqRoomList_.playModeList.Count == 0)
                        {
                            reqRoomList_.playModeList.Add(PlayMode.Single2);
                            reqRoomList_.playModeList.Add(PlayMode.Single3);
                            reqRoomList_.playModeList.Add(PlayMode.Single4);
                            reqRoomList_.playModeList.Add(PlayMode.Team);
                        }
                        if (reqRoomList_.playTimeList.Count == 0)
                        {
                            reqRoomList_.playTimeList.Add(PlayTime.Sec_30);
                            reqRoomList_.playTimeList.Add(PlayTime.Sec_60);

                        }
                        if (reqRoomList_.disCloseList.Count == 0)
                        {
                            reqRoomList_.disCloseList.Add(false);
                            reqRoomList_.disCloseList.Add(true);
                        }
                        foreach (GameType gameType in reqRoomList_.gameTypeList)
                        {
                            if (room.gameType == gameType)
                            {
                                foreach (PlayMode playMode in reqRoomList_.playModeList)
                                {
                                    if (room.playMode == playMode)
                                    {
                                        foreach (PlayTime playTime in reqRoomList_.playTimeList)
                                        {
                                            if (room.playTime == playTime)
                                            {
                                                foreach (bool disClose in reqRoomList_.disCloseList)
                                                {
                                                    if (room.disClose == disClose)
                                                    {
                                                        if (roomInfoList.Count <= reqRoomList_.count)
                                                        {
                                                            roomInfoList.Add(new RoomInfo(room.roomID, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, userInfoList, isMine, room.isPlaying));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
            }

            RoomList roomList = new RoomList(PacketTypeList.SCNotiRoomList.ToString(), roomInfoList);
            return roomList;
        }

        private void RetrieveRoomList()
        {
            Room room = null;
            for (int i = this.Count - 1; i >= 0; i--)
            {
                room = this.ElementAt(i).Value;

                if (room.EnableRoomList)
                {
                    UserInfo playerInfo = null;
                    List<string> removePlayerSessionList = null;
                    foreach (var player in room.GetPlayers())
                    {
                        playerInfo = GameServer.GetUserInfo(player.session);
                        if (playerInfo != null)
                        {
                            if (string.IsNullOrEmpty(playerInfo.roomID))
                            {
                                if(removePlayerSessionList ==null) removePlayerSessionList = new List<string>();
                                removePlayerSessionList.Add(player.session);
                            }
                        }
                        else
                        {
                            if (player.playerName.ToLower() != "eve")
                            {
                                if (removePlayerSessionList == null) removePlayerSessionList = new List<string>();
                                removePlayerSessionList.Add(player.session);
                            }
                            
                        }
                    }
                    if (removePlayerSessionList != null)
                    {
                        foreach (var removePlayerSession in removePlayerSessionList)
                        {
                            room.ExitPlayer(removePlayerSession);
                        }
                        //since some players exit, so resend playerlist
                        room.SendPlayerList();
                        room.SendPlayerStatus();
                        room.SendPlayerItemInfo();
                    }

                    if (room.GetPlayers().Count == 1)
                    {
                        foreach (var player in room.GetPlayers())
                        {
                            //if player is not creator then remove
                            if(!room.IsCreator(player.session))
                            {
                                room.ExitPlayer(player.session);
                                break;
                            }

                            playerInfo = GameServer.GetUserInfo(player.session);
                            if (playerInfo.userServerState == UserServerStatus.DisConnect)
                            {
                                room.ExitPlayer(player.session);
                                break;
                            }
                        }
                    }

                    if (room.GetPlayers().Count == 0)
                    {
                        //remove room
                        RoomManager.Instance.TryRemove(room.roomID, out room);
                    }
                }
            }

        }

        internal void JoinRoom(string serverSession, string userSession, string roomName)
        {
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo == null)
            {
                GlobalManager.Instance.NullUserInfo(serverSession);
                return;
            }
            if (userInfo.userDataBase.Stamina < 10)
            {
                PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.LackStamina);
                return; //lack stamina
            }

            Room room = null;
            Room findRoom = null;
            //bool isNormalRoom = false;
            foreach (var roomPair in RoomManager.Instance)
            {
                room = roomPair.Value;
                //room name or room id???
                if (!room.WantRemoveThisRoom)
                {
                    if (room.roomID == roomName)
                    {
                        if (room.isRequirePlayer)
                        {
                            //if (room.roomID == roomName)
                            //{
                            //break;
                            //}
                            findRoom = room;
                            //isNormalRoom = true;
                        }
                        else
                        {
                            //no need player
                            PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.FullRoom);

                            //SendSCNotiRoomList
                            RoomList roomList = RoomManager.Instance.GetAllRoomList(userSession);
                            if (roomList != null) PacketsHandlerServer.SendSCNotiRoomList(serverSession, roomList);
                            return;
                        }
                        break;
                    }
                    
                }
            }
            if (findRoom != null)
            {
                if (findRoom.isRequested)
                {
                    PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.JoinReject);

                    RoomList roomList = RoomManager.Instance.GetAllRoomList(userSession);
                    if (roomList != null) PacketsHandlerServer.SendSCNotiRoomList(serverSession, roomList);

                    return;
                }
                if (userInfo.userDataBase.Gold < findRoom.stake)
                {
                    PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.LackGold);
                    return; //lack gold
                }

                //ask creator
                findRoom.AskCreator(serverSession, userInfo.session);

                //if (findRoom.Join(userInfo.session, false))
                //{
                //    // send packet
                //    //MoonGlow.Data.ConditionInfo conditionInfo = MoonGlow.Data.ConditionManager.Instance.Get(((int)room.gameType - 1));
                //    //if (conditionInfo != null)
                //    //{
                //        PacketsHandlerServer.SendSCNotiEnterRoom(serverSession, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, room.goalHP, room.goalResource/*conditionInfo.goalHP, conditionInfo.goalResource*/);

                //        if (GameServer._isCloud == true)
                //        {
                //            //play log
                //            GlobalManager.Instance.LogPlayInsert(serverSession, userInfo.userID, userInfo.userDataBase.nickName, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, LogPlayType.Join, string.Empty, string.Empty);
                //        }
                //    //}
                //}
                //else
                //{
                //    // fail to join, go to Home
                //    userInfo.SetHome();
                //    PacketsHandlerServer.SendSCNotiGotoScene(serverSession, SceneType.Home);
                //}
            }
            else
            {
                //no room
                PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.NoRoom);

                //SendSCNotiRoomList
                RoomList roomList = RoomManager.Instance.GetAllRoomList(userSession);
                if (roomList != null) PacketsHandlerServer.SendSCNotiRoomList(serverSession, roomList); 
            }
        }

        private int savenowtick = 0;
        internal bool isRun(int nowtick_)
        {
            bool retvalue = false;
            bool wantchange = false;

            // check 24.9 day rotation of tickcount...
            if (savenowtick > nowtick_) wantchange = true;
            savenowtick = nowtick_;

            // check increase count
            if ( wantchange || (_currentTickCount + tickIntervalRun) < nowtick_)
            {
                _currentTickCount = nowtick_;
                retvalue = true;
                //Console.Write("\r!!!Room run tick: " + retvalue.ToString() + ":" + nowtick_.ToString()+"\n");
            }

            //Console.Write("\r!!!Room run tick: "+ retvalue.ToString()+":" + nowtick_.ToString());
            return retvalue;
        }
    }
}
