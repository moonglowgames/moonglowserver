﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoonGlow.Data;

namespace MoonGlow.Room
{
   public  class Player
    {
        public int goalHP;
        public int goalResource;
        
        public string playerName;
        public string session;
        public int playerNumber;
        public int faceIndex;
        public int HP;
        public int SP;
        public int level;
        public int moonDustRed;
        public int manaBlue;
        public int rodEnergyGreen;
        public int moonStoneRedPlus;
        public int zenStoneBluePlus;
        public int moonCrystalGreenPlus;
        public bool playAgain;
        public bool isUser = true;
        public bool IsUseCard = false;
        public bool IsRecieveCard = false;
        public bool IsRoomEnterReady = false;

        private RoomCardManager _roomCardManager;
        internal bool forceLose = false;
        internal bool noReward = false;
        internal int autoPlayCount = 0;//AI OR DISCONNECT
        internal int autoDiscardCount = 0;
        public bool addSP = false;
        public bool addHP = false;
        internal bool setAI = false;

        internal int tryHP = 0;
        internal int trySP = 0;
        internal int tryMoonDustRed = 0;
        internal int tryManaBlue = 0;
        internal int tryRodEnergyGreen = 0;
        internal int trymoonStoneRedPlus = 0;
        internal int tryZenStoneBluePlus = 0;
        internal int tryMoonCrystalGreenPlus = 0;
        internal bool wantForceExit = false;

        public Player() { }

        internal decimal[] GetEvaluateStatus()
        {
            decimal u_hp = (decimal)HP / goalHP;
            decimal u_sp = (decimal)((SP == 0) ? 0 : 1 / (1 + 20 / (SP * SP)));
            decimal u_respower = (decimal)((moonStoneRedPlus == 0 || zenStoneBluePlus == 0 || moonCrystalGreenPlus == 0) ?
                                          0 : ((1 / (1 + 10 / (moonStoneRedPlus * moonStoneRedPlus)))
                                             * (1 / (1 + 10 / (zenStoneBluePlus * zenStoneBluePlus)))
                                             * (1 / (1 + 10 / (moonCrystalGreenPlus * moonCrystalGreenPlus)))));
            decimal u_res = (decimal)((moonDustRed > goalResource || manaBlue > goalResource || rodEnergyGreen > goalResource) ?
                            1 : ((moonDustRed * manaBlue * rodEnergyGreen) / (goalResource * goalResource * goalResource)));

            decimal[] u = new decimal[4];
            u[0] = u_hp;
            u[1] = u_sp;
            u[2] = u_respower;
            u[3] = u_res;

            return u;
        }
        internal decimal[] GetTryEvaluateStatus()
        {
            decimal u_hp = (decimal)tryHP / goalHP;
            decimal u_sp = (decimal) ((trySP == 0) ? 0 : 1 / (1 + 20 / (trySP * trySP)));
            decimal u_respower = (decimal)((trymoonStoneRedPlus == 0 || tryZenStoneBluePlus == 0 || tryMoonCrystalGreenPlus == 0) ? 
                                          0 : ((1 / (1 + 10 / (trymoonStoneRedPlus * trymoonStoneRedPlus)))
                                             * (1 / (1 + 10 / (tryZenStoneBluePlus * tryZenStoneBluePlus)))
                                             * (1 / (1 + 10 / (tryMoonCrystalGreenPlus * tryMoonCrystalGreenPlus)))));
            decimal u_res = (decimal)((tryMoonDustRed > goalResource || tryManaBlue > goalResource || tryRodEnergyGreen > goalResource) ?
                            1 : ((tryMoonDustRed * tryManaBlue * tryRodEnergyGreen) / (goalResource * goalResource * goalResource)));

            decimal[] u = new decimal[4];
            u[0] = u_hp;
            u[1] = u_sp;
            u[2] = u_respower;
            u[3] = u_res;

            return u;
        }

        public Player(RoomCardManager roomCardManager_)
        {
            _roomCardManager = roomCardManager_;
        }

        public void InitResources(GameType gameType_, int faceIndex_, string playerName_, string session_, int level_)
        {
            //;iDx, GameType, GoalHP, GoalResource, MoonDust, MoonStone, Mana, ZenStone, RodEnergy, MoonCrystal, StartHP, StartSP
            // 1,  Mercury,   50,     150,         5,         2,         5,    2,        5,         2,           20,       5

            ConditionInfo conditionInfo = ConditionManager.Instance.Get(((int)gameType_-1));
            {
                if (conditionInfo != null)
                {
                    goalHP = conditionInfo.goalHP;
                    goalResource = conditionInfo.goalResource;
                    moonDustRed = conditionInfo.moonDust;
                    moonStoneRedPlus = conditionInfo.moonStone;
                    manaBlue = conditionInfo.mana;
                    zenStoneBluePlus = conditionInfo.zenStone;
                    rodEnergyGreen = conditionInfo.rodEnergy;
                    moonCrystalGreenPlus = conditionInfo.moonCrystal;
                    HP = conditionInfo.startHP;
                    SP = conditionInfo.startSP;

                    UserInfo userInfo = GameServer.GetUserInfo(session_);//null;
                    if (userInfo != null)
                    {
                        if (userInfo.userDataBase.userProfile.addSP == 1) addSP = true;
                        if (userInfo.userDataBase.userProfile.addHP == 1) addHP = true;
                    }
                }
            }
            playerName = playerName_; // just test we need nickname later
            session = session_;
            level = level_; // just test get we need databaseinfo later
            faceIndex = faceIndex_;
        }

        public bool IsWinCondition { 
            get {
                //bool retvalue = false;
                //if ((HP >= goalHP) || (manaBlue >= goalResource && moonDustRed >= goalResource && rodEnergyGreen >= goalResource))
                //{
                //    retvalue = true;
                //}

                //return retvalue;
                return ((HP >= goalHP) || (manaBlue >= goalResource && moonDustRed >= goalResource && rodEnergyGreen >= goalResource)) ? true : false;
            } 
        }
        
       public bool IsLoseCondition 
       {
           get
           {
               //bool retvalue = false;

               //if (HP <= 0 || forceLose == true || randDiscardCount >= 5)
               //{
               //    retvalue = true;
               //}

               //return retvalue;
                return (HP <= 0 || forceLose == true || autoPlayCount >= 5 || autoDiscardCount >= 5) ? true : false;
           } 
        }


        public bool IsCardUseable(Card card_ , int targetNumber_)
       {
            bool retvalue = false;

            if (targetNumber_ == 0 || card_ == null)
            {
                retvalue = true;
            }
            else if (card_.Index == CardManager.JOKER_INDEX)
            {
                UserInfo userInfo = GameServer.GetUserInfo(session);
                if (userInfo != null)
                {
                    if (userInfo.userDataBase.userJoker.spellIdxList != null)
                    {
                        int priceMoonDustRed = 0;
                        int priceManaBlue = 0;
                        int priceRodEnergyGreen = 0;
                        foreach (var spellIdx in userInfo.userDataBase.userJoker.spellIdxList)
                        {
                            SpellInfo spellInfo = SpellManager.Instance.GetSpellInfo(spellIdx);
                            if (spellInfo != null)
                            {
                                priceMoonDustRed += spellInfo.ability.priceMD;
                                priceManaBlue += spellInfo.ability.priceM;
                                priceRodEnergyGreen += spellInfo.ability.priceRE;
                            }
                        }
                        if (moonDustRed >= priceMoonDustRed && manaBlue >= priceManaBlue && rodEnergyGreen >= priceRodEnergyGreen)
                        {
                            retvalue = true;
                        }
                    }
                }
            }
            else
            {
                CardAbility cardAbility_ = card_.GetCardAbility();
                // check card price then return
                if (moonDustRed >= cardAbility_.priceMoonDustRed &&
                        manaBlue >= cardAbility_.priceManaBlue &&
                        rodEnergyGreen >= cardAbility_.priceRodEnergyGreen)
                {
                    retvalue = true;
                }
            }

            return retvalue;

       }

       public void TargetApplyCard( CardAbility cardAbility_ , Player owner_ )
       {
           //targetEffectType_ = GetEffectId( cardAbility_ , true);

           // target
            HP  += cardAbility_.HPEnemy;
            if (HP < 0) HP = 0;
            SP  += cardAbility_.SPEnemy;
            if (SP < 0) SP = 0;
            moonDustRed += cardAbility_.moonDustRedEnemy;
            if (moonDustRed < 0) moonDustRed = 0;
            manaBlue += cardAbility_.manaBlueEnemy;
            if (manaBlue < 0) manaBlue = 0;
            rodEnergyGreen += cardAbility_.rodEnergyGreenEnemy;
            if (rodEnergyGreen < 0) rodEnergyGreen = 0;
            moonStoneRedPlus += cardAbility_.moonStoneRedplusEnemy;
            if (moonStoneRedPlus < 0) moonStoneRedPlus = 0;
            zenStoneBluePlus += cardAbility_.zenStoneBlueplusEnemy;
            if (zenStoneBluePlus < 0) zenStoneBluePlus = 0;
            moonCrystalGreenPlus += cardAbility_.moonCrystalGreenplusEnemy;
            if (moonCrystalGreenPlus < 0) moonCrystalGreenPlus = 0;

            
            if (cardAbility_.damage > 0)
            {
                int remaindamage = cardAbility_.damage;

                if (SP >= remaindamage)
                {
                    SP -= remaindamage;
                }
                else
                {
                    remaindamage -= SP;
                    SP = 0;
                    HP -= remaindamage;
                    if (HP < 0) HP = 0;
                }
            }
           

           // owner
            //ownerEffectType_ = GetEffectId(cardAbility_, false);
            owner_.HP += cardAbility_.myHP;
            if (owner_.HP < 0) owner_.HP = 0;
            owner_.SP += cardAbility_.SP;
            if (owner_.SP < 0) owner_.SP = 0;
            
            owner_.moonStoneRedPlus += cardAbility_.moonStoneRedplus;
            if (owner_.moonStoneRedPlus < 0) owner_.moonStoneRedPlus = 0;
            owner_.zenStoneBluePlus += cardAbility_.zenStoneBlueplus;
            if (owner_.zenStoneBluePlus < 0) owner_.zenStoneBluePlus = 0;
            owner_.moonCrystalGreenPlus += cardAbility_.moonCrystalGreenplus;
            if (owner_.moonCrystalGreenPlus < 0) owner_.moonCrystalGreenPlus = 0;
            
           if (cardAbility_.damageMe > 0)
            {
                int remaindamage = cardAbility_.damageMe;

                if (owner_.SP >= remaindamage)
                {
                    owner_.SP -= remaindamage;
                }
                else
                {
                    remaindamage -= owner_.SP;
                    owner_.SP = 0;
                    owner_.HP -= remaindamage;
                    if (owner_.HP < 0) owner_.HP = 0;
                }
            }
            //owner_.playAgain = ( cardAbility_.playAgain == 0 ) ? false : true;
            
           // Owner Use Card ....
           //owner_.IsUseCard = true;
       }

        public void TryTargetApplyCard(CardAbility cardAbility_, Player owner_)
        {
            //targetEffectType_ = GetEffectId( cardAbility_ , true);

            // target
            tryHP += cardAbility_.HPEnemy;
            if (tryHP < 0) tryHP = 0;
            trySP += cardAbility_.SPEnemy;
            if (trySP < 0) trySP = 0;
            tryMoonDustRed += cardAbility_.moonDustRedEnemy;
            if (tryMoonDustRed < 0) tryMoonDustRed = 0;
            tryManaBlue += cardAbility_.manaBlueEnemy;
            if (tryManaBlue < 0) tryManaBlue = 0;
            tryRodEnergyGreen += cardAbility_.rodEnergyGreenEnemy;
            if (tryRodEnergyGreen < 0) tryRodEnergyGreen = 0;
            trymoonStoneRedPlus += cardAbility_.moonStoneRedplusEnemy;
            if (trymoonStoneRedPlus < 0) trymoonStoneRedPlus = 0;
            tryZenStoneBluePlus += cardAbility_.zenStoneBlueplusEnemy;
            if (tryZenStoneBluePlus < 0) tryZenStoneBluePlus = 0;
            tryMoonCrystalGreenPlus += cardAbility_.moonCrystalGreenplusEnemy;
            if (tryMoonCrystalGreenPlus < 0) tryMoonCrystalGreenPlus = 0;


            if (cardAbility_.damage > 0)
            {
                int remaindamage = cardAbility_.damage;

                if (trySP >= remaindamage)
                {
                    trySP -= remaindamage;
                }
                else
                {
                    remaindamage -= SP;
                    trySP = 0;
                    tryHP -= remaindamage;
                    if (tryHP < 0) tryHP = 0;
                }
            }


            // owner
            //ownerEffectType_ = GetEffectId(cardAbility_, false);
            owner_.tryHP += cardAbility_.myHP;
            if (owner_.tryHP < 0) owner_.tryHP = 0;
            owner_.trySP += cardAbility_.SP;
            if (owner_.trySP < 0) owner_.trySP = 0;

            owner_.trymoonStoneRedPlus += cardAbility_.moonStoneRedplus;
            if (owner_.trymoonStoneRedPlus < 0) owner_.trymoonStoneRedPlus = 0;
            owner_.tryZenStoneBluePlus += cardAbility_.zenStoneBlueplus;
            if (owner_.tryZenStoneBluePlus < 0) owner_.tryZenStoneBluePlus = 0;
            owner_.tryMoonCrystalGreenPlus += cardAbility_.moonCrystalGreenplus;
            if (owner_.tryMoonCrystalGreenPlus < 0) owner_.tryMoonCrystalGreenPlus = 0;

            if (cardAbility_.damageMe > 0)
            {
                int remaindamage = cardAbility_.damageMe;

                if (owner_.trySP >= remaindamage)
                {
                    owner_.trySP -= remaindamage;
                }
                else
                {
                    remaindamage -= owner_.trySP;
                    owner_.trySP = 0;
                    owner_.tryHP -= remaindamage;
                    if (owner_.tryHP < 0) owner_.tryHP = 0;
                }
            }
            //owner_.playAgain = ( cardAbility_.playAgain == 0 ) ? false : true;

            // Owner Use Card ....
            //owner_.IsUseCard = true;
        }

        internal void RefillPriceResource()
       {
           rodEnergyGreen += moonCrystalGreenPlus;
           moonDustRed += moonStoneRedPlus;
           manaBlue += zenStoneBluePlus;
       }

    }
}
