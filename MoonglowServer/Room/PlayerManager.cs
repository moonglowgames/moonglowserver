﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Room
{
    public class PlayerManager:List<Player>
    {
        private int currentPlayerNum = 0 ; // [ 1. count ]
        public Player currentPlayer = null;
        public Player winnerPlayer = null;
        public bool doubleTurn = false;
        
        private RoomCardManager _roomCardManager;
        public  Player creator = null;

        public PlayerManager(RoomCardManager roomCardManager_)
        {
            _roomCardManager = roomCardManager_;
        }

        public PlayerList GetPlayerList( string sender_ )
        {
            PlayerList playerList = null;
            List<PlayerInfo> infolist = new List<PlayerInfo>();
            foreach (var player in this)
            {
                infolist.Add(new PlayerInfo(player.playerNumber, player.faceIndex, player.playerName, player.level, player.isUser,( sender_ == player.session)));
            }

            playerList = new PlayerList(PacketTypeList.SCNotiPlayerList.ToString(), infolist);

            return playerList;
        }

        internal decimal GetEvaluateFunction(bool _isTry)
        {
            decimal[] u_owner = new decimal[4];
            decimal[] u_enemy = new decimal[4];
            decimal f = 0;

            foreach (var player in this)
            {
                if (!_isTry)
                {
                    if (player == this.currentPlayer) u_owner = player.GetEvaluateStatus();
                    else u_enemy = player.GetEvaluateStatus();
                }
                else
                {
                    if (player == this.currentPlayer) u_owner = player.GetTryEvaluateStatus();
                    else u_enemy = player.GetTryEvaluateStatus();
                }
            }

            int weight_hp = 3;
            int weight_sp = 1;
            int weight_respower = 2;
            int weight_res = 3;

            int[] weight = new int[4];
            weight[0] = weight_hp;
            weight[1] = weight_sp;
            weight[2] = weight_respower;
            weight[3] = weight_res;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    f += weight[i] * u_owner[i] - weight[j] * u_enemy[j];
                }
            }

            return f;
        }

        public  List<string> GetUserSessionList(Room room_)
        {
           // throw new NotImplementedException();
            List<string> userSessionList = new List<string>();

           foreach(var player in this)
           {
                if (room_._earlyOutPlayerSessionList == null)
                {
                    userSessionList.Add(player.session);
                }
                else
                {
                    bool isEarlyOut = false;
                    foreach (var earlyOutPlayerSession in room_._earlyOutPlayerSessionList)
                    {
                        if (earlyOutPlayerSession == player.session)
                        {
                            isEarlyOut = true;
                            break;
                        }
                    }
                    if (!isEarlyOut)
                    {
                        userSessionList.Add(player.session);
                    }
                }
            }
           return userSessionList;
        }

        public List<string> GetUserIDList( string exceptUserId )
        {
            // throw new NotImplementedException();
            List<string> userIDList = new List<string>();
            UserInfo playerInfo = null;
            foreach (var player in this)
            {
                playerInfo = GameServer.GetUserInfo(player.session);
                if (playerInfo != null)
                {
                    if (exceptUserId != playerInfo.userID)
                    {
                        userIDList.Add(playerInfo.userID);
                    }
                }
            }
            return userIDList;
        }

        public void ChooseFirstPlayer()
        { 
            Random rand = new Random();
            currentPlayerNum = rand.Next(1,this.Count+1);
            
            currentPlayer = this[currentPlayerNum - 1];
        }

        public void ChooseNextPlayer(Room _room)
        {
            if (currentPlayer != null)
            {
                if (currentPlayer.playAgain == true)
                {
                    doubleTurn = true;
                    currentPlayer.playAgain = false;
                    return;
                }
                else 
                {
                    doubleTurn = false;

                    //when turn end, reset deckInfoList
                    _room._deckInfolist = new List<DeckInfo>();
                    
                }
               
                if (this.Count <= 1)
                {
                    //Console.WriteLine("CAUTION!!! Choose Next Player is {0}", this.Count);
                    if (GameServer._logger.IsWarnEnabled)
                    {
                        string log = string.Format("Choose next player is: {0}", this.Count);
                        GameServer._logger.Warn(log);
                    }
                    return;
                }

                // if some player meet lose condition then cancel turn and choose next...
                for (int i = 0; i < this.Count; i++)
                {
                    if (currentPlayerNum >= this.Count)
                    {
                        currentPlayerNum = 1;
                    }
                    else
                    {
                        currentPlayerNum++;
                    }

                    if ((currentPlayerNum - 1) < this.Count)
                    {
                        if (!this[currentPlayerNum - 1].IsLoseCondition) break;
                    }
                    else
                    {
                        //Console.WriteLine("CAUTION!!! Choose Next Player PART1 is {0}/{1}", currentPlayerNum-1,  this.Count);
                        if (GameServer._logger.IsWarnEnabled)
                        {
                            string log = string.Format("Choose next player part1 is {0}/{1}", currentPlayerNum -1, this.Count );
                            GameServer._logger.Warn(log);
                        }
                    }
                }

                if ((currentPlayerNum - 1) < this.Count)
                {
                    currentPlayer = this[currentPlayerNum - 1];
                }
                else
                {
                    //Console.WriteLine("CAUTION!!! Choose Next Player PART2 is {0}/{1}", currentPlayerNum - 1, this.Count);
                    if (GameServer._logger.IsWarnEnabled)
                    {
                        string log = string.Format("Choose next player part2 is {0}/{1}", currentPlayerNum - 1, this.Count);
                        GameServer._logger.Warn(log);
                    }
                }
            }
        }

        public bool WinnerPlayerNumber( ref int winnerPlayerNumber_)
        {
            bool retvalue = false;
            winnerPlayerNumber_ = 0;
            if (winnerPlayer != null)
            {
                winnerPlayerNumber_ = winnerPlayer.playerNumber;
                retvalue = true;
            }

            return retvalue;
        }
       
        public bool IsReachWinCondition()
        {
            bool retvalue = false;
           // throw new NotImplementedException();
            int losecount = 0;
            
            foreach (Player player in this)
            {
                if (player.IsWinCondition)
                {
                    retvalue = true;
                    winnerPlayer = player;
                }

                if (player.IsLoseCondition)
                {
                    losecount++;
                }
            }

            if (losecount >= this.Count - 1)
            {
                foreach (Player player in this)
                {
                    if (!player.IsLoseCondition)
                    {
                        retvalue = true;
                        winnerPlayer = player;
                        break;
                    }
                }

                //all lose
                if (winnerPlayer == null)
                {
                    winnerPlayer = currentPlayer;
                    retvalue = true;
                }
            }
            return retvalue;
        }

        internal bool GetPlayer(int playerNumber_, out Player player_)
        {
            bool retvalue = false;
            player_ = null;

            foreach(var player in this)
            {
                if (player.playerNumber == playerNumber_)
                {
                    player_ = player;
                    retvalue = true;
                    break;
                }
            }

            return retvalue;
        }

        internal bool IsUserExist(string userSession_)
        {
            bool retvalue = false;
            foreach (Player player in this)
            {
                if (player.session == userSession_)
                {
                    retvalue = true;
                    break;
                }
            }
            return retvalue;
        }

        public bool IsAllPlayerReady {
            get
            {
                bool result = false;
                
                bool playerAllReady = true;

                if (this.Count == 0)
                {
                    playerAllReady = false;
                }
                else
                {
                    foreach (var player in this)
                    {
                        if (!player.IsRoomEnterReady)
                        {
                            playerAllReady = false;
                            break;
                        }
                    }
                }

                result = playerAllReady;

                return result;
            }
        }

        internal void AddRecommendFriends()
        {
            List<UserInfo> userInfoList = new List<UserInfo>();
            UserInfo userInfo = null;
            foreach(var player in this)
            {
                userInfo = GameServer.GetUserInfo(player.session);
                if (userInfo != null)
                {
                    userInfoList.Add(userInfo);
                }
            }

            for (int i = 0; i < userInfoList.Count; i++)
            {
                Dictionary<string, FriendData> tmpDic = new Dictionary<string, FriendData>();
                foreach (var friend in userInfoList[i]._recommendFriends)
                {
                    if (!tmpDic.ContainsKey(friend.friendID))
                    { tmpDic.Add(friend.friendID, friend); }
                }
                foreach (var friend in userInfoList[i]._myFriends)
                {
                    if (!tmpDic.ContainsKey(friend.friendID))
                    { tmpDic.Add(friend.friendID, friend); }
                }
                foreach (var friend in userInfoList[i]._requestFriends)
                {
                    if (!tmpDic.ContainsKey(friend.friendID))
                    { tmpDic.Add(friend.friendID, friend); }
                }

                foreach(var info in userInfoList)
                {
                    if (info != userInfoList[i] && !tmpDic.ContainsKey(info.userID))
                    {
                        FriendData friendData = new FriendData();
                        friendData.friendID = info.userID;
                        friendData.friendNick = info.userDataBase.nickName;
                        friendData.faceIdx = info.userDataBase.FaceIdx;
                        friendData.level = info.userDataBase.Level;
                        friendData.friendType = FriendType.Recommend;
                        friendData.friendLastLoginDate = info.userDataBase.lastLoginDate;
                        friendData.itemSendDate = string.Empty;
                        friendData.enableGetItem = false;

                        if (userInfoList[i]._recommendFriends == null) userInfoList[i]._recommendFriends = new List<FriendData>();
                        userInfoList[i]._recommendFriends.Add(friendData);
                    }
                }
            }
        }
    }
}
