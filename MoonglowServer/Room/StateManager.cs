﻿using System.Collections;
using System.Collections.Generic;
using System;

// T = enum ClientState , U ( Interface or Class )= IStateitem
//
public class StateManager<T, U> : Dictionary<T,U> where  T: struct where U : IStateItem<T>
{
	T currentState = default(T);
	T beforeState = default(T);
	U currentStateItem = default(U);
    public T State{ get{ return currentState; }}

	public void SetState( T nextState )
	{

		if (currentState.ToString() == nextState.ToString ()) {
            if (!object.Equals(currentStateItem, default(U)))
            {
                currentStateItem.OnUpdate();
            }
		}
		else
		{
			// exit
			//null check 
			if( !object.Equals(currentStateItem, default(U)) )
			{

			//	Debug.Log("not null  enter.........");
				currentStateItem.OnExit();
			}


			// change state
			if ( this.ContainsKey( nextState ))
			{
				// change stateitem
				U nextStateitem =  this[nextState];

				// change state
				beforeState = currentState;
				currentState = nextState;

				currentStateItem = nextStateitem;

				//enter
				nextStateitem.OnEnter( beforeState );
			}
			else
			{
				// don't have value
			}
		}
	}
}
