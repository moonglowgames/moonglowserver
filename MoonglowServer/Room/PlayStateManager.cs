﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Room
{
    public enum PlayState{Unknown, Ready, Start, TurnUseCard,TurnRecieveCard, End, Result, Delete}
    public class PlayStateManager : StateManager<PlayState, IStateItem<PlayState>>, IRuleAble
    {
        public Rule rule {get;set;}

        public PlayStateManager()
        {
           // SetState(PlayState.Unknown);
        }

        public void InitStateItem( Rule rule_)
        {
            rule = rule_;

            this.Add(PlayState.Unknown, new PSIUnkown());
            this.Add(PlayState.Ready, new PSIReady());
            this.Add(PlayState.Start, new PSIStart());
            this.Add(PlayState.TurnUseCard, new PSITurnUseCard());
            this.Add(PlayState.TurnRecieveCard, new PSITurnRecieveCard());
            this.Add(PlayState.End, new PSIEnd());
            this.Add(PlayState.Result, new PSIResult());
            this.Add(PlayState.Delete, new PSIDelete());

            foreach (PSBase ruleObject in this.Values)
            {
                ruleObject.rule = rule_;
            }

            SetState(PlayState.Unknown);
        }
    }
}
