﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MoonGlow.Room
{
    public enum CommandSendType { Unknown, Single, PlayersExceptMe, Players, Inspectors, AllExceptMe, All }
    public enum CommandTag {Room , Rule, Player, Deck, Client}
    public interface ICommand
    {
        IPacket pck { get; set; }
        CommandSendType sendType { get; set; }
        CommandTag receiver { get; set; }
        CommandTag sender { get; set; }
        List<string> sendUserSession { get; set; }
        int executeTime { get; set; }
        void Execute();
        void ExecuteAsync(int delay_ );
    }

    public class Command : ICommand 
    {
        public IPacket pck
        {
            get;
            set;
        }

        public CommandSendType sendType
        {
            get;
            set;
        }

        public int executeTime
        {
            get;
            set;
        }


        public CommandTag receiver
        {
            get;
            set;
        }

        public CommandTag sender
        {
            get;
            set;
        }

        public List<string> sendUserSession
        {
            get;
            set;
        }
        public void Execute()
        {
            pck.Execute();
        }

        public async void ExecuteAsync(int delay_)
        {           
            await Task.Delay(delay_);
            // after delay execute
            Execute();
        }

    }
}
