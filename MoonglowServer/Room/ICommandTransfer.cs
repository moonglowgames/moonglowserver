﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Room
{
    public interface ICommandTransfer
    {
        CommandTag myTag { get; set; }
        void SendToParent(ICommand cmd_);
        void SendToChild(ICommand cmd_);

        void SetParent(ICommandTransfer cmdTransferParent_);
        void ResetParent(ICommandTransfer cmdTransferParent_);
        void AddChild(ICommandTransfer cmdTransferChild_);
    }

    public class CommandTransfer : ICommandTransfer
    {
        ICommandTransfer _parent = null;
        List<ICommandTransfer> _children = null;

        public CommandTag myTag
        {
            get;
            set;
        }
        public virtual void SendToParent(ICommand cmd_)
        {

            if (cmd_.receiver == myTag)
            {
                if (cmd_.executeTime > 0) { cmd_.ExecuteAsync(cmd_.executeTime); }
                else { cmd_.Execute(); }
            }
            else 
            {
                if (_parent != null)
                {
                    _parent.SendToParent(cmd_);
                }
                else
                {
                    // maby room
                }
            }    

        }

        public void SendToChild(ICommand cmd_)
        {
            if (cmd_.receiver == myTag)
            {
                if (cmd_.executeTime > 0) { cmd_.ExecuteAsync(cmd_.executeTime); }
                else { cmd_.Execute(); }
            }
            else
            {

                if (_children != null)
                {
                    foreach (var child in _children)
                    {
                        child.SendToChild(cmd_);
                    }
                }
                else
                {
                    //player or deck
                }
            }
        }

        public void SetParent(ICommandTransfer cmdTransferParent_)
        {
            _parent = cmdTransferParent_;
        }

        public void ResetParent(ICommandTransfer cmdTransferParent_)
        {
            _parent = null;
        }

        public void AddChild(ICommandTransfer cmdTransferChild_)
        {
            if(_children == null) _children = new List<ICommandTransfer>();

            _children.Add(cmdTransferChild_);
            cmdTransferChild_.SetParent(this);
        }
    }
}
