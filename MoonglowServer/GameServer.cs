﻿using System;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;
using System.Collections.Generic;
using System.Collections.Concurrent;
using LitJson;
using StackExchange.Redis;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
using MoonGlow.Room;
using log4net;

namespace MoonGlow
{
    public enum SQLType { MySQL, MSSQL}
    public class GameServer : AppServer<UserSession>
    {
        public static GameServer instance = null;
        public static readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static bool _isLoginServer = true;
        public static bool _isServerStart = false;
        ExecuteThread _executeThread = null;
        DBThread _dbThread = null;
        RedisDBThread _redisDBThread = null;
        LogThread _logThread = null;
        public static string _localIP = string.Empty;
        public static bool _isCloud = false;
        //public static string _serverName = string.Empty;
        public static MSSqlConnect msSqlConnect = null;

        public static AzureStorageConnect azureStorage = null;
        public static DocumentDBConnect documentDBConnect = null;

        //public static string _connectIP = string.Empty;
        public static int _port = 0;
        public static string _DNS = string.Empty;
        public static string _type = string.Empty;
        public static string _version = ServerManager.SERVER_VERSION;
        public static bool _enable = false;

        //public static Dictionary<string, int> _gameServerDic = null;
        public static List<ServerInfo> _gameServerList = null;
        internal static List<ServerInfo> _allServerList = null;
        
        public GameServer()
        {
            FileInfo exefileinfo = new FileInfo(Application.ExecutablePath);
            string path = exefileinfo.Directory.FullName.ToString(); //프로그램 실행되고 있는데 path 가져오기
            string fileName = "/config.ini";
            string filePath = path + fileName;
            ConfigManager ini = new ConfigManager(filePath);

            ConfigurationOptions option = new ConfigurationOptions();
            Console.WriteLine("Connect Thread :{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);


            //isLoginServer
            _isLoginServer = Boolean.Parse(ini.GetIniValue("Server", "ISLoginServer"));
            //isCloud
            _isCloud = Boolean.Parse(ini.GetIniValue("Server", "IsCloud"));

            _localIP = ini.GetIniValue("Server","ServerIP");
            _port = int.Parse((ini.GetIniValue("Server", "ServerPort")));

            _DNS = ini.GetIniValue("Server", "DNS");

            _type = _isLoginServer ? "Login" : "Game";
            _enable = true;
            //GetConnectIP();

            //Redis Connection
            RedisConnect.connectString = ini.GetIniValue("Redis", "RedisIP");
            Console.WriteLine("Session Redis IP: " + RedisConnect.connectString);
            RedisConnect.Instance.SubscribeChannel();

            //Rank Connection
            RankConnect.connectString = ini.GetIniValue("RankRedis","RankRedisIP");
            RankConnect.defaultDB = int.Parse(ini.GetIniValue("RankRedis", "DefaultDatabase"));
            //Console.WriteLine("Rank Redis IP: " + RankConnect.connectString + ", Default DB: " + RankConnect.defaultDB);

            //Ban Redis Connections
            BanRedisConnect.connectString = ini.GetIniValue("BanRedis", "BanRedisIP");
            BanRedisConnect.defaultDB = int.Parse(ini.GetIniValue("BanRedis", "DefaultDatabase"));
            //Console.WriteLine("Ban Redis IP: " + BanRedisConnect.connectString + ", Default DB: " + BanRedisConnect.defaultDB);

            //Mysql Connection
            //Console.WriteLine(ini.GetIniValue("Mysql", "MysqlConnectvalue"));
            //MySqlConnect.connectString = ini.GetIniValue("Mysql", "MysqlConnectvalue");

            //MS SQL connection
            MSSqlConnect.connectString = ini.GetIniValue("Mssql", "SqlConnectvalue");
            msSqlConnect = new MSSqlConnect();

            Console.WriteLine(MSSqlConnect.connectString);

            //AzureStroage connection
            if (_isCloud == true)
            {
                AzureStorageConnect.connectString = ini.GetIniValue("Azure", "StroageConnectvalue");
                Console.WriteLine(AzureStorageConnect.connectString);
                azureStorage = new AzureStorageConnect();
                azureStorage.ResetTable();
            }

            //Azure Document DB
            DocumentDBConnect.EndpointUrl = ini.GetIniValue("Azure", "DocumentDBEndpointUrl");
            DocumentDBConnect.AuthorizationKey = ini.GetIniValue("Azure", "DocumentDBKey");
            Console.WriteLine("DocumentDBEndpointUrl: " + DocumentDBConnect.EndpointUrl);
            Console.WriteLine("AuthorizationKey: " + DocumentDBConnect.AuthorizationKey);
            documentDBConnect = new DocumentDBConnect();
            //documentDBConnect.Test();

            //mail
            GlobalManager.mailSender = ini.GetIniValue("Mail", "SenderID");
            GlobalManager.mailPW = ini.GetIniValue("Mail", "SenderPW");

            //GlobalServerState Connection
            ServerStateRedisConnect.connectString = ini.GetIniValue("GlobalServerRedis", "GlobalServerRedisIP");
            ServerStateRedisConnect.defaultDB = int.Parse(ini.GetIniValue("GlobalServerRedis", "DefaultDatabase"));
            //Console.WriteLine("GlobalServer Redis IP: " + ServerStateRedisConnect.connectString + "Default DB: " + ServerStateRedisConnect.defaultDB);
            ServerStateRedisConnect.Instance.SubscibeChannel();

            if (_isLoginServer == true) _isServerStart = ServerStateRedisConnect.Instance.GetGlobalServerState().Result;
            else _isServerStart = true;

            //Sever Excute
            MakeExecuteThread();
            MakeDBThread();
            MakeRedisDBThread();
            if (_isCloud == true) MakeLogThread();
            instance = this;

            //this.SessionCount
        }

        //private void GetConnectIP()
        //{
        //    try
        //    {
        //        // Get the local computer host name.
        //        //String hostName = Dns.GetHostName();
        //        //Console.WriteLine("Computer name :" + hostName);
        //        //IPAddress[] addressList;
        //        if (_isCloud)
        //        {
        //            //addressList = Dns.GetHostAddresses(hostName + ".cloudapp.net");
        //            //ip6
        //            //_connectIP = addressList[0].ToString();
        //            _connectIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
        //            _connectIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
        //                                           .Matches(_connectIP)[0].ToString();
        //        }
        //        else
        //        {
        //            String hostName = Dns.GetHostName();
        //            IPAddress[] addressList;

        //            addressList = Dns.GetHostAddresses(hostName);
        //            //local
        //            _connectIP = addressList[2].ToString();
        //        }

        //        Console.WriteLine("Connect IP: " + _connectIP);

        //        //foreach (var address in addressList)
        //        //{
        //        //    if (address.AddressFamily == AddressFamily.InterNetwork)
        //        //    {
        //        //        _connectIP = address.ToString();
        //        //        //Console.WriteLine("---Connect IP: " + _connectIP);
        //        //        break;
        //        //    }
        //        //}
        //        //string externalIP = "";


        //    }
        //    catch (SocketException e)
        //    {
        //        Console.WriteLine("SocketException caught!!!");
        //        Console.WriteLine("Source : " + e.Source);
        //        Console.WriteLine("Message : " + e.Message);
        //    }
        //}

        internal static void GetClientVersions()
        {
            msSqlConnect.GetClientVersions();
        }
        internal static void GetAllDevList()
        {
            msSqlConnect.GetAllDevList();

        }

        protected override void OnNewSessionConnected(UserSession session)
        {
            //Console.WriteLine("OnNewSessionConnect ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);

            if (_isLoginServer == true)
            {
                ServerManager.Instance.SetServerInfo("MoonGlowLoginServer", "MGLV1.0.0", "Welcome to MoonGlow Card Game!");
                ServerManager.Instance.SetServerSate(/*ServerState.Ready*/GameServer._isServerStart ? ServerState.Ready : ServerState.ShutDown);
            }
            else
            {
                ServerManager.Instance.SetServerInfo("MoonGlowGameServer", "MGLV1.0.0", "Welcome to MoonGlow Card Game!");
                ServerManager.Instance.SetServerSate(/*ServerState.Ready*/GameServer._isServerStart ? ServerState.Normal : ServerState.ShutDown);
            }


            //SCNotiHello
            MoonGlow.PacketsHandlerServer.SendSCNotiHello(session.SessionID, ServerManager.Instance.serverName, ServerManager.Instance.serverVersion, ServerManager.Instance.serverState, ServerManager.Instance.serverInfo);
        }

        protected override void OnSessionClosed(UserSession session, SuperSocket.SocketBase.CloseReason reason)
        {
            if (_isLoginServer == false)
            {
                string serverSession = session.SessionID;

                //1. save userInfo
                if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
                {
                    string userSession = SessionIdManager.Instance._sessionDic[serverSession];

                    UserInfo userInfo = GetUserInfo(userSession);//serverSession
                    if (userInfo == null)
                    {
                        GlobalManager.Instance.NullUserInfo(serverSession);
                        return;
                    }

                    userInfo.OnSessionClose();
                }
                else
                {
                    //removed already via logout/or client pause
                }
            }

            //if (/*Logger*/_logger.IsDebugEnabled)
            //{
            //    string log = string.Format("Session : " + session.SessionID + " is Closed by " + reason.ToString());

            //    /*Logger*/
            //    _logger.Info(log);
            //}
        }

        //public void OnLoginUserId(string userSession, string userid)
        //{
        //    if (Logger.IsDebugEnabled)
        //    {
        //        Logger.Debug((userid==null)? "UserId is null":"Checked Success UserId");
        //    }

        //    // 1. userinfo.session = session;
        //    //if (SessionIdManager.Instance.AddUserId(userSession, userid))
        //    //{
        //    //    if (Logger.IsInfoEnabled)
        //    //    {
        //    //        string log = string.Format("Sucsses Login : (Session :{0}, UserID: {1})", userSession, userid);
        //    //        Logger.Info(log);
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    if (Logger.IsInfoEnabled)
        //    //    {
        //    //        string log = string.Format("Sucsses Login fail: (Session :{0}, UserID: {1})", userSession, userid);
        //    //        Logger.Info(log);
        //    //    }
        //    //}
        //}

        //protected void OnLogOutUserId(UserSession session, string userid)
        //{
        //    if (string.IsNullOrEmpty(userid))
        //    {
        //        if (Logger.IsDebugEnabled)
        //        {
        //            Logger.Debug("UserId is null");
        //        }
        //    }
        //    else
        //    {
        //        if (Logger.IsDebugEnabled)
        //        {
        //            Logger.Debug("Checked Sucsses UserId");
        //        }
        //    }

        //    // 1. userinfo.session = session;
        //    //if (SessionIdManager.Instance.RemoveUserId(userid))
        //    //{
        //    //    if (Logger.IsInfoEnabled)
        //    //    {
        //    //        string log = string.Format("Sucsses Logout : (Session :{0}, UserID: {1})", session.SessionID, userid);
        //    //        Logger.Info(log);
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    if (Logger.IsInfoEnabled)
        //    //    {
        //    //        string log = string.Format("Sucsses Logout fail: (Session :{0}, UserID: {1})", session.SessionID, userid);
        //    //        Logger.Info(log);
        //    //    }
        //    //}
        //}

        protected override void ExecuteCommand(UserSession session, StringRequestInfo requestInfo)
        {
            string msg = string.Empty;
            string encryptStr = string.Empty;
            string packetName = string.Empty;
            IPacket pck = null;

            // get json string
            if ( string.IsNullOrEmpty( requestInfo.Body ))
            {
                encryptStr = requestInfo.Key;
            }
            else
            {
                encryptStr = requestInfo.Key + " " + requestInfo.Body;
            }

            msg = encryptStr.Decryption();

            //Console.WriteLine(session.SessionID + ": " + msg);
            if (/*Logger*/_logger.IsDebugEnabled)
            {
                string log = session.SessionID + ": " + msg;
                /*Logger*/
                _logger.Debug(log);
            }

            // get package name
            JsonData jsonData = null;
            try
            {
                //json to object
                jsonData = JsonMapper.ToObject(msg);
                Console.WriteLine(msg);
            }
            catch (Exception ex)
            {
                //Console.WriteLine("error : "+ex);
                if (/*Logger*/_logger.IsErrorEnabled)
                {
                    string log = string.Format("error : " + ex);
                    /*Logger*/
                    _logger.Error(log);
                }
                jsonData = null;
            }

            if (jsonData == null)
            {
                if (/*Logger*/_logger.IsErrorEnabled)
                {
                    string log = string.Format("JsonData Read error");
                    /*Logger*/
                    _logger.Error(log);
                }
                return;
            }
            packetName = jsonData["name"].ToString();

            //if (/*Logger*/_logger.IsDebugEnabled)
            //{
            //    string log = string.Format("packet Name :" + packetName);
            //    /*Logger*/
            //    _logger.Debug(log);
            //}

            if (packetName == string.Empty)
            {
                if (/*Logger*/_logger.IsErrorEnabled)
                {
                    string log = string.Format("packet Name is empty, jsonData: {0}", jsonData);
                    /*Logger*/
                    _logger.Error(log);
                }
                return;
            }

            try
            {
                //pck = (IPacket)Activator.CreateInstance(Type.GetType(packetName));
                pck = Make(Type.GetType(packetName));
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Error: " + ex.Message);
                if (/*Logger*/_logger.IsFatalEnabled)
                {
                    /*Logger*/
                    _logger.Fatal("GameServer.cs GetPacket exception:" + ex.Message);
                }
                pck = null;
            }

            if (pck != null)
            {
                // make json to element
                pck.Decode(EncodeType.Json, msg);
                AddExecuteQueue(session.SessionID, pck);               
            }

            //BC(msg);
            //session.Send (msg);
        }

        private IPacket Make(Type ObjType)
        {
            return (IPacket)Activator.CreateInstance(ObjType);
        }

        public void AddExecuteQueue(string serverSession_,  IPacket packet_)
        {
            //this._executeThread.Queue.AddPacket(serverSession_, packet_);
            QueueManager.Instance.AddWorkQueue(serverSession_, packet_);
        }

        protected void MakeExecuteThread()
        {
            _executeThread = new ExecuteThread(_logger);
        }

        protected void MakeDBThread()
        {
            _dbThread = new DBThread(_logger);
        }

        protected void MakeRedisDBThread()
        {
            _redisDBThread = new RedisDBThread(_logger);
        }

        protected void MakeLogThread()
        {
            _logThread = new LogThread(_logger);
        }

        public void SaveSession(string session)
        {
            ConcurrentDictionary<string, string> sessionid = new ConcurrentDictionary<string, string>();
        }

        public static void SendMsgWithUserSession(string userSession, IPacket packet)
        {
            UserInfo tempuserinfo = GameServer.GetUserInfo(userSession);// null;
            if (tempuserinfo != null)
            {
                SendMsg(tempuserinfo.serverSession, packet);
            }
        }

        public static void SendMsgWithUserSession(List<string> userSessionList, IPacket packet)
        {
            if (userSessionList != null)
            {
                // packet send to all player in the room ( me and others )
                foreach (string userSession in userSessionList)
                {
                    // userid change to session and send packet
                    // room BC ..
                    SendMsgWithUserSession(userSession, packet);
                }
            }
        }

        public static void SendMsg(string serverSession, IPacket packet)
        {
            if (instance == null) return;
            var sessioninfo = instance.GetSessionByID(serverSession);
            //Console.WriteLine(sessioninfo);

            string sendMsg = packet.Encode(EncodeType.Json);

            if ( string.IsNullOrEmpty(sendMsg))
            {
                // log error...
                if (_logger.IsFatalEnabled)
                {
                    string log = string.Format("Packets send msg is empty, packet: {0}", packet.GetType().ToString());
                    _logger.Fatal(log);
                }
            }
            else
            {
                if (packet.GetType() != typeof(SCNotiHello))
                {
                   Console.WriteLine("SEND[{0}]:{1}", serverSession, packet.GetType().ToString());
                    if (_logger.IsDebugEnabled)
                    {
                        //string log = session.SessionID + ": " + msg;
                        string log = string.Format("SEND[{0}]:{1}", serverSession, /*packet.GetType().ToString()*/sendMsg);
                        _logger.Debug(log);
                    }
                }
                try
                {
                    sessioninfo.Send(sendMsg.Encription());
                }
                catch (Exception e)
                {
                    //Console.WriteLine("!!!SendMsg Error: " + e.Message);
                    if (/*Logger*/_logger.IsFatalEnabled)
                    {
                        /*Logger*/
                        _logger.Fatal("!!!GameServer SendMsg exception:" + e.Message);
                    }
                }
            }
        }

        public static void SendBC(IPacket packet)
        {
            string sendMsg = packet.Encode(EncodeType.Json);
            //Console.WriteLine("BROAD>>" + sndMsg);
            foreach (var session in instance.GetAllSessions())
            {
                session.Send(sendMsg.Encription());
            }
        }

        public static Room.Room GetRoom(string userSession)
        {
            //throw new NotImplementedException();
            UserInfo userinfo = GameServer.GetUserInfo(userSession);
            Room.Room room = null;
            if (userinfo != null)
            {
                if (RoomManager.Instance.ContainsKey(userinfo.roomID))
                {
                    room = RoomManager.Instance[userinfo.roomID];
                }
            }

            return room;
        }

        internal static UserInfo GetUserInfo(string userSession)
        {
            return SessionIdManager.Instance.ContainsKey(userSession) ? SessionIdManager.Instance[userSession] : null;
        }

    }
}

