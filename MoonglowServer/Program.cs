﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace MoonGlow
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            FileInfo exefileinfo = new FileInfo(Application.ExecutablePath);
            string path = exefileinfo.Directory.FullName.ToString(); //프로그램 실행되고 있는데 path 가져오기
            string fileName = "/config.ini";
            string filePath = path + fileName;
            ConfigManager ini = new ConfigManager(filePath);
            // Console.WriteLine("Press any key to start the server!");

            // Console.ReadKey();
            // Console.WriteLine();

            var appServer = new GameServer();

            if (!appServer.Setup(GameServer._localIP, GameServer._port)) //Setup with listening port
            {
                Console.WriteLine("Failed to setup!");
                Console.ReadKey();
                return;
            }

            //Console.WriteLine();

            //Try to start the appServer
            if (!appServer.Start())
            {
                Console.WriteLine("Failed to start!");
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = "Faile to start server!!!";
                    GameServer._logger.Error(log);
                }
                Console.ReadKey();
                return;
            }

            //Console.WriteLine("The server started successfully, press key 'q' to stop it!");

            while (Console.ReadKey().KeyChar != 'q')
            {
                Console.WriteLine();
                continue;
            }

            //Stop the appServer
            appServer.Stop();
            if (GameServer._logger.IsInfoEnabled)
            {
                GameServer._logger.Info("Game Stop.");
            }

            Console.WriteLine("The server was stopped, press any key to exit!");
            Console.ReadKey();
            System.Environment.Exit(0);
        }
    }

    /*
        public class ADD : CommandBase<AppSession, StringRequestInfo>
        {
            public override void ExecuteCommand(AppSession session, StringRequestInfo requestInfo)
            {
                session.Send(requestInfo.Parameters.Select(p => Convert.ToInt32(p)).Sum().ToString());
            }
        }

        public class MULT : CommandBase<AppSession, StringRequestInfo>
        {
            public override void ExecuteCommand(AppSession session, StringRequestInfo requestInfo)
            {
                var result = 1;

                foreach (var factor in requestInfo.Parameters.Select(p => Convert.ToInt32(p)))
                {
                    result *= factor;
                }

                session.Send(result.ToString());
            }
        }
    */
}
