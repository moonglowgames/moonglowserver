﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections;

namespace MoonGlow
{
    
    public class SessionIdManager : ConcurrentDictionary<string,UserInfo>
    {
        private static SessionIdManager _instance = null;
        //private ConcurrentDictionary<string, UserInfo> _userIdList = new ConcurrentDictionary<string, UserInfo>();
        public Dictionary<string, string> _sessionDic = new Dictionary<string, string>();

        public static SessionIdManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SessionIdManager();

                return _instance; 
            }
        }

        public bool AddSession(string userSession, string serverSession, Language language)
        {
            bool retvalue = false;

            if (!this.ContainsKey(userSession))
            {
                if(!_sessionDic.ContainsKey(serverSession)) _sessionDic.Add(serverSession, userSession);

                UserInfo userinfo = new UserInfo( userSession, serverSession );
                userinfo.language = language;
                retvalue = this.TryAdd( userSession, userinfo);
            }

            return retvalue;
        }

        public bool RemoveSession(string userSession)
        {
            bool retvalue = false;

            UserInfo userinfo = null;
            if( this.TryRemove( userSession , out userinfo ) )
            {
                userinfo.SetDisConnect();

                retvalue = true;
                //if (isRemoveUserID == false)
                //{
                //    retvalue = true;
                //}
                //else
                //{
                //    retvalue = RemoveUserId(userinfo.userID);
                //}
            }
            return retvalue;        
        }

        //public bool AddUserId(string userSession, string userId)
        //{
        //    bool retvalue = false;

        //    UserInfo userinfo = this[userSession];
        //    if (_userIdList.ContainsKey(userId))
        //    {
        //        RemoveUserId(userId);
        //    }

        //    if (!_userIdList.ContainsKey(userId) && userinfo != null)
        //    {
        //        if (_userIdList.TryAdd(userId, userinfo))
        //        {
        //            userinfo.SetUserID(userId);
        //            retvalue = true;
        //        }
        //    }

        //    return retvalue;
        //}

        //public bool RemoveUserId(string userId )
        //{
        //    bool retvalue = false;

        //    if (userId != null && userId != string.Empty)
        //    {
        //        UserInfo userinfo = null;
        //        if (_userIdList.TryRemove(userId, out userinfo))
        //        {
        //            userinfo.SetLogOut();
        //            retvalue = true;
        //        }
        //    }
        //    return retvalue;
        //}

        //public bool GetUserInfoWithUserId(string userid , out UserInfo userInfo )
        //{
        //    bool retvalue = false;
        //    userInfo = null;

        //    if (string.IsNullOrEmpty(userid)) return false;

        //    if( _userIdList.ContainsKey( userid ))
        //    {
        //        userInfo = _userIdList[userid];
        //        retvalue = true;
        //    }

        //    return retvalue;
        //}
    }
}
