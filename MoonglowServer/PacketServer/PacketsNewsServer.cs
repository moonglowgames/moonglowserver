﻿using System;
using System.Collections.Generic;
using MoonGlow.Room;
using MoonGlow.Data;
using MoonGlow;

//CSReqNews
public partial class CSReqNews : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        NewsPacketsManager.Instance.GetCSReqNews(serverSession, this.session);
    }
}

//SCNotiNews
public partial class SCNotiNews : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
