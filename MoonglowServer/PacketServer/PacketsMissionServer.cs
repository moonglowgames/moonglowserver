﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//CSReqMissionList
public partial class CSReqMissionList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        MissionPacketsManager.Instance.GetCSReqMissionList(serverSession, this.session);
    }
}

//SCNotiMissionList
public partial class SCNotiMissionList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
