#define SERVER 

#if SERVER 

//CSReqRoomList
public  partial class  CSReqRoomList  : Packet {
	public override void Execute () { }

    public override void Execute( string serverSession)
    {
        //search room list
        HomePacketsManager.Instance.GetCSReqRoomList(serverSession, this.reqRoomList);
    }
}

//CSReqRoomCreate
public partial class CSReqRoomCreate : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        HomePacketsManager.Instance.GetCSReqRoomCreate(serverSession, this.session, this.gameType, this.playMode, this.playTime, this.stake, this.singlePlay, this.disClose, this.isLock);
    }
}

//CSReqQuickStart
public  partial class  CSReqQuickStart  : Packet {
	public override void Execute () { }

    public override void Execute(string serverSession)
    {
        HomePacketsManager.Instance.GetCSReqQuickStart(serverSession, this.session, this.gameType, this.playMode, this.playTime, this.stake, this.disClose, this.isLock);
    }
}

//CSReqJoinRoom
public partial class CSReqJoinRoom : Packet
{
    public override void Execute()  { }
    public override void Execute(string serverSession)
    {
        HomePacketsManager.Instance.GetCSReqJoinRoom(serverSession, this.session, this.roomName);
    }
}

//CSNotiSceneEnter
public partial class CSNotiSceneEnter : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        HomePacketsManager.Instance.GetCSNotiSceneEnter(serverSession, this.session);
    }
}

//CSNotiEnterRoom
public partial class CSNotiEnterRoom : Packet
{
    public override void Execute() { }

    public override void Execute(string serverSession)
    {
        HomePacketsManager.Instance.GetCSNotiEnterRoom(serverSession, this.session);
    }
}

//CSReqReadTutorial
public partial class CSReqReadTutorial : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        HomePacketsManager.Instance.GetCSReqReadTutorial(serverSession, this.session);
    }
}

//CSNotiChangeLanguage
public partial class CSNotiChangeLanguage : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        HomePacketsManager.Instance.GetCSNotiChangeLanguage(serverSession, this.session, this.language);
    }
}

//CSRspPlayerJoin
public partial class CSRspPlayerJoin : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        PlayPacketsManager.Instance.GetCSRspPlayerJoin(serverSession, this.session, this.askerUserID, this.rspResult);
    }
}

//CSNotiCancelRoom
public partial class CSNotiCancelRoom : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        PlayPacketsManager.Instance.GetCSNotiCancelRoom(serverSession, this.session);
    }
}

//CSReqCancelJoin
public partial class CSReqCancelJoin : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        PlayPacketsManager.Instance.GetCSReqCancelJoin(serverSession, this.session);
    }
}

//SCNotiEnterRoom
public partial class SCNotiEnterRoom : Packet
{
    public override void Execute() { }
}
//SCNotiRoomList 
public partial class SCNotiRoomList : Packet
{
    public override void Execute() { }
}
//SCNotiUserInfo
public partial class SCNotiUserInfo : Packet
{
    public override void Execute() { }
}
//SCNotiUserElement
public partial class SCNotiUserElement : Packet
{
    public override void Execute() { }
}
//SCNotiItemInfo
public partial class SCNotiItemInfo : Packet
{
    public override void Execute() { }
}
//SCNotiChatIconSet
public partial class SCNotiChatIconSet : Packet
{
    public override void Execute() { }
}
//SCNotiMessage 
public partial class SCNotiMessage : Packet
{
    public override void Execute() { }
}
//SCNotiPlayerJoin
public partial class SCNotiPlayerJoin : Packet
{
    public override void Execute() { }
}
//SCNotiPlayerJoinCancel
public partial class SCNotiPlayerJoinCancel : Packet
{
    public override void Execute() { }
}
//SCNotiMenuAlarm
public partial class SCNotiMenuAlarm : Packet
{
    public override void Execute() { }
}
#endif