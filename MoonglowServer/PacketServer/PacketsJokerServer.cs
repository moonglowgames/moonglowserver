﻿using System;
using LitJson;
using System.Collections.Generic;
using MoonGlow;
using MoonGlow.Data;

//CSReqJokerInfo
public partial class CSReqJokerInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        JokerPacketsManager.Instance.GetCSReqJokerInfo(serverSession, this.session);
    }
}

//CSReqSpellList
//public partial class CSReqSpellList : Packet
//{
//    public override void Execute() { }
//    public override void Execute(string serverSession)
//    {
//        JokerPacketsManager.Instance.GetCSReqSpellList(serverSession, this.session, this.spellType);
//    }
//}

//CSReqUpgradeSpell
public partial class CSReqUpgradeSpell : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        JokerPacketsManager.Instance.GetCSReqUpgradeSpell(serverSession, this.session, this.currentSpellIdx);
    }
}

//CSReqDowngradeSpell
public partial class CSReqDowngradeSpell : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        JokerPacketsManager.Instance.GetCSReqDowngradeSpell(serverSession, this.session, this.currentSpellIdx);
    }
}

//CSReqCancelUpgrade
public partial class CSReqJokerCancel : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        JokerPacketsManager.Instance.GetCSReqJokerCancel(serverSession, this.session);
    }
}

//CSReqApplySpells 
public partial class CSReqJokerApply : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        JokerPacketsManager.Instance.GetCSReqJokerApply(serverSession, this.session/*, this.spellType*/);
    }
}

//SCNotiJokerInfo
public partial class SCNotiJokerInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}

//SCNotiSpellList
public partial class SCNotiSpellList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}

//SCRspUpgradeSpell
public partial class SCRspUpdateSpell : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}

//SCRspApplySpells 
public partial class SCRspJokerApply : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}

