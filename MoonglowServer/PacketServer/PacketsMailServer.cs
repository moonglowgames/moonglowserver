﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//CSReqMailList 
public partial class CSReqMailList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) 
    {
        MailPacketsManager.Instance.GetCSReqMailList(serverSession, this.session);
    }
}

//CSReqGetItem 
public partial class CSReqGetItem : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) 
    {
        MailPacketsManager.Instance.GetCSReqGetItem(serverSession, this.session, this.mailIdx);
    }
}

//CSReqGetAllItems 
public partial class CSReqGetAllItems : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) 
    {
        MailPacketsManager.Instance.CSReqGetAllItems(serverSession, this.session);
    }
}

//CSReqRemoveList 
//public partial class CSReqRemoveOld : Packet
//{
//    public override void Execute() { }
//    public override void Execute(string serverSession) 
//    {
//        MailPacketsManager.Instance.GetCSReqRemoveOld(serverSession, this.session);
//    }
//}

//SCNotiMailList 
public partial class SCNotiMailList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}

//SCRspGetItem 
public partial class SCRspGetItem : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}

//SCRspGetAllItems 
public partial class SCRspGetAllItems : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
