﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//CSReqFriendList
public partial class CSReqFriendList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) 
    {
        FriendPacketsManager.Instance.GetCSReqFriendList(serverSession, this.session, this.tabType);
    }
}

//CSReqRequestFriend
public partial class CSReqRequestFriend : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        FriendPacketsManager.Instance.GetCSReqRequestFriend(serverSession, this.session, this.tabTybe, this.iDx);
    }
}

//CSReqAcceptFriend 
public partial class CSReqAcceptFriend : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) 
    {
        FriendPacketsManager.Instance.GetCSReqAcceptFriend(serverSession, this.session,  this.tabTybe, this.iDx);
    }
}

//CSReqDeleteFriend 
public partial class CSReqDeleteFriend : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) 
    {
        FriendPacketsManager.Instance.GetCSReqDeleteFriend(serverSession, this.session, this.tabTybe, this.iDx);
    }
}

//CSReqSendGift 
public partial class CSReqSendGift : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) 
    {
        FriendPacketsManager.Instance.GetCSReqSendGift(serverSession, this.session, this.tabTybe, this.iDx);
    }
}

//CSReqGetGift 
public partial class CSReqGetGift : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) 
    {
        FriendPacketsManager.Instance.GetCSReqGetGift(serverSession, this.session, this.tabTybe, this.iDx);
    }
}

//CSReqSendAllGifts 
public partial class CSReqSendAllGifts : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        FriendPacketsManager.Instance.GetCSReqSendAllGifts(serverSession, this.session, this.tabTybe);
    }
}

//CSReqGetAllGifts 
public partial class CSReqGetAllGifts : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) 
    {
        FriendPacketsManager.Instance.GetCSReqGetAllGifts(serverSession, this.session, this.tabTybe);
    }
}

//SCNotiFriendList
public partial class SCNotiFriendList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}

//SCRspFriendResut
public partial class SCRspFriendResut : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}