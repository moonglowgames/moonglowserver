﻿using MoonGlow;
using MoonGlow.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//CSReqAchieveList
public partial class CSReqAchieveList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) 
    {
        AchievePacketsManager.Instance.GetCSReqAchieveList(serverSession, this.session);
    }
}

//SCNotiAchieveList
public partial class SCNotiAchieveList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
