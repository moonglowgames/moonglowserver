#define SERVER 

#if SERVER 

using System.Collections;
using System.Collections.Generic;
using System;
using MoonGlow;
using MoonGlow.Room;

//CSReqUseCard
public  partial class  CSReqUseCard  : Packet {
	public override void Execute () { }
    public override void Execute(string serverSession)
    {
        PlayPacketsManager.Instance.GetCSReqUseCard(serverSession, this.session, this.targetNumber, this.handPosition);
    }

}

//CSReqWantGame
public  partial class  CSReqWantGame  : Packet {
	public override void Execute () { }

    public override void Execute( string serverSession)
    {
        PlayPacketsManager.Instance.GetCSReqWantGame(serverSession, this.session, this.want);
    }
}

//CSReqQuickInventory
public partial class CSReqQuickInventory : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        PlayPacketsManager.Instance.GetCSReqQuickInventory(serverSession, this.session);
    }
}

//CSReqAIPlay
public partial class CSReqAIPlay : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        PlayPacketsManager.Instance.GetCSReqAIPlay(serverSession, this.session, this.use);
    }
}

//SCNotiPlayerList
public partial class SCNotiPlayerList : Packet
{
    public override void Execute() { }
}

//SCNotiPlayerStatus 
public partial class SCNotiPlayerStatus : Packet
{
    public override void Execute() { }
}

//SCNotiPlayerItemInfo
public partial class SCNotiPlayerItemInfo : Packet
{
    public override void Execute() { }
}
//SCNotiStartGame
public partial class SCNotiStartGame : Packet
{
    public override void Execute() { }
}

//SCNotiDeckShuffle
public partial class SCNotiDeckShuffle : Packet
{
    public override void Execute() { }
}

//SCNotiDecToHand
public partial class SCNotiDeckToHand : Packet
{
    public override void Execute() { }
}

//SCNotiPlayerFocus
public partial class SCNotiPlayerFocus : Packet
{
    public override void Execute() { }
}

//SCNotiUseCard
public partial class SCNotiUseCard : Packet
{
    public override void Execute() { }
}

//SCNotiDeckInfo
public partial class SCNotiDeckInfo : Packet
{
    public override void Execute() { }
}

//SCNotiGameEnd
public partial class SCNotiGameEnd : Packet
{
    public override void Execute() { }
}

//SCNotiGameResult
public partial class SCNotiGameResult : Packet
{
    public override void Execute() { }
}
#endif

//SCNotiQuickInventory
public partial class SCNotiQuickInventory : Packet
{
    public override void Execute() { }
}

//SCNotiContinueGame
public partial class SCNotiContinueGame : Packet
{
    public override void Execute() { }
}