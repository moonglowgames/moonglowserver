﻿using MoonGlow;
using MoonGlow.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//CSReqMyInfo 
public partial class CSReqMyInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        MyPagePacketsManager.Instance.GetCSReqMyInfo(serverSession, this.session);
    }
}

//CSReqInventory
public partial class CSReqInventory : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        MyPagePacketsManager.Instance.GetCSReqInventory(serverSession, this.session, this.tabType);
    }
}

//CSReqUseItem 
public partial class CSReqUseItem : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        MyPagePacketsManager.Instance.GetCSReqUseItem(serverSession, this.session, this.itemIdx, this.use);
    }
}

//CSReqChangeNick
public partial class CSReqChangeNick : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        MyPagePacketsManager.Instance.GetCSReqChangeNick(serverSession, this.session, this.newNick);
    }
}

//SCNotiMyInfo
public partial class SCNotiMyInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
//SCNotiInventory 
public partial class SCNotiInventory : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
//SCRspUseItem 
public partial class SCRspUseItem : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
//SCRspChangeNick
public partial class SCRspChangeNick : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
