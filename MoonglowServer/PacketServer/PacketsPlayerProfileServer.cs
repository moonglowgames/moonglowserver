﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//CSReqPlayerProfile
public partial class CSReqPlayerProfile : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        PlayerProfilePacketsManager.Instance.GetCSReqPlayerProfile(serverSession, this.session, this.playerNick);
    }
}

//SCNotiPlayerProfile
public partial class SCNotiPlayerProfile : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
