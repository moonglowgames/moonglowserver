﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoonGlow.Data;
using MoonGlow;

/* EtherealInk: reset joker
 * SaqqaraTablet: rename
 * RosemaryOil: full charge stamina
 * SLElixir: stamina up to 200
 * EPElixir: +1HP during 2hrs
 * SOElixir: +1SP during 2hrs
 */

//CSReqShopList
public partial class CSReqShopList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        ShopPacketsManager.Instance.GetCSReqShopList(serverSession, this.session);
    }
}

//CSReqBuyElement 
public partial class CSReqBuyElement : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        ShopPacketsManager.Instance.GetCSReqBuyElement(serverSession, this.session, this.elementIdx, this.count);
    }
}

//CSReqFinishAD
public partial class CSNotiFinishAD : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        ShopPacketsManager.Instance.GetCSNotiFinishAD(serverSession, this.session, this.guid);
    }
}

//CSReqIAP
public partial class CSReqIAP : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        ShopPacketsManager.Instance.GetCSReqIAP(serverSession, this.session, this.iAPType, this.elementIdx);
    }
}

//CSReqIAPResult
public partial class CSReqIAPResult : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        ShopPacketsManager.Instance.GetCSReqIAPResult(serverSession, this.session, this.guid, this.result);
    }
}

//SCNotiShopList
public partial class SCNotiShopList : Packet
{
    public override void Execute() { }
}
//SCRspBuyElement 
public partial class SCRspBuyElement : Packet
{
    public override void Execute() { }
}
//SCNotiShowAD
public partial class SCNotiShowAD : Packet
{
    public override void Execute() { }
}
//SCNotiGetADItem
public partial class SCNotiGetADItem : Packet
{
    public override void Execute() { }
}

//SCRspIAP
public partial class SCRspIAP : Packet
{
    public override void Execute() { }
}
//SCRspIAPResult
public partial class SCRspIAPResult : Packet
{
    public override void Execute() { }
}