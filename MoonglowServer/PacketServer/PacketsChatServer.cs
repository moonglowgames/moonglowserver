#define SERVER

#if SERVER


using System;
using System.Collections.Generic;
using MoonGlow.Room;
using MoonGlow;
using System.Text;

public partial class CSNotiChat:Packet
{
	public override void Execute () { }

    public override void Execute(string serverSession)
    {
        ChatPacketsManager.Instance.GetCSNotiChat(serverSession, this.session, this.chatMsg, this.iconIdx);
    }
}

//CSReqCMD
public partial class CSReqCMD : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        ChatPacketsManager.Instance.GetCSReqCMD(serverSession, this.session, this.msg);
    }
}

//CSReqChangeChatChannel
public partial class CSNotiChangeChatChannel : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        ChatPacketsManager.Instance.GetCSNotiChangeChatChannel(serverSession, this.session, this.chatChannel);
    }
}

public partial class SCNotiChat:Packet
{
	public override void Execute () { }
}

//SCNotiServerNoti
public partial class SCNotiServerNoti : Packet
{
    public override void Execute(){ }
}

//SCNotiCMD
public partial class SCNotiCMD : Packet
{
    public override void Execute() { }
}


#endif
