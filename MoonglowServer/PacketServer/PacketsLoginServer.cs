﻿#define SERVER 

#if SERVER 

public partial class CSReqRegister : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        LoginPacketsManager.Instance.GetCSReqRegister(serverSession, this.accountType, this.account, this.password, this.nick, this.faceIdx, this.deviceID);
    }
}

//CSReqFindPassword
public partial class CSReqFindPassword : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        LoginPacketsManager.Instance.GetCSReqFindPassword(serverSession, this.account);
    }
}

//CSReqChangePassword
public partial class CSReqChangePassword : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        LoginPacketsManager.Instance.GetCSReqChangePassword(serverSession, this.account, this.oldPassword, this.newPassword);
    }
}

public partial class CSReqLogin : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        LoginPacketsManager.Instance.GetCSReqLogin(serverSession, this.accountType, this.account, this.password, this.deviceID, this.conType, this.conVer, this.language);
    }
}

public  partial class  CSReqSessionLogin  : Packet {
	public override void Execute () { }
    public override void Execute(string serverSession)
    {
        LoginPacketsManager.Instance.GetCSReqSessionLogin(serverSession, this.userID, this.session, this.language);
    }
}

public  partial class  CSReqGotoScene : Packet {
	public override void Execute () { }
    public override void Execute(string serverSession)
    {
        LoginPacketsManager.Instance.GetCSReqGotoScene(serverSession, session, this.scene);
    }
}

//CSReqLogout
public partial class CSReqLogout : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        LoginPacketsManager.Instance.GetCSReqLogout(serverSession, this.session);
    }
}

//CSReqClientPause
public partial class CSReqClientPause : Packet
{
    public override void Execute(){}
    public override void Execute(string serverSession)
    {
        LoginPacketsManager.Instance.GetCSReqClientPause(serverSession, session);
    }
}

//CSReqClientFocus
public partial class CSReqClientFocus : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        LoginPacketsManager.Instance.GetCSReqClientFocus(serverSession, session);
    }
}

//CSReqUrl
public partial class CSReqUrl : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        LoginPacketsManager.Instance.GetCSReqUrl(serverSession, this.session, this.urlType);
    }
}

//CSRspPang 
public partial class CSRspPang : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        LoginPacketsManager.Instance.GetCSRspPang(serverSession, this.session, this.pingCount);
    }
}

//SCNotiHello
public partial class  SCNotiHello : Packet
{
	public override void Execute () { }
}
//SCRspRegister
public partial class SCRspRegister : Packet
{
    public override void Execute() { }
}
//SCNotiSessionLogin
public partial class  SCNotiSessionLogin : Packet
{
	public override void Execute () { }
}
//SCRspLogin
public partial class  SCNotiLogin : Packet
{
	public override void Execute () { }
}
//SCNotiDownload
public partial class SCNotiDownload : Packet
{
    public override void Execute() { }
}

//SCNotiGotoScene
public partial class SCNotiGotoScene : Packet
{
    public override void Execute() { }
}
//SCNotiLogout
public partial class SCNotiLogout : Packet
{
    public override void Execute(){ }
}
//SCNotiUrl
public partial class SCNotiUrl : Packet
{
    public override void Execute() { }
}
//SCNotiConnections
public partial class SCNotiConnections : Packet
{
    public override void Execute() { }
}
//SCReqPing  
public partial class SCReqPing : Packet
{
    public override void Execute() { }
}
//SCNotiClientFocus
public partial class SCNotiClientFocus : Packet
{
    public override void Execute() { }
}
//SCRspPasswordResult
public partial class SCRspPasswordResult : Packet
{
    public override void Execute() { }
}
//SCNotiReconnect
public partial class SCNotiReconnect : Packet
{
    public override void Execute() { }
}
//SCNotiQuit
public partial class SCNotiQuit : Packet
{
    public override void Execute() { }
}
#endif