﻿using MoonGlow;
using MoonGlow.Data;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//CSReqChanelList
public partial class CSReqChanelList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        RankPacketsManager.Instance.GetCSReqChanelList(serverSession, this.session);
    }
}

//CSReqJoinChanel 
public partial class CSReqJoinChanel : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        RankPacketsManager.Instance.GetCSReqJoinChanel(serverSession, this.session, this.chanelIdx);
    }
}

//CSReqRankList 
public partial class CSReqRankList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        RankPacketsManager.Instance.GetCSReqRankList(serverSession, this.session);
    }
}

//CSReqRewardList 
public partial class CSReqRewardList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        RankPacketsManager.Instance.GetCSReqRewardList(serverSession, this.session);
    }
}

//CSReqMyRankInfo 
public partial class CSReqMyRankInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        RankPacketsManager.Instance.GetCSReqMyRankInfo(serverSession, this.session);
    }
}

//CSReqGetRankItem  
public partial class CSReqGetRankItem : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        RankPacketsManager.Instance.GetCSReqGetRankItem(serverSession, this.session, this.rankIdx);
    }
}

//SCNotiChanelList
public partial class SCNotiChanelList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
//SCNotiRankList 
public partial class SCNotiRankList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
//SCNotiRewardList 
public partial class SCNotiRewardList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
//SCNotiMyRankInfo 
public partial class SCNotiMyRankInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}
//SCRspRankResult  
public partial class SCRspRankResult : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession) { }
}