﻿using System;


public class Singleton < T > where T : class , new()
{
    private static T _instance = default(T);

    public static T Instance 
    {
        get 
        {
            if (_instance == default(T))
            {
                _instance = new T();
            }

            return _instance;
        }
    }
}

