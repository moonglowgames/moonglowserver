﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum LocalizationKey
{
    LOW_VERSION, SERVER_CHECK, DAILY_GIFT_SUBJECT, DAILY_GIFT_CONTENT, ACHIEVE_GIFT_SUBJECT, MISSION_GIFT_SUBJECT, JOIN_GIFT_SUBJECT,
    JOIN_GIFT_CONTENT, QUEST_GOLD_CONTENT, QUEST_RUBY_CONTENT, QUEST_BADGE_CONTENT, QUEST_PET_CONTENT,
    ITEM_ETHEREALINK, ITEM_SAQQARATABLET, ITEM_ROSEMARYOIL, ITEM_SLELIXIR, ITEM_EPELIXIR, ITEM_SOELIXIR
}

public class LocalizationManager:Singleton<LocalizationManager>
{
    public static Dictionary<string, string[]> _dict = new Dictionary<string, string[]>();
    //private Dictionary<int, string[]> _dict = new Dictionary<int, string[]>();
    //private const string _FILE = @"Data/NewsLocalization.csv";
    //private const int DECODE_PARSE_COUNT = 3;

    //public NewsLocalizationManager()
    //{
    //    LoadFromFile();
    //}

    //void LoadFromFile()
    //{
    //    string content = string.Empty;
    //    using (StreamReader sr = new StreamReader(_FILE))
    //    {
    //        content = sr.ReadToEnd();
    //    }

    //    string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
    //    char[] charsToTrim = { ' ', '\t' };

    //    foreach (string ln in lines)
    //    {
    //        ln.Trim(charsToTrim);

    //        if (ln.Length == 0) continue;
    //        if (ln[0] == ';') { continue; }
    //        else
    //        {
    //            if (ln != string.Empty)
    //            {
    //                string[] parsings = ln.Split(',');
    //                try
    //                {
    //                    if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
    //                    {
    //                        _dict[Convert.ToInt32(parsings[0].Trim(charsToTrim))] = new string[DECODE_PARSE_COUNT - 1];
    //                        for (int i = 0; i < DECODE_PARSE_COUNT - 1; i++)
    //                        {
    //                            _dict[Convert.ToInt32(parsings[0].Trim(charsToTrim))][i] = parsings[i + 1].Trim(charsToTrim);
    //                        }
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    Console.WriteLine(ln);
    //                    Console.WriteLine(ex.Message);
    //                }

    //            }
    //        }
    //    }
    //}

    public void ReadLocalization()
    {
        GameServer.msSqlConnect.ReadLocalization();
    }

    public string Get(/*int key*/string key, Language lang)
    {
        if (_dict.ContainsKey(key))
        {
            return _dict[key][(int)lang];
        }
        else
        {
            return null;
        }
    }

    //public bool Get(string key, Language lang, out string value)
    //{
    //    value = string.Empty;
    //    if (!_dict.ContainsKey(key))
    //    {
    //        return false;
    //    }
    //    else
    //    {
    //        value = _dict[key][(int)lang];
    //        return true;
    //    }
    //}
}