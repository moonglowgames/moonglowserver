﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Data
{
    public struct SpellAbility
    {
        //PriceMD	PriceM	PriceRE	HPEnemy	Damage	Mana	Zenstone	MoonDust	Moonstone	RodEnergy	MoonCrystal	ManaEnemy	ZenstoneEnemy	MoonDustEnemy	MoonstoneEnemy	RodEnergyEnemy	MoonCrystalEnemy
        public int priceMD;
        public int priceM;
        public int priceRE;
        public int hpEnemy;
        public int damage;
        public int manaBlue;
        public int zenstoneBlue;
        public int moonDustRed;
        public int moonstoneRed;
        public int rodEnergyGreen;
        public int moonCrystalGreen;
        public int manaEnemyBlue;
        public int zenstoneEnemyBlue;
        public int moonDustEnemyRed;
        public int moonstoneEnemyRed;
        public int rodEnergyEnemyGreen;
        public int moonCrystalEnemyGreen;
    }
    public class SpellInfo
    {
        //;idx	ReqInk	NextReqInk	SpellType	Level	MaxLevel	NextSpellIdx	
        //cardUseType Special	Name	Description
        public readonly int iDx;
        public readonly int reqInk;
        public readonly int lastReqInk;
        public readonly int nextReqInk;
        public readonly SpellType spellType;
        public readonly int level;
        public readonly int maxLevel;
        public readonly int lastSpellIdx;
        public readonly int nextSpellIdx;
        public readonly SpellAbility ability;
        public readonly CardUseType target;
        public readonly int special;
        public readonly string Name;
        public readonly string Description;

        const int DECODE_PARSE_COUNT = 28;
        public char[] charsToTrim = { ' ', '\t' };

        public bool IsDecodeOK { get; private set; }
        public SpellInfo(string cvsLineData_)
        {
            IsDecodeOK = false;

            if (cvsLineData_ != string.Empty)
            {
                cvsLineData_.Trim(charsToTrim);

                switch (cvsLineData_[0])
                {
                    case ';':
                        break;
                    default:
                        string[] parsings = cvsLineData_.Split(',');
                        int parscount = 0;
                        if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                        {
                            try
                            {
                                this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.reqInk = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.lastReqInk = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.nextReqInk = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.spellType = (SpellType)Enum.Parse(typeof(SpellType), parsings[parscount++].Trim(charsToTrim));
                                this.level = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.maxLevel = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.lastSpellIdx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.nextSpellIdx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.priceMD = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.priceM = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.priceRE = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.hpEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.damage = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.manaBlue = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.zenstoneBlue = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.moonDustRed = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.moonstoneRed = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));                                
                                this.ability.rodEnergyGreen = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.moonCrystalGreen = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.manaEnemyBlue = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.zenstoneEnemyBlue = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.moonDustEnemyRed = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.moonstoneEnemyRed = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.rodEnergyEnemyGreen = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.moonCrystalEnemyGreen = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.special = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.target = (CardUseType)Enum.Parse(typeof(CardUseType), parsings[parscount++].Trim(charsToTrim));
                                this.Name = parsings[parscount++].Trim(charsToTrim);
                                this.Description = parsings[parscount++].Trim(charsToTrim);

                                IsDecodeOK = true;
                            }
                            catch (Exception /* e */ )
                            {
                                IsDecodeOK = false;
                            }
                        }
                        break;
                }
            }
        }
    }
}
