﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class BanNickManager : Singleton<BanNickManager>
{
    private Dictionary<string, string> _Dic = new Dictionary<string, string>();
    private const string _FILE = @"Data/BanNickData.txt";
    // Use this for initialization

    public BanNickManager()
    {
        LoadFromFile();
    }

    public void LoadFromFile()
    {
        string content = string.Empty;
        using (StreamReader sr = new StreamReader(_FILE))
        {
            content = sr.ReadToEnd();
        }

        char[] charsToTrim = { ' ', '\t' };

        string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

        string tempValue = string.Empty;

        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);

            if (ln.Length == 0) continue;

            if (ln[0] == ';')
            {
                // ignore comment line
            }
            else
            {
                try
                {
                    if(!_Dic.ContainsKey(ln)) _Dic[ln] = string.Empty;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ln);
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }

    public bool IsNickBan(string key)
    {
        //foreach (var infoPair in _Dic)
        //{
        //    //Console.WriteLine(infoPair.Key);
        //}
        return (_Dic != null) ? _Dic.ContainsKey(key.ToUpper()) : false;
    }
}



