﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Data
{
    class MissionManager : Singleton<MissionManager>
    {
        public static List<MissionInfo> _items = new List<MissionInfo>();
        //private Dictionary<int, MissionInfo> _items = new Dictionary<int, MissionInfo>();
        //private const string _FILE = @"Data/MissionData.csv";
        //// Use this for initialization

        //public MissionManager()
        //{
        //    LoadFromFile();
        //}

        //public void LoadFromFile()
        //{

        //    string content = string.Empty;
        //    using (StreamReader sr = new StreamReader(_FILE))
        //    {
        //        content = sr.ReadToEnd();
        //    }


        //    char[] charsToTrim = { ' ', '\t' };


        //    string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

        //    MissionInfo tempLevelExpInfo = null;

        //    foreach (string ln in lines)
        //    {
        //        ln.Trim(charsToTrim);

        //        if (ln.Length == 0) continue;

        //        if (ln[0] == ';')
        //        {
        //            // ignore comment line
        //        }
        //        else
        //        {
        //            tempLevelExpInfo = new MissionInfo(ln);
        //            if (tempLevelExpInfo.IsDecodeOK == true)
        //            {
        //                _items[tempLevelExpInfo.iDx] = tempLevelExpInfo;
        //            }
        //            else
        //            {
        //                Console.WriteLine(ln);
        //            }
        //        }
        //    }
        //}

        public void ReadMission()
        {
            GameServer.msSqlConnect.ReadMission();
        }

        public List<MissionInfo> GetMissionList()
        {
            List<MissionInfo> missionList = new List<MissionInfo>();
            DateTime startTime = new DateTime();
            DateTime expireTime = new DateTime();

            //MissionInfo info = null;
            //foreach(var infoPair in _items)
            //{
            //    info = infoPair.Value;
            //    if (info.disable == false)
            //    {
            //        if (info.missionType == MissionType.Mission)
            //        {
            //            if (GlobalManager.Instance.ConvertToDateTime(info.startDate, out startTime) && GlobalManager.Instance.ConvertToDateTime(info.expireDate, out expireTime))
            //            {
            //                if (startTime <= DateTime.Now && expireTime > DateTime.Now)
            //                {
            //                    missionList.Add(info);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            missionList.Add(info);
            //        }
            //    }
            //}
            foreach (var mission in _items)
            {
                //if (info.disable == false)
                //{
                    if (mission.missionType == MissionType.Mission)
                    {
                        if (GlobalManager.Instance.ConvertToDateTime(mission.startDate, out startTime) && GlobalManager.Instance.ConvertToDateTime(mission.expireDate, out expireTime))
                        {
                            if (startTime <= DateTime.Now && expireTime > DateTime.Now)
                            {
                                missionList.Add(mission);
                            }
                        }
                    }
                    else
                    {
                        missionList.Add(mission);
                    }
                //}
            }
            return missionList;
        }

        internal GameType GetGameType(int missionIdx)
        {
            GameType retvalue = GameType.Unknown;
            //MissionInfo info = null;
            //foreach (var infoPair in _items)
            //{
            //    info = infoPair.Value;
            //    if (info.iDx == missionIdx)
            //    {
            //        retvalue = info.gameType;
            //        break;
            //    }
            //}
            foreach (var mission in _items)
            {
                if (mission.iDx == missionIdx)
                {
                    retvalue = mission.gameType;
                    break;
                }
            }
            return retvalue;
        }

        internal AchieveType GetAchieveType(int missionIdx)
        {
            AchieveType retvalue = AchieveType.Unknown;
            //MissionInfo info = null;
            //foreach (var infoPair in _items)
            //{
            //    info = infoPair.Value;
            //    if (info.iDx == missionIdx)
            //    {
            //        retvalue = info.achieveType;
            //        break;
            //    }
            //}
            foreach (var mission in _items)
            {
                if (mission.iDx == missionIdx)
                {
                    retvalue = mission.achieveType;
                    break;
                }
            }
            return retvalue;
        }
    }
}
