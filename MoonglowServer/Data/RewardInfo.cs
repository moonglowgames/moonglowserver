﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Data
{
    class RewardInfo
    {
        public readonly int iDx;
        public readonly GameType gameType;
        public readonly PlayMode playMode;
        public readonly int winExp;
        public readonly int loseExp;
        public readonly int winCoin;
        public readonly int loseCoin;
        public readonly  int rankPoint;

        const int DECODE_PARSE_COUNT = 8;
        public char[] charsToTrim = { ' ', '\t' };

        public bool IsDecodeOK { get; private set; }
        public RewardInfo (string cvsLineData_)
        {
            IsDecodeOK = false;

            if (cvsLineData_ != string.Empty)
            {
                cvsLineData_.Trim(charsToTrim);

                switch (cvsLineData_[0])
                {
                    case ';':
                        break;
                    default:
                        string[] parsings = cvsLineData_.Split(',');
                        int parscount = 0;
                        if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                        {
                            try
                            {
                                this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.gameType = (GameType)Enum.Parse(typeof(GameType),parsings[parscount++].Trim(charsToTrim));
                                this.playMode = (PlayMode)Enum.Parse(typeof(PlayMode),parsings[parscount++].Trim(charsToTrim));
                                this.winExp = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.loseExp = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.winCoin = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.loseCoin = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.rankPoint = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));

                                IsDecodeOK = true;
                            }
                            catch (Exception /* e */ )
                            {
                                IsDecodeOK = false;
                            }
                        }
                        break;
                }
            }
        }

    }
}
