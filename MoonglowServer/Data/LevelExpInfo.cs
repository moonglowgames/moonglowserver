﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Data
{
    class LevelExpInfo
    {
        public readonly int iDx;
        public readonly int level;
        public readonly int minExp;
        public readonly int maxExp;

        const int DECODE_PARSE_COUNT = 4;
        public char[] charsToTrim = { ' ', '\t' };

        public bool IsDecodeOK { get; private set; }

        public LevelExpInfo (string cvsLineData_)
        {
            IsDecodeOK = false;

            if (cvsLineData_ != string.Empty)
            {
                cvsLineData_.Trim(charsToTrim);

                switch (cvsLineData_[0])
                {
                    case ';':
                        break;
                    default:
                        string[] parsings = cvsLineData_.Split(',');
                        int parscount = 0;
                        if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                        {
                            try
                            {
                                this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.level = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.minExp = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.maxExp = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));

                                IsDecodeOK = true;
                            }
                            catch (Exception /* e */ )
                            {
                                IsDecodeOK = false;
                            }
                        }
                        break;
                }
            }
        }

    }
}
