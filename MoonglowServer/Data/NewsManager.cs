﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Data
{
    class NewsManager : Singleton<NewsManager>
    {
        public static List<NewsInfo> _items = new List<NewsInfo>();
        //private Dictionary<int, NewsInfo> _items = new Dictionary<int, NewsInfo>();
        //private const string _FILE = @"Data/NewsData.csv";
        //// Use this for initialization

        //public NewsManager()
        //{
        //    LoadFromFile();
        //}

        //public void LoadFromFile()
        //{
        //    string content = string.Empty;
        //    using (StreamReader sr = new StreamReader(_FILE))
        //    {
        //        content = sr.ReadToEnd();
        //    }

        //    char[] charsToTrim = { ' ', '\t' };

        //    string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

        //    NewsInfo tempNewsInfo = null;

        //    foreach (string ln in lines)
        //    {
        //        ln.Trim(charsToTrim);

        //        if (ln.Length == 0) continue;

        //        if (ln[0] == ';')
        //        {
        //            // ignore comment line
        //        }
        //        else
        //        {
        //            tempNewsInfo = new NewsInfo(ln);
        //            if (tempNewsInfo.IsDecodeOK == true)
        //            {
        //                _items[tempNewsInfo.iDx] = tempNewsInfo;
        //            }
        //            else
        //            {
        //                Console.WriteLine(ln);
        //            }
        //        }
        //    }
        //}

        internal void ReadNews()
        {
            GameServer.msSqlConnect.ReadNews();
        }
         
        internal List<News> GetNewsList(Language language)
        {
            List<News> newList = new List<News>();
            DateTime startDate;
            DateTime endDate;
            string newsTitle = string.Empty;
            string newsUrl = string.Empty;
            foreach (var news in _items)
            {
                if (GlobalManager.Instance.ConvertToDateTime(news.startDate, out startDate) && GlobalManager.Instance.ConvertToDateTime(news.endDate, out endDate))
                {
                    if (startDate < DateTime.Now &&  DateTime.Now < endDate)
                    {
                        newsTitle = (LocalizationManager._dict.ContainsKey(news.title))?LocalizationManager.Instance.Get(news.title, language) : news.title;
                        switch (language)
                        {
                            case Language.English: newsUrl = news.urlEng; break;
                            case Language.Korean: newsUrl = news.urlKor; break;
                            case Language.Chinese: newsUrl = news.urlChi; break;
                            case Language.Japanese: newsUrl = news.urlJap; break;
                            case Language.French: newsUrl = news.urlFre; break;
                            case Language.German: newsUrl = news.urlGer; break;
                            default:break;
                        }
                        if (!string.IsNullOrEmpty(newsUrl))
                        {
                           newList.Add(new News(news.newsType, newsTitle, newsUrl));
                        }
                    }
                }
            }
            return newList;
        }
    }
}
