﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace MoonGlow
{
    public class CardManager : Singleton<CardManager>
    {
        private Dictionary<int, CardInfo> _items = new Dictionary<int, CardInfo>();
        private const string _FILE = @"Data/CardData.csv";
        public const int JOKER_INDEX = 999;//joker index is 999

        // Use this for initialization

        public CardManager()
        {
            LoadFromFile();
        }

        public void LoadFromFile()
        {

            //		TextAsset file = Resources.Load(_FILE) as TextAsset;
            string content = string.Empty;
            using (StreamReader sr = new StreamReader(_FILE))
            {
                content = sr.ReadToEnd();
            }


            char[] charsToTrim = { ' ', '\t' };

            //		Debug.Log ("LOADED CARD DATA FILE" + file.name);

            string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            CardInfo tempCardDataInfo = null;

            foreach (string ln in lines)
            {
                ln.Trim(charsToTrim);
                
                if (ln.Length == 0) continue;

                //			Debug.Log (ln);
                if (ln[0] == ';')
                {
                    // ignore comment line
                }
                else
                {
                    tempCardDataInfo = new CardInfo(ln);
                    try
                    {
                        _items[tempCardDataInfo.iDx] = tempCardDataInfo;
                    }
                    catch( Exception  ex)
                    {
                        Console.WriteLine(ln);
                        Console.WriteLine(ex.Message);
                    }
                }
            }

            //		foreach( var info in _deck)
            //		{
            //			Debug.Log ( info.ToString());
            //		}
        }

        public CardInfo Get(int iDx)
        {
            //if (_items.ContainsKey(iDx))
            //{
            //    return _items[iDx];
            //}
            //else
            //{
            //    return null;
            //}

            return _items.ContainsKey(iDx) ? _items[iDx] : null;
        }

        public Dictionary<int, CardInfo>.KeyCollection GetItemKeys { get { return _items.Keys; } }

    }
}