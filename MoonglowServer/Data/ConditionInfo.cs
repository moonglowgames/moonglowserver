﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Data
{
    public class ConditionInfo
    {
        //iDx, GameType, GoalHP, GoalResource, MoonDust, MoonStone, Mana, ZenStone, RodEnergy, MoonCrystal, StartHP, StartSP
        public readonly int iDx;
        public readonly GameType gameType;
        public readonly int goalHP;
        public readonly int goalResource;
        public readonly int moonDust;
        public readonly int moonStone;
        public readonly int mana;
        public readonly int zenStone;
        public readonly int rodEnergy;
        public readonly int moonCrystal;
        public readonly int startHP;
        public readonly int startSP;

        const int DECODE_PARSE_COUNT = 12;
        public char[] charsToTrim = { ' ', '\t' };
        public bool IsDecodeOK { get; private set; }

        public ConditionInfo(string cvsLineData_)
        {
            IsDecodeOK = false;

            if (cvsLineData_ != string.Empty)
            {
                cvsLineData_.Trim(charsToTrim);

                switch (cvsLineData_[0])
                {
                    case ';':
                        break;
                    default:
                        string[] parsings = cvsLineData_.Split(',');
                        int parscount = 0;
                        if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                        {
                            try
                            {
                                this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.gameType = (GameType)Enum.Parse(typeof(GameType),parsings[parscount++].Trim(charsToTrim));
                                this.goalHP = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.goalResource = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.moonDust = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.moonStone = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.mana = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.zenStone = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.rodEnergy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.moonCrystal = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.startHP = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.startSP = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));

                                IsDecodeOK = true;
                            }
                            catch (Exception /* e */ )
                            {
                                IsDecodeOK = false;
                            }
                        }
                        break;
                }
            }

        }
    }
}
