﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Data
{
    public class ItemManager : Singleton<ItemManager>
    {
        private Dictionary<int, ItemElementInfo> _items = new Dictionary<int, ItemElementInfo>();
        private const string _FILE = @"Data/ItemData.csv";
        // Use this for initialization

        public ItemManager()
        {
            LoadFromFile();
        }

        public void LoadFromFile()
        {
            string content = string.Empty;
            using (StreamReader sr = new StreamReader(_FILE))
            {
                content = sr.ReadToEnd();
            }

            char[] charsToTrim = { ' ', '\t' };

            string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            ItemElementInfo tempItemInfo = null;

            foreach (string ln in lines)
            {
                ln.Trim(charsToTrim);

                if (ln.Length == 0) continue;

                if (ln[0] == ';')
                {
                    // ignore comment line
                }
                else
                {
                    tempItemInfo = new ItemElementInfo(ln);
                    if (tempItemInfo.IsDecodeOK == true)
                    {
                        _items[tempItemInfo.iDx] = tempItemInfo;
                    }
                    else
                    {
                        Console.WriteLine(ln);
                    }
                }
            }
        }

        public ItemElementInfo Get(int iDx)
        {
            //if (_items.ContainsKey(iDx))
            //{
            //    return _items[iDx];
            //}
            //else
            //{
            //    return null;
            //}
            return _items.ContainsKey(iDx) ? _items[iDx] : null;
        }

        public void GetShopList(out List<ShopListInfo> shopList)
        {
            shopList = new List<ShopListInfo>();
            ItemElementInfo info = null;
            foreach (var itempair in _items)
            {
                info = itempair.Value;
                if (info.enabled == true && info.payType != PayType.None)
                {
                   shopList.Add(new ShopListInfo(info.tabType, info.iDx, info.payType, info.quantity, info.price, info.specialType, info.salePrice));
                }
            }
        }

        public  ItemElementInfo GetElement(int elementIdx_)
        {
            ItemElementInfo retvalue = null;

            ItemElementInfo info = null;
            foreach (var itempair in _items)
            {
                info = itempair.Value;
                if (info.iDx == elementIdx_)
                {
                    retvalue = info;
                    break;
                }
            }

            return retvalue;
        }

        internal void GetItemElementList(out List<ItemElementInfo> inventoryList, TabType tabType_)
        {
            inventoryList = new List<ItemElementInfo>();

            ItemElementInfo info = null;
            foreach (var itempair in _items)
            {
                info = itempair.Value;
                if (info.tabType == tabType_ && info.enabled == true)
                {
                    inventoryList.Add(info);
                }
            }
        }

        internal bool GetPetCardBaseIdx(int petIdx, out int petCardBaseIdx)
        {
            bool retvalue = false;
            petCardBaseIdx = 0;
            ItemElementInfo info = null;
            foreach (var itemPair in _items)
            {
                info = itemPair.Value;
                if (info.iDx == petIdx)
                {
                    petCardBaseIdx = info.baseCard;
                    retvalue = true;
                    break;
                }
            }
            return retvalue;
        }

        public bool GetPetIdxWithCardIdx(int cardIdx, out int petIdx)
        {
            bool retvalue = false;
            petIdx = 0;
            int baseCardIdx = cardIdx;
            ItemElementInfo info = null;
            foreach (var itemPair in _items)
            {
                info = itemPair.Value;
                if (info.baseCard == baseCardIdx)
                {
                    petIdx = info.iDx;
                    retvalue = true;
                    break;
                }
            }
            return retvalue;
        }

        internal TabType GetTabType(int itemIdx)
        {
            ItemElementInfo info = null;

            TabType tabType = TabType.Unknown;
            foreach (var infoPair in _items)
            {
                info = infoPair.Value;
                if (info.iDx == itemIdx)
                {
                    tabType = info.tabType;
                    break;
                }
            }
            return tabType;
        }

        internal void GetJokerFaceType(int itemIdx, out List<SpellType> spellTypeList)
        {
            int jokerFaceOption = 0;
            spellTypeList = new List<SpellType>();

            ItemElementInfo info = null;
            foreach (var infoPair in _items)
            {
                info = infoPair.Value;
                if (info.iDx == itemIdx)
                {
                    jokerFaceOption = info.option;
                    break;
                }
            }

            if ((jokerFaceOption & (int)JokerFaceOption.MoonDust) == (int)JokerFaceOption.MoonDust)
            {
                //option |= JokerFaceOption.MoonDust;
                spellTypeList.Add(SpellType.MoonDust);
            }
            if ((jokerFaceOption & (int)JokerFaceOption.Mana) == (int)JokerFaceOption.Mana)
            {
                //option |= JokerFaceOption.Mana;
                spellTypeList.Add(SpellType.Mana);
            }
            if ((jokerFaceOption & (int)JokerFaceOption.RodEnergy) == (int)JokerFaceOption.RodEnergy)
            {
                //option |= JokerFaceOption.RodEnergy;
                spellTypeList.Add(SpellType.RodEnergy);
            }
        }

        internal void GetJokerFaceType(int jokerFace, out object spellTypeList)
        {
            throw new NotImplementedException();
        }
    }
}
