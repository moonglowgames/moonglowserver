﻿namespace MoonGlow.Data
{
    public class ChanelInfo
    {
        //;idx	chanel	name	description	minLevel	maxLevel	startDate	endDate
        public int chanelIdx;
        public ChanelType chanelType;
        public string name;
        public string description;
        public int minLevel;
        public int maxLevel;
        public string startDate;
        public string endDate;
        public int maxCount;
        public int currentCount;

        /*const int DECODE_PARSE_COUNT = 8;
        public char[] charsToTrim = { ' ', '\t' };

        public bool Decode(string cvsLineData_)
        {
            bool retvalue = false;

            if (cvsLineData_ != string.Empty)
            {
                cvsLineData_.Trim(charsToTrim);

                switch (cvsLineData_[0])
                {
                    case ';':
                        break;
                    default:
                        string[] parsings = cvsLineData_.Split(',');
                        int parscount = 0;
                        if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                        {
                            try
                            {
                                //;idx chanel name description minLevel maxLevel startDate endDate
                                this.chanelIdx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.chanelType = (ChanelType)Enum.Parse(typeof(ChanelType),parsings[parscount++].Trim(charsToTrim));
                                this.name = parsings[parscount++].Trim(charsToTrim);
                                this.description = parsings[parscount++].Trim(charsToTrim);
                                this.minLevel = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.maxLevel = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.startDate = parsings[parscount++].Trim(charsToTrim);
                                this.endDate = parsings[parscount++].Trim(charsToTrim);

                                retvalue = true;
                            }
                            catch (Exception  e )
                            {
                                retvalue = false;
                            }
                        }
                        break;
                }
            }
            return retvalue;
        }*/

    }
}
