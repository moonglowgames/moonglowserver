﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MoonGlow.Data
{
    public class ConditionManager : Singleton<ConditionManager> 
    {
        private Dictionary<int, ConditionInfo> _items = new Dictionary<int, ConditionInfo>();
	    private const string _FILE = @"Data/WinCondition.csv";
	    // Use this for initialization

        public ConditionManager()
        {
            LoadFromFile();
        }

	    public void LoadFromFile(){

            string content =  string.Empty;
            using( StreamReader sr = new StreamReader(_FILE ))
            {
                content = sr.ReadToEnd();
            }


		    char[] charsToTrim = { ' ','\t'};
		
		
		    string[] lines = content.Split (new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            ConditionInfo tempConditionInfo = null;

		    foreach (string ln in lines) 
		    {	
			    ln.Trim(charsToTrim);

                if (ln.Length == 0) continue;

			    if( ln[0] ==';')
			    {
				    // ignore comment line
			    }
                else
                {
                    tempConditionInfo = new ConditionInfo(ln);
                    //tempConditionInfo.Decode(ln);
                    if (tempConditionInfo.IsDecodeOK) _items[tempConditionInfo.iDx] = tempConditionInfo;
                    else Console.WriteLine(ln);
			    }
		    }
	    }

        public ConditionInfo Get(int iDx)
        {
		    //if (_items.ContainsKey(iDx))
		    //{
			   // return _items[iDx];
		    //}else
		    //{
			   // return null;
		    //}
            return _items.ContainsKey(iDx) ? _items[iDx] : null;
        }
    }
}
