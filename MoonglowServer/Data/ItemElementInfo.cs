﻿using System;

namespace MoonGlow
{
    public class ItemElementInfo
    {
        public readonly int iDx;
        public readonly TabType tabType;
        public readonly bool enabled;
        public readonly int rqrLevel;
        public readonly string name;
        public readonly PayType payType;
        public readonly int quantity;
        public readonly double price;
        public readonly SpecialType specialType;
        public readonly double salePrice;
        public readonly int baseCard;
        public readonly int option;//use for joker face
        public readonly string description;


        const int DECODE_PARSE_COUNT = 12;
        public char[] charsToTrim = { ' ', '\t' };

        public bool IsDecodeOK { get; private set; }
         
        public ItemElementInfo( string cvsLineData_ )
        {
             IsDecodeOK = false;

            if (cvsLineData_ != string.Empty)
            {
                cvsLineData_.Trim(charsToTrim);

                switch (cvsLineData_[0])
                {
                    case ';':
                        break;
                    default:
                        string[] parsings = cvsLineData_.Split(',');
                        int parscount = 0;
                        if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                        {
                            try
                            {
                                this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.tabType = (TabType)Enum.Parse(typeof(TabType), parsings[parscount++].Trim(charsToTrim));
                                this.enabled = (Convert.ToInt32(parsings[parscount++].Trim(charsToTrim)) == 1)? true:false;
                                this.rqrLevel = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.name = parsings[parscount++].Trim(charsToTrim);
                                this.payType = (PayType)Enum.Parse(typeof(PayType),parsings[parscount++].Trim(charsToTrim));
                                this.quantity = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.price = Convert.ToDouble(parsings[parscount++].Trim(charsToTrim));
                                this.specialType = (SpecialType)Enum.Parse(typeof(SpecialType),parsings[parscount++].Trim(charsToTrim));
                                this.salePrice = Convert.ToDouble(parsings[parscount++].Trim(charsToTrim));
                                this.baseCard = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.option = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.description = parsings[parscount++].Trim(charsToTrim);

                                IsDecodeOK = true;
                            }
                            catch (Exception /* e */ )
                            {
                                IsDecodeOK = false;
                            }
                        }
                        break;
                }
            }

        }

    }
    
}