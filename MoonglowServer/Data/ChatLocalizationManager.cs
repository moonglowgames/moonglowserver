﻿using System;
using System.Collections.Generic;
using System.IO;

public class ChatLocalizationManager : Singleton<ChatLocalizationManager>
{
    private Dictionary<int, string[]> _dict = new Dictionary<int, string[]>();
    private const string _FILE = @"Data/ChatLocalization.csv";
    private const int DECODE_PARSE_COUNT = 3;

    public ChatLocalizationManager()
    {
        LoadFromFile();
    }

    void LoadFromFile()
    {
        string content = string.Empty;
        using (StreamReader sr = new StreamReader(_FILE))
        {
            content = sr.ReadToEnd();
        }

        string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        char[] charsToTrim = { ' ', '\t' };

        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);

            if (ln.Length == 0) continue;
            if (ln[0] == ';') { continue; }
            else
            {
                if (ln != string.Empty)
                {
                    string[] parsings = ln.Split(',');
                    try
                    {
                        if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                        {
                            _dict[Convert.ToInt32(parsings[0].Trim(charsToTrim))] = new string[DECODE_PARSE_COUNT - 1];
                            for (int i = 0; i < DECODE_PARSE_COUNT - 1; i++)
                            {
                                _dict[Convert.ToInt32(parsings[0].Trim(charsToTrim))][i] = parsings[i + 1].Trim(charsToTrim);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine(ln);
                        //Console.WriteLine(ex.Message);
                        if (MoonGlow.GameServer._logger.IsErrorEnabled)
                        {
                            string log = "ChatLocalization LoadFromFile exception: (" + ex.Message +  ")";
                            MoonGlow.GameServer._logger.Error(log);
                        }
                    }

                }
            }
        }
    }

    internal string GetPhrase(int iconIdx, Language language)
    {
        string retvalue = string.Empty;
        int setCount = 0;
        int currentSet = 0;
        foreach (var key in _dict.Keys)
        {
            currentSet = key / 100;
            if (setCount < currentSet)
            {
                setCount = currentSet;
            }
        }

        Random rand = new Random();
        int randKey = rand.Next(1, setCount + 1) * 100 + iconIdx;

        retvalue = Get(randKey, language);
        return retvalue;
    }

    public string Get(int key, Language lang)
    {
        if (_dict.ContainsKey(key))
        {
            return _dict[key][(int)lang];
        }
        else
        {
            return null;
        }
    }
}
