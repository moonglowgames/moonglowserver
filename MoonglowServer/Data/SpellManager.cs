﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MoonGlow.Data
{
    public class SpellManager : Singleton<SpellManager>
    {
        private Dictionary<int, SpellInfo> _items = new Dictionary<int, SpellInfo>();
        private const string _FILE = @"Data/JokerData.csv";
        // Use this for initialization

        public SpellManager()
        {
            LoadFromFile();
        }

        public void LoadFromFile()
        {
            string content = string.Empty;
            using (StreamReader sr = new StreamReader(_FILE))
            {
                content = sr.ReadToEnd();
            }


            char[] charsToTrim = { ' ', '\t' };


            string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            SpellInfo tempJokerInfo = null;

            foreach (string ln in lines)
            {
                ln.Trim(charsToTrim);

                if (ln.Length == 0) continue;

                if (ln[0] == ';')
                {
                    // ignore comment line
                }
                else
                {
                    tempJokerInfo = new SpellInfo(ln);
                    if (tempJokerInfo.IsDecodeOK) _items[tempJokerInfo.iDx] = tempJokerInfo;
                    else Console.WriteLine(ln);
                }
            }
        }
        
        internal SpellInfo GetSpellInfo(int spellIdx)
        {
            SpellInfo retvalue = null;
            SpellInfo info = null;
            foreach (var infoPair in _items)
            {
                info = infoPair.Value;
                if (info.iDx == spellIdx)
                {
                    retvalue = info;
                    break;
                }
            }
            return retvalue;
        }


    }
}
