﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MoonGlow.Data
{
    class LevelExpManager : Singleton<LevelExpManager> 
    {
        private Dictionary<int, LevelExpInfo> _items = new Dictionary<int, LevelExpInfo>();
	    private const string _FILE = @"Data/LevelExp.csv";
	    // Use this for initialization

        public LevelExpManager()
        {
            LoadFromFile();
        }

	    public void LoadFromFile(){

            string content =  string.Empty;
            using( StreamReader sr = new StreamReader(_FILE ))
            {
                content = sr.ReadToEnd();
            }


		    char[] charsToTrim = { ' ','\t'};
		
		
		    string[] lines = content.Split (new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            LevelExpInfo tempLevelExpInfo = null;

		    foreach (string ln in lines) 
		    {	
			    ln.Trim(charsToTrim);

                if (ln.Length == 0) continue;

			    if( ln[0] ==';')
			    {
				    // ignore comment line
			    }
                else
                {
                    tempLevelExpInfo = new LevelExpInfo(ln);
                    if (tempLevelExpInfo.IsDecodeOK == true)
                    {
                        _items[tempLevelExpInfo.iDx] = tempLevelExpInfo;
                    }
                    else 
                    {
                        Console.WriteLine(ln);
                    }
			    }
		    }
	    }

        public LevelExpInfo Get(int iDx)
        {
            //if (_items.ContainsKey(iDx))
            //{
            // return _items[iDx];
            //}else
            //{
            // return null;
            //}
            return _items.ContainsKey(iDx) ? _items[iDx] : null;
        }

        internal int GetDownLevelExp(int currentLevel)
        {
            int retvalue = 0;

            LevelExpInfo info = null;
            foreach (var infoPair in _items)
            {
                info = infoPair.Value;
                if (info.level == currentLevel)
                {
                    retvalue = info.minExp;
                    break;
                }              
            }

            return retvalue;
        }

        internal int GetUpLevelExp(int currentLevel)
        {
            int retvalue = 0;

            LevelExpInfo info = null;
            foreach (var infoPair in _items)
            {
                info = infoPair.Value;
                if (info.level == currentLevel)
                {
                    retvalue = info.maxExp;
                    break;
                }
            }

            return retvalue;
        }

        //public int GetNextLevelExp(int currentLevel)
        //{
        //    int retvalue = 0;

        //    int nextlevel = currentLevel + 1;
        //    if (_items.ContainsKey(nextlevel))
        //    {
        //        var info = _items[nextlevel];
        //        retvalue = info.exp;
        //    }

        //    return retvalue;
        //}

        //internal int GetCurrentLevelExp(int currentLevel)
        //{
        //    int retvalue = 0;

        //    if (_items.ContainsKey(currentLevel))
        //    {
        //        var info = _items[currentLevel];
        //        retvalue = info.exp;
        //    }

        //    return retvalue;
        //}

    }
}
