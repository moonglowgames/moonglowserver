﻿using System;

namespace MoonGlow.Data
{
    public class ChatInfo
    {
        //iDX, type, content
        public readonly int iDx;
        public readonly ChatType type;
        public readonly string content;

        const int DECODE_PARSE_COUNT = 3;
        public char[] charsToTrim = { ' ', '\t' };

        public bool IsDecodeOK { get; private set; }

        public ChatInfo(string cvsLineData_) 
        {
            IsDecodeOK = false;
            if (cvsLineData_ != string.Empty)
            {
                cvsLineData_.Trim(charsToTrim);

                switch (cvsLineData_[0])
                {
                    case ';':
                        break;
                    default:
                        string[] parsings = cvsLineData_.Split(',');
                        int parscount = 0;
                        if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                        {
                            try
                            {
                                this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.type= (ChatType)Enum.Parse(typeof(ChatType), parsings[parscount++].Trim(charsToTrim));
                                this.content = parsings[parscount++].Trim(charsToTrim);

                                IsDecodeOK = true;
                            }
                            catch (Exception /* e */ )
                            {
                                IsDecodeOK = false;
                            }
                        }
                        break;
                }
            }
        }

    }
}
