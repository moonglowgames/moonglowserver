﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Data
{
    class ChannelRewardManager : Singleton<ChannelRewardManager>
    {
        public static List<ChanelRewardInfo> _items = new List<ChanelRewardInfo>();

        /*private Dictionary<int, ChanelRewardInfo> _items = new Dictionary<int, ChanelRewardInfo>();
        private const string _FILE = @"Data/ChanelReward.csv";
        // Use this for initialization

        public ChanelRewardManager()
        {
           LoadFromFile();
        }

        public void LoadFromFile()
        {
           string content = string.Empty;
           using (StreamReader sr = new StreamReader(_FILE))
           {
               content = sr.ReadToEnd();
           }

           char[] charsToTrim = { ' ', '\t' };

           string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

           ChanelRewardInfo tmpChanelRewardInfo = null;

           foreach (string ln in lines)
           {
               ln.Trim(charsToTrim);

               if (ln.Length == 0) continue;

               if (ln[0] == ';')
               {
                   // ignore comment line
               }
               else
               {
                   tmpChanelRewardInfo = new ChanelRewardInfo();
                   if (tmpChanelRewardInfo.Decode(ln) == true)
                   {
                       _items[tmpChanelRewardInfo.iDx] = tmpChanelRewardInfo;
                   }
                   else
                   {
                       Console.WriteLine(ln);
                   }
               }
           }
        }

        public ChanelRewardInfo Get(int iDx)
        {
           if (_items.ContainsKey(iDx))
           {
               return _items[iDx];
           }
           else
           {
               return null;
           }
        }*/

        internal void ReadChannelRewardInfo()
        {
            GameServer.msSqlConnect.ReadChannelRewardInfo();
        }

        public void GetRewardList(int channelIdx, out List<Reward> rewardList)
        {
            ChanelType chanelType = ChannelManager.Instance.GetChannelType(channelIdx);
            rewardList = new List<Reward>();
            //ChanelRewardInfo info = null;
            foreach (var info in _items)
            {
                //info = infoPair.Value;
                if (info.rankIdx/1000 == (int)chanelType/*channelIdx/100*/) //confirm channelIdx/100 same with channelType
                {
                    rewardList.Add(new Reward(info.rank, info.rankType, info.itemIdx1, info.itemCount1, info.itemIdx2, info.itemCount2, info.itemIdx3, info.itemCount3));
                }
            }
        }

        internal void CheckRankItemExist(int chanelIdx, int currentCount, int currentPercent, out bool itemExist)
        {
            itemExist = false;

            List<Reward> rewardList = null;
            GetRewardList(chanelIdx, out rewardList);
            for (int i = 0; i < rewardList.Count; i++)
            {
                if (rewardList[i].rankType == RankType.Count)
                {
                    if (rewardList[i].rank == currentCount) { itemExist = true; }
                }

                if (itemExist == false && rewardList[i].rankType == RankType.Percent)
                {
                    // if (i < rewardList.Count - 1)
                    if ( i > 0 && rewardList[i - 1].rankType == RankType.Percent)
                    {
                        if (currentPercent <= rewardList[i].rank && currentPercent > rewardList[i - 1].rank) { itemExist = true; }
                    }
                    else if (i > 0 && rewardList[i - 1].rankType == RankType.Count || i == 0)
                    {
                        if (currentPercent <= rewardList[i].rank) { itemExist = true; }
                    }
                }

                if (itemExist == true) { break; }
            }
        }

        internal void GetItemList(int chanelIdx, int rankCount, int rankPercent, out List<ItemInfo> chanelItemList)
        {
            chanelItemList = new List<ItemInfo>();
            List<Reward> rewardList = null;
            GetRewardList(chanelIdx, out rewardList);
            for (int i = 0; i < rewardList.Count; i++)
            {
                if (rewardList[i].rankType == RankType.Count)
                {
                    if (rewardList[i].rank == rankCount)
                    {
                        if(rewardList[i].itemIdx1 != 0)
                            chanelItemList.Add(new ItemInfo(rewardList[i].itemIdx1, rewardList[i].itemCount1));
                        if(rewardList[i].itemIdx2 != 0)
                            chanelItemList.Add(new ItemInfo(rewardList[i].itemIdx2, rewardList[i].itemCount2));
                        if(rewardList[i].itemIdx3 != 0)
                            chanelItemList.Add(new ItemInfo(rewardList[i].itemIdx3, rewardList[i].itemCount3));
                    }
                }
                else if(rewardList[i].rankType == RankType.Percent)
                {
                    // if (i < rewardList.Count - 1)
                    if (i > 0 && rewardList[i - 1].rankType == RankType.Percent)
                    {
                        if (rankPercent <= rewardList[i].rank && rankPercent > rewardList[i - 1].rank)
                        {
                            if (rewardList[i].itemIdx1 != 0)
                                chanelItemList.Add(new ItemInfo(rewardList[i].itemIdx1, rewardList[i].itemCount1));
                            if (rewardList[i].itemIdx2 != 0)
                                chanelItemList.Add(new ItemInfo(rewardList[i].itemIdx2, rewardList[i].itemCount2));
                            if (rewardList[i].itemIdx3 != 0)
                                chanelItemList.Add(new ItemInfo(rewardList[i].itemIdx3, rewardList[i].itemCount3));
                        }
                    }
                    else if ((i > 0 && rewardList[i - 1].rankType == RankType.Count) || i == 0)
                    {
                        if (rankPercent <= rewardList[i].rank)
                        {
                            if(rewardList[i].itemIdx1 != 0)
                            chanelItemList.Add(new ItemInfo(rewardList[i].itemIdx1, rewardList[i].itemCount1));
                        if(rewardList[i].itemIdx2 != 0)
                            chanelItemList.Add(new ItemInfo(rewardList[i].itemIdx2, rewardList[i].itemCount2));
                        if(rewardList[i].itemIdx3 != 0)
                            chanelItemList.Add(new ItemInfo(rewardList[i].itemIdx3, rewardList[i].itemCount3));
                        }
                    }
                }
            }
        }
    }

}
