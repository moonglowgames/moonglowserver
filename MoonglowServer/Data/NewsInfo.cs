﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Data
{
    class NewsInfo
    {
        //public readonly int iDx;
        public readonly int newsType;
        public readonly string title;
        public readonly string startDate;
        public readonly string endDate;
        public readonly string urlEng;
        public readonly string urlKor;
        public readonly string urlChi;
        public readonly string urlJap;
        public readonly string urlFre;
        public readonly string urlGer;

        //public readonly bool enable;

        //const int DECODE_PARSE_COUNT = 7;
        //public char[] charsToTrim = { ' ', '\t' };

        //public bool IsDecodeOK { get; private set; }
        //public NewsInfo (string cvsLineData_)
        //{
        //    IsDecodeOK = false;

        //    if (cvsLineData_ != string.Empty)
        //    {
        //        cvsLineData_.Trim(charsToTrim);

        //        switch (cvsLineData_[0])
        //        {
        //            case ';':
        //                break;
        //            default:
        //                string[] parsings = cvsLineData_.Split(',');
        //                int parscount = 0;
        //                if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
        //                {
        //                    try
        //                    {
        //                        this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
        //                        this.newsType = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
        //                        this.title = parsings[parscount++].Trim(charsToTrim);
        //                        this.url = parsings[parscount++].Trim(charsToTrim);
        //                        this.startDate = parsings[parscount++].Trim(charsToTrim);
        //                        this.endDate = parsings[parscount++].Trim(charsToTrim);
        //                        this.enable = (parsings[parscount++].Trim(charsToTrim) == "1")?true:false;

        //                        IsDecodeOK = true;
        //                    }
        //                    catch (Exception  e  )
        //                    {
        //                        Console.WriteLine("!!!Exception Error: " + e.Message);
        //                        IsDecodeOK = false;
        //                    }
        //                }
        //                break;
        //        }
        //    }
        //}

        public NewsInfo(int newsType_, string title_, string startDate_, string endDate_, string urlEng_, string urlKor_, string urlChi_, string urlJap_, string urlFre_, string urlGer_)
        {
            newsType = newsType_;
            title = title_;
            //url = url_;
            startDate = startDate_;
            endDate = endDate_;

            urlEng = urlEng_;
            urlKor = urlKor_;
            urlChi = urlChi_;
            urlJap = urlJap_;
            urlFre = urlFre_;
            urlGer = urlGer_;
        }
    }
}
