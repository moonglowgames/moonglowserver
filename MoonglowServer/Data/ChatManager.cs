﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MoonGlow.Data
{
    class ChatManager : Singleton<ChatManager>
    {
        private Dictionary<int, ChatInfo> _items = new Dictionary<int, ChatInfo>();
        private const string _FILE = @"Data/ChatData.csv";
        // Use this for initialization

        public ChatManager()
        {
            LoadFromFile();
        }

        public void LoadFromFile()
        {

            string content = string.Empty;
            using (StreamReader sr = new StreamReader(_FILE))
            {
                content = sr.ReadToEnd();
            }


            char[] charsToTrim = { ' ', '\t' };


            string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            ChatInfo tempChatInfo = null;

            foreach (string ln in lines)
            {
                ln.Trim(charsToTrim);

                if (ln.Length == 0) continue;

                if (ln[0] == ';')
                {
                    // ignore comment line
                }
                else
                {
                    tempChatInfo = new ChatInfo(ln);
                    if (tempChatInfo.IsDecodeOK == true)
                    {
                        _items[tempChatInfo.iDx] = tempChatInfo;
                    }
                    else
                    {
                        Console.WriteLine(ln);
                    }
                }
            }
        }

        internal ChatType GetChatType(int chatIdx)
        {
            ChatType retvalue = ChatType.Emoji;
            ChatInfo info = null;
            foreach (var infoPair in _items)
            {
                info = infoPair.Value;
                if (info.iDx == chatIdx)
                {
                    retvalue = info.type;
                    break;
                }
            }
            return retvalue;
        }
    }
}
