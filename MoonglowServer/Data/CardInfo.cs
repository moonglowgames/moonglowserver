﻿using System;

namespace MoonGlow
{
    public struct CardAbility
    {
        // enable card use
        public int priceMoonDustRed;
        public int priceRodEnergyGreen;
        public int priceManaBlue;
   
        // increase or decrease my HP    
        public int myHP;
        // increase or decrease target HP direct without decrese SP
        public int HPEnemy;
        // increase or decrease my SP
        public int SP;
        // increase or decrease target SP
        public int SPEnemy;
        
        // 1. decrease SP , 2. decrease HP
        public int damage;
        // 1. decrease SP self , 2 decrease HP self
        public int damageMe;
        
        // add or remove elements to me
        public int moonDustRed;
        public int moonStoneRedplus;
        public int manaBlue;
        public int zenStoneBlueplus;
        public int rodEnergyGreen;
        public int moonCrystalGreenplus;
        
        // add or remove enemy's elements
        public int moonDustRedEnemy;
        public int moonStoneRedplusEnemy;
        public int manaBlueEnemy;
        public int zenStoneBlueplusEnemy;
        public int rodEnergyGreenEnemy;
        public int moonCrystalGreenplusEnemy;
        
        // play once more
        public int playAgain;

    }

    public class CardInfo
    {
        public readonly int iDx;
        public readonly string name;
        public readonly CardAbility ability;
        public readonly int priceColor;
        public readonly string artName;
        public readonly string priceTagName;
        public readonly string miniCardName;

        //special and useCardType
        public readonly CardUseType cardUseType;
        public readonly int special;
        public readonly int myEffectIdx;
        public readonly int targetEffectIdx;
        public readonly int globalEffectIdx;

        const int DECODE_PARSE_COUNT = 33;
        public char[] charsToTrim = { ' ', '\t' };

        public bool IsDecodeOK { get; private set; }

        public CardInfo (string cvsLineData_)
        {
            IsDecodeOK = false;

            if (cvsLineData_ != string.Empty)
            {
                cvsLineData_.Trim(charsToTrim);

                switch (cvsLineData_[0])
                {
                    case ';':
                        break;
                    default:
                        string[] parsings = cvsLineData_.Split(',');
                        int parscount = 0;
                        if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                        {
                            try
                            {
                                this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.name = parsings[parscount++].Trim(charsToTrim);
                                this.ability.priceMoonDustRed = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.priceRodEnergyGreen = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.priceManaBlue = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.priceColor = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.myHP = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.HPEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.SP = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.SPEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.damage = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.damageMe = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.moonDustRed = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.moonStoneRedplus = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.manaBlue = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.zenStoneBlueplus = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.rodEnergyGreen = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.moonCrystalGreenplus = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.moonDustRedEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.moonStoneRedplusEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.manaBlueEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.zenStoneBlueplusEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.rodEnergyGreenEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.moonCrystalGreenplusEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.ability.playAgain = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.artName = parsings[parscount++].Trim(charsToTrim);
                                this.priceTagName = parsings[parscount++].Trim(charsToTrim);
                                this.miniCardName = parsings[parscount++].Trim(charsToTrim);
                                this.cardUseType = (CardUseType)Enum.Parse(typeof(CardUseType),parsings[parscount++].Trim(charsToTrim));
                                this.special = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.myEffectIdx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.targetEffectIdx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                                this.globalEffectIdx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));

                                IsDecodeOK = true;
                            }
                            catch (Exception /* e */ )
                            {
                                IsDecodeOK = false;
                            }
                        }
                        break;
                }
            }
        }

        public override string ToString()
        {
            return string.Format(iDx.ToString() + "," + name);
        }

        public int GetPrice()
        {
            switch (priceColor)
            {
                case 0:
                    {
                        return ability.priceMoonDustRed;
                    }
                case 1:
                    {
                        return ability.priceRodEnergyGreen;
                    }
                case 2:
                    {
                        return ability.priceManaBlue;
                    }
                default:
                    {
                        //Debug.Log("GetPrice mismatch price color CardInfo.cs");
                        return 0;
                    }
            }
        }

        public string GetDiscription()
        {
            string returnValue = string.Empty; //"normal" + iDx.ToString() + mana.ToString();
            //normal card
            if (special == 0)
            {
                if (ability.myHP != 0)
                {
                    returnValue += CreateDiscription(ability.myHP, "to Hit Point");
                }
                if (ability.damage != 0)
                {
                    returnValue += CreateDiscription(ability.damage, "damage");
                }
                if (ability.manaBlue != 0)
                {
                    returnValue += CreateDiscription(ability.manaBlue, "mana");
                }
            }
            else
            {
                returnValue = "special";
            }

            return returnValue;
        }

        string CreateDiscription(int amount, string type)
        {//, string color){
            return ((amount > 0) ? "+" + amount.ToString() : amount.ToString()) + " " + type + "\r\n";
        }
    }

}