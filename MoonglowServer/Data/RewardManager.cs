﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MoonGlow.Data
{
    class RewardManager : Singleton<RewardManager>
    {
        private Dictionary<int, RewardInfo> _items = new Dictionary<int, RewardInfo>();
	    private const string _FILE = @"Data/Reward.csv";
	    // Use this for initialization

        public RewardManager()
        {
            LoadFromFile();
        }

	    public void LoadFromFile(){

            string content =  string.Empty;
            using( StreamReader sr = new StreamReader(_FILE ))
            {
                content = sr.ReadToEnd();
            }


		    char[] charsToTrim = { ' ','\t'};
		
		
		    string[] lines = content.Split (new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            RewardInfo tempRewardInfo = null;

		    foreach (string ln in lines) 
		    {	
			    ln.Trim(charsToTrim);

                if (ln.Length == 0) continue;

			    if( ln[0] ==';')
			    {
				    // ignore comment line
			    }
                else
                {
                    tempRewardInfo = new RewardInfo(ln);
                    //tempRewardInfo.Decode(ln);
                    if (tempRewardInfo.IsDecodeOK) _items[tempRewardInfo.iDx] = tempRewardInfo;
                    else Console.WriteLine(ln);
			    }
		    }
	    }

        public RewardInfo Get(int iDx)
        {
            //if (_items.ContainsKey(iDx))
            //{
            // return _items[iDx];
            //}else
            //{
            // return null;
            //}
            return _items.ContainsKey(iDx) ? _items[iDx] : null;
        }

        internal int GetWinExp(GameType gameType_, PlayMode playMode_)
        {
            int retvalue = 0;
            RewardInfo info = null;
            foreach ( var itempair in _items)
            {
                info = itempair.Value;
                if (info.gameType == gameType_ && info.playMode == playMode_)
                {
                    retvalue = info.winExp;
                    break;
                }
            }

            return retvalue;
        }

        internal int GetWinCoin(GameType gameType_, PlayMode playMode_)
        {
            int retvalue = 0;
            RewardInfo info = null;
            foreach (var itempair in _items)
            {
                info = itempair.Value;
                if (info.gameType == gameType_ && info.playMode == playMode_)
                {
                    retvalue = info.winCoin;
                    break;
                }
            }
            return retvalue;
        }

        internal int GetLoseExp(GameType gameType_, PlayMode playMode_)
        {
            int retvalue = 0;
            RewardInfo info = null;
            foreach (var itempair in _items)
            {
                info = itempair.Value;
                if (info.gameType == gameType_ && info.playMode == playMode_)
                {
                    retvalue = info.loseExp;
                    break;
                }
            }
            return retvalue;
        }

        internal int GetLoseCoin(GameType gameType_, PlayMode playMode_)
        {
            int retvalue = 0;
            RewardInfo info = null;
            foreach (var itempair in _items)
            {
                info = itempair.Value;
                if (info.gameType == gameType_ && info.playMode == playMode_)
                {
                    retvalue = info.loseCoin;
                    break;
                }
            }
            return retvalue;
        }

        internal int GetRankPoint(GameType gameType_, PlayMode playMode_)
        {
            int retvalue = 0;
            RewardInfo info = null;
            foreach (var itempair in _items)
            {
                info = itempair.Value;
                if (info.gameType == gameType_ && info.playMode == playMode_)
                {
                    retvalue = info.rankPoint;
                    break;
                }
            }
            return retvalue;
        }
    }
}
