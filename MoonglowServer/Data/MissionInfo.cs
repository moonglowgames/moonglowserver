﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Data
{
    public class MissionInfo
    {
        //;idx missionType achieveType title description startDate timeSpan expireDate targetValue itemIdx1 itemCount1 itemIdx2 itemCount2 disable
        public readonly int iDx;
        public readonly AchieveType achieveType;
        public readonly MissionType missionType;
        public readonly string title;
        public readonly string description;
        public readonly string startDate;
        public readonly string timeSpan;
        public readonly string expireDate;
        public readonly int targetValue;
        public readonly int itemIdx1;
        public readonly int itemCount1;
        public readonly int itemIdx2;
        public readonly int itemCount2;
        public readonly GameType gameType;
        //public readonly bool disable;

        //const int DECODE_PARSE_COUNT = 15;
        //public char[] charsToTrim = { ' ', '\t' };

        //public bool IsDecodeOK { get; private set; }

        //public MissionInfo (string cvsLineData_)
        //{
        //    IsDecodeOK = false;

        //    if (cvsLineData_ != string.Empty)
        //    {
        //        cvsLineData_.Trim(charsToTrim);

        //        switch (cvsLineData_[0])
        //        {
        //            case ';':
        //                break;
        //            default:
        //                string[] parsings = cvsLineData_.Split(',');
        //                int parscount = 0;
        //                if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
        //                {
        //                    try
        //                    {
        //                        this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
        //                        this.achieveType = (AchieveType)Enum.Parse(typeof(AchieveType), parsings[parscount++].Trim(charsToTrim));
        //                        this.missionType = (MissionType)Enum.Parse(typeof(MissionType), parsings[parscount++].Trim(charsToTrim));
        //                        this.title = parsings[parscount++].Trim(charsToTrim);
        //                        this.description = parsings[parscount++].Trim(charsToTrim);
        //                        this.startDate = parsings[parscount++].Trim(charsToTrim);
        //                        this.timeSpan = parsings[parscount++].Trim(charsToTrim);
        //                        this.expireDate = parsings[parscount++].Trim(charsToTrim);
        //                        this.targetValue = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
        //                        this.itemIdx1 = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
        //                        this.itemCount1 = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
        //                        this.itemIdx2 = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
        //                        this.itemCount2 = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
        //                        this.gameType = (GameType)Enum.Parse(typeof(GameType), parsings[parscount++].Trim(charsToTrim));
        //                        this.disable = (Convert.ToInt32(parsings[parscount++].Trim(charsToTrim)) == 0)?false:true;

        //                        IsDecodeOK = true;
        //                    }
        //                    catch (Exception /* e */ )
        //                    {
        //                        IsDecodeOK = false;
        //                    }
        //                }
        //                break;
        //        }
        //    }
        //}

        public MissionInfo(int missionIdx_, AchieveType achieveType_, MissionType missionType_, string title_, string description_, string startDate_, string timeSpan_, string expireDate_, int targetValue_, int itemIdx1_, int itemCount1_, int itemIdx2_, int itemCount2_, GameType gameType_)
        {
            iDx = missionIdx_;
            achieveType = achieveType_;
            missionType = missionType_;
            title = title_;
            description = description_;
            startDate = startDate_;
            timeSpan = timeSpan_;
            expireDate = expireDate_;
            targetValue = targetValue_;
            itemIdx1 = itemIdx1_;
            itemCount1 = itemCount1_;
            itemIdx2 = itemIdx2_;
            itemCount2 = itemCount2_;
            gameType = gameType_;
        }

    }
}

