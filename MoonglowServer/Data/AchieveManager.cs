﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Data
{
    class AchieveManager : Singleton<AchieveManager>
    {
        private Dictionary<int, AchieveInfo> _items = new Dictionary<int, AchieveInfo>();
        private const string _FILE = @"Data/AchieveData.csv";
        // Use this for initialization

        public AchieveManager()
        {
            LoadFromFile();
        }

        public void LoadFromFile()
        {

            string content = string.Empty;
            using (StreamReader sr = new StreamReader(_FILE))
            {
                content = sr.ReadToEnd();
            }


            char[] charsToTrim = { ' ', '\t' };


            string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            AchieveInfo tempLevelExpInfo = null;

            foreach (string ln in lines)
            {
                ln.Trim(charsToTrim);

                if (ln.Length == 0) continue;

                if (ln[0] == ';')
                {
                    // ignore comment line
                }
                else
                {
                    tempLevelExpInfo = new AchieveInfo(ln);
                    if (tempLevelExpInfo.IsDecodeOK == true)
                    {
                        _items[tempLevelExpInfo.iDx] = tempLevelExpInfo;
                    }
                    else
                    {
                        Console.WriteLine(ln);
                    }
                }
            }
        }

        public AchieveInfo Get(int iDx)
        {
            return  _items.ContainsKey(iDx)? _items[iDx]: null;
        }

        public Dictionary<int,AchieveInfo> GetAchieveListDic()
        {
            Dictionary<int, AchieveInfo> achieveListDic = new Dictionary<int,AchieveInfo>();

            AchieveInfo info = null;
            Dictionary<AchieveType, int> tmpIdxDic = new Dictionary<AchieveType, int>();
            foreach(var infoPair in _items)
            {
                info = infoPair.Value;
                if(info.disable == true)
                {
                    if (tmpIdxDic.ContainsKey(info.achieveType))
                    {
                        if (info.iDx < tmpIdxDic[info.achieveType])
                        {
                            tmpIdxDic[info.achieveType] = info.iDx;
                        }
                    }
                    else
                    {
                        tmpIdxDic.Add(info.achieveType, info.iDx); 
                    }
                }
            }

            foreach (var infoPair in _items)
            {
                info = infoPair.Value;
                if (tmpIdxDic.ContainsKey(info.achieveType))
                {
                    foreach (var iDxPair in tmpIdxDic)
                    {
                        if (info.achieveType == iDxPair.Key)
                        {
                            if (info.iDx < iDxPair.Value)
                            {
                                achieveListDic.Add(info.iDx,info);
                            }
                            break;
                        }
                    }
                }
                else
                {
                    achieveListDic.Add(info.iDx,info); 
                }
            }
            return achieveListDic;
        }

        internal AchieveType GetAchieveType(int achieveIdx)
        {
            AchieveType retvalue = AchieveType.Unknown;
            AchieveInfo info = null;
            foreach (var infoPair in _items)
            {
                info = infoPair.Value;
                if (info.iDx == achieveIdx)
                {
                    retvalue = info.achieveType;
                    break;
                }
            }
            return retvalue;
        }

        internal GameType GetGameType(int achieveIdx)
        {
            GameType retvalue = GameType.Unknown;
            AchieveInfo info = null;
            foreach (var infoPair in _items)
            {
                info = infoPair.Value;
                if (info.iDx == achieveIdx)
                {
                    retvalue = info.gameType;
                    break;
                }
            }
            return retvalue;
        }
    }
}
