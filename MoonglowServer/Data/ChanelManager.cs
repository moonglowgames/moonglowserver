﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Data
{
    class ChannelManager : Singleton<ChannelManager>
    {
        public static List<ChanelInfo> _items = new List<ChanelInfo>();
        /*private Dictionary<int, ChanelInfo> _items = new Dictionary<int, ChanelInfo>();
        private const string _FILE = @"Data/Chanel.csv";

        // Use this for initialization

        public ChanelManager()
        {
            LoadFromFile();
        }

        public void LoadFromFile()
        {
            string content = string.Empty;
            using (StreamReader sr = new StreamReader(_FILE))
            {
                content = sr.ReadToEnd();
            }

            char[] charsToTrim = { ' ', '\t' };

            string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            ChanelInfo tmpChanelInfo = null;

            foreach (string ln in lines)
            {
                ln.Trim(charsToTrim);

                if (ln.Length == 0) continue;

                if (ln[0] == ';')
                {
                    // ignore comment line
                }
                else
                {
                    tmpChanelInfo = new ChanelInfo();
                    if (tmpChanelInfo.Decode(ln) == true)
                    {
                        _items[tmpChanelInfo.iDx] = tmpChanelInfo;
                    }
                    else
                    {
                        Console.WriteLine(ln);
                    }
                }
            }
        }

        public ChanelInfo Get(int iDx)
        {
            if (_items.ContainsKey(iDx))
            {
                return _items[iDx];
            }
            else
            {
                return null;
            }
        }*/

        internal void ReadChannelInfo()
        {
            //PacketsHandlerDB.SDReqReadChanelInfo();
            GameServer.msSqlConnect.ReadChannelInfo();
        }

        internal void CheckChanelDate(string serverSession, string userSession, int chanelIdx)
        {
            if (chanelIdx == 0) return;

            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo == null)
            {
                GlobalManager.Instance.NullUserInfo(serverSession);
                return;
            }

            DateTime endDate = new DateTime();
            foreach (var info in _items)
            {
                if (info.chanelIdx == chanelIdx)
                {
                    if (GlobalManager.Instance.ConvertToDateTime(info.endDate, out endDate))
                    {
                        if (endDate <= DateTime.Now)
                        {
                            /*float*/int rankPoint = userInfo.userDataBase.userProfile.rankPoint;
                            int rankWin = userInfo.userDataBase.userProfile.rankWin;
                            int rankLose = userInfo.userDataBase.userProfile.rankLose;
                            int rankDraw = userInfo.userDataBase.userProfile.rankDraw;

                            PacketsHandlerInner.SMReqCurrentRank(userInfo.serverSession, userSession, userInfo.userID, chanelIdx, info.name, rankPoint, rankWin, rankLose, rankDraw, true);

                            userInfo.userDataBase.userProfile.chanelIdx = 0;
                            userInfo.userDataBase.userProfile.rankPoint = 0;

                            userInfo.userDataBase.userProfile.rankWin = 0;
                            userInfo.userDataBase.userProfile.rankLose = 0;
                            userInfo.userDataBase.userProfile.rankDraw = 0;


                            //save userInfo 
                            //save chanelIdx and rankPoint
                            userInfo.SaveUserInfo();
                        }
                    }
                    break;
                }
            }
        }

        internal ChanelType GetChannelType(int channelIdx)
        {
            ChanelType channelType = ChanelType.Beginner;
            foreach (var info in _items)
            {
                if(info.chanelIdx == channelIdx)
                {
                    channelType = info.chanelType;
                    break;
                }
            }
            return channelType;
        }

        internal void GetChanelList(string serverSession, string userSession)
        {
            List<ChanelInfo> chanelInfoList = new List<ChanelInfo>();
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();

            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo == null)
            {
                GlobalManager.Instance.NullUserInfo(serverSession);
                return;
            }

            foreach (var info in _items)
            {
                if (GlobalManager.Instance.ConvertToDateTime(info.startDate, out startDate) && GlobalManager.Instance.ConvertToDateTime(info.endDate, out endDate))
                {
                    if (startDate <= DateTime.Now && endDate > DateTime.Now)
                    {
                        chanelInfoList.Add(info);
                    }
                }
            }

            PacketsHandlerInner.SMReqGetChanelList(userInfo.serverSession, new ChanelInfoList(PacketTypeList.SMReqGetChanelList.ToString(), userInfo.session, chanelInfoList));
        
        }

        internal void GetChanelInfoList(out List<ChanelInfo> chanelInfoList)
        {
            chanelInfoList = new List<ChanelInfo>();
            //ChanelInfo info = null;
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();
            foreach (var info in _items)
            {
                //info = infoPair.Value;
                if (GlobalManager.Instance.ConvertToDateTime(info.startDate, out startDate) && GlobalManager.Instance.ConvertToDateTime(info.endDate, out endDate))
                {
                    if (startDate <= DateTime.Now && endDate > DateTime.Now)
                    {
                        chanelInfoList.Add(info);
                    }
                }
            }
        }

        internal int GetChannelMaxCount(int channelIdx_)
        {
            int retvalue = 0;
            foreach (var info in _items)
            {
                if (info.chanelIdx == channelIdx_)
                {
                    retvalue = info.maxCount;
                    break;
                }
            }
            return retvalue;
        }
    }
}
