﻿using MoonGlow;
using MoonGlow.Room;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

public class ItemTime
{
    public DateTime lastEventDate;
    public TimeSpan eventTimeSpan;
    public DateTime endEventDate;
    public bool isSet = false;
    public UserInfo userInfo;

    public void CheckEvent(string userSession)
    {
        if (isSet == false) return;
        if (userInfo == null)
        {
            userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo != null)
            {
                //userinfo is not null now...
            }
        }
        // check doevent
        if (DateTime.Now >= endEventDate)
        {
            isSet = false;
            OnEndEvent();
        }
    }
    
    public void SetEvent( UserInfo userInfo_ )
    {
        userInfo = userInfo_;
        SetTimeSpan();
        SetEventDate();
        OnStartEvent();
        isSet = true;
    }

    protected virtual void SetEventDate() { }
    protected virtual void SetTimeSpan() { }
    protected virtual void OnStartEvent() { }
    protected virtual void OnEndEvent() { }
}

public class StaminaTime : ItemTime
{
    protected override void SetTimeSpan()
    {
        eventTimeSpan = new TimeSpan(0, 4, 0);
    }

    protected override void SetEventDate()
    {
        lastEventDate = userInfo.userDataBase.staminaLastEventDate;
        endEventDate = lastEventDate.Add(eventTimeSpan);
    }

    protected override void OnStartEvent()
    {
        CheckOverTime();
    }

    protected void CheckOverTime()
    {
        if (DateTime.Now > endEventDate)
        {
            TimeSpan overTimeSpan = DateTime.Now - endEventDate;
            int num = Convert.ToInt32(overTimeSpan.Ticks / eventTimeSpan.Ticks);
            if (userInfo.userDataBase.Stamina < 100)
            {
                if (userInfo.userDataBase.Stamina + num >= 100)
                {
                    userInfo.userDataBase.Stamina = 100;
                }
                else
                {
                    userInfo.userDataBase.Stamina += num;
                }
               //auto load elementinfo when client enter home scene
            }
            TimeSpan eventOverTimeSpan = TimeSpan.FromTicks(eventTimeSpan.Ticks * num);
            userInfo.userDataBase.staminaLastEventDate = DateTime.Now - eventOverTimeSpan;
            
            SetEventDate();
        }
        else
        {
            //continue...
        }
    }

    override protected void OnEndEvent()
    {
        // if stamina < 100 then add stamina
        if (userInfo.userDataBase.Stamina < 100 )
        {
            userInfo.userDataBase.Stamina ++;

            if (userInfo.isReadDone)
            {
                //SCNotiUserElement
                userInfo.SetSendElementInfoList();
            }
        }
        //End one staminaEvent and restart, update last stamina event date
        userInfo.userDataBase.staminaLastEventDate = DateTime.Now;
        SetEvent(userInfo);
    }
}

public class EPElixirTime : ItemTime
{
    protected override void SetTimeSpan()
    {
        eventTimeSpan = new TimeSpan(2, 0, 0);
    }

    protected override void SetEventDate()
    {
        endEventDate = DateTime.Now.Add(eventTimeSpan);
        userInfo.userDataBase.epElixirEndEventDate = endEventDate;
    }

    protected override void OnStartEvent()
    {
        //userinfo couble be null, solve it tomorrow
        userInfo.userDataBase.userProfile.addHP = 1;

        //eat item during game
        //send playerItemInfo
        if (userInfo.sceneType == SceneType.Room)
        {
            if (!string.IsNullOrEmpty(userInfo.roomID))
            {
                MoonGlow.Room.Room room = GameServer.GetRoom(userInfo.session);
                if (userInfo._playerItemInfo != null) { userInfo._playerItemInfo.hpPlus = true; }
                room._rule.SendSCNotiPlayerItemInfo(/*userInfo.session, */userInfo._playerItemInfo);
            }
        }
    }

    protected override void OnEndEvent() 
    {
        userInfo.userDataBase.userProfile.addHP = 0;

        //eat item during game
        //send playerItemInfo
        if (userInfo.sceneType == SceneType.Room)
        {
            if (!string.IsNullOrEmpty(userInfo.roomID))
            {
                Room room = GameServer.GetRoom(userInfo.session);
                if (userInfo._playerItemInfo != null) { userInfo._playerItemInfo.hpPlus = false; }
                room._rule.SendSCNotiPlayerItemInfo(/*userInfo.session,*/ userInfo._playerItemInfo);
            }
        }
    }
}

public class SOElixirTime : ItemTime
{
    protected override void SetTimeSpan()
    {
        eventTimeSpan = new TimeSpan(2, 0, 0);
    }

    protected override void SetEventDate()
    {
        endEventDate = DateTime.Now.Add(eventTimeSpan);
        userInfo.userDataBase.soElixirEndEventDate = endEventDate;
    }

    protected override void OnStartEvent()
    {
        userInfo.userDataBase.userProfile.addSP = 1;

        //eat item during game
        //send playerItemInfo
        if (userInfo.sceneType == SceneType.Room)
        {
            if (!string.IsNullOrEmpty(userInfo.roomID))
            {
                MoonGlow.Room.Room room = GameServer.GetRoom(userInfo.session);

                if (userInfo._playerItemInfo != null) { userInfo._playerItemInfo.spPlus = false; }
                room._rule.SendSCNotiPlayerItemInfo(/*userInfo.session, */userInfo._playerItemInfo);
            }
        }
    }

    protected override void OnEndEvent() 
    {
        userInfo.userDataBase.userProfile.addSP = 0;

        //eat item during game
        //send playerItemInfo
        if (userInfo.sceneType == SceneType.Room)
        {
            if (!string.IsNullOrEmpty(userInfo.roomID))
            {
                Room room = GameServer.GetRoom(userInfo.session);
                if (userInfo._playerItemInfo != null) { userInfo._playerItemInfo.spPlus = false; }
                room._rule.SendSCNotiPlayerItemInfo(/*userInfo.session,*/ userInfo._playerItemInfo);
            }
        }
    }
}
