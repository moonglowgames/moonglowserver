﻿using System;
using System.Collections.Concurrent;

namespace MoonGlow
{
    public class ConcorentManager<T>
    {
        private ConcurrentDictionary<string, T > _item  = 
            new ConcurrentDictionary<string, T>();

        public ConcorentManager() { }

        public T Get( string key ) 
        {
            //T retvalue = default(T);
            //if (_item.ContainsKey(key))
            //{
            //    retvalue = _item[key];
            //}
            //return retvalue;

            return _item.ContainsKey(key) ? _item[key] : default(T);
        }

        public bool Add( string key , T Info )
        {
            //bool retvalue = false;
            if (_item.ContainsKey(key))
            {
                Console.WriteLine("UserManager.cs Item Add Error Key : " + key.ToString() );
            }
            //else
            //{
            //    retvalue = _item.TryAdd(key, Info);
            //}

            //return retvalue;
            return _item.ContainsKey(key) ? false : _item.TryAdd(key, Info);
        }

        public bool Remove( string key , out T Info)
        {
            //bool retvalue = false;
            Info = default(T);

            //if (_item.ContainsKey(key))
            //{
            //    retvalue = _item.TryRemove(key, out Info);
            //}
            //return retvalue;

            return _item.ContainsKey(key) ? _item.TryRemove(key, out Info) : false;
        }
    }
}

