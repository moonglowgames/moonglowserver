﻿using LitJson;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class LogAccountData: TableEntity
{
    public LogAccountData() { }
    public string nick { get; set; }
    public string date { get; set; }
    public string type { get; set; }
    public LogAccountData(string userID,string nick_, string date_, LogAccountType type_)
    {
        this.PartitionKey = userID;
        this.RowKey = String.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.Ticks) + GlobalManager.LOG_ACCOUNT_COUNT.ToString();
        nick = nick_;
        date = date_;
        type = type_.ToString();
    }
}
public class PCKAccountLog
{
    public string name { get; set; }
    public LogAccountData data { get; set; }
    public PCKAccountLog() { }
    public PCKAccountLog(string packetName, LogAccountData data_)
    {
        name = packetName;
        data = data_;
    }
}
public partial class SLogReqAccountInsert : Packet
{
    public PCKAccountLog pckData = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SLogReqAccountInsert;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(this.pckData);

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            pckData = JsonMapper.ToObject<PCKAccountLog>(Json);

             retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public class LogLevelData : TableEntity
{
    public LogLevelData() { }
    public string nick { get; set; }
    public string date { get; set; }
    public int last { get; set; }
    public int current { get; set; }
    public LogLevelData(string userID, string nick_, string date_, int last_, int current_)
    {
        this.PartitionKey = userID;
        this.RowKey = String.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.Ticks) + GlobalManager.LOG_LEVEL_COUNT.ToString();
        nick = nick_;
        date = date_;
        last = last_;
        current = current_;
    }
}
public class PCKLevelLog
{
    public string name { get; set; }
    public LogLevelData data { get; set; }
    public PCKLevelLog() { }
    public PCKLevelLog(string packetName, LogLevelData data_)
    {
        name = packetName;
        data = data_;
    }
}
public partial class SLogReqLevelInsert : Packet
{
    public PCKLevelLog pckData = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SLogReqLevelInsert;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(this.pckData);

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            pckData = JsonMapper.ToObject<PCKLevelLog>(Json);

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public class LogItemData : TableEntity
{
    public LogItemData() { }
    public string nick { get; set; }
    public string date { get; set; }
    public int itemIdx { get; set; }
    public string itemName { get; set; }
    public int itemCount { get; set; }
    public string type { get; set; }
    public LogItemData(string userID, string nick_, string date_, int itemIdx_, string itemName_, int itemCount_, LogItemType type_)
    {
        this.PartitionKey = userID;
        this.RowKey = String.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.Ticks + GlobalManager.LOG_ITEM_COUNT);
        nick = nick_;
        date = date_;
        itemIdx = itemIdx_;
        itemName = itemName_;
        itemCount = itemCount_;
        type = type_.ToString();
    }
}
public class PCKItemLog
{
    public string name { get; set; }
    public LogItemData data { get; set; }
    public PCKItemLog() { }
    public PCKItemLog(string packetName, LogItemData data_)
    {
        name = packetName;
        data = data_;
    }
}
public partial class SLogReqItemInsert : Packet
{
    public PCKItemLog pckData = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SLogReqItemInsert;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(this.pckData);

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            pckData = JsonMapper.ToObject<PCKItemLog>(Json);

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public class LogPlayData : TableEntity
{
    public LogPlayData() { }
    public string nick { get; set; }
    public string date { get; set; }
    public string gameType { get; set; }
    public string playMode { get; set; }
    public string playTime { get; set; }
    public int stake { get; set; }
    public bool disClose { get; set; }
    public bool isLock {get;set; }
    public string type { get; set; }
    public string gameTime { get; set; }
    public string result { get; set; }
    public LogPlayData(string userID, string nick_, string date_, GameType gameType_, PlayMode playMode_, PlayTime playTime_, int stake_, bool disClose_, bool isLock_, LogPlayType type_, string gameTime_, string result_)
    {
        this.PartitionKey = userID;
        this.RowKey = String.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.Ticks) + GlobalManager.LOG_PLAY_COUNT.ToString();
        nick = nick_;
        date = date_;
        gameType = gameType_.ToString();
        playMode = playMode_.ToString();
        playTime = playTime_.ToString();
        stake = stake_;
        disClose = disClose_;
        isLock = isLock_;
        type = type_.ToString();
        gameTime = gameTime_;
        result = result_;
    }
}
public class PCKPlayLog
{
    public string name { get; set; }
    public LogPlayData data { get; set; }
    public PCKPlayLog() { }
    public PCKPlayLog(string packetName, LogPlayData data_)
    {
        name = packetName;
        data = data_;
    }
}
public partial class SLogReqPlayInsert : Packet
{
    public PCKPlayLog pckData = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SLogReqPlayInsert;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(this.pckData);

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            pckData = JsonMapper.ToObject<PCKPlayLog>(Json);

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public class LogProfileData : TableEntity
{
    public LogProfileData() { }
    public string nick { get; set; }
    public string date { get; set; }
    public int winCount {get;set;}
    public int loseCount { get; set; }
    public int drawCount { get; set; }
    public int rankWin { get; set; }
    public int rankLose { get; set; }
    public int rankDraw { get; set; }
    public LogProfileData(string userID, string nick_, string date_, int winCount_, int loseCount_, int drawCOunt_, int rankWin_, int rankLose_, int rankDraw_)
    {
        this.PartitionKey = userID;
        this.RowKey = String.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.Ticks) + GlobalManager.LOG_PROFILE_COUNT.ToString();
        nick = nick_;
        date = date_;
        winCount = winCount_;
        loseCount = loseCount_;
        drawCount = drawCOunt_;
        rankWin = rankWin_;
        rankLose = rankLose_;
        rankDraw = rankDraw_;
    }
}
public class PCKProfileLog
{
    public string name { get; set; }
    public LogProfileData data { get; set; }
    public PCKProfileLog() { }
    public PCKProfileLog(string packetName, LogProfileData data_)
    {
        name = packetName;
        data = data_;
    }
}
public partial class SLogReqProfileInsert : Packet
{
    public PCKProfileLog pckData = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SLogReqProfileInsert;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(this.pckData);

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            pckData = JsonMapper.ToObject<PCKProfileLog>(Json);

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}
