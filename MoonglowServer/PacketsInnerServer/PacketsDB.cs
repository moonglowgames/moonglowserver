﻿using LitJson;
using MoonGlow;
using MoonGlow.Data;
using StackExchange.Redis;
using System;
using System.Collections.Generic;

public partial class SMReqAddRedis : Packet
{
    public string userID = string.Empty;
    public UserType userType = UserType.User;
    public string nick = string.Empty;
    public Language language = Language.English;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqAddRedis;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;
        element["userType"] = this.userType.ToString();
        element["nick"] = this.nick;
        element["language"] = this.language.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            userType = (UserType)Enum.Parse(typeof(UserType), jsonData["userType"].ToString());
            nick = jsonData["nick"].ToString();
            language = (Language)Enum.Parse(typeof(Language), jsonData["language"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public partial class MSRspAddRedis : Packet
{
    public bool addResult = false;
    public string session = string.Empty;
    public string userID = string.Empty;
    public UserType userType = UserType.User;
    public string nick = string.Empty;
    public string gameServerUrl = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MSRspAddRedis;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["addResult"] = this.addResult;
        element["session"] = this.session;
        element["userID"] = this.userID;
        element["userType"] = this.userType.ToString();
        element["nick"] = this.nick;
        element["gameServerUrl"] = this.gameServerUrl;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            addResult = Boolean.Parse(jsonData["addResult"].ToString());
            session = jsonData["session"].ToString();
            userID = jsonData["userID"].ToString();
            userType = (UserType)Enum.Parse(typeof(UserType), jsonData["userType"].ToString());
            nick = jsonData["nick"].ToString();
            gameServerUrl = jsonData["gameServerUrl"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public partial class SMReqRemoveRedis : Packet
{
    public string userID = string.Empty;
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqRemoveRedis;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;
        element["session"] = this.session;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

// executethread thread call this function
// MSNotiRemoveRedis
public partial class MSNotiRemoveRedis : Packet
{
    public bool removeResult = false;
    public string session = string.Empty;
    public bool isLogout = false;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MSNotiRemoveRedis;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["removeResult"] = this.removeResult;
        element["session"] = this.session;
        element["isLogout"] = this.isLogout;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            removeResult = Boolean.Parse(jsonData["removeResult"].ToString());
            session = jsonData["session"].ToString();
            isLogout = Boolean.Parse(jsonData["isLogout"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public partial class SDReqNewUserData : Packet
{
    public AccountType accountType = AccountType.Unknown;
    public string account = string.Empty;
    public string password = string.Empty;
    public string nick = string.Empty;
    public int faceIdx = 0;
    public string deviceID = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqNewUserData;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["accountType"] = this.accountType.ToString();
        element["account"] = this.account;
        element["password"] = this.password;
        element["nick"] = this.nick;
        element["faceIdx"] = this.faceIdx;
        element["deviceID"] = this.deviceID;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            accountType = (AccountType)Enum.Parse(typeof(AccountType), jsonData["accountType"].ToString());
            account = jsonData["account"].ToString();
            password = jsonData["passwork"].ToString();
            nick = jsonData["nick"].ToString();
            faceIdx = int.Parse(jsonData["faceIdx"].ToString());
            deviceID = jsonData["deviceID"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public partial class DSRspNewUserData : Packet
{
    public AccountType accountType = AccountType.Unknown;
    public bool createResult = false;
    public string userID = string.Empty;
    public string nick = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspNewUserData;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["accountType"] = this.accountType.ToString();
        element["createResult"] = this.createResult;
        element["userID"] = this.userID;
        element["nick"] = this.nick;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            accountType = (AccountType)Enum.Parse(typeof(AccountType), jsonData["accountType"].ToString());
            createResult = Boolean.Parse(jsonData["createResult"].ToString());
            userID = jsonData["userID"].ToString();
            nick = jsonData["nick"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// Database thread call ths function
// SDReqReadUserData
public partial class SDReqReadUserData : Packet
{
    public string session = string.Empty;
    public string userID = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqReadUserData;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["userID"] = this.userID;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            userID = jsonData["userID"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// executethread call this function...
// DSRspReadUserData
public partial class DSRspReadUserData : Packet
{
    public bool readResult = false;
    public string session = string.Empty;
    //public string nick = string.Empty;
    //public int level = 0;
    //public int faceIdx = 0;
    //public /*float*/int rankPoint = 0;
    //public string lastLoginDate = string.Empty;
    ////public string createDate = string.Empty;
    //public int friendCount = 0;
    //public int checkInDays = 0;
    public string jsonString = string.Empty;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspReadUserData;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["readResult"] = this.readResult;
        element["session"] = this.session;
        //element["nick"] = this.nick;
        //element["level"] = this.level;
        //element["faceIdx"] = this.faceIdx;
        //element["rankPoint"] = this.rankPoint;
        //element["lastLoginDate"] = this.lastLoginDate;
        ////element["createDate"] = this.createDate;
        //element["friendCount"] = this.friendCount;
        //element["checkInDays"] = this.checkInDays;
        element["jsonString"] = this.jsonString;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            //nick = jsonData["nick"].ToString();
            //level = int.Parse(jsonData["level"].ToString());
            //faceIdx = int.Parse(jsonData["faceIdx"].ToString());
            //rankPoint = /*float*/int.Parse(jsonData["rankPoint"].ToString());
            //lastLoginDate = jsonData["lastLoginDate"].ToString();
            ////createDate = jsonData["createDate"].ToString();
            //friendCount = int.Parse(jsonData["friendCount"].ToString());
            //checkInDays = int.Parse(jsonData["checkInDays"].ToString());
            jsonString = jsonData["jsonString"].ToString();
            readResult = Boolean.Parse(jsonData["readResult"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// SDReqUpdateUserInfo
public partial class SDReqUpdateUserInfo : Packet
{
    public UserData userData = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqUpdateUserInfo;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        retvalue = JsonMapper.ToJson(userData);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userData = JsonMapper.ToObject<UserData>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// SDReqGetMailList
public partial class SDReqGetMailList : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqGetMailList;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// SDReqUpdateMail
public partial class SDReqUpdateMail : Packet
{
    public string session = string.Empty;
    public int mailDBIdx = 0;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqUpdateMail;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["mailDBIdx"] = this.mailDBIdx;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            mailDBIdx = int.Parse(jsonData["mailDBIdx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// Database thread call ths function
// SDReqNewMail
public partial class SDReqNewMail : Packet
{
    public MailData mailData = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqNewMail;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        //element["mailDBIdx"] = this.mailDBIdx;

        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(mailData);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            //mailDBIdx = int.Parse(jsonData["mailDBIdx"].ToString());
            mailData = JsonMapper.ToObject<MailData>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// executethread thread call ths function
// DSRspNewMail
public partial class DSRspNewMail : Packet
{
    public bool addResult = false;
    public string session = string.Empty;
    public string senderID = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspNewMail;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["addResult"] = this.addResult;
        element["session"] = this.session;
        element["senderID"] = this.senderID;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            addResult = Boolean.Parse(jsonData["addResult"].ToString());
            session = jsonData["session"].ToString();
            senderID = jsonData["senderID"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// Database thread call ths function
// SDReqReadFriendList
public partial class SDReqGetFriendList : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqGetFriendList;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// executethread thread call ths function
// DSRspReadFriendList
public partial class DSRspGetFriendList : Packet
{
    public bool getResult = false;
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspGetFriendList;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["getResult"] = this.getResult;
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            getResult = Boolean.Parse(jsonData["getResult"].ToString());
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// Database thread call ths function
// SDReqGetFriends
public partial class SDReqGetMyFriends : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqGetMyFriends;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// executethread thread call ths function
// DSRspGetMyFriends
public partial class DSRspGetMyFriends : Packet
{
    public bool getResult = false;
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspGetMyFriends;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["getResult"] = this.getResult;
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            getResult = Boolean.Parse(jsonData["getResult"].ToString());
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// Database thread call ths function
// SDReqGetRequestFriends
public partial class SDReqGetRequestFriends : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqGetRequestFriends;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// executethread thread call ths function
// DSRspGetRequestFriends
public partial class DSRspGetRequestFriends : Packet
{
    public bool getResult = false;
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspGetRequestFriends;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["getResult"] = this.getResult;
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            getResult = Boolean.Parse(jsonData["getResult"].ToString());
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// Database thread call ths function
// SDReqNewFriend
public partial class SDReqNewFriend : Packet
{
    public string userID = string.Empty;
    //public string userNick = string.Empty;
    public string friendID = string.Empty;
    //public string friendNick = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqNewFriend;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;
        //element["userNick"] = this.userNick;
        element["friendID"] = this.friendID;
        //element["friendNick"] = this.friendNick;
        retvalue = element.ToJson();
        //retvalue = JsonMapper.ToJson(mailData);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            //userNick = jsonData["userNick"].ToString();
            friendID = jsonData["friendID"].ToString();
            //friendNick = jsonData["friendNick"].ToString();
            //mailData = JsonMapper.ToObject<MailData>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// executethread thread call ths function
// DSRspNewFriend
public partial class DSRspNewFriend : Packet
{
    public bool addResult = false;
    public string userID = string.Empty;
    public string friendID = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspNewFriend;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["addResult"] = this.addResult;
        element["userID"] = this.userID;
        element["friendID"] = this.friendID;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            addResult = Boolean.Parse(jsonData["addResult"].ToString());
            userID = jsonData["userID"].ToString();
            friendID = jsonData["friendID"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// Database thread call ths function
// SDReqUpdateFriendType
public partial class SDReqUpdateFriendType : Packet
{
    public string session = string.Empty;
    public string userID = string.Empty;
    public string friendID = string.Empty;
    public int otherFriendCount = 0;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqUpdateFriendType;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["userID"] = this.userID;
        element["friendID"] = this.friendID;
        element["otherFriendCount"] = this.otherFriendCount.ToString();
        retvalue = element.ToJson();
        //retvalue = JsonMapper.ToJson(mailData);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            userID = jsonData["userID"].ToString();
            friendID = jsonData["friendID"].ToString();
            otherFriendCount = int.Parse(jsonData["otherFriendCount"].ToString());
            //mailData = JsonMapper.ToObject<MailData>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// executethread thread call ths function
// DSRspUpdateFriendType
public partial class DSRspUpdateFriendType : Packet
{
    public bool updateResult = false;
    public string session = string.Empty;
    public string friendID = string.Empty;
    public int otherFriendCount = 0;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspUpdateFriendType;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["updateResult"] = this.updateResult;
        element["session"] = this.session;
        element["friendID"] = this.friendID;
        element["otherFriendCount"] = this.otherFriendCount.ToString();
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            updateResult = Boolean.Parse(jsonData["updateResult"].ToString());
            session = jsonData["session"].ToString();
            friendID = jsonData["friendID"].ToString();
            otherFriendCount = int.Parse(jsonData["otherFriendCount"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// Database thread call ths function
// SDReqUserFriendCount
public partial class SDReqUserFriendCount : Packet
{
    public string session = string.Empty;
    public string friendID = string.Empty;
    public int iDx = 0;
    public PacketTypeList sender = PacketTypeList.Unknown;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqUserFriendCount;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["friendID"] = this.friendID;
        element["iDx"] = this.iDx.ToString();
        element["sender"] = this.sender.ToString();
        retvalue = element.ToJson();
        //retvalue = JsonMapper.ToJson(mailData);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            friendID = jsonData["friendID"].ToString();
            iDx = int.Parse(jsonData["iDx"].ToString());
            sender = (PacketTypeList)Enum.Parse(typeof(PacketTypeList), jsonData["sender"].ToString());

            //mailData = JsonMapper.ToObject<MailData>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// executethread thread call ths function
// DSRspUserFriendCount
public partial class DSRspUserFriendCount : Packet
{
    public bool getResult = false;
    public string session = string.Empty;
    //public string friendID = string.Empty;
    public int iDx = 0; 
    public int otherFriendCount = 0;
    public PacketTypeList sender = PacketTypeList.Unknown;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspUserFriendCount;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["getResult"] = this.getResult;
        element["session"] = this.session;
        //element["friendID"] = this.friendID;
        element["iDx"] = this.iDx.ToString();
        element["otherFriendCount"] = this.otherFriendCount.ToString();
        element["sender"] = this.sender.ToString();
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            getResult = Boolean.Parse(jsonData["getResult"].ToString());
            session = jsonData["session"].ToString();
            //friendID = jsonData["friendID"].ToString();
            iDx = int.Parse(jsonData["iDx"].ToString());
            otherFriendCount = int.Parse(jsonData["otherFriendCount"].ToString());
            sender = (PacketTypeList)Enum.Parse(typeof(PacketTypeList), jsonData["sender"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// SDReqDeleteFriend
public partial class SDReqDeleteFriend : Packet
{
    public string session = string.Empty;
    public string friendID = string.Empty;
    public FriendType friendType = FriendType.Requesting;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqDeleteFriend;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["friendID"] = this.friendID;
        element["friendType"] = this.friendType.ToString();
        retvalue = element.ToJson();
        //retvalue = JsonMapper.ToJson(mailData);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            friendID = jsonData["friendID"].ToString();
            friendType = (FriendType)Enum.Parse(typeof(FriendType), jsonData["friendType"].ToString());
            //mailData = JsonMapper.ToObject<MailData>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// executethread thread call ths function
// DSRspDeleteFriend
public partial class DSRspDeleteFriend : Packet
{
    public bool deleteResult = false;
    public string session = string.Empty;
    public int otherFriendCount = 0;
    public string friendID = string.Empty;
    public FriendType friendType = FriendType.Accepting;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspDeleteFriend;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["deleteResult"] = this.deleteResult;
        element["session"] = this.session;
        element["otherFriendCount"] = this.otherFriendCount;
        element["friendID"] = this.friendID;
        element["friendType"] = this.friendType.ToString();
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            deleteResult = Boolean.Parse(jsonData["deleteResult"].ToString());
            session = jsonData["session"].ToString();
            otherFriendCount = int.Parse(jsonData["otherFriendCount"].ToString());
            friendID = jsonData["friendID"].ToString();
            friendType = (FriendType)Enum.Parse(typeof(FriendType), jsonData["friendType"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// Database thread call ths function
// SDReqUpdateSendFriendItem
public partial class SDReqUpdateSendFriendItem : Packet
{
    public string session = string.Empty;
    public string friendID = string.Empty;
    public string sendDate = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqUpdateSendFriendItem;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["friendID"] = this.friendID;
        element["sendDate"] = this.sendDate;
        retvalue = element.ToJson();
        //retvalue = JsonMapper.ToJson(mailData);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            friendID = jsonData["friendID"].ToString();
            sendDate = jsonData["sendDate"].ToString();
            //mailData = JsonMapper.ToObject<MailData>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// SDReqUpdateGetFriendItem
public partial class SDReqUpdateGetFriendItem : Packet
{
    public string session = string.Empty;
    public string friendID = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqUpdateGetFriendItem;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["friendID"] = this.friendID;
        retvalue = element.ToJson();
        //retvalue = JsonMapper.ToJson(mailData);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            friendID = jsonData["friendID"].ToString();
            //mailData = JsonMapper.ToObject<MailData>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// Database thread call ths function
// SDReqGiftSendDate
public partial class SDReqGiftSendDate : Packet
{
    public string session = string.Empty;
    public string friendID = string.Empty;
    public string sendDate = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqGiftSendDate;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["friendID"] = this.friendID;
        element["sendDate"] = this.sendDate;
        retvalue = element.ToJson();
        //retvalue = JsonMapper.ToJson(mailData);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            friendID = jsonData["friendID"].ToString();
            sendDate = jsonData["sendDate"].ToString();
            //mailData = JsonMapper.ToObject<MailData>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class ChanelInfoList
{
    public string name { get; set; }
    public string userSession { get; set; }
    public List<ChanelInfo> chanelInfoList { get; set; }
    public ChanelInfoList() { }
    public ChanelInfoList(string packetName, string userSession_, List<ChanelInfo> chanelInfoList_)
    {
        name = packetName;
        userSession = userSession_;
        chanelInfoList = chanelInfoList_;
    }
}

// redis thread call ths function
// SMReqGetChanelList
public partial class SMReqGetChanelList : Packet
{
    public ChanelInfoList chanelInfoList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqGetChanelList;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(chanelInfoList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            chanelInfoList = JsonMapper.ToObject<ChanelInfoList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// execute thread call ths function
// MSRspGetChanelList
public partial class MSRspGetChanelList : Packet
{
    public ChanelInfoList chanelInfoList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MSRspGetChanelList;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(chanelInfoList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            chanelInfoList = JsonMapper.ToObject<ChanelInfoList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// redis thread call ths function
// SMReqAddRank
public partial class SMReqAddRank : Packet
{
    public int chanelIdx = 0;
    public string chanelName = string.Empty;
    public string session = string.Empty;
    public /*float*/int rankPoint = 0;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqAddRank;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["chanelIdx"] = this.chanelIdx;
        element["chanelName"] = this.chanelName;
        element["session"] = this.session;
        element["rankPoint"] = this.rankPoint;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            chanelIdx = int.Parse(jsonData["chanelIdx"].ToString());
            chanelName = jsonData["chanelName"].ToString();
            session = jsonData["session"].ToString();
            rankPoint = /*float*/int.Parse(jsonData["rankPoint"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// execute thread call ths function
// MSRspAddRank
public partial class MSRspAddRank : Packet
{
    public ChanelResult result = ChanelResult.Unknown;
    public string session = string.Empty;
    public int currentCount = 0;
    public int channelIdx = 0;
    public string chanelName = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MSRspAddRank;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["result"] = this.result.ToString();
        element["session"] = this.session;
        element["currentCount"] = this.currentCount;
        element["channelIdx"] = this.channelIdx;
        element["chanelName"] = this.chanelName;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            result = (ChanelResult)Enum.Parse(typeof(ChanelResult),jsonData["result"].ToString());
            session = jsonData["session"].ToString();
            currentCount = int.Parse(jsonData["currentCount"].ToString());
            channelIdx = int.Parse(jsonData["channelIdx"].ToString());
            chanelName = jsonData["chanelName"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// redis thread call ths function
// SMReqRankRange
public partial class SMReqRankRange : Packet
{
    public string session = string.Empty;
    public string chanelName = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqRankRange;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["chanelName"] = this.chanelName;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            chanelName = jsonData["chanelName"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class RankRange
{
    public string name { get; set; }
    public string userSession { get; set; }
    public string chanelName { get; set; }
    public int currentCount { get; set; }
    public SortedSetEntry[] rangeEntry { get; set; }
    public RankRange() { }
    public RankRange(string packetName, string userID_, string chanelName_, int currentCount_, SortedSetEntry[] rangeEntry_)
    {
        name = packetName;
        userSession = userID_;
        chanelName = chanelName_;
        currentCount = currentCount_;
        rangeEntry = rangeEntry_;
    }
}

// datebase thread call ths function
// MDReqRankRange
public partial class MDReqRankRange : Packet
{
    public RankRange rankRange = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MDReqRankRange;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(rankRange);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            rankRange = JsonMapper.ToObject<RankRange>(Json);

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// execute thread call ths function
// DSRspRankRange
public partial class DSRspRankRange : Packet
{
    public RankList rankList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspRankRange;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(rankList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            rankList = JsonMapper.ToObject<RankList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// datebase thread call ths function
// SDReqRankHistory
public partial class SDReqRankHistory : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqRankHistory;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class PCKRankInfo
{
    public string chanelName { get; set; }
    public int rankCount { get; set; }
    public int rankPercent { get; set; }
    public int rankPoint { get; set; }
    public int rankWin { get; set; }
    public int rankLose { get; set; }
    public string winRate { get; set; }
    public bool itemExist { get; set; }
    public bool itemGot { get; set; }
    public PCKRankInfo() { }
    public PCKRankInfo(string chanelName_, int rankCount_, int rankPercent_, int rankPoint_, int rankWin_, int rankLose_, string winRate_, bool itemExist_, bool itemGot_)
    {
        chanelName = chanelName_;
        rankCount = rankCount_;
        rankPercent = rankPercent_;
        rankPoint = rankPoint_;

        rankWin = rankWin_;
        rankLose = rankLose_;
        winRate = winRate_;

        itemExist = itemExist_;
        itemGot = itemGot_;
    }
}
public class PCKRankHisList
{
    public string name { get; set; }
    public string session { get; set; }
    public List<PCKRankInfo> rankHisList { get; set; }
    public PCKRankHisList() { }
    public PCKRankHisList(string packetName, string session_, List<PCKRankInfo> rankHisList_)
    {
        name = packetName;
        session = session_;
        rankHisList = rankHisList_;
    }
}
// execute thread call ths function
// DSRspRankHistory
public partial class DSRspRankHistory : Packet
{
    public PCKRankHisList rankHisList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspRankHistory;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(rankHisList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            rankHisList = JsonMapper.ToObject<PCKRankHisList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// datebase thread call ths function
// SMReqCurrentRank
public partial class SMReqCurrentRank : Packet
{
    public string session = string.Empty;
    public string userID = string.Empty;
    public int chanelIdx = 0;
    public string chanelName = string.Empty;
    public bool isChanelClose = false;
    public /*float*/int rankPoint = 0;
    public int rankWin = 0;
    public int rankLose = 0;
    public int rankDraw = 0;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqCurrentRank;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["userID"] = this.userID;
        element["chanelIdx"] = this.chanelIdx;
        element["chanelName"] = this.chanelName;
        element["isChanelClose"] = this.isChanelClose;
        element["rankPoint"] = this.rankPoint;
        element["rankWin"] = this.rankWin;
        element["rankLose"] = this.rankLose;
        element["rankDraw"] = this.rankDraw;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            userID = jsonData["userID"].ToString();
            chanelIdx = int.Parse(jsonData["chanelIdx"].ToString());
            chanelName = jsonData["chanelName"].ToString();
            isChanelClose = Boolean.Parse(jsonData["isChanelClose"].ToString());
            rankPoint = /*float*/int.Parse(jsonData["rankPoint"].ToString());
            rankWin = int.Parse(jsonData["rankWin"].ToString());
            rankLose = int.Parse(jsonData["rankLose"].ToString());
            rankDraw = int.Parse(jsonData["rankDraw"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// execute thread call ths function
// MSRspCurrentRank
public partial class MSRspCurrentRank : Packet
{
    public string session = string.Empty;
    public string userID = string.Empty;
    public int chanelIdx = 0;
    public string chanelName = string.Empty;
    public int currentCount = 0;
    public int totalCount = 0;
    public /*float*/int rankPoint = 0;
    public int rankWin = 0;
    public int rankLose = 0;
    public int rankDraw = 0;
    public bool isChanelClose = false;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MSRspCurrentRank;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["userID"] = this.userID;
        element["chanelIdx"] = this.chanelIdx;
        element["chanelName"] = this.chanelName;
        element["currentCount"] = this.currentCount;
        element["totalCount"] = this.totalCount;
        element["rankPoint"] = this.rankPoint;
        element["rankWin"] = this.rankWin;
        element["rankLose"] = this.rankLose;
        element["rankDraw"] = this.rankDraw;
        element["isChanelClose"] = this.isChanelClose;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            userID = jsonData["userID"].ToString();
            chanelIdx = int.Parse(jsonData["chanelIdx"].ToString());
            chanelName = jsonData["chanelName"].ToString();
            currentCount = int.Parse(jsonData["currentCount"].ToString());
            totalCount = int.Parse(jsonData["totalCount"].ToString());
            rankPoint = /*float*/int.Parse(jsonData["rankPoint"].ToString());
            rankWin = int.Parse(jsonData["rankWin"].ToString());
            rankLose = int.Parse(jsonData["rankLose"].ToString());
            rankDraw = int.Parse(jsonData["rankDraw"].ToString());
            isChanelClose = Boolean.Parse(jsonData["isChanelClose"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// datebase thread call ths function
// SDReqNewRankHis
public partial class SDReqNewRankHis : Packet
{
    public string session = string.Empty;
    public string userID = string.Empty;
    public string chanelName = string.Empty;
    public int currentCount = 0;
    public int currentPercent = 0;
    public /*float*/int currentPoint = 0;
    public int currentRankWin = 0;
    public int currentRankLose = 0;
    public int currentRankDraw = 0;
    public bool itemExist = false;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqNewRankHis;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["userID"] = this.userID;
        element["chanelName"] = this.chanelName;
        element["currentCount"] = this.currentCount;
        element["currentPercent"] = this.currentPercent;
        element["currentPoint"] = this.currentPoint;
        element["currentRankWin"] = this.currentRankWin;
        element["currentRankLose"] = this.currentRankLose;
        element["currentRankDraw"] = this.currentRankDraw;
        element["itemExist"] = this.itemExist;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            userID = jsonData["userID"].ToString();
            chanelName = jsonData["chanelName"].ToString();
            currentCount = int.Parse(jsonData["currentCount"].ToString());
            currentPercent = int.Parse(jsonData["currentPercent"].ToString());
            currentPoint = /*float*/int.Parse(jsonData["currentPoint"].ToString());
            currentRankWin = int.Parse(jsonData["currentRankWin"].ToString());
            currentRankLose = int.Parse(jsonData["currentRankLose"].ToString());
            currentRankDraw = int.Parse(jsonData["currentRankDraw"].ToString());
            itemExist = Boolean.Parse(jsonData["itemExist"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// execute thread call ths function
// DSRspNewRankHis
public partial class DSRspNewRankHis : Packet
{
    public bool addResult = false;
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspNewRankHis;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["addResult"] = this.addResult;
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            addResult = Boolean.Parse(jsonData["addResult"].ToString());
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// datebase thread call ths function
// SDReqUpdateRankHis
public partial class SDReqUpdateRankHis : Packet
{
    public string userID = string.Empty;
    //public int chanelIdx = 0;
    public string chanelName = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqUpdateRankHis;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;
        //element["iDx"] = this.chanelIdx;
        element["chanelName"] = this.chanelName;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            //chanelIdx = int.Parse(jsonData["iDx"].ToString());
            chanelName = jsonData["chanelName"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// redis thread call ths function
// SMReqRemoveRankHis
public partial class SMReqUpdateRank : Packet
{
    public string chanelName = string.Empty;
    public string session = string.Empty;
    public string userID = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqUpdateRank;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["chanelName"] = this.chanelName;
        element["session"] = this.session;
        element["userID"] = this.userID;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            chanelName = jsonData["chanelName"].ToString();
            session = jsonData["session"].ToString();
            userID = jsonData["userID"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// database thread call ths function
// SDReqAddChanelUser
public partial class SDReqAddChanelUser : Packet
{
    public string chanelName = string.Empty;
    public string userID = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqAddChanelUser;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["chanelName"] = this.chanelName;
        element["userID"] = this.userID;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            chanelName = jsonData["chanelName"].ToString();
            userID = jsonData["userID"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// database thread call ths function
// SDReqUpdateChanelInfo
public partial class SDReqUpdateChanelInfo : Packet
{
    public int chanelIdx = 0;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqUpdateChanelInfo;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["chanelIdx"] = this.chanelIdx;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            chanelIdx = int.Parse(jsonData["chanelIdx"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// database thread call ths function
// SDReqRemoveChanelUser
public partial class SDReqRemoveChanelUser : Packet
{
    public string chanelName = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqRemoveChanelUser;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["chanelName"] = this.chanelName;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            chanelName = jsonData["chanelName"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

// database thread call ths function
// SDReqUpdateChanelInfoDate
public partial class SDReqUpdateChanelInfoDate : Packet
{
    public int chanelIdx = 0;
    public string dateTime = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqUpdateChanelInfoDate;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["chanelIdx"] = this.chanelIdx;
        element["dateTime"] = this.dateTime;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            chanelIdx = int.Parse(jsonData["chanelIdx"].ToString());
            dateTime = jsonData["dateTime"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SDReqRepairChanel
public partial class SDReqRepairChanel : Packet
{
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqRepairChanel;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class ChanelUser
{
    public string chanelName { get; set; }
    public string userID { get; set; }
    public int rankPoint { get; set; }
    public ChanelUser() { }
    public ChanelUser(string chanelName_, string userID_, int rankPoint_)
    {
        chanelName = chanelName_;
        userID = userID_;
        rankPoint = rankPoint_;
    }
}
public class ChanelUserList
{
    public string name { get; set; }
    public List<ChanelUser> chanelUserList { get; set; }
    public ChanelUserList() { }
    public ChanelUserList(string packetName, List<ChanelUser> chanelUserList_)
    {
        name = packetName;
        chanelUserList = chanelUserList_;
    }
}

//redis thread call ths function
//DMReqRepairChanel
public partial class DMReqRepairChanel : Packet
{
    public ChanelUserList chanelUserList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DMReqRepairChanel;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(chanelUserList);
        //retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            chanelUserList = JsonMapper.ToObject<ChanelUserList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//execute thread call ths function
//MSRspRepairChanel
public partial class MSRspRepairChanel : Packet
{
    public bool repairResult = false;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MSRspRepairChanel;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["repairResult"] = this.repairResult;
        
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            repairResult = Boolean.Parse(jsonData["repairResult"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//redis thread call this function
//SMReqBC
public partial class SMReqBCServerNoti : Packet
{
    public string message = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCServerNoti;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["message"] = this.message;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            this.message = jsonData["message"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//execute thread call this function
//MSNotiBC
public partial class MSNotiBCServerNoti : Packet
{
    public string message = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MSNotiBCServerNoti;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["message"] = this.message;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            message = jsonData["message"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//redis thread call this function
//SMReqLoginSessionCheck
public partial class SMReqLoginSessionCheck : Packet
{
    public string userID = string.Empty;
    public string session = string.Empty;
    public Language language = Language.English;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqLoginSessionCheck;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;
        element["session"] = this.session;
        element["language"] = this.language.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            session = jsonData["session"].ToString();
            language = (Language)Enum.Parse(typeof(Language), jsonData["language"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//redis thread call this function
//MSRspLoginSessionCheck
public partial class MSRspLoginSessionCheck : Packet
{
    public bool result = false;
    public string session = string.Empty;
    public string userID = string.Empty;
    public Language language = Language.English;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MSRspLoginSessionCheck;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["result"] = this.result;
        element["session"] = this.session;
        element["userID"] = this.userID;
        element["language"] = this.language.ToString();
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            result = Boolean.Parse(jsonData["result"].ToString());
            session = jsonData["session"].ToString();
            userID = jsonData["userID"].ToString();
            language = (Language)Enum.Parse(typeof(Language), jsonData["language"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public partial class SDReqRegisterCheck : Packet
{
    public AccountType accountType = AccountType.Unknown;
    public string account = string.Empty;
    public string password = string.Empty;
    public string nick = string.Empty;
    public int faceIdx = 0;
    public string deviceID = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqRegisterCheck;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["accountType"] = this.accountType.ToString();
        element["account"] = this.account;
        element["password"] = this.password;
        element["nick"] = this.nick;
        element["faceIdx"] = this.faceIdx;
        element["deviceID"] = this.deviceID;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            accountType = (AccountType)Enum.Parse(typeof(AccountType), jsonData["accountType"].ToString());
            account = jsonData["account"].ToString();
            password = jsonData["password"].ToString();
            nick = jsonData["nick"].ToString();
            faceIdx = int.Parse(jsonData["faceIdx"].ToString());
            deviceID = jsonData["deviceID"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public partial class DSRspRegisterCheck : Packet
{
    public LoginState state = LoginState.Unknown;
    public AccountType accountType = AccountType.Unknown;
    public string account = string.Empty;
    public string password = string.Empty;
    public string nick = string.Empty;
    public int faceIdx = 0;
    public string deviceID = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspRegisterCheck;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["state"] = this.state.ToString();
        element["accountType"] = this.accountType.ToString();
        element["account"] = this.account;
        element["password"] = this.password;
        element["nick"] = this.nick;
        element["faceIdx"] = this.faceIdx;
        element["deviceID"] = this.deviceID;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            state = (LoginState)Enum.Parse(typeof(LoginState), jsonData["state"].ToString());
            accountType = (AccountType)Enum.Parse(typeof(AccountType), jsonData["accountType"].ToString());
            account = jsonData["account"].ToString();
            password = jsonData["password"].ToString();
            nick = jsonData["nick"].ToString();
            faceIdx = int.Parse(jsonData["faceIdx"].ToString());
            deviceID = jsonData["deviceID"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public partial class SDReqLoginCheck : Packet
{
    public AccountType accountType = AccountType.Unknown;
    public string account = string.Empty;
    public string password = string.Empty;
    public string deviceID = string.Empty;
    public Language language = Language.English;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqLoginCheck;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["accountType"] = this.accountType.ToString();
        element["account"] = this.account;
        element["password"] = this.password;
        element["deviceID"] = this.deviceID;
        element["language"] = this.language.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            accountType = (AccountType)Enum.Parse(typeof(AccountType), jsonData["accountType"].ToString());
            account = jsonData["account"].ToString();
            password = jsonData["password"].ToString();
            deviceID = jsonData["deviceID"].ToString();
            language = (Language)Enum.Parse(typeof(Language), jsonData["language"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public partial class DSRspLoginCheck : Packet
{
    //public string account = string.Empty;
    public string userID = string.Empty;
    public UserType userType = UserType.User;
    public string nick = string.Empty;
    public bool sameDevice = false;
    public string deviceID = string.Empty;
    public LoginState state = LoginState.Unknown;
    public Language language = Language.English;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspLoginCheck;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        //element["account"] = this.account;
        element["userID"] = this.userID;
        element["userType"] = this.userType.ToString();
        element["nick"] = this.nick;
        element["sameDevice"] = this.sameDevice;
        element["deviceID"] = this.deviceID;
        element["state"] = this.state.ToString();
        element["language"] = this.language.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            //account = jsonData["account"].ToString();
            userID = jsonData["userID"].ToString();
            userType = (UserType)Enum.Parse(typeof(UserType), jsonData["userType"].ToString());
            nick = jsonData["nick"].ToString();
            sameDevice = Boolean.Parse(jsonData["sameDevice"].ToString());
            deviceID = jsonData["deviceID"].ToString();
            state = (LoginState)Enum.Parse(typeof(LoginState), jsonData["state"].ToString());
            language = (Language)Enum.Parse(typeof(Language), jsonData["language"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//database thread call this function
//SDReqExpireSession
public partial class SMReqExpireUserInfo : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqExpireUserInfo;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqSetServerGlobalState
public partial class SMReqSetServerGlobalState : Packet
{
    public string state = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqSetServerGlobalState;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["state"] = this.state;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            state = jsonData["state"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MSRspSetServerGlobalState
public partial class MSRspSetServerGlobalState : Packet
{
    public bool setResult = false;
    public string state = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MSRspSetServerGlobalState;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["setResult"] = this.setResult;
        element["state"] = this.state;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            setResult = Boolean.Parse(jsonData["setResult"].ToString());
            state = jsonData["state"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMBCGlobalServerState
public partial class SMReqBCServerState : Packet
{
    public string serverState = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCServerState;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["serverState"] = this.serverState;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            serverState = jsonData["serverState"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MSNotiBCServerState
public partial class MSNotiServerState : Packet
{
    public string serverState = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MSNotiServerState;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["serverState"] = this.serverState;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            serverState = jsonData["serverState"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SDReqSaveToSql
public partial class SDReqSaveToSql : Packet
{
    public string userID = string.Empty;
    public string nickName = string.Empty;
    public int faceIdx = 0;
    public int level = 0;
    public int rankPoint = 0;
    public string lastLoginDate = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqSaveToSql;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;
        element["nickName"] = this.nickName;
        element["faceIdx"] = faceIdx;
        element["level"] = level;
        element["rankPoint"] = rankPoint;
        element["lastLoginDate"] = lastLoginDate;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            nickName = jsonData["nickName"].ToString();
            faceIdx = int.Parse(jsonData["faceIdx"].ToString());
            level = int.Parse(jsonData["level"].ToString());
            rankPoint = int.Parse(jsonData["rankPoint"].ToString());
            lastLoginDate = jsonData["lastLoginDate"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SDReqSetDeveloper
public partial class SDReqSetDeveloper : Packet
{
    public string nick = string.Empty;
    public UserType userType = UserType.User;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqSetDeveloper;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["nick"] = this.nick;
        element["userType"] = this.userType.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            nick = jsonData["nick"].ToString();
            userType = (UserType)Enum.Parse(typeof(UserType), jsonData["userType"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//DSRspSetDeveloper
public partial class DSRspSetDeveloper : Packet
{
    public string nick = string.Empty;
    public UserType userType = UserType.User;
    public bool setResult = false;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.DSRspSetDeveloper;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["nick"] = this.nick;
        element["userType"] = this.userType.ToString();
        element["setResult"] = this.setResult;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            nick = jsonData["nick"].ToString();
            userType = (UserType)Enum.Parse(typeof(UserType), jsonData["userType"].ToString());
            setResult = Boolean.Parse(jsonData["setResult"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqBCReFreshUserType
public partial class SMReqBCReFreshUserType : Packet
{
    public UserType userType = UserType.User;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCReFreshUserType;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userType"] = this.userType.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userType = (UserType)Enum.Parse(typeof(UserType), jsonData["userType"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MSNotiBCReFreshUserType
public partial class MSNotiBCReFreshUserType : Packet
{
    public UserType userType = UserType.User;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MSNotiBCReFreshUserType;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userType"] = this.userType.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userType = (UserType)Enum.Parse(typeof(UserType), jsonData["userType"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SDReqDevList
public partial class SDReqDevList : Packet
{
    public UserType userType = UserType.User;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SDReqDevList;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userType"] = this.userType.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userType = (UserType)Enum.Parse(typeof(UserType), jsonData["userType"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqExpireRedis
public partial class SMReqExpireRedis : Packet
{
    public string userID = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqExpireRedis;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqBCForceClose
public partial class SMReqBCForceClose : Packet
{
    public string userID = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCForceClose;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MSNotiBCForceClose
public partial class MSNotiBCForceClose : Packet
{
    public string gameServerUrl = string.Empty;
    public string userSession = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.MSNotiBCForceClose;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["gameServerUrl"] = this.gameServerUrl;
        element["userSession"] = this.userSession;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            gameServerUrl = jsonData["gameServerUrl"].ToString();
            userSession = jsonData["userSession"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SDReqUpdatedeviceID
public partial class SDReqUpdatedeviceID : Packet
{
    public string userID = string.Empty;
    //public string account = string.Empty;
    public string deviceID = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SDReqUpdatedeviceID;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;
        //element["account"] = this.account;
        element["deviceID"] = this.deviceID;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            //account = jsonData["account"].ToString();
            deviceID = jsonData["deviceID"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SDReqUpdatePassword
public partial class SDReqUpdatePassword : Packet
{
    public string account = string.Empty;
    public string oldPassword = string.Empty;
    public string newPassword = string.Empty;
    public bool isFind = false;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SDReqUpdatePassword;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["account"] = this.account;
        element["oldPassword"] = this.oldPassword;
        element["newPassword"] = this.newPassword;
        element["isFind"] = this.isFind;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            account = jsonData["account"].ToString();
            oldPassword = jsonData["oldPassword"].ToString();
            newPassword = jsonData["newPassword"].ToString();
            isFind = Boolean.Parse(jsonData["isFind"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//DSRspUpdatePassword
public partial class DSRspUpdatePassword : Packet
{
    public PacketTypeList pckType = PacketTypeList.Unknown;
    public LoginState state = LoginState.Unknown;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.DSRspUpdatePassword;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["state"] = this.state.ToString();
        element["pckType"] = this.pckType.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            state = (LoginState)Enum.Parse(typeof(LoginState), jsonData["state"].ToString());
            pckType = (PacketTypeList)Enum.Parse(typeof(PacketTypeList), jsonData["pckType"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SDReqUpdateFriendResult
public partial class SDReqUpdateFriendFight : Packet
{
    public string userID = string.Empty;
    public string friendID = string.Empty;
    public Result result = Result.Draw;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SDReqUpdateFriendFight;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;
        element["friendID"] = this.friendID;
        element["result"] = this.result.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            friendID = jsonData["friendID"].ToString();
            result = (Result)Enum.Parse(typeof(Result), jsonData["result"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SDReqNickCheck
public partial class SDReqChangeNick : Packet
{
    public string session = string.Empty;
    public string nick = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SDReqChangeNick;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["nick"] = this.nick;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            nick = jsonData["nick"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MSRspUpdateServerEnable
public partial class MSRspUpdateServerEnable : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.MSRspUpdateServerEnable;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MSNotiBCReFreshServerList
public partial class SMReqRefreshServerList : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqRefreshServerList;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqBCReFreshServerList
public partial class SMReqBCReFreshServerList : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCReFreshServerList;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqBCReFreshChannelInfo
public partial class SMReqBCReFreshChannelInfo : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCReFreshChannelInfo;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MDNotiRefreshChannelInfo
public partial class MDNotiRefreshChannelInfo : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.MDNotiRefreshChannelInfo;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqBCReFreshClientVersions
public partial class SMReqBCReFreshClientVersions : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCReFreshClientVersions;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MDNotiRefreshClientVersion
public partial class MDNotiRefreshClientVersion : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.MDNotiRefreshClientVersion;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MSNotiUpdateEnable
public partial class SMNotiUpdateEnable : Packet
{
    public string connectDNS = string.Empty;
    public int connectPort = 0;
    public bool enable = false;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMNotiUpdateEnable;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["connectDNS"] = this.connectDNS;
        element["connectPort"] = this.connectPort;
        element["enable"] = this.element.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            connectDNS = jsonData["connectDNS"].ToString();
            connectPort = int.Parse(jsonData["connectPort"].ToString());
            enable = Boolean.Parse(jsonData["enable"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqBCUpdateServerEnable
public partial class SMReqBCUpdateServerEnable : Packet
{
    public string connectDNS = string.Empty;
    public int connectPort = 0;
    public bool enable = false;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCUpdateServerEnable;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["connectDNS"] = this.connectDNS;
        element["connectPort"] = this.connectPort;
        element["enable"] = this.element.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            connectDNS = jsonData["connectDNS"].ToString();
            connectPort = int.Parse(jsonData["connectPort"].ToString());
            enable = Boolean.Parse(jsonData["enable"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqGetAllServerState
public partial class SMReqGetAllServerInfo : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqGetAllServerInfo;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqGetThisServerInfo
public partial class SMReqGetThisServerInfo : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqGetThisServerInfo;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}


public class ServerInfoList
{
    public string name { get; set; }
    public List<ServerInfo> serverList { get; set; }
    public bool enable { get; set; }
    public ServerInfoList() { }
    public ServerInfoList(string packetName_, List<ServerInfo> serverList_, bool enable_)
    {
        name = packetName_;
        serverList = serverList_;
        enable = enable_;
    }
}

//SMReqBCUpdateAllServerEnable
public partial class SMReqBCUpdateAllServerEnable : Packet
{
    public bool enable = false;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCUpdateAllServerEnable;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["enable"] = this.enable;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            enable = Boolean.Parse(jsonData["enable"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqChangeServer
public partial class SMReqChangeServer : Packet
{
    public string session = string.Empty;
    public string connectUrl = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqChangeServer;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["connectUrl"] = this.connectUrl;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            connectUrl = jsonData["connectUrl"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SDReqUpdateUserBan
public partial class SDReqUpdateUserBan : Packet
{
    public string userNick = string.Empty;
    public bool ban = false;
    public int minutes = 0;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SDReqUpdateUserBan;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["userNick"] = this.userNick;
        element["ban"] = this.ban;
        element["minutes"] = this.minutes;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userNick = jsonData["userNick"].ToString();
            ban = Boolean.Parse(jsonData["ban"].ToString());
            minutes = int.Parse(jsonData["minutes"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//DMRepBCUBanUser
public partial class DMRepBCBanUser : Packet
{
    public string userID = string.Empty;
    public string userNick = string.Empty;
    public bool ban = false;
    public int minutes = 0;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.DMRepBCBanUser;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["userID"] = this.userID;
        element["userNick"] = this.userNick;
        element["ban"] = this.ban;
        element["minutes"] = this.minutes;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            userNick = jsonData["userNick"].ToString();
            ban = Boolean.Parse(jsonData["ban"].ToString());
            minutes = int.Parse(jsonData["minutes"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMNotiBanUser
public partial class SMNotiBanUser : Packet
{
    public string userID = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMNotiBanUser;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["userID"] = this.userID;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqCheckUserBan
public partial class SMReqCheckUserBan : Packet
{
    public string userID = string.Empty;
    public UserType userType = UserType.User;
    public string nick = string.Empty;
    public bool sameDevice = false;
    public string deviceID = string.Empty;
    public LoginState state = LoginState.Unknown;
    public Language language = Language.Korean;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqCheckUserBan;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["userID"] = this.userID;
        element["userType"] = this.userType.ToString();
        element["nick"] = this.nick;
        element["sameDevice"] = this.sameDevice;
        element["deviceID"] = deviceID;
        element["state"] = this.state.ToString();
        element["language"] = this.language.ToString();
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            //userID, userType, nick, sameDevice, deviceID_, state, language
            userID = jsonData["userID"].ToString();
            userType = (UserType)Enum.Parse(typeof(UserType), jsonData["userType"].ToString());
            nick = jsonData["nick"].ToString();
            sameDevice = Boolean.Parse(jsonData["sameDevice"].ToString());
            deviceID = jsonData["deviceID"].ToString();
            state = (LoginState)Enum.Parse(typeof(LoginState), jsonData["state"].ToString());
            language = (Language)Enum.Parse(typeof(Language), jsonData["language"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqBCUpdateLimit
public partial class SMReqBCUpdateLimit : Packet
{
    public int count = 0;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCUpdateLimit;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["count"] = this.count;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            count = int.Parse(jsonData["count"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MSNotiUpdateLimit
public partial class MSNotiUpdateLimit : Packet
{
    public int count = 0;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.MSNotiUpdateLimit;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["count"] = this.count;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            count = int.Parse(jsonData["count"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SDReqAddIAP
public partial class SDReqAddIAP : Packet
{
    public string userID = string.Empty;
    public string userNick = string.Empty;
    public IAPType iAPType = IAPType.Unknown;
    public int itemIdx = 0;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SDReqAddIAP;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;
        element["userNick"] = this.userNick;
        element["iAPType"] = this.iAPType.ToString();
        element["itemIdx"] = this.itemIdx;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            userNick = jsonData["userNick"].ToString();
            iAPType = (IAPType)Enum.Parse(typeof(IAPType), jsonData["iAPType"].ToString());
            itemIdx = int.Parse(jsonData["itemIdx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SDReqIAPResult
public partial class SDReqIAPResult : Packet
{
    public string session = string.Empty;
    public string guid = string.Empty;
    public IAPResult result = IAPResult.Unknown;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SDReqIAPResult;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["guid"] = this.guid;
        element["result"] = this.result.ToString();
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            guid = jsonData["guid"].ToString();
            result = (IAPResult)Enum.Parse(typeof(IAPResult), jsonData["result"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SDReqGetPlayerProfile
public partial class SDReqGetPlayerProfile : Packet
{
    public string playerNick = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SDReqGetPlayerProfile;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["playerNick"] = this.playerNick;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            playerNick = jsonData["playerNick"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqBCReFreshLocalization
public partial class SMReqBCReFreshLocalization : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCReFreshLocalization;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MDNotiUpdateLocalization
public partial class MDNotiUpdateLocalization : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.MDNotiUpdateLocalization;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqBCReFreshNews
public partial class SMReqBCReFreshNews : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCReFreshNews;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MDNotiUpdateNews
public partial class MDNotiUpdateNews : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.MDNotiUpdateNews;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqBCReFreshMission
public partial class SMReqBCReFreshMission : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCReFreshMission;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MDNotiUpdateMission
public partial class MDNotiUpdateMission : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.MDNotiUpdateMission;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//DMRepBCNewFriendGift
public partial class DMRepBCNewFriendGift : Packet
{
    public string userID = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.DMRepBCNewFriendGift;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MSNotiNewFriendGift
public partial class MSNotiNewFriendGift : Packet
{
    public string userID = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.MSNotiNewFriendGift;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["userID"] = this.userID;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            userID = jsonData["userID"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqBCAlarmShop
public partial class SMReqBCAlarmShop : Packet
{
    public string alarmString = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCAlarmShop;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["alarmString"] = this.alarmString;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            alarmString = jsonData["alarmString"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MSNotiAlarmShop
public partial class MSNotiAlarmShop : Packet
{
    public string alarmString = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.MSNotiAlarmShop;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["alarmString"] = this.alarmString;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            alarmString = jsonData["alarmString"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SMReqBCAlarmNews
public partial class SMReqBCAlarmNews : Packet
{
    public string alarmString = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SMReqBCAlarmNews;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["alarmString"] = this.alarmString;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            alarmString = jsonData["alarmString"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//MSNotiAlarmNews
public partial class MSNotiAlarmNews : Packet
{
    public string alarmString = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.MSNotiAlarmNews;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["alarmString"] = this.alarmString;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            alarmString = jsonData["alarmString"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

