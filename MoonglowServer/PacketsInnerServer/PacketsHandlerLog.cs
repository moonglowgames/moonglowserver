﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class PacketsHandlerLog
{
    internal static void SLogReqAccountInsert(string serverSession, PCKAccountLog data)
    {
        SLogReqAccountInsert pck = new SLogReqAccountInsert();
        pck.pckData = data;
        QueueManager.Instance.AddLogQueue(serverSession, pck);
    }

    internal static void SLogReqLevelInsert(string serverSession, PCKLevelLog data)
    {
        SLogReqLevelInsert pck = new SLogReqLevelInsert();
        pck.pckData = data;
        QueueManager.Instance.AddLogQueue(serverSession, pck);
    }

    internal static void SLogReqItemInsert(string serverSession, PCKItemLog data)
    {
        SLogReqItemInsert pck = new SLogReqItemInsert();
        pck.pckData = data;
        QueueManager.Instance.AddLogQueue(serverSession, pck);
    }

    internal static void SLogReqPlayInsert(string serverSession, PCKPlayLog data)
    {
        SLogReqPlayInsert pck = new SLogReqPlayInsert();
        pck.pckData = data;
        QueueManager.Instance.AddLogQueue(serverSession, pck);
    }

    internal static void SLogReqProfileInsert(string serverSession, PCKProfileLog data)
    {
        SLogReqProfileInsert pck = new SLogReqProfileInsert();
        pck.pckData = data;
        QueueManager.Instance.AddLogQueue(serverSession, pck);
    }
}
