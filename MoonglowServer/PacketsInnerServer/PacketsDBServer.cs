﻿public partial class SMReqAddRedis : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqAddRedis(serverSession, this.userID, this.userType, this.nick, this.language);
    }
}

public partial class MSRspAddRedis : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSRspAddRedis(serverSession, this.addResult, this.session, this.userID, this.userType, this.nick, this.gameServerUrl);
    }
}

// MemDB thread call this function
// SMReqRemoveRedis
public partial class SMReqRemoveRedis : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqRemoveRedis(serverSession, this.userID, this.session);
    }
}

// executethread thread call this function
// MSNotiRemoveRedis
public partial class MSNotiRemoveRedis : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSNotiRemoveRedis(serverSession, this.isLogout, this.removeResult, this.session);
    }
}

public partial class SDReqNewUserData : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqNewUserData(serverSession, this.accountType, this.account, this.password, this.nick, this.faceIdx, this.deviceID);
    }
}

public partial class DSRspNewUserData : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspNewUserData(serverSession, this.accountType, this.createResult, this.userID, this.nick);
    }
}

// Database thread call ths function
// SDReqReadUserData
public partial class SDReqReadUserData : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqReadUserData(serverSession, this.session, this.userID);
    }
}

// executethread call this function...
// DSRspReadUserData
public partial class DSRspReadUserData : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspReadUserData(serverSession, this.readResult, this.session,/*this.nick, this.level, this.faceIdx, this.rankPoint, this.lastLoginDate, this.friendCount, this.checkInDays,*/ this.jsonString);
    }
}

// Database thread call this function...
// SDReqUpdateUserInfo
public partial class SDReqUpdateUserInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUpdateUserInfo(serverSession, this.userData);
    }
}

// Database thread call ths function
// SDReqGetMailList
public partial class SDReqGetMailList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqGetMailList(serverSession, this.session);
    }
}

// Database thread call ths function
// SDReqUpdateMail
public partial class SDReqUpdateMail : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUpdateMail(serverSession, this.session, this.mailDBIdx);
    }
}

// Database thread call ths function
// SDReqNewMail
public partial class SDReqNewMail : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqNewMail(serverSession, this.mailData);
    }
}

// executethread thread call ths function
// DSRspNewMail
public partial class DSRspNewMail : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspNewMail(serverSession, this.addResult, this.session, this.senderID);
    }
}

// Database thread call ths function
// SDReqGetFriendList
public partial class SDReqGetFriendList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqGetFriendList(serverSession, this.session);
    }
}

// executethread thread call ths function
// DSRspGetFriendList
public partial class DSRspGetFriendList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspGetFriendList(serverSession, this.getResult, this.session);
    }
}

// Database thread call ths function
// SDReqGetMyFriends
public partial class SDReqGetMyFriends : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqGetMyFriends(serverSession, this.session);
    }
}

// executethread thread call ths function
// DSRspGetMyFriends
public partial class DSRspGetMyFriends : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspGetMyFriends(serverSession, this.getResult, this.session);
    }
}

// Database thread call ths function
// SDReqGetRequestFriends
public partial class SDReqGetRequestFriends : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqGetRequestFriends(serverSession, this.session);
    }
}

// executethread thread call ths function
// DSRspGetRequestFriends
public partial class DSRspGetRequestFriends : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspGetRequestFriends(serverSession, this.getResult, this.session);
    }
}

// Database thread call ths function
// SDReqNewFriend
public partial class SDReqNewFriend : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqNewFriend(serverSession, this.userID, /*this.userNick,*/ this.friendID/*, this.friendNick*/);
    }
}

// executethread thread call ths function
// DSRspNewFriend
public partial class DSRspNewFriend : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspNewFriend(serverSession, this.addResult, this.userID, this.friendID);
    }
}

// Database thread call ths function
// SDReqUpdateFriend
public partial class SDReqUpdateFriendType : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUpdateFriendType(serverSession,this.session,  this.userID, this.friendID, this.otherFriendCount);
    }
}

// executethread thread call ths function
// DSRspUpdateFriend
public partial class DSRspUpdateFriendType : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspUpdateFriendType(serverSession, this.updateResult, this.session, this.friendID, this.otherFriendCount);
    }
}

// Database thread call ths function
// SDReqUserFriendCount
public partial class SDReqUserFriendCount : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUserFriendCount(serverSession, this.session, this.friendID, this.iDx, this.sender);
    }
}

// executethread thread call ths function
// DSRspUserFriendCount
public partial class DSRspUserFriendCount : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspUserFriendCount(serverSession, this.getResult, this.session, this.iDx, this.otherFriendCount, this.sender);
    }
}

// Database thread call ths function
// SDReqDeleteFriend
public partial class SDReqDeleteFriend : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqDeleteFriend(serverSession, this.session, this.friendID, this.friendType);
    }
}

// executethread thread call ths function
// DSRspDeleteFriend
public partial class DSRspDeleteFriend : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspDeleteFriend(serverSession, this.deleteResult, this.session, this.friendType, this.friendID, this.otherFriendCount);
    }
}

// Database thread call ths function
// SDReqUpdateSendFriendItem
public partial class SDReqUpdateSendFriendItem : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUpdateSendFriendItem(serverSession, this.session, this.friendID, this.sendDate);
    }
}

// Database thread call ths function
// SDReqUpdateGetFriendItem
public partial class SDReqUpdateGetFriendItem : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUpdateGetFriendItem(serverSession,this.session, this.friendID);
    }
}

// Database thread call ths function
// SDReqGiftSendDate
public partial class SDReqGiftSendDate : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqGiftSendDate(serverSession, this.session, this.friendID, this.sendDate);
    }
}

// redis thread call ths function
// SMReqGetChanelList
public partial class SMReqGetChanelList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqGetChanelList(serverSession, chanelInfoList);
    }
}

// execute thread call ths function
// MSRspGetChanelList
public partial class MSRspGetChanelList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSRspGetChanelList(serverSession, this.chanelInfoList);
    }
}

// redis thread call ths function
// SMReqAddRank
public partial class SMReqAddRank : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqAddRank(serverSession, this.session, this.rankPoint, this.chanelName, this.chanelIdx);
    }
}

// execute thread call ths function
// MSRspAddRank
public partial class MSRspAddRank : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSRspAddRank(serverSession, this.result, this.session, this.chanelName, this.currentCount, this.channelIdx);
    }
}

// redis thread call ths function
// SMReqRankRange
public partial class SMReqRankRange : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqRankRange(serverSession, this.session, this.chanelName);
    }
}

// DateBase thread call ths function
// MDReqRankRange
public partial class MDReqRankRange : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMDReqRankRange(serverSession, this.rankRange);
    }
}

// execute thread call ths function
// DSRspRankRange
public partial class DSRspRankRange : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspRankRange(serverSession, this.rankList);
    }
}

// datebase thread call ths function
// SDReqRankHistory
public partial class SDReqRankHistory : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqRankHistory(serverSession, this.session);
    }
}

public class UserRank
{
    public int rankIdx { get; set; }
    public int chanelIdx { get; set; }
    public string chanelName { get; set; }
    public int currentCount { get; set; }
    public int currentPercent { get; set; }
    public bool itemExist { get; set; }
    public bool itemGot { get; set; }
    public UserRank() { }
    public UserRank(int rankIdx_, int chanelIdx_, string chanelName_, int currentCount_, int currentPercent_, bool itemExist_, bool itemGot_)
    {
        rankIdx = rankIdx_;
        chanelIdx = chanelIdx_;
        chanelName = chanelName_;
        currentCount = currentCount_;
        currentPercent = currentPercent_;
        itemExist = itemExist_;
        itemGot = itemGot_;
    }
}
// execute thread call ths function
// DSRspRankHistory
public partial class DSRspRankHistory : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspRankHistory(serverSession, this.rankHisList);
    }
}

// Redis thread call ths function
// SMReqCurrentRank
public partial class SMReqCurrentRank : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqCurrentRank(serverSession, this.session, this.userID, this.chanelName, this.chanelIdx, this.rankPoint, this.rankWin, this.rankLose, this.rankDraw, this.isChanelClose);
    }
}

// execute thread call this function
// MSRspCurrentRank
public partial class MSRspCurrentRank : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSRspCurrentRank(serverSession, this.session, this.userID, this.rankPoint, this.rankWin, this.rankLose, this.rankDraw, this.chanelName, this.chanelIdx, this.totalCount, this.currentCount, this.isChanelClose);
    }
}

// datebase thread call ths function
// SDReqNewRankHis
public partial class SDReqNewRankHis : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqNewRankHis(serverSession, this.session, this.userID, this.chanelName, this.currentCount, this.currentPercent, this.currentPoint, this.currentRankWin, this.currentRankLose, this. currentRankDraw, this.itemExist);
    }
}

// execute thread call ths function
// DSRspNewRankHis
public partial class DSRspNewRankHis : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspNewRankHis(serverSession, this.addResult, this.session);
    }
}

// datebase thread call ths function
// SDReqUpdateRankHis
public partial class SDReqUpdateRankHis : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUpdateRankHis(serverSession,this.userID, this.chanelName);
    }
}

// redis thread call ths function
// SMReqRemoveRankHis
public partial class SMReqUpdateRank : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqUpdateRank(serverSession, this.session, this.userID, this.chanelName);
    }
}

// redis thread call ths function
// MSRspRemoveRank
//public partial class MSRspUpdateRank : Packet
//{
//    public override void Execute() { }
//    public override void Execute(string serverSession)
//    {
//        InnerPacketsManager.Instance.GetMSRspUpdateRank(serverSession, this.removeResult, this.session, this.chanelName);
//    }
//}

// database thread call ths function
// SDReqAddChanelUser
public partial class SDReqAddChanelUser : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqAddChanelUser(serverSession, this.userID, this.chanelName);
    }
}

// database thread call ths function
// SDReqUpdateChanelInfo
public partial class SDReqUpdateChanelInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUpdateChanelInfo(serverSession, this.chanelIdx);
    }
}

// database thread call ths function
// SDReqRemoveChanelUser
public partial class SDReqRemoveChanelUser : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqRemoveChanelUser(serverSession, this.chanelName);
    }
}

// database thread call ths function
// SDReqUpdateChanelInfoDate
public partial class SDReqUpdateChanelInfoDate : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUpdateChanelInfoDate(serverSession, this.chanelIdx, this.dateTime);
    }
}

//SDReqRepairChanel
public partial class SDReqRepairChanel : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqRepairChanel(serverSession);
    }
}

//redis thread call ths function
//DMReqRepairChanel
public partial class DMReqRepairChanel : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDMReqRepairChanel(serverSession, this.chanelUserList);
    }
}

//execute thread call ths function
//MSRepairChanel
public partial class MSRspRepairChanel : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSRspRepairChanel(serverSession, this.repairResult);
    }
}

//redis thread work
//SMReqBC
public partial class SMReqBCServerNoti : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCServerNoti(serverSession, this.message);
    }
}

//redis thread work
//MSNotiBC
public partial class MSNotiBCServerNoti : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSNotiBCServerNoti(this.message);
    }
}

//redis thread call this function
//SMReqCheckSession
public partial class SMReqLoginSessionCheck : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqSessionCheck(serverSession, this.userID, this.session, this.language);
    }
}

//execute thread call this function
//MSRspCheckSession
public partial class MSRspLoginSessionCheck : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSRspSessionCheck(serverSession, this.result, this.session, this.userID, this.language);
    }
}

public partial class SDReqRegisterCheck : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqRegisterCheck(serverSession, this.accountType, this.account, this.password, this.nick, this.faceIdx, this.deviceID);
    }
}

public partial class DSRspRegisterCheck : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspRegisterCheck(serverSession, this.state, this.accountType, this.account, this.password, this.nick, this.faceIdx, this.deviceID);
    }
}

public partial class SDReqLoginCheck : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqLoginCheck(serverSession, this.accountType, this.account, this.password, this.deviceID, this.language);
    }
}

public partial class DSRspLoginCheck : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspLoginCheck(serverSession, this.state, this.sameDevice, this.deviceID, this.userID, this.userType, this.nick, this.language);
    }
}

//database thread call this function
//SDReqExpireSession0
public partial class SMReqExpireUserInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqExpireUserInfo(serverSession, this.session);        
    }
}

//SMReqSetServerGlobalState
public partial class SMReqSetServerGlobalState : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqSetServerGlobalState(serverSession, this.state);
    }
}

//MSRspSetServerGlobalState
public partial class MSRspSetServerGlobalState : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSRspSetServerGlobalState(serverSession, this.setResult, this.state);
    }
}

//SMReqBCServerState
public partial class SMReqBCServerState : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCServerState(this.serverState);
    }
}

//MSNotiBCServerState
public partial class MSNotiServerState : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSNotiBCServerState(serverSession, this.serverState);
    }
}

//SDReqSaveToSql
public partial class SDReqSaveToSql : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqSaveToSql(serverSession, this.userID, this.nickName, this.faceIdx, this.level, this.rankPoint, this.lastLoginDate);
    }
}

public partial class SDReqSetDeveloper : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqSetDeveloper(serverSession, this.nick, this.userType);
    }
}

public partial class DSRspSetDeveloper : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspSetDeveloper(serverSession, this.nick, this.userType, this.setResult);
    }
}

//SMReqBCReFreshUserType
public partial class SMReqBCReFreshUserType : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCReFreshUserType(serverSession, this.userType);
    }
}

//MSNotiBCReFreshUserType
public partial class MSNotiBCReFreshUserType : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSNotiBCReFreshUserType(serverSession, this.userType);
    }
}

//SDReqDevList
public partial class SDReqDevList : Packet
{
    public override void Execute() { }
    public /*async*/ override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqDevList(this.userType);
    }
}

//SMReqExpireRedis
public partial class SMReqExpireRedis : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqExpireRedis(this.userID);
    }
}

//SMReqBCForceClose
public partial class SMReqBCForceClose : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCForceClose(this.userID);
    }
}

//MSRspBCForceClose
public partial class MSNotiBCForceClose : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSNotiBCForceClose(this.gameServerUrl, this.userSession);
    }
}

//SDReqUpdatedeviceID
public partial class SDReqUpdatedeviceID : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUpdatedeviceID(this.userID, /*this.account,*/ this.deviceID);
    }
}

//SDReqUpdatePassword
public partial class SDReqUpdatePassword : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUpdatePassword(serverSession, this.account, this.oldPassword, this.newPassword, this.isFind);
    }
}

//DSRspUpdatePassword
public partial class DSRspUpdatePassword : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDSRspUpdatePassword(serverSession, this.pckType, this.state);
    }
}

//SDReqUpdateFriendResult
public partial class SDReqUpdateFriendFight : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUpdateFriendFight(this.userID, this.friendID, this.result);
    }
}

//SDReqNickCheck
public partial class SDReqChangeNick : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqChangeNick(serverSession, this.session, this.nick);
    }
}

//MSRspUpdateServerEnable
public partial class MSRspUpdateServerEnable : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSRspUpdateServerEnable(serverSession);
    }
}

//SMReqFreshServerList
public partial class SMReqRefreshServerList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqFreshServerList(serverSession);
    }
}

//SMReqBCReFreshServerList
public partial class SMReqBCReFreshServerList : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCReFreshServerList(serverSession);
    }
}

//SDReqGetChannelInfo
public partial class SMReqBCReFreshChannelInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCReFreshChannelInfo(serverSession);
    }
}

//MDNotiRefreshChannelInfo
public partial class MDNotiRefreshChannelInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMDNotiRefreshChannelInfo(serverSession);
    }
}

//SMReqBCReFreshClientVersions
public partial class SMReqBCReFreshClientVersions : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCReFreshClientVersions(serverSession);
    }
}

//MDNotiRefreshClientVersion
public partial class MDNotiRefreshClientVersion : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMDNotiRefreshClientVersion(serverSession);
    }
}

//SMNotiUpdateEnable
public partial class SMNotiUpdateEnable : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMNotiUpdateEnable(serverSession, this.connectDNS, connectPort, enable);
    }
}
//SMReqBCUpdateServerEnable
public partial class SMReqBCUpdateServerEnable : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCUpdateServerEnable(serverSession, this.connectDNS, connectPort, enable);
    }
}

//SMReqBCUpdateAllServerEnable
public partial class SMReqBCUpdateAllServerEnable : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCUpdateAllServerEnable(serverSession, this.enable);
    }
}

//SMReqGetAllServerState
public partial class SMReqGetAllServerInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqGetAllServerInfo(serverSession);
    }
}

//SMReqGetThisServerInfo
public partial class SMReqGetThisServerInfo : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqGetThisServerInfo(serverSession);
    }
}

//SMReqChangeServer
public partial class SMReqChangeServer : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqChangeServer(serverSession, this.session, this.connectUrl);
    }
}

//SDReqUpdateUserBan
public partial class SDReqUpdateUserBan : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqUpdateUserBan(serverSession, this.userNick, this.ban, this.minutes);
    }
}

//DMRspBCUpdateUserBan
public partial class DMRepBCBanUser : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDMReqBCBanUser(serverSession, this.userID, this.userNick, this.ban, this.minutes);
    }
}

//SMNotiBanUser
public partial class SMNotiBanUser : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMNotiBanUser(serverSession, this.userID);
    }
}

//SMReqCheckUserBan
public partial class SMReqCheckUserBan : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqCheckUserBan(serverSession, this.userID, this.nick, this.userType, this.sameDevice, this.deviceID, this.state, this.language);
    }
}

//SMReqBCUpdateLimit
public partial class SMReqBCUpdateLimit : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCUpdateLimit(serverSession, this.count);
    }
}

//MSNotiUpdateLimit
public partial class MSNotiUpdateLimit : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSNotiUpdateLimit(serverSession, this.count);
    }
}

//SDReqAddIAP
public partial class SDReqAddIAP : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqAddIAP(serverSession, this.userID, this.userNick, this.iAPType, this.itemIdx);
    }
}

//SDReqIAPResult
public partial class SDReqIAPResult : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqIAPResult(serverSession, this.session, this.guid, this.result);
    }
}
//SDReqGetPlayerProfile
public partial class SDReqGetPlayerProfile : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSDReqGetPlayerProfile(serverSession, this.playerNick);
    }
}

//SMReqBCReFreshLocalization
public partial class SMReqBCReFreshLocalization : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCReFreshLocalization(serverSession);
    }
}

//MDNotiUpdateLocalization
public partial class MDNotiUpdateLocalization : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMDNotiUpdateLocalization(serverSession);
    }
}

//SMReqBCReFreshNews
public partial class SMReqBCReFreshNews : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCReFreshNews(serverSession);
    }
}

//MDNotiUpdateNews
public partial class MDNotiUpdateNews : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMDNotiUpdateNews(serverSession);
    }
}

//SMReqBCReFreshMission
public partial class SMReqBCReFreshMission : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCReFreshMission(serverSession);
    }
}

//MDNotiUpdateMission
public partial class MDNotiUpdateMission : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMDNotiUpdateMission(serverSession);
    }
}

//DMRepBCNewFriendGift
public partial class DMRepBCNewFriendGift : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetDMRepBCNewFriendGift(serverSession, this.userID);
    }
}

//MSNotiNewFriendGift
public partial class MSNotiNewFriendGift : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSNotiNewFriendGift(serverSession, this.userID);
    }
}

//SMReqBCAlarmShop
public partial class SMReqBCAlarmShop : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCAlarmShop(serverSession, this.alarmString);
    }
}

//MSNotiAlarmShop
public partial class MSNotiAlarmShop : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSNotiAlarmShop(serverSession, this.alarmString);
    }
}

//SMReqBCAlarmNews
public partial class SMReqBCAlarmNews : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSMReqBCAlarmNews(serverSession, this.alarmString);
    }
}

//MSNotiAlarmNews
public partial class MSNotiAlarmNews : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetMSNotiAlarmNews(serverSession, this.alarmString);
    }
}

