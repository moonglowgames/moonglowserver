﻿using LitJson;
using MoonGlow;
using MoonGlow.Item;
using MoonGlow.Room;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//SSReqCheckEvent
public partial class SSReqCheckEvent : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(session);
        if (userInfo != null)
        {
            if (userInfo.userDataBase.Stamina < 100)
            {
                userInfo.CheckStaminaEvent();
            }
            if (userInfo.userDataBase.userProfile.addHP == 1)
            {
                userInfo.CheckEPElixirEvent();
            }
            if (userInfo.userDataBase.userProfile.addSP == 1)
            {
                userInfo.CheckSOElixirEvent();
            }
        }
        else
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
        }
    }
}

//SSReqClientPause
public partial class SSReqClientPause : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        InnerPacketsManager.Instance.GetSSReqClientPause(serverSession, this.session);
    }
}
