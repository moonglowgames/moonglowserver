﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public partial class SLogReqAccountInsert : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        //Console.WriteLine("Account Log Data:" + "PartitionKey: " + this.pckData.data.PartitionKey + "RowKey: " + this.pckData.data.RowKey);
        GameServer.azureStorage.AccountDataInsert(this.pckData.data);
    }
}

public partial class SLogReqLevelInsert : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        GameServer.azureStorage.LevelDataInsert(this.pckData.data);
    }
}

public partial class SLogReqItemInsert : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        GameServer.azureStorage.ItemDataInsert(this.pckData.data);
    }
}

public partial class SLogReqPlayInsert : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        GameServer.azureStorage.PlayDataInsert(this.pckData.data);
    }
}

public partial class SLogReqProfileInsert : Packet
{
    public override void Execute() { }
    public override void Execute(string serverSession)
    {
        GameServer.azureStorage.ProfileDataInsert(this.pckData.data);
    }
}
