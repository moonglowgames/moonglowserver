﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;
using MoonGlow;

public class PacketsHandlerInner
{
    public static void SMReqAddRedis(string serverSession, string userID, UserType userType, string nick, Language language)
    {
        SMReqAddRedis pck = new SMReqAddRedis();
        pck.userID = userID;
        pck.userType = userType;
        pck.nick = nick;
        pck.language = language;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    public static void MSRspAddRedis(string serverSession, bool addResult, string userSession, string userID, UserType userType, string nick, string gameServerUrl)
    {
        MSRspAddRedis pck = new MSRspAddRedis();
        pck.addResult = addResult;
        pck.session = userSession;
        pck.userID = userID;
        pck.userType = userType;
        pck.nick = nick;
        pck.gameServerUrl = gameServerUrl;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SMReqRefreshServerList(string serverSession)
    {
        SMReqRefreshServerList pck = new SMReqRefreshServerList();
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SDReqChangeNick(string serverSession, string userSession, string newNick)
    {
        SDReqChangeNick pck = new SDReqChangeNick();
        pck.session = userSession;
        pck.nick = newNick;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void MDNotiRefreshChannelInfo(string serverSession)
    {
        MDNotiRefreshChannelInfo pck = new MDNotiRefreshChannelInfo();
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void MDNotiRefreshClientVersion(string serverSession)
    {
        MDNotiRefreshClientVersion pck = new MDNotiRefreshClientVersion();
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
        
    }

    internal static void SMNotiUpdateEnable(string serverSession, string connectDNS_, int port_, bool _enable)
    {
        SMNotiUpdateEnable pck = new SMNotiUpdateEnable();
        pck.connectDNS = connectDNS_;
        pck.connectPort = port_;
        pck.enable = _enable;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SDReqGetPlayerProfile(string serverSession, string playerNick)
    {
        SDReqGetPlayerProfile pck = new SDReqGetPlayerProfile();
        pck.playerNick = playerNick;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SMReqRemoveRedis
    public static void SMReqRemoveRedis(string serverSession, string userID, string userSession)
    {
        SMReqRemoveRedis pck = new SMReqRemoveRedis();
        pck.userID = userID;
        pck.session = userSession;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //MSNotiRemoveRedis
    public static void MSNotiRemoveRedis(string serverSession, bool removeResult, string userSession, bool isLogout)
    {
        MSNotiRemoveRedis pck = new MSNotiRemoveRedis();
        pck.removeResult = removeResult;
        pck.session = userSession;
        pck.isLogout = isLogout;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    public static void SDReqNewUserData(string serverSession, AccountType accountType, string account, string password, string nick, int faceIdx, string deviceID)
    {
        SDReqNewUserData pck = new SDReqNewUserData();
        pck.accountType = accountType;
        pck.account = account;
        pck.password = password;
        pck.nick = nick;
        pck.faceIdx = faceIdx;
        pck.deviceID = deviceID;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    public static void DSRspNewUserData(string serverSession, AccountType accountType, bool createResult, string userID, string nick)
    {
        DSRspNewUserData pck = new DSRspNewUserData();
        pck.accountType = accountType;
        pck.createResult = createResult;
        pck.userID = userID;
        pck.nick = nick;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqReadUserData
    public static void SDReqReadUserData(string serverSession, string userSession, string userID)
    {
        SDReqReadUserData pck = new SDReqReadUserData();
        pck.session = userSession;
        pck.userID = userID;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void MSNotiServerState(string serverSession, string serverState)
    {
        MSNotiServerState pck = new MSNotiServerState();
        pck.serverState = serverState;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //DSRspReadUserData
    public static void DSRspReadUserData(string serverSession, bool readResult, string userSession, string jsonString)
    {
        DSRspReadUserData pck = new DSRspReadUserData();
        pck.readResult = readResult;
        pck.session = userSession;
        pck.jsonString = jsonString;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqUpdateUserInfo
    public static void SDReqUpdateUserInfo(string serverSession, UserData userData)
    {
        SDReqUpdateUserInfo pck = new SDReqUpdateUserInfo();
        pck.userData = userData;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SSReqCheckEvent
    public static void SSReqCheckEvent(string serverSession, string userSession)
    {
        SSReqCheckEvent pck = new SSReqCheckEvent();
        pck.session = userSession;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqGetMailList
    public static void SDReqGetMailList(string serverSession, string userSession)
    {
        SDReqGetMailList pck = new SDReqGetMailList();
        pck.session = userSession;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void SMReqCheckUserBan(string serverSession_, string userID_, UserType userType_, string nick_, bool sameDevice_, string deviceID_, LoginState state_, Language language_)
    {
        SMReqCheckUserBan pck = new SMReqCheckUserBan();
        pck.userID = userID_;
        pck.userType = userType_;
        pck.nick = nick_;
        pck.sameDevice = sameDevice_;
        pck.deviceID = deviceID_;
        pck.state = state_;
        pck.language = language_;
        QueueManager.Instance.AddMemDBQueue(serverSession_, pck);
    }

    //SDReqUpdateMail
    public static void SDReqUpdateMailItem(string serverSession, string userSession, int mailDBIdx)
    {
        SDReqUpdateMail pck = new SDReqUpdateMail();
        pck.session = userSession;
        pck.mailDBIdx = mailDBIdx;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SDReqNewMail
    public static void SDReqNewMail(string serverSession, MailData mailData)
    {
        SDReqNewMail pck = new SDReqNewMail();
        pck.mailData = mailData;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //DSRspNewMail
    public static void DSRspNewMail(string serverSession, bool addResult, string userSession, string senderD)
    {
        DSRspNewMail pck = new DSRspNewMail();
        pck.addResult = addResult;
        pck.session = userSession;
        pck.senderID = senderD;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqGetFriendList
    public static void SDReqGetFriendList(string serverSession, string userSession)
    {
        SDReqGetFriendList pck = new SDReqGetFriendList();
        pck.session = userSession;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //DSRspGetFriendList
    public static void DSRspGetFriendList(string serverSession, bool getResult, string userSession)
    {
        DSRspGetFriendList pck = new DSRspGetFriendList();
        pck.getResult = getResult;
        pck.session = userSession;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqNewFriend
    public static void SDReqNewFriend(string serverSession, string userID,/* string userNick,*/ string friendID/*, string friendNick*/)
    {
        SDReqNewFriend pck = new SDReqNewFriend();
        pck.userID = userID;
        //pck.userNick = userNick;
        pck.friendID = friendID;
        //pck.friendNick = friendNick;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //DSRspNewFriend
    public static void DSRspNewFriend(string serverSession, bool addResult, string userID, string friendID)
    {
        DSRspNewFriend pck = new DSRspNewFriend();
        pck.addResult = addResult;
        pck.userID = userID;
        pck.friendID = friendID;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqUpdateFriendType
    public static void SDReqUpdateFriendType(string serverSession,string userSession, string userID, string friendID, int otherFriendCount)
    {
        SDReqUpdateFriendType pck = new SDReqUpdateFriendType();
        pck.session = userSession;
        pck.userID = userID;
        pck.friendID = friendID;
        pck.otherFriendCount = otherFriendCount;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //DSRspUpdateFriendType
    public static void DSRspUpdateFriendType(string serverSession, bool updateResult, string userSession, string friendID, int otherFriendCount)
    {
        DSRspUpdateFriendType pck = new DSRspUpdateFriendType();
        pck.updateResult = updateResult;
        pck.session = userSession;
        pck.friendID = friendID;
        pck.otherFriendCount = otherFriendCount;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqUserFriendCount
    public static void SDReqUserFriendCount(string serverSession, string userSession, string friendID, int iDx, PacketTypeList packetTypeList)
    {
        SDReqUserFriendCount pck = new SDReqUserFriendCount();
        pck.session = userSession;
        pck.friendID = friendID;
        pck.iDx = iDx;
        pck.sender = packetTypeList;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //DSRspUserFriendCount
    public static void DSRspUserFriendCount(string serverSession, bool getResult,string userSession, int iDx, int otherFriendCount, PacketTypeList sender)
    {
        DSRspUserFriendCount pck = new DSRspUserFriendCount();
        pck.getResult = getResult;
        pck.session = userSession;
        //pck.friendID = friendID;
        pck.iDx = iDx;
        pck.otherFriendCount = otherFriendCount;
        pck.sender = sender;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqGetMyFriends
    public static void SDReqGetMyFriends(string serverSession, string userSession)
    {
        SDReqGetMyFriends pck = new SDReqGetMyFriends();
        pck.session = userSession;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //DSRspGetMyFriends
    public static void DSRspGetMyFriends(string serverSession, bool getResult, string userSession)
    {
        DSRspGetMyFriends pck = new DSRspGetMyFriends();
        pck.getResult = getResult;
        pck.session = userSession;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqGetRequestFriends
    public static void SDReqGetRequestFriends(string serverSession, string userSession)
    {
        SDReqGetRequestFriends pck = new SDReqGetRequestFriends();
        pck.session = userSession;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //DSRspGetRequestFriends
    public static void DSRspGetRequestFriends(string serverSession, bool getResult, string userSession)
    {
        DSRspGetRequestFriends pck = new DSRspGetRequestFriends();
        pck.getResult = getResult;
        pck.session = userSession;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqDeleteFriend
    public static void SDReqDeleteFriend(string serverSession, string userSession, string friendID, FriendType friendType)
    {
        SDReqDeleteFriend pck = new SDReqDeleteFriend();
        pck.session = userSession;
        pck.friendID = friendID;
        pck.friendType = friendType;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //DSRspDeleteFriend
    public static void DSRspDeleteFriend(string serverSession, bool delResult, string userSession, int otherFriendCount, string friendID, FriendType friendType)
    {
        DSRspDeleteFriend pck = new DSRspDeleteFriend();
        pck.deleteResult = delResult;
        pck.session = userSession;
        pck.otherFriendCount = otherFriendCount;
        pck.friendID = friendID;
        pck.friendType = friendType;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqUpdateSendFriendItem
    public static void SDReqUpdateSendFriendItem(string serverSession, string userSession, string friendID, string sendDate)
    {
        SDReqUpdateSendFriendItem pck = new SDReqUpdateSendFriendItem();
        pck.session = userSession;
        pck.friendID = friendID;
        pck.sendDate = sendDate;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SDReqUpdateGetFriendItem
    public static void SDReqUpdateGetFriendItem(string serverSession, string userSession, string friendID)
    {
        SDReqUpdateGetFriendItem pck = new SDReqUpdateGetFriendItem();
        pck.session = userSession;
        pck.friendID = friendID;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SDReqGiftSendDate
    public static void SDReqGiftSendDate(string serverSession, string userSession, string friendID, string sendDate)
    {
        SDReqGiftSendDate pck = new SDReqGiftSendDate();
        pck.session = userSession;
        pck.friendID = friendID;
        pck.sendDate = sendDate;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SMReqGetChanelList
    public static void SMReqGetChanelList(string serverSession, ChanelInfoList chanelInfoList)
    {
        SMReqGetChanelList pck = new SMReqGetChanelList();
        pck.chanelInfoList = chanelInfoList;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //MSRspGetChanelList
    public static void MSRspGetChanelList(string serverSession, ChanelInfoList chanelInfoList)
    {
        MSRspGetChanelList pck = new MSRspGetChanelList();
        pck.chanelInfoList = chanelInfoList;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SMReqAddRank
    public static void SMReqAddRank(string serverSession, /*bool isJoin,*/ int channelIdx, string chanelName, string userSession, /*float*/int rankPoint)
    {
        SMReqAddRank pck = new SMReqAddRank();
        //pck.isJoin = isJoin;
        pck.chanelIdx = channelIdx;
        pck.chanelName = chanelName;
        pck.session = userSession;
        pck.rankPoint = rankPoint;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //MSRspAddRank
    public static void MSRspAddRank(string serverSession, ChanelResult result, string userSession, int currentCount, int chanelIdx, string chanelName)
    {
        MSRspAddRank pck = new MSRspAddRank();
        pck.result = result;
        pck.session = userSession;
        pck.channelIdx = chanelIdx;
        pck.chanelName = chanelName;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SMReqRankRange
    public static void SMReqRankRange(string serverSession, string userSession, string chanelName)
    {
        SMReqRankRange pck = new SMReqRankRange();
        pck.session = userSession;
        pck.chanelName = chanelName;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //MDReqRankRange
    public static void MDReqRankRange(string serverSession, RankRange rankRange)
    {
        MDReqRankRange pck = new MDReqRankRange();
        pck.rankRange = rankRange;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //DSRspRankRange
    public static void DSRspRankRange(string serverSession, RankList rankList)
    {
        DSRspRankRange pck = new DSRspRankRange();
        pck.rankList = rankList;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqRankHistory
    public static void SDReqRankHistory(string serverSession, string userSession)
    {
        SDReqRankHistory pck = new SDReqRankHistory();
        pck.session = userSession;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //DSRspRankHistory
    public static void DSRspRankHistory(string serverSession, PCKRankHisList rankHisList)
    {
        DSRspRankHistory pck = new DSRspRankHistory();
        pck.rankHisList = rankHisList;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SMReqCurrentRank
    public static void SMReqCurrentRank(string serverSession, string userSession, string userID, int chanelIdx, string chanelName, int rankPoint, int rankWin, int rankLose, int rankDraw, bool isChanelClose)
    {
        SMReqCurrentRank pck = new SMReqCurrentRank();
        pck.session = userSession;
        pck.userID = userID;
        pck.chanelIdx = chanelIdx;
        pck.chanelName = chanelName;
        pck.rankPoint = rankPoint;
        pck.rankWin = rankWin;
        pck.rankLose = rankLose;
        pck.rankDraw = rankDraw;
        pck.isChanelClose = isChanelClose;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //MSRspCurrentRank
    public static void MSRspCurrentRank(string serverSession, string userSession, string userID, int chanelIdx, string chanelName, int currentCount, int totalCount, int rankPoint, int rankWin, int rankLose, int rankDraw, bool isChanelClose)
    {
        MSRspCurrentRank pck = new MSRspCurrentRank();
        pck.session = userSession;
        pck.userID = userID;
        pck.chanelIdx = chanelIdx;
        pck.chanelName = chanelName;
        pck.currentCount = currentCount;
        pck.totalCount = totalCount;
        pck.rankPoint = rankPoint;
        pck.rankWin = rankWin;
        pck.rankLose = rankLose;
        pck.rankDraw = rankDraw;
        pck.isChanelClose = isChanelClose;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqNewRankHis
    public static void SDReqNewRankHis(string serverSession, string userSession, string userID, string chanelName, int currentCount, int currentPercent, /*float*/int currentPoint, int currentRankWin, int currentRankLose, int currentRankDraw, bool itemExist)
    {
        SDReqNewRankHis pck = new SDReqNewRankHis();
        pck.session = userSession;
        pck.userID = userID;
        pck.chanelName = chanelName;
        pck.currentCount = currentCount;
        pck.currentPercent = currentPercent;
        pck.currentPoint = currentPoint;
        pck.currentRankWin = currentRankWin;
        pck.currentRankLose = currentRankLose;
        pck.currentRankDraw = currentRankDraw;
        pck.itemExist = itemExist;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //DSRspNewRankHis
    public static void DSRspNewRankHis(string serverSession, bool addResult, string userSession)
    {
        DSRspNewRankHis pck = new DSRspNewRankHis();
        pck.addResult = addResult;
        pck.session = userSession;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SDReqUpdateRankHis
    public static void SDReqUpdateRankHis(string serverSession, string userID, string chanelName)
    {
        SDReqUpdateRankHis pck = new SDReqUpdateRankHis();
        pck.userID = userID;
        pck.chanelName = chanelName;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void SDReqSaveToSql(string serverSession, string userID, string nickName, int faceIdx, int level, int rankPoint, string lastLoginDate)
    {
        SDReqSaveToSql pck = new SDReqSaveToSql();
        pck.userID = userID;
        pck.nickName = nickName;
        pck.faceIdx = faceIdx;
        pck.level = level;
        pck.rankPoint = rankPoint;
        pck.lastLoginDate = lastLoginDate;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SMReqRemoveRankHis
    public static void SMReqUpdateRank(string serverSession, string chanelName,string userSession, string userID)
    {
        SMReqUpdateRank pck = new SMReqUpdateRank();
        pck.chanelName = chanelName;
        pck.session = userSession;
        pck.userID = userID;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //SDReqAddChanelUser
    public static void SDReqAddChanelUser(string serverSession, string chanelName, string userID)
    {
        SDReqAddChanelUser pck = new SDReqAddChanelUser();
        pck.chanelName = chanelName;
        pck.userID = userID;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SDReqUpdateChanelInfo
    public static void SDReqUpdateChanelInfo(string serverSession, int chanelIdx)
    {
        SDReqUpdateChanelInfo pck = new SDReqUpdateChanelInfo();
        pck.chanelIdx = chanelIdx;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SDReqRemoveChanelUser
    public static void SDReqRemoveChanelUser(string serverSession, string chanelName)
    {
        SDReqRemoveChanelUser pck = new SDReqRemoveChanelUser();
        pck.chanelName = chanelName;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SDReqUpdateChanelInfoDate
    public static void SDReqUpdateChanelInfoDate(string serverSession, int chanelIdx, string dateTime)
    {
        SDReqUpdateChanelInfoDate pck = new SDReqUpdateChanelInfoDate();
        pck.chanelIdx = chanelIdx;
        pck.dateTime = dateTime;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SDReqRepairChanel
    public static void SDReqRepairChanel(string serverSession)
    {
        SDReqRepairChanel pck = new SDReqRepairChanel();
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //DMReqRepairChanel
    public static void DMReqRepairChanel(string serverSession, ChanelUserList chanelUserList)
    {
        DMReqRepairChanel pck = new DMReqRepairChanel();
        pck.chanelUserList = chanelUserList;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //MSRepairChanel
    public static void MSRspRepairChanel(string serverSession, bool repairResult)
    {
        MSRspRepairChanel pck = new MSRspRepairChanel();
        pck.repairResult = repairResult;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SMReqBC
    internal static void SMReqBCServerNoti(string serverSession, string message)
    {
        SMReqBCServerNoti pck = new SMReqBCServerNoti();
        pck.message = message;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //MSNotiBC
    internal static void MSNotiBCServerNoti(string serverSession, string message)
    {
        MSNotiBCServerNoti pck = new MSNotiBCServerNoti();
        pck.message = message;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SMReqSessionCheck
    internal static void SMReqLogingSessionCheck(string serverSession, string userID, string userSession, Language language)
    {
        SMReqLoginSessionCheck pck = new SMReqLoginSessionCheck();
        pck.userID = userID;
        pck.session = userSession;
        pck.language = language;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //MSRspLoginSessionCheck
    internal static void MSRspLoginSessionCheck(string serverSession, bool result, string userSession, string userID, Language language)
    {
        MSRspLoginSessionCheck pck = new MSRspLoginSessionCheck();
        pck.result = result;
        pck.session = userSession;
        pck.userID = userID;
        pck.language = language;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SDReqRegisterCheck(string serverSession, AccountType accountType, string account, string password, string nick, int faceIdx, string deviceID)
    {
        SDReqRegisterCheck pck = new SDReqRegisterCheck();
        pck.accountType = accountType;
        pck.account = account;
        pck.password = password;
        pck.nick = nick;
        pck.faceIdx = faceIdx;
        pck.deviceID = deviceID;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void DSRspRegisterCheck(string serverSession, LoginState state, AccountType accountType, string account, string password, string nick, int faceIdx, string deviceID)
    {
        DSRspRegisterCheck pck = new DSRspRegisterCheck();
        pck.state = state;
        pck.accountType = accountType;
        pck.account = account;
        pck.password = password;
        pck.nick = nick;
        pck.faceIdx = faceIdx;
        pck.deviceID = deviceID;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SDReqLoginCheck(string serverSession, AccountType idType, string account, string password, string deviceID, Language language)
    {
        SDReqLoginCheck pck = new SDReqLoginCheck();
        pck.accountType = idType;
        pck.account = account;
        pck.password = password;
        pck.deviceID = deviceID;
        pck.language = language;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void DSRspLoginCheck(string serverSession, string userID, UserType userType, string nick, bool sameDevice, string deviceID, LoginState state, Language language)
    {
        DSRspLoginCheck pck = new DSRspLoginCheck();
        //pck.account = account;
        pck.userID = userID;
        pck.userType = userType;
        pck.nick = nick;
        pck.sameDevice = sameDevice;
        pck.deviceID = deviceID;
        pck.state = state;
        pck.language = language;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    //SMReqExpireSession
    internal static void SMReqExpireUserInfo(string serverSession, string userSession)
    {
        SMReqExpireUserInfo pck = new SMReqExpireUserInfo();
        pck.session = userSession;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //SSReqClientPause
    internal static void SSReqClientPause(string serverSession     , string userSession)
    {
        SSReqClientPause pck = new SSReqClientPause();
        pck.session = userSession;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SMReqSetServerGlobalState(string serverSession, string state)
    {
        SMReqSetServerGlobalState pck = new SMReqSetServerGlobalState();
        pck.state = state;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void MSRspSetServerGlobalState(string serverSession, bool result, string state)
    {
        MSRspSetServerGlobalState pck = new MSRspSetServerGlobalState();
        pck.setResult = result;
        pck.state = state;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SMReqBCGlobalServerState(string serverSession, string serverState)
    {
        SMReqBCServerState pck = new SMReqBCServerState();
        pck.serverState = serverState;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SDReqSetDeveloper(string serverSession, string userNick, UserType userType)
    {
        SDReqSetDeveloper pck = new SDReqSetDeveloper();
        pck.nick = userNick;
        pck.userType = userType;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void DSRspSetDeveloper(string serverSession, string userNick, UserType userType, bool setResult)
    {
        DSRspSetDeveloper pck = new DSRspSetDeveloper();
        pck.nick = userNick;
        pck.userType = userType;
        pck.setResult = setResult;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SMReqBCReFreshUserType(string serverSession, UserType userType)
    {
        SMReqBCReFreshUserType pck = new SMReqBCReFreshUserType();
        pck.userType = userType;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void MSNotiBCReFreshUserType(string serverSession, UserType userType)
    {
        MSNotiBCReFreshUserType pck = new MSNotiBCReFreshUserType();
        pck.userType = userType;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SDReqDevList(UserType userType)
    {
        SDReqDevList pck = new SDReqDevList();
        pck.userType = userType;
        QueueManager.Instance.AddDataBaseQueue(string.Empty, pck);
    }

    internal static void SMReqExpireRedis(string serverSession, string userID)
    {
        SMReqExpireRedis pck = new SMReqExpireRedis();
        pck.userID = userID;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SMReqBCForceClose(string serverSession, string userID)
    {
        SMReqBCForceClose pck = new SMReqBCForceClose();
        pck.userID = userID;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void MSNotiBCForceClose(string serverSession, string gameServerUrl, string userSession)
    {
        MSNotiBCForceClose pck = new MSNotiBCForceClose();
        pck.gameServerUrl = gameServerUrl;
        pck.userSession = userSession;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SDReqUpdatedeviceID(string serverSession, string userID, /*string account,*/ string deviceID)
    {
        SDReqUpdatedeviceID pck = new SDReqUpdatedeviceID();
        pck.userID = userID;
        //pck.account = account;
        pck.deviceID = deviceID;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void SDReqUpdatePassword(string serverSession, string account, string oldPassword, string newPassword, bool isFind)
    {
        SDReqUpdatePassword pck = new SDReqUpdatePassword();
        pck.account = account;
        pck.oldPassword = oldPassword;
        pck.newPassword = newPassword;
        pck.isFind = isFind;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void DSRspUpdatePassword(string serverSession, PacketTypeList pckType, LoginState state)
    {
        DSRspUpdatePassword pck = new DSRspUpdatePassword();
        pck.pckType = pckType;
        pck.state = state;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SDReqUpdateFriendFight(string serverSession, string userID, string friendID, Result result)
    {
        SDReqUpdateFriendFight pck = new SDReqUpdateFriendFight();
        pck.userID = userID;
        pck.friendID = friendID;
        pck.result = result;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void MSRspUpdateServerEnable(string serverSession)
    {
        MSRspUpdateServerEnable pck = new MSRspUpdateServerEnable();
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SMReqBCReFreshServerList(string serverSession)
    {
        SMReqBCReFreshServerList pck = new SMReqBCReFreshServerList();
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SMReqBCReFreshChannelInfo(string serverSession)
    {
        SMReqBCReFreshChannelInfo pck = new SMReqBCReFreshChannelInfo();
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SMReqBCReFreshClientVersions(string serverSession)
    {
        SMReqBCReFreshClientVersions pck = new SMReqBCReFreshClientVersions();
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SMReqBCUpdateServerEnable(string serverSession, string connectDNS_, int connectPort_, bool enable_)
    {
        SMReqBCUpdateServerEnable pck = new SMReqBCUpdateServerEnable();
        pck.connectDNS = connectDNS_;
        pck.connectPort = connectPort_;
        pck.enable = enable_;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);

    }

    internal static void SMReqGetAllServerInfo(string serverSession)
    {
        SMReqGetAllServerInfo pck = new SMReqGetAllServerInfo();
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SMReqGetThisServerInfo(string serverSession)
    {
        SMReqGetThisServerInfo pck = new SMReqGetThisServerInfo();
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SMReqBCUpdateAllServerEnable(string serverSession, bool enable)
    {
        SMReqBCUpdateAllServerEnable pck = new SMReqBCUpdateAllServerEnable();
        pck.enable = enable;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SMReqChangeServer(string serverSession, string userSession, string connectUrl)
    {
        SMReqChangeServer pck = new SMReqChangeServer();
        pck.session = userSession;
        pck.connectUrl = connectUrl;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SDReqUpdateUserBan(string serverSession, string userNick, bool ban, int minutes)
    {
        SDReqUpdateUserBan pck = new SDReqUpdateUserBan();
        pck.userNick = userNick;
        pck.ban = ban;
        pck.minutes = minutes;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void DMRepBCBanUser(string serverSession, string userID, string userNick, bool ban, int minutes)
    {
        DMRepBCBanUser pck = new DMRepBCBanUser();
        pck.userID = userID;
        pck.userNick = userNick;
        pck.ban = ban;
        pck.minutes = minutes;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SMNotiBanUser(string serverSession, string userID)
    {
        SMNotiBanUser pck = new SMNotiBanUser();
        pck.userID = userID;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void SMReqBCUpdateLimit(string serverSession, int count)
    {
        SMReqBCUpdateLimit pck = new SMReqBCUpdateLimit();
        pck.count = count;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void MSNotiUpdateLimit(string serverSession, int count)
    {
        MSNotiUpdateLimit pck = new MSNotiUpdateLimit();
        pck.count = count;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SDReqAddIAP(string serverSession, string userID_, string userNick_, IAPType iAPType_, int itemIdx_)
    {
        SDReqAddIAP pck = new SDReqAddIAP();
        pck.userID = userID_;
        pck.userNick = userNick_;
        pck.iAPType = iAPType_;
        pck.itemIdx = itemIdx_;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    internal static void SDReqIAPResult(string serverSession, string userSession_, string guid_, IAPResult result_)
    {
        SDReqIAPResult pck = new SDReqIAPResult();
        pck.session = userSession_;
        pck.guid = guid_;
        pck.result = result_;
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SMReqBCReFreshLocalization
    internal static void SMReqBCReFreshLocalization(string serverSession)
    {
        SMReqBCReFreshLocalization pck = new SMReqBCReFreshLocalization();
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //MDNotiUpdateLocalization
    internal static void MDNotiUpdateLocalization(string serverSession)
    {
        MDNotiUpdateLocalization pck = new MDNotiUpdateLocalization();
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SMReqBCReFreshNews
    internal static void SMReqBCReFreshNews(string serverSession)
    {
        SMReqBCReFreshNews pck = new SMReqBCReFreshNews();
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //MDNotiUpdateNews
    internal static void MDNotiUpdateNews(string serverSession)
    {
        MDNotiUpdateNews pck = new MDNotiUpdateNews();
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //SMReqBCReFreshMission
    internal static void SMReqBCReFreshMission(string serverSession)
    {
        SMReqBCReFreshMission pck = new SMReqBCReFreshMission();
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    //MDNotiUpdateMission
    internal static void MDNotiUpdateMission(string serverSession)
    {
        MDNotiUpdateMission pck = new MDNotiUpdateMission();
        QueueManager.Instance.AddDataBaseQueue(serverSession, pck);
    }

    //DMRepBCNewFriendGift
    internal static void DMRepBCNewFriendGift(string serverSession, string userID)
    {
        DMRepBCNewFriendGift pck = new DMRepBCNewFriendGift();
        pck.userID = userID;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void MSNotiNewFriendGift(string serverSession, string userID)
    {
        MSNotiNewFriendGift pck = new MSNotiNewFriendGift();
        pck.userID = userID;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SMReqBCAlarmShop(string serverSession, string alarmString)
    {
        SMReqBCAlarmShop pck = new SMReqBCAlarmShop();
        pck.alarmString = alarmString;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void MSNotiAlarmShop(string serverSession, string alarmString)
    {
        MSNotiAlarmShop pck = new MSNotiAlarmShop();
        pck.alarmString = alarmString;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }

    internal static void SMReqBCAlarmNews(string serverSession, string alarmString)
    {
        SMReqBCAlarmNews pck = new SMReqBCAlarmNews();
        pck.alarmString = alarmString;
        QueueManager.Instance.AddMemDBQueue(serverSession, pck);
    }

    internal static void MSNotiAlarmNews(string serverSession, string alarmString)
    {
        MSNotiAlarmNews pck = new MSNotiAlarmNews();
        pck.alarmString = alarmString;
        QueueManager.Instance.AddWorkQueue(serverSession, pck);
    }


}


