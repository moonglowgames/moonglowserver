﻿using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//SSReqCheckEvent
public partial class SSReqCheckEvent : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SSReqCheckEvent;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        //retvalue = JsonMapper.ToJson(userInfo);
        element["session"] = this.session;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            //userInfo = JsonMapper.ToObject<UserInfo>(Json);
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SSReqClientPause
public partial class SSReqClientPause : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SSReqClientPause;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}


