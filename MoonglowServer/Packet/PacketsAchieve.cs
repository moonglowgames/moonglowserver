﻿using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//CSReqAchieveList 
public partial class CSReqAchieveList : Packet
{
    public string session = "";

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqAchieveList;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion


}

public class Achievement
{
    public int achieveIdx { get; set; }
    public int currentValue { get; set; }
    public int targetValue { get; set; }
    public Achievement() { }
    public Achievement(int achieveIdx_, int currentValue_, int targetValue_) 
    {
        achieveIdx = achieveIdx_;
        currentValue = currentValue_;
        targetValue = targetValue_;
    }
}
public class AchieveList
{
    public string name { get; set; }
    public List<Achievement> achieveList { get; set; }
    public AchieveList() { }
    public AchieveList(string packetName, List<Achievement> achieveList_)
    {
        name = packetName;
        achieveList = achieveList_;
    }
}

//SCNotiAchieveList
public partial class SCNotiAchieveList : Packet
{
    public AchieveList achieveList = null; 
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiAchieveList;
        base.Init();

        //throw new NotImplementedException ();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(achieveList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            achieveList = JsonMapper.ToObject<AchieveList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion


}
