﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum PacketSendType { Unknown = 0, CSRps = 1, SCNoti = 2, CSReq = 3, CSNoti = 4 };
public enum EncriptionType
{
    Unknown = 0, Telnet = 1, Json = 2, Base64Telnet = 11,
    Base64Json = 12, ZipTelnet = 21, ZipJson = 22, EncriptTelnet = 31, EncriptJson = 32
};
public enum EncodeType { Telnet, Json }

public enum UserType { User = 0, Operator = 1, OperatorManager = 2, Developer = 3, DeveloperManager = 4, All = 5 }

public enum GameType { Unknown = 0, All = 1, Colmaret = 2, Joha = 3, Cheshire = 4, Zatoo = 5, Armdree = 6, Tarassen = 7, Krazan = 8, Yanan = 9 }
public enum PlayMode { Unknown = 0, All = 1, Single2 = 2, Single3 = 3, Single4 = 4, Team = 5 }
public enum PlayTime { Unknown = 0, All = 1, Sec_30 = 2, Sec_60 = 3 }
public enum ElementType { Unknow = 0, Stamina = 1, Gold = 2, MoonRuby = 3, HP = 4, SP = 5, Ink = 6, JokerLine = 7, Stake = 8 }
public enum ServerState { Unkown = 0, Normal = 1, Ready = 2, ShutDown = 3 }

public enum AccountType { Unknown = 0, Email = 1, Google = 2, Facebook = 3, Guest = 4}
public enum ConnectType { Unkown = 0, iOS = 100, Android = 200,  WinStore = 300,  Web = 400, PC = 500, Mac = 600 }

public enum UrlType { Unknown = 0, HomePage = 1, AppStore = 2, GooglePlay = 3, MSStore = 4, Manual = 5, Facebook = 6 }

public enum LoginState { Unknown = 0, OK = 1, Fail = 2, NoID = 3, NoNick = 4, MissPassword = 5, IDExist = 6, NickExist = 7, IDAndNickExist = 8, ServerReadyFail = 9, OldVersion = 10, Ban = 11, GuestOK = 12 }
public enum SceneType { Unknown = 0, Close = 1, loading = 2, Home = 3, Room = 4, Shop = 5, MyPage = 6, Joker = 7, News = 8, Mail = 9,
    Achievement = 10, Mission = 11, Friend = 12, Chanel = 13, Rank = 14, RankHistory = 15}

public enum SendMsgType { Me, RoomAll, RoomOthers, ServerAll }

public enum Language { English, Korean, Chinese, Japanese, French, German }

/* EtherealInk: reset joker
 * SaqqaraTablet: rename
 * RosemaryOil: full charge stamina
 * SLElixir: stamina up to 200
 * EPElixir: +1HP during 2hrs
 * SOElixir: +1SP during 2hrs
 */
public enum UserItems { EtherealInk = 101, SaqqaraTablet = 102, RosemaryOil = 103, SLElixir = 104, EPElixir = 105, SOElixir = 106}

public enum GameAction { Unknown = 0, Exit = 1, EndGame = 2, Restart = 3 }
public enum EffectType { None = 0, Buff01, Buff02, DeBuff, ElectricAttack, FireWorkAttack, FlameAttack, FlashAttack, IceAttack, LightAttack, StarDustAttack01, StarDustAttack02 }
public enum CardUseType { Unknown = 0, Me = 1, Enemy = 2, Others = 3, Deck = 4, MeBasedOnEnemy = 5, EnemyAndAll = 6, All = 7 }
public enum ShowAnimationType { ShowCard, AnimationFromDeck }

public enum TabType { Unknown = 0, Items = 1, Joker = 2, Gold = 3, Ruby = 4, Character= 5, Pet = 6, Badge = 7, CardSkin = 8}
public enum PayType { None = 0, Gold = 1, Ruby = 2, Cash = 3, AD_GOLD = 4, AD_RUBY=5 }
public enum SpecialType { Normal = 0, Sale = 1, Best = 2, SoldOut = 3, SaleAndBest = 4, SaleAndSoldOut = 5 }
public enum ShopResult { Unknown = 0, OK = 1, LackRuby = 2, LackGold = 3, CancelPayment = 4, AlreadyExist = 5, Fail = 6 }

public enum SpellType { Unknown = 0, MoonDust = 1, Mana = 2, RodEnergy = 3 }
public enum SpellResult { Unknown = 0, OK = 1, LackInk = 2, MaxSpellLevel = 3, NoUpgrade = 4, Fail = 5 }

public enum MissionType { Mission = 1, Event = 2 }
public enum MissionState { Normal = 0, Complete = 1, Fail = 2 }

public enum FriendTabType { Unknown = 0, MyFriends = 1, RequestFriends = 2, RecommendFriends = 3 }
public enum FriendType { Friend = 0, Requesting = 1, Accepting = 2, Recommend = 3}
public enum FriendResult { Unknown = 0, OK = 1, NotExist = 2, NotAccept = 3, Fail = 4 }

public enum ChanelType { Unknown = 0, Beginner = 1, Intermediate = 2, Advance = 3}
public enum RankType { Unknown = 0, Count = 1, Percent = 2}
public enum ChanelResult {Unknown = 0, OK = 1, NotAccept = 2, LevelLimit = 3, Fail = 4 }

public enum Command { Message = 0, BanTime = 1, BanAfterGame = 2}

public enum Result { Lose, Draw, Win}
public enum MessageType { Unknown = 0, LackStamina = 1, LowLevel = 2, MaxPetCountAchieved = 3, AlreaySet = 4, LackGold = 5, JoinReject = 6, NoRoom = 7, FullRoom = 8, SendJoinOk = 9, AlreadyPlay = 10, RoomCreator = 11 }

//Logging area
public enum LogAccountType { Unknown, Register, Login, SessionLogin, Logout, Disconnect}
public enum LogItemType { Unknown, Reward, Friend, Purchase, Use, Return, Game, LevelUp, LevelDown }
public enum LogPlayType {Unknown, Create, Join, End }
public enum ChangeNickResult { Unknown, OK, Fail, LackItem, NickExist }

[Flags]
public enum JokerFaceOption {  Unknown = 0x0, MoonDust = 0x01, Mana = 0x02, RodEnergy = 0x04 }

public enum IAPType { Unknown = 0, AppStore = 1, GooglePlay = 2 }
public enum IAPResult { Unknown = 0, OK = 1, Fail = 2, Cancel = 3}

public enum CardPriceColor { Moondust = 0, RodEnergy = 1, Mana = 2 }
public enum Option { Setting, Mail, Quest, MyPage, Rank, Friend, Joker, Shop, News }
public enum GameEndReason { Unknown, IHPFull, IResFull, IHPZero, IExit, IExitWithinTenPlay, EnemyHPFull, EnemyResFull, EnemyHPZero, EnemyExit, EnemyExitWithinTenPlay }
public enum ChatChannel { Kor, Eng, Any, All}
public enum ChatType { Emoji = 0, Phrase = 1}
public enum EffectPosition { TargetCenter, TargetBottom, TargetTop, MeToTarget, TargetTome, SkyTotarget, PlayCenter, ActiveCard }
