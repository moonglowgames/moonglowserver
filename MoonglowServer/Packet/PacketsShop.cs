﻿using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//public enum TabType { Items =1, Joker = 2, Gold = 3, Ruby = 4}
//public enum PayType { Gold = 0 , Ruby = 1, Cash = 2}
//public enum SpecialType { Normal = 0, Sale = 1, Best = 2, SoldOut = 3, SaleAndBest = 4, SaleAndSoldOut = 5 }
//public enum ShopResult { Unknown = 0, OK=1, LackRuby = 200, LackGold = 300, CancelPayment = 400 }

//CSReqShopList
public partial class CSReqShopList : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqShopList;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class ShopListInfo 
{
    public TabType tabID { get; set; } //( Items (default) = 0, Joker = 1, Gold = 2, Ruby = 3 )
    public int elementIdx { get; set; }
    public PayType payType  { get; set; } // ( Gold = 0, Ruby = 1, Cash = 2 )
    public int quantity { get; set; }
    public double price { get; set; }
    public SpecialType specialType { get; set; } //( Normal = 0, Sale = 1, Best = 2 )
    public double salePrice { get; set; }

    public ShopListInfo() { }
    public ShopListInfo(TabType tabID_, int elementIdx_, PayType payType_, int quantity_, double price_, SpecialType specialType_, double salePrice_)
    {
        tabID = tabID_;
        elementIdx = elementIdx_;
        payType = payType_;
        this.quantity = quantity_;
        price = price_;
        specialType = specialType_;
        salePrice = salePrice_;
    }
}
public class ShopList 
{
    public List<ShopListInfo> shopList { get; set; }
    public string name { get; set; }

    public ShopList() { }
    public ShopList(string packetName_, List<ShopListInfo> shopList_) 
    {
        name = packetName_;
        shopList = shopList_;
    }
}

//SCNotiShopList
public partial class SCNotiShopList : Packet
{
    public ShopList shopList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiShopList;
        base.Init();

        //throw new NotImplementedException ();
    }
   
    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(shopList);

        //retvalue = element.ToJson ();

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            shopList = JsonMapper.ToObject<ShopList>(Json);

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqBuyElement 
public partial class CSReqBuyElement : Packet
{
    public string session = string.Empty;
    public int elementIdx = 0;
    public int count = 0;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqBuyElement;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["elementIdx"] = this.elementIdx;
        element["count"] = this.count;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            elementIdx = Convert.ToInt32(jsonData["elementIdx"].ToString());
            count = int.Parse(jsonData["count"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SCRspBuyElement 
public partial class SCRspBuyElement : Packet
{
    public ShopResult result = ShopResult.OK;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCRspBuyElement;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["result"] = this.result.ToString();

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            result = (ShopResult)Enum.Parse(typeof(ShopResult), jsonData["result"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SCNotiShowAD
public partial class SCNotiShowAD : Packet
{
    public bool isOK = false;
    public string guid = string.Empty;
    public int remainCount = 0;
    public string remainTime = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiShowAD;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["isOK"] = this.isOK;
        element["guid"] = this.guid;
        element["todayRemainCount"] = this.remainCount;
        element["remainTime"] = this.remainTime;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            isOK = Boolean.Parse(jsonData["isOK"].ToString());
            guid = jsonData["guid"].ToString();
            remainCount = int.Parse(jsonData[remainCount].ToString());
            remainTime = jsonData["remainTime"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqFinishAD
public partial class CSNotiFinishAD : Packet
{
    public string session = string.Empty;
    public string guid = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSNotiFinishAD;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["guid"] = this.guid;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            guid = jsonData["guid"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SCNotiGetItem
public partial class SCNotiGetADItem : Packet
{
    public int itemIdx = 0;
    public int itemCount = 0;
    public EffectPosition pos = EffectPosition.PlayCenter;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiGetADItem;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["itemIdx"] = this.itemIdx;
        element["itemCount"] = this.itemCount;
        element["pos"] = this.pos.ToString();

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            itemIdx = int.Parse(jsonData["itemIdx"].ToString());
            itemCount = int.Parse(jsonData["itemCount"].ToString());
            pos = (EffectPosition)Enum.Parse(typeof(EffectPosition), jsonData["pos"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqIAP
public partial class CSReqIAP : Packet
{
    public string session = string.Empty;
    public IAPType iAPType = IAPType.Unknown;
    public int elementIdx = 0;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqIAP;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["elementIdx"] = this.elementIdx;
        element["iAPType"] = this.iAPType.ToString();
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            elementIdx = int.Parse(jsonData["elementIdx"].ToString());
            iAPType = (IAPType)Enum.Parse(typeof(IAPType), jsonData["iAPType"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SCRspIAP
public partial class SCRspIAP : Packet
{
    public string guid = string.Empty;
    public int elementIdx = 0;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCRspIAP;

        //call base init...
        base.Init();
    }

    /*
    public override void Execute ()
    {
        throw new NotImplementedException ();
    }
    */
    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["guid"] = this.guid;
        element["elementIdx"] = this.elementIdx;
        retvalue = element.ToJson();

        return retvalue;
        //throw new NotImplementedException ();
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            guid = jsonData["guid"].ToString();
            elementIdx = int.Parse(jsonData["elementIdx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}

//CSReqIAPResult
public partial class CSReqIAPResult : Packet
{
    public string session = string.Empty;
    public string guid = string.Empty;
    public IAPResult result = IAPResult.Unknown;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqIAPResult;

        //call base init...
        base.Init();
    }

    /*
    public override void Execute ()
    {
        throw new NotImplementedException ();
    }
    */
    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["guid"] = this.guid;
        element["result"] = this.result.ToString();
        retvalue = element.ToJson();

        return retvalue;
        //throw new NotImplementedException ();
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            guid = jsonData["guid"].ToString();
            result = (IAPResult)Enum.Parse(typeof(IAPResult), jsonData["result"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}

//SCRspIAPResult
public partial class SCRspIAPResult : Packet
{
    public string guid = string.Empty;
    public ShopResult result = ShopResult.Unknown;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCRspIAPResult;

        //call base init...
        base.Init();
    }

    /*
    public override void Execute ()
    {
        throw new NotImplementedException ();
    }
    */
    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["guid"] = this.guid;
        element["result"] = this.result.ToString();
        retvalue = element.ToJson();

        return retvalue;
        //throw new NotImplementedException ();
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            guid = jsonData["guid"].ToString();
            result = (ShopResult)Enum.Parse(typeof(ShopResult), jsonData["result"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}

