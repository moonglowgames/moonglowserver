﻿using System;
using LitJson;
using System.Collections.Generic;

//CSReqJokerInfo
public partial class CSReqJokerInfo : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqJokerInfo;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class SpellIdxList 
{
    public string name { get; set; }
    public int ink { get; set; }
    public int tryInkCount { get; set; }
    public int totalInk { get; set; }

    public List<int> spellIdxList { get; set; }

    public SpellIdxList() { }
    public SpellIdxList(string packetName, /*SpellType spellType_,*/ int ink_, int tryInckCount_, int totalInk_, List<int> spellIdxList_)
    {
        name = packetName;
        //spellType = spellType_;
        ink = ink_;
        tryInkCount = tryInckCount_;
        totalInk = totalInk_;
        spellIdxList = spellIdxList_;
    }
}

//SCNotiJokerInfo
public partial class SCNotiJokerInfo : Packet
{
    //public SpellIdxList spellIdxList = null;
    public int jokerFace = 0;
    public bool isTry = false;
    //public int inkItemCount = 0;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiJokerInfo;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["jokerFace"] = this.jokerFace;
        element["isTry"] = this.isTry;
        //element["inkItemCount"] = this.inkItemCount;

        //retvalue = JsonMapper.ToJson(spellIdxList);
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            //spellIdxList = JsonMapper.ToObject<SpellIdxList>(Json);
            jokerFace = int.Parse(jsonData["jokerFace"].ToString());
            isTry = Boolean.Parse(jsonData["isTry"].ToString());
            //inkItemCount = int.Parse(jsonData["inkItemCount"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqSpellList
//public partial class CSReqSpellList : Packet
//{
//    public string session = string.Empty;
//    public SpellType spellType = SpellType.Unknown;

//    #region implemented abstract members of Packet

//    public override void Init()
//    {
//        packetType = PacketTypeList.CSReqSpellList;
//        // element = new JsonData ();
//        // call base init...
//        base.Init();
//    }

//    protected override string ElementToJson()
//    {
//        string retvalue = base.ElementToJson();

//        element["session"] = this.session;
//        element["spellType"] = this.spellType.ToString();

//        retvalue = element.ToJson();
//        return retvalue;

//    }

//    protected override string ElementToTelnet()
//    {
//        throw new NotImplementedException();
//    }

//    protected override bool JsonToElement(string Json)
//    {
//        bool retvalue = false;
//        JsonData jsonData = JsonMapper.ToObject(Json);

//        string name_ = jsonData["name"].ToString();
//        if (name == name_)
//        {
//            session = jsonData["session"].ToString();
//            spellType = (SpellType)Enum.Parse(typeof(SpellType), jsonData["spellType"].ToString());

//            retvalue = true;
//        }
//        else
//        {
//            // throw new Exception("Packet name error");

//        }
//        return retvalue;
//    }

//    protected override bool TelnetToElement(string Telnet)
//    {
//        throw new NotImplementedException();
//    }

//    #endregion
//}

//SCNotiSpellList 
public partial class SCNotiSpellList : Packet
{
    public SpellIdxList spellIdxList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiSpellList;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(spellIdxList);
        //retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            spellIdxList = JsonMapper.ToObject<SpellIdxList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqUpgradeSpell 
public partial class CSReqUpgradeSpell : Packet
{
    public string session = string.Empty;
    public int currentSpellIdx = 0;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqUpgradeSpell;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["currentSpellIdx"] = this.currentSpellIdx;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            currentSpellIdx = Convert.ToInt32( jsonData["currentSpellIdx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqDowngradeSpell
public partial class CSReqDowngradeSpell : Packet
{
    public string session = string.Empty;
    public int currentSpellIdx = 0;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqDowngradeSpell;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["currentSpellIdx"] = this.currentSpellIdx;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            currentSpellIdx = Convert.ToInt32(jsonData["currentSpellIdx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SCRspUpgradeSpell 
public partial class SCRspUpdateSpell : Packet
{
    public SpellResult applyResult = SpellResult.Unknown;
    //public bool applyResult = false;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCRspUpdateSpell;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["applyResult"] = this.applyResult.ToString();
        //element["applyResult"] = this.applyResult;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            applyResult = (SpellResult)Enum.Parse(typeof(SpellResult), jsonData["applyResult"].ToString());
            //applyResult = Boolean.Parse(jsonData["applyResult"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqCancelUpgrade 
public partial class CSReqJokerCancel : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqJokerCancel;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqApplySpells 
public partial class CSReqJokerApply : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqJokerApply;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SCRspApplySpells 
public partial class SCRspJokerApply : Packet
{
    public bool applyResult = false;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCRspJokerApply;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["applyResult"] = this.applyResult;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            applyResult = Boolean.Parse(jsonData["applyResult"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}