﻿using System;

public enum PacketTypeList
{
    Unknown = 0,
    SCNotiHello = 100,

    CSReqRegister,
    //CSReqRegister2,

    SCRspRegister,

    CSReqFindPassword,
    CSReqChangePassword,
    SCRspPasswordResult,

    CSReqLogin = 110,
    //CSReqLogin2 = 113,
    SCNotiLogin = 111,
    SCNotiDownload = 112,

    CSReqLogout = 120,
    SCNotiLogout = 121,
    SCNotiConnections = 140,
    SCReqPing = 150,
    CSRspPang = 151,

    SCNotiReconnect = 160,
    SCNotiQuit = 170,

    CSReqSessionLogin = 200,
    SCNotiSessionLogin = 201,

    CSReqGotoScene = 210,
    SCNotiGotoScene = 211,
    CSNotiSceneEnter = 212,
    CSReqClientPause = 213,
    CSReqClientFocus = 214,
    SCNotiClientFocus = 215,

    CSReqUrl = 220,
    SCNotiUrl = 221,

    SCNotiMenuAlarm = 230,

    SCNotiUserInfo = 300,
    SCNotiItemInfo = 301,
    SCNotiUserElement = 302,
    SCNotiChatIconSet = 303,

    CSNotiChangeChatChannel = 309,
    CSNotiChat = 310,
    SCNotiChat = 311,
    SCNotiServerNoti = 312,

    CSReqPlayerProfile,
    SCNotiPlayerProfile,

    CSReqRoomList = 320,
    SCNotiRoomList = 321,

    CSReqRoomCreate = 330,

    CSReqQuickStart = 340,

    CSReqJoinRoom = 350,
    SCNotiPlayerJoin = 351,
    CSRspPlayerJoin = 352,
    CSNotiCancelRoom = 353,
    CSReqCancelJoin=354,
    SCNotiPlayerJoinCancel = 355,

    SCNotiEnterRoom = 360,
    CSNotiEnterRoom = 361,
    SCNotiMessage = 370,
    CSReqReadTutorial = 380,
    CSNotiChangeLanguage = 390,

    SCNotiPlayerList = 400,
    SCNotiPlayerStatus = 401,
    SCNotiPlayerItemInfo = 402,

    SCNotiStartGame = 410,
    SCNotiDeckShuffle = 411,

    SCNotiDeckToHand = 420,

    SCNotiPlayerFocus = 430,

    CSReqUseCard = 440,
    SCNotiUseCard = 441,
    SCNotiDeckInfo = 442,

    SCNotiGameEnd = 450,
    SCNotiGameResult = 451,
    CSReqWantGame = 452,

    CSReqQuickInventory = 460,
    SCNotiQuickInventory = 470,
    SCNotiContinueGame,
    CSReqAIPlay,
  
    CSReqShopList = 500,
    SCNotiShopList = 501,
    CSReqBuyElement = 502,
    SCRspBuyElement = 503,

    SCNotiShowAD = 510,
    CSNotiFinishAD = 511,
    SCNotiGetADItem = 512,

    CSReqIAP = 520,
    SCRspIAP = 521,
    CSReqIAPResult = 522,
    SCRspIAPResult = 523,

    CSReqMyInfo = 600,
    SCNotiMyInfo = 601,
    CSReqInventory = 610,
    SCNotiInventory = 611,
    CSReqUseItem = 620,
    SCRspUseItem = 621,

    CSReqChangeNick = 630,
    SCRspChangeNick = 631,

    CSReqJokerInfo = 700,
    SCNotiJokerInfo = 701,
    CSReqSpellList = 710,
    SCNotiSpellList = 711,
    CSReqUpgradeSpell = 720,
    CSReqDowngradeSpell = 721,
    SCRspUpdateSpell = 722,

    CSReqJokerCancel = 730,
    CSReqJokerApply = 740,
    SCRspJokerApply = 741,

    CSReqNews = 800,
    SCNotiNews  = 801,

    CSReqMailList = 900,
    SCNotiMailList = 901,
    CSReqGetItem = 902,
    SCRspGetItem = 903,
    CSReqGetAllItems = 904,
    SCRspGetAllItems = 905,
    CSReqRemoveOld = 906,

    CSReqAchieveList = 1000,
    SCNotiAchieveList = 1001,
    CSReqMissionList = 1100,
    SCNotiMissionList = 1101,

    CSReqFriendList = 1200,
    SCNotiFriendList = 1201,
    CSReqRequestFriend = 1202,
    CSReqAcceptFriend = 1203,
    CSReqDeleteFriend = 1204,
    CSReqSendGift = 1205,
    CSReqGetGift = 1206,
    CSReqSendAllGifts = 1207,
    CSReqGetAllGifts = 1208,
    SCRspFriendResut = 1209,
    CSReqChanelList = 1300,
    SCNotiChanelList = 1301,
    CSReqJoinChanel = 1302,
    CSReqRankList = 1310,
    SCNotiRankList = 1311,
    CSReqRewardList = 1320,
    SCNotiRewardList = 1321,
    CSReqMyRankInfo = 1322,
    SCNotiMyRankInfo = 1323,
    CSReqGetRankItem = 1324,
    SCRspRankResult = 1330,

    CSReqCMD = 1400,
    SCNotiCMD = 1401,

    SMReqAddRedis,
    //SMReqAddRedis2,

    MSRspAddRedis,
    //MSRspAddRedis2,
    //SMReqGetRedis,
    //MSRspGetRedis,
    SMReqRemoveRedis,
    MSNotiRemoveRedis,
    SDReqNewUserData,
    DSRspNewUserData,
    //SDReqNewUserData2,
    //DSRspNewUserData2,

    SDReqReadUserData,
    DSRspReadUserData,
    SDReqUpdateLoginDate,
    DSRspUpdateLoginDate,
    SDReqUpdateUserInfo,
    DSRspUpdateUserInfo,
    SDReqSaveToSql,
    DSRspSaveToSql,
    DSRspUpdateLogoutDate,
    SDReqUpdateLogoutDate,
    SSReqCheckEvent,
    SDReqGetMailList,
    DSRspGetMailList,
    SDReqUpdateMail,
    DSRspUpdateMail,
    SDReqUpdateMailDel,
    DSRspUpdateMailDel,
    SDReqNewMail,
    DSRspNewMail,
    SDReqGetFriendList,
    DSRspGetFriendList,
    SDReqNewFriend,
    DSRspNewFriend,
    SDReqUpdateFriendType,
    DSRspUpdateFriendType,
    SDReqUserFriendCount,
    DSRspUserFriendCount,
    SDReqUpdateFriendCount,
    DSRspUpdateFriendCount,
    SDReqGetMyFriends,
    DSRspGetMyFriends,
    SDReqGetRequestFriends,
    DSRspGetRequestFriends,
    SDReqDeleteFriend,
    DSRspDeleteFriend,
    SDReqUpdateSendFriendItem,
    DSRspUpdateSendFriendItem,
    SDReqUpdateGetFriendItem,
    DSRspUpdateGetFriendItem,
    SDReqGiftSendDate,
    DSRspGiftSendDate,
    SMReqGetChanelList,
    MSRspGetChanelList,
    SMReqAddRank,
    MSRspAddRank,
    SMReqRankRange,
    MDReqRankRange,
    DSRspRankRange,
    SDReqRankHistory,
    DSRspRankHistory,
    SMReqCurrentRank,
    MSRspCurrentRank,
    SDReqNewRankHis,
    DSRspNewRankHis,
    SDReqUpdateRankHis,
    DSRspUpdateRankHis,
    SDReqAddChanelUser,
    DSRspAddChanelUser,
    SDReqUpdateChanelInfo,
    DSRspUpdateChanelInfo,
    SDReqRemoveChanelUser,
    DSRspRemoveChanelUser,
    SDReqUpdateChanelInfoDate,
    DSRspUpdateChanelInfoDate,
    SDReqRepairChanel,
    DMReqRepairChanel,
    MSRspRepairChanel,
    SDReqGetUserType,
    DSRspGetUserType,
    SMReqUpdateRank,
    MSRspUpdateRank,
    SDReqUpdateUserLevel,
    DSRspUpdateUserLevel,
    SMReqBCServerNoti,
    MSNotiBCServerNoti,
    SMReqLoginSessionCheck,
    MSRspLoginSessionCheck,

    SDReqRegisterCheck,
    //SDReqRegisterCheck2,

    DSRspRegisterCheck,
    //DSRspRegisterCheck2,

    SDReqLoginCheck,
    //SDReLoginCheck2,

    DSRspLoginCheck,
    //DSRspLoginCheck2,

    SMReqExpireUserInfo,
    //MSRspExpireUserInfo,

    SSReqClientPause,
    //log area
    SLogReqAccountInsert,
    SLogReqLevelInsert,
    SLogReqItemInsert,
    SLogReqPlayInsert,
    SLogReqProfileInsert,

    SMReqSetServerGlobalState,
    MSRspSetServerGlobalState,
    SMReqBCServerState,
    MSNotiServerState,

    SDReqSetDeveloper,
    DSRspSetDeveloper,
    SMReqBCReFreshUserType,
    MSNotiBCReFreshUserType,

    SDReqDevList,
    SMReqExpireRedis,
    SMReqBCForceClose,
    MSNotiBCForceClose,
    SDReqUpdatedeviceID,

    SDReqUpdatePassword,
    DSRspUpdatePassword,

    SDReqUpdateFriendFight,

    SDReqChangeNick,
    DSRspNickCheck,
    MSRspUpdateServerEnable,
    SMReqRefreshServerList,
    SMReqBCReFreshServerList,
    SMReqBCReFreshChannelInfo,
    MDNotiRefreshChannelInfo,
    SMReqBCReFreshClientVersions,
    MDNotiRefreshClientVersion,
    SMReqBCUpdateServerEnable,
    SMReqBCUpdateAllServerEnable,
    SMNotiUpdateEnable,
    SMReqGetAllServerInfo,
    SMReqGetThisServerInfo,
    SMReqChangeServer,
    SDReqUpdateUserBan,
    DMRepBCBanUser,
    SMNotiBanUser,
    SMReqCheckUserBan,
    SMReqBCUpdateLimit,
    MSNotiUpdateLimit,
    SDReqAddIAP,
    SDReqIAPResult,
    SDReqGetPlayerProfile,
    SMReqBCReFreshLocalization,
    MDNotiUpdateLocalization,
    SMReqBCReFreshNews,
    MDNotiUpdateNews,
    SMReqBCReFreshMission,
    MDNotiUpdateMission,
    DMRepBCNewFriendGift,
    MSNotiNewFriendGift,
    SMReqBCAlarmShop,
    MSNotiAlarmShop,
    SMReqBCAlarmNews,
    MSNotiAlarmNews
};
