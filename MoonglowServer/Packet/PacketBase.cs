﻿#define INIT_TEST

using System;
using LitJson;
using MoonGlow;


//public enum PacketSendType{ Unknown = 0, CSRps = 1, SCNoti = 2 , CSReq = 3 , CSNoti = 4 };
//public enum EncriptionType{ Unknown = 0, Telnet =1 , Json= 2, Base64Telnet = 11 , 
//							Base64Json = 12 , ZipTelnet= 21 , ZipJson=22 , EncriptTelnet = 31 ,EncriptJson = 32 };

public class PacketBase {
	public PacketSendType Type = PacketSendType.Unknown;
	public int Idx = 0;
	public EncriptionType Enc = EncriptionType.Unknown;
	public IPacket PacketData;

	// telnet stype  == 1,1,2,[scNotiHello,MoonGlowLoginServer,MGLV1.0.0,0,Don'tConnect 8:00 PM]\r\n
	// JSON Stype = {"Type":1,"Idx":1,"Enc":2,"Data":"{"pckName":"scNotiHello","svrname":"MoonGlowLoginServer","ver":"MGLV1.0.0..."}"}\r\n
	public void Decode( string encodeData ){

		throw new NotImplementedException ();
	}

	public string Encode()
	{
		string retvaue = "";

		// throw new NotImplementedException ();
		return retvaue;
	}
}

public interface IPacket{

	void Init();
    void Execute( string serverSession ); // serverside source...
	void Execute();
    
    void Decode( EncodeType encType , string encodeData );
	string Encode( EncodeType encType );
}

//public enum EncodeType{ Telnet, Json }
abstract public class Packet : IPacket {
	public string name = "";
	protected PacketTypeList packetType = PacketTypeList.Unknown;
	protected JsonData element;

	public Packet(){ Init(); }
	
	public void Decode( EncodeType encType , string encodeData )
	{
		switch( encType )
		{
		case EncodeType.Json : JsonToElement( encodeData ); break;
		case EncodeType.Telnet : TelnetToElement( encodeData ); break;
		default:break;
		}
	}

	public string Encode( EncodeType encType )
	{
		string retvalue = string.Empty;

		switch ( encType )
		{
		case EncodeType.Json : 
			retvalue =  ElementToJson(); 
			break;
		case EncodeType.Telnet : 
			retvalue =  ElementToTelnet();
			break;
		default: 
			throw new NotImplementedException ();
		}

		if ( retvalue == string.Empty ) 
		{
            // Logger.instance.Log( " Encode return value is Null " + EncType.ToString(),  )lst;
            //throw new NotImplementedException ();
            if (GameServer._logger.IsFatalEnabled)
            {
                string log = string.Format("Encode return value is null, encType: {0}", encType.ToString());
                GameServer._logger.Fatal(log);
            }
		}

		return retvalue;
	}

	virtual public void Init()
	{
		name = packetType.ToString ();

#if INIT_TEST
		InitTest ();
#endif
	}
    public virtual void Execute( string serverSession ){}
    abstract public void Execute(); 

	/*
	abstract protected string ServerExecute();
	abstract protected string ClientExecute();
    */

	virtual protected string ElementToJson(){

		if (element == null) {
			
			element = new JsonData();
			element ["name"] = this.name;
		}

		return string.Empty;
	}
	abstract protected string ElementToTelnet();
	abstract protected bool JsonToElement( string Json );
	abstract protected bool TelnetToElement( string Telnet );

    virtual public void InitTest(){}

}


