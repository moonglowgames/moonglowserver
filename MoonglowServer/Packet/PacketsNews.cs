﻿using System;
using LitJson;
using System.Collections.Generic;

//CSReqNews
public partial class CSReqNews : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqNews;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion


}

public class News
{
    public int newsType { get; set; } // 1= images, 2= html
    public string title { get; set; }
    public string url { get; set; }
    public News() { }
    public News(int newsType_, string title_, string url_)
    {
        newsType = newsType_;
        title = title_;
        url = url_;
    }
}
public class NewsList
{
    public string name { get; set; }
    public List<News> newsList { get; set; }
    public NewsList() { }
    public NewsList(string packetName, List<News> newsList_)
    {
        name = packetName;
        newsList = newsList_;
    }    
}
//SCNotiNews
public partial class SCNotiNews : Packet
{
    public NewsList newsList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiNews;
        base.Init();

        //throw new NotImplementedException ();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(newsList);
        //retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (name == name_)
        {
            newsList = JsonMapper.ToObject<NewsList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion


}
