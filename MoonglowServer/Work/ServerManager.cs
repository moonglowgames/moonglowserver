﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ServerManager : Singleton<ServerManager>
{
    public const string GLOBAL_SERVER_NAME = "Server Global State";
    public const string GLOBAL_SERVER_STATE = "State";
    public const string GLOBAL_SERVER_LIMIT = "Limit";

    public const string GLOBAL_SERVER_LIST = "Server List";
    public const string GLOBAL_SERVER_URL = "Url";
    public const string GLOBAL_SERVER_TYPE = "Type";
    public const string GLOBAL_SERVER_VERSION = "Version";
    public const string GLOBAL_SERVER_CONNECTIONS = "Connections";
    public const string GLOBAL_SERVER_ENABLE = "Enable";

    public const string SERVER_VERSION = "1.12.7.11";
    public static string CURRENT_IOS_VERSION = "1.9.0";
    public static string CURRENT_ANDROID_VERSION = "1.9.0";
    public static string CURRENT_WINDOWS_VERSION = "1.9.0";
    public static string CURRENT_WEB_VERSION = "1.9.0";
    public static string CURRENT_MAC_VERSION = "1.9.0";
    public static string CURRENT_PC_VERSION = "1.9.0";

    public static int SERVER_CONNECTION_LIMIT = 500;
    public string serverName;
    public ServerState serverState;
    public string serverVersion;
    public string serverInfo;

    internal void SetServerInfo(string name, string version, string info)
    {
        serverName = name;
        serverVersion = version;
        serverInfo = info;
    }
    internal void SetServerSate(ServerState state)
    {
        serverState = state;
    }

    internal bool isServerStart(string serverSession, UserType userType, Language language)
    {
        bool retvalue = true;
        if (userType == UserType.User && GameServer._isLoginServer == true && GameServer._isServerStart == false)
        {
            //check language
            string msg = (LocalizationManager._dict.ContainsKey(LocalizationKey.SERVER_CHECK.ToString()))?
                          LocalizationManager.Instance.Get(LocalizationKey.SERVER_CHECK.ToString(), language) : LocalizationKey.SERVER_CHECK.ToString();
            PacketsHandlerServer.SendSCNotiServerNoti(serverSession, Command.Message, msg);

            retvalue = false;
        }
        return retvalue;
    }

    internal bool isClientVersionMatch(string serverSession_, UserType userType_, ConnectType connectType_, string[] clientVer_)
    {
        bool retvalue = true;
        string[] _currentVer = null;
        switch (connectType_)
        {
            case ConnectType.iOS:
                _currentVer = CURRENT_IOS_VERSION.Split('.');
                break;
            case ConnectType.Android:
                _currentVer =CURRENT_ANDROID_VERSION.Split('.');
                break;
            case ConnectType.WinStore:
                _currentVer =CURRENT_WINDOWS_VERSION.Split('.');
                break;
            case ConnectType.Web:
                _currentVer =CURRENT_WEB_VERSION.Split('.');
                break;
            case ConnectType.PC:
                _currentVer =CURRENT_PC_VERSION.Split('.');
                break;
            case ConnectType.Mac:
                _currentVer =CURRENT_MAC_VERSION.Split('.');
                break;
            default: break;
        }
        

        if (userType_ == UserType.User)
        {
            if (clientVer_.Length > 2 && _currentVer.Length > 2)
            {
                if (clientVer_[1] != _currentVer[1]) retvalue = false; 
            }
            else retvalue = false; 
        }

        return retvalue;
    }
}
