﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class QueueManager
{
    public static QueueManager _instance = null;

    private ExecuteQueue _workQueue = new ExecuteQueue();
    private DBQueue _dbQueue = new DBQueue();
    private RedisDBQueue _memDBQueue = new RedisDBQueue();
    private LogQueue _logQueue = new LogQueue();

    public static QueueManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new QueueManager();
            }
            return _instance;
        }
    }

    public ExecuteQueue WorkQueue { get { return _workQueue; } }
    public DBQueue DBQueue { get { return _dbQueue; } }
    public RedisDBQueue MemDBQueue { get { return _memDBQueue; } }
    public LogQueue LogQueue { get { return _logQueue; } }

    public void AddWorkQueue(string serverSession_, IPacket packet_)
    {
        IPacketCommand pckcmd = new PacketCommand();
        pckcmd.session = serverSession_;
        pckcmd.packet = packet_;
        _workQueue.Enqueue(pckcmd);
    }

    public bool GetWorkQueue(out PacketCommand pckcmd_)
    {
        bool retvalue = false;
        pckcmd_ = null;

        return retvalue;
    }

    public void AddDataBaseQueue(string serverSession_, IPacket packet_)
    {
        IPacketCommand pckcmd = new PacketCommand();
        pckcmd.session = serverSession_;
        pckcmd.packet = packet_;
        _dbQueue.Enqueue(pckcmd);
    }

    public bool GetDataBaseQueue(out PacketCommand pckcmd_)
    {
        bool retvalue = false;
        pckcmd_ = null;

        return retvalue;
    }

    public void AddMemDBQueue(string serverSession_, IPacket packet_)
    {
        IPacketCommand pckcmd = new PacketCommand();
        pckcmd.session = serverSession_;
        pckcmd.packet = packet_;
        _memDBQueue.Enqueue(pckcmd);
    }

    public bool GetMemDBQueue(out PacketCommand pckcmd_)
    {
        bool retvalue = false;
        pckcmd_ = null;

        return retvalue;
    }

    public void AddLogQueue(string serverSession_, IPacket packet_)
    {
        IPacketCommand pckcmd = new PacketCommand();
        pckcmd.session = serverSession_;
        pckcmd.packet = packet_;
        _logQueue.Enqueue(pckcmd);
    }

    public bool GetLogQueue(out PacketCommand pckcmd_)
    {
        bool retvalue = false;
        pckcmd_ = null;

        return retvalue;
    }
}

