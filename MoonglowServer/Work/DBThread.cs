﻿using System;
using System.Threading;
using System.Diagnostics;
using MoonGlow;
using MoonGlow.Data;
using log4net;

public class DBThread
{
    private volatile bool _shouldStop = false;
    private DBQueue _dbQueue = null;
    private int _dbQueueCount = 0;
    private ILog _logger = null;
    private Thread _dbThread = null;
    private IPacketCommand packetcmd = null;

    public DBThread(ILog logger_)
    {
        // injections..
        _logger = logger_;
        _dbQueue = QueueManager.Instance.DBQueue;
        // thread create
        _dbThread = new Thread(DBWork);
        _dbThread.Start();
    }

    public void DBWork()
    {
        //GET DEVELOPER LIST
        GameServer.GetAllDevList();
        //read localization
        LocalizationManager.Instance.ReadLocalization();

        if (!MoonGlow.GameServer._isLoginServer)
        {
            //read channelinfo
            ChannelManager.Instance.ReadChannelInfo();
            //read channelrewardinfo
            ChannelRewardManager.Instance.ReadChannelRewardInfo();
            //read news
            NewsManager.Instance.ReadNews();
            //read mission
            MissionManager.Instance.ReadMission();

            Console.WriteLine("\nGame Server starts successfully, press key 'q' to stop it!");
            Console.WriteLine(string.Format("Game Server Version: {0}, State: {1}\n", ServerManager.SERVER_VERSION, GameServer._isServerStart ? "start" : "stop"));
        }
        else
        {
            //Get Client Versions
            GameServer.GetClientVersions();
            //get ban user list

            Console.WriteLine("\nLogin Server starts successfully, press key 'q' to stop it!");
            Console.WriteLine(string.Format("Login Server Version: {0}, State: {1}\n", ServerManager.SERVER_VERSION, GameServer._isServerStart ? "start" : "stop"));

            //RedisConnect.Instance.AddLoginServerState();
        }


        Stopwatch sw = new Stopwatch();

        while (!_shouldStop)
        {
            _dbQueueCount = _dbQueue.Count;
            //Console.WriteLine("**The Database queue count is: " + _dbQueueCount);
            //Thread.Sleep(1000);

            if ((_dbQueueCount > 0/*_queue.Count > 0*/) && (_dbQueue.TryDequeue(out packetcmd)))
            {
                try
                {
                    Console.WriteLine(packetcmd.packet.GetType().ToString());

                    sw = Stopwatch.StartNew();

                    packetcmd.packet.Execute(packetcmd.session);

                    sw.Stop();
                    Console.WriteLine("SQL ExecuteTime: " + sw.Elapsed);
                }
                catch (Exception ex)
                {
                    //  _logger.Debug(ex.Message);
                    //Console.WriteLine("!!!DBWork packetExecuteError: " + ex.Message);
                    if (_logger.IsErrorEnabled)
                    {
                        string log = "DBWork packet execute exception" + "(" + ex.Message + ")";
                        _logger.Error(log);
                    }
                }
            }
            else
            {
               Thread.Sleep(1);
            }
        }
    }

    public void RequestStop()
    {
        _shouldStop = true;
    }

    public bool isAlive { get { return _dbThread.IsAlive; } }

}


