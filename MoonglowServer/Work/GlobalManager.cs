﻿using MoonGlow;
using MoonGlow.Data;
using System;
using System.Collections.Generic;
using System.Net.Mail;

public class GlobalManager: Singleton<GlobalManager>
{
    public const int FULL_STAMINA = 100;
    public const int DEFAULT_INIT_INK = 6;
    public const int DEFAULT_FACE_IDX = 500;
    public const int AI_FACE_IDX = 507;
    public const int MAX_FRIEND = 20;
    public const int GIFT_STAMINA_COUNT = 3;
    public const int MAX_CARD_SKIN = 1;
    public const int MAX_PET = 3;
    public const int MAX_BADGE = 1;
    //public const int CHANEL_MAX_COUNT = 1000;//just test
    public const GameType DEFAULT_GAME_TYPE = GameType.Colmaret;
    public const PlayMode DEFAULT_GAME_MODE = PlayMode.Single2;
    public const PlayTime DEFAULT_GAME_TIME = PlayTime.Sec_30;
    public const int DEFAULT_GAME_STAKE = 0;
    public const bool DEFAULT_GAME_DISCLOSE = false;
    public const bool DEFAULT_GAME_LOCK = false;
    public const int DAY_GIFT = 2;
    public const int JOIN_GIFT_GOLD = 500;
    public const int JOIN_GIFT_RUBY = 50;
    public const int AD_SHOW_COUNT = 5;
    public const string GLOBAL_USER_SESSION = "Session";
    public const string GLOBAL_USER_SERVER = "ServerUrl";

    public static TimeSpan EXPIRE_SERVER_TIMESPAN = new TimeSpan(0, 2, 0);
    public static TimeSpan EXPIRE_SESSION_TIMESPAN = new TimeSpan(0, 15, 0);

    //private int _currentTickCount = 0;
    public const int ITEMTIME_TICK_INTERNAL = 1000;
    public const int PING_TICK_INTERNAL = 60000;// 60000 msec = 1 min
    public const int EXPIRE_TICK_INTERNAL = 1000;

    public const int SESISON_TICK_INTERNAL = 10 * 60 * 1000; //10 mins

    public static long LOG_ACCOUNT_COUNT = 0;
    public static long LOG_LEVEL_COUNT = 0;
    public static long LOG_ITEM_COUNT = 0;
    public static long LOG_PLAY_COUNT = 0;
    public static long LOG_PROFILE_COUNT = 0;

    public static string mailSender = string.Empty;
    public static string mailPW = string.Empty;

    internal void SendEmail(string receiver, string nickName, string text)
    {
        //string sender = "moonglowgamesdev@gmail.com"; //"moonglowgames@moonglowgames.com";
        MailMessage message = new MailMessage();
        message.From = new MailAddress(mailSender);
        message.To.Add(receiver);
        message.Subject = "Retrieve Password - Moonglow The Magic Card";
        message.IsBodyHtml = true;
        message.Body = string.Format(@"
                                       <p>Don't worry, we all forget sometimes</p>
                                       <hr style=""border: none; solid; color:#F5F5F5"" />
                                       <h3>Hi {0},</h3>
                                       <p style=""margin-bottom: 20px"">You've recently asked to reset the password for this account: {1}</p>
                                       <p style=""margin-bottom: 20px"" >Here is the Temporary Password we reset for you:</p>
                                       <h2 style=""color:#2E9AFE"">{2}</h2>      
                                       <br/><br/><br/>
                                       <p>Cheers,</p>
                                       <p>Moonglow The Magic Card</p>",
                                       nickName, receiver, text );
        //"Temporary Password"


        try
        {
            SmtpClient mailClient = new SmtpClient("smtp.gmail.com", 587);
            mailClient.EnableSsl = true;
            // Add credentials if the SMTP server requires them.
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = new System.Net.NetworkCredential(mailSender, mailPW);

            mailClient.Send(message);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception caught in CreateMessage(): {0}", ex.ToString());
        }
    }

    internal string GeneratePassword()
    {
        string retvalue = string.Empty;
        string strPwdchar = "abcdefghijklmnopqrstuvwxyz0123456789#+@&$ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random rnd = new Random();
        for (int i = 0; i <= 7; i++)
        {
            int iRandom = rnd.Next(0, strPwdchar.Length - 1);
            retvalue += strPwdchar.Substring(iRandom, 1);
        }
        return retvalue;
    }

    public bool ConvertToDateTime(string value, out DateTime expireDate)
    {
        bool retvalue = false;
        expireDate = new DateTime();
        try
        {
            expireDate = Convert.ToDateTime(value);
            retvalue = true;
        }
        catch (FormatException)
        {
            Console.WriteLine("'{0}' is not in the proper format.", value);
        }
        return retvalue;
    }

    public bool ConvertToTimeSpan(string value, out TimeSpan timeSpan)
    {
        bool retvalue = false;
        timeSpan = new TimeSpan();
        try
        {
            timeSpan = TimeSpan.Parse(value);
            //Console.WriteLine("{0} --> {1}", value, timeSpan.ToString("c"));
            retvalue = true;
        }
        catch (FormatException)
        {
            Console.WriteLine("{0}: Bad Format", value);
        }
        catch (OverflowException)
        {
            Console.WriteLine("{0}: Overflow", value);
        }
        return retvalue;
    }

    internal void GetUserType(string account, out UserType userType)
    {
        userType = UserType.User;
        bool isNormalUser = true;

        if (isNormalUser == true)
        {
            if (DeveloperManager._developerList != null)
            {
                foreach (var developerID in DeveloperManager._developerList)
                {
                    if (account == developerID)//or IP inner MoonGlow, later...
                    {
                        userType = UserType.Developer;
                        isNormalUser = false;
                        break;
                    }
                }
            }
            
        }

        if (isNormalUser == true)
        {
            if (DeveloperManager._developerManagerList != null)
            {
                foreach (var developerManagerID in DeveloperManager._developerManagerList)
                {
                    if (account == developerManagerID)//or IP inner MoonGlow, later...
                    {
                        userType = UserType.DeveloperManager;
                        isNormalUser = false;
                        break;
                    }
                }
            }
        }

        if (isNormalUser == true)
        {
            if (DeveloperManager._operatorList != null)
            {
                foreach (var operatorID in DeveloperManager._operatorList)
                {
                    if (account == operatorID)//or IP inner MoonGlow, later...
                    {
                        userType = UserType.Operator;
                        isNormalUser = false;
                        break;
                    }
                }
            }
        }

        if (isNormalUser == true)
        {
            if (DeveloperManager._operatorManagerList != null)
            {
                foreach (var operatorManagerID in DeveloperManager._operatorManagerList)
                {
                    if (account == operatorManagerID)//or IP inner MoonGlow, later...
                    {
                        userType = UserType.OperatorManager;
                        isNormalUser = false;
                        break;
                    }
                }
            }
        }
    }

    internal void RemoveUserInfo(string session)
    {
        UserInfo userInfo = GameServer.GetUserInfo(session);
        if (userInfo != null)
        {
            if (!string.IsNullOrEmpty(userInfo.roomID))
            {
                MoonGlow.Room.Room room = GameServer.GetRoom(userInfo.session);
                if (room != null)
                {
                    if (room.isPlaying)
                    {
                        foreach (var player in room.GetPlayers())
                        {
                            if (player.session == session)
                            {
                                player.forceLose = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        room.ExitPlayer(session);
                        room.SendPlayerList();
                        room.SendPlayerStatus();
                        room.SendPlayerItemInfo();
                    }

                }
                userInfo.roomID = null;
            }

            if (GameServer._isCloud == true)
            {
                //Disconnect log
                GlobalManager.Instance.LogAccountInsert(userInfo.serverSession, userInfo.userID, userInfo.userDataBase.nickName, LogAccountType.Disconnect);
            }
        }

        //remove userInfo
        if (SessionIdManager.Instance.RemoveSession(session))
        {
            // ok deleted
            // log userid, session id
            if (/*GameServer.instance.Logger*/GameServer._logger.IsDebugEnabled)
            {
                string log = string.Format("Session Removed from Session Manager:( Session:{0} )", session);
                /*GameServer.instance.Logger*/
                GameServer._logger.Debug(log);
            }
        }
        else
        {
            //fail deleted
            if (/*GameServer.instance.Logger*/GameServer._logger.IsErrorEnabled)
            {
                string log = string.Format("Failed to Remove Session: ( Session:{0} )", session);
                /*GameServer.instance.Logger*/
                GameServer._logger.Error(log);
            }
        }
    }

    internal void NullUserInfo(string serverSession)
    {
        //Console.WriteLine("!!!Error: userInfo is null!!!");
        PacketsHandlerServer.SendSCNotiSessionLogin(serverSession, false, string.Empty, UserType.User);
        //or send SCNotiMessage

        //remove server from _Dic
        if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
        {
            SessionIdManager.Instance._sessionDic.Remove(serverSession);
        }
    }

    internal void InitUserData(string userID, string account, string nick, int faceIdx, out UserData userData_)
    {
        /* EtherealInk: reset joker
         * SaqqaraTablet: rename
         * RosemaryOil: full charge stamina
         * SLElixir: stamina up to 200
         * EPElixir: +1HP during 2hrs
         * SOElixir: +1SP during 2hrs
         */
        ItemData itemData = new ItemData();
        itemData.ePElixir = 1;
        itemData.sOElixir = 1;
        itemData.rosemaryOil = 1;
        itemData.sLElixir = 1;
        itemData.etherealInk = 1;
        itemData.saqqaraTablet = 1;

        //pet & badge list...default for test
        //itemData.petList.Add(new Pet(601, true));
        //itemData.petList.Add(new Pet(602, true));
        //itemData.badgeList.Add(new Badge(712, true));
        //itemData.badgeList.Add(new Badge(721, true));

        //joker face idx, face[0] is no face ^^
        // itemData.jokerFaceList = new List<int>();
        ResourceData resourceData = new ResourceData();
        resourceData.Gold = 0;
        resourceData.Ruby = 0;
        resourceData.Stamina = 100;
        resourceData.maxStamina = 100;
        resourceData.Ink = DEFAULT_INIT_INK;
        resourceData.JokerLine = 1;

        JokerData jokerData = new JokerData();
        // face[0] is no face, ^^
        jokerData.faceIdx = 0;
        //jokerData.spellIdxList.Add(101);

        ProfileData profileData = new ProfileData();
        profileData.addHP = 0;
        profileData.addSP = 0;
        profileData.winCount = 0;
        profileData.drawCount = 0;
        profileData.loseCount = 0;
        profileData.conWinCount = 0;
        profileData.CoConWinCount = 0;
        profileData.JoConWinCount = 0;
        profileData.ChConWinCount = 0;
        profileData.ZaConWinCount = 0;
        profileData.ArConWinCount = 0;
        profileData.TaConWinCount = 0;
        profileData.KrConWinCount = 0;
        profileData.YaConWinCount = 0;
        profileData.friendCount = 0;
        profileData.checkInDays = 1;
        profileData.conCheckInDays = 1;
        profileData.chanelIdx = 0;

        profileData.rankPoint = 0;
        profileData.rankWin = 0;
        profileData.rankLose = 0;
        profileData.rankDraw = 0;
        profileData.chatChannel = ChatChannel.Any;

        //item use count
        //profileData.etherealInkUseCount = 0;
        //profileData.saqqaraTabletUseCount = 0;
        //profileData.sLElixirUseCount = 0;
        //profileData.rosemaryOilUseCount = 0;
        //profileData.epElixirUseCount = 0;
        //profileData.soElixirUseCount = 0;

        userData_ = new UserData();
        userData_.id = userID;
        userData_.account = account;
        userData_.nick = nick;
        //userData_.pw = password;
        userData_.faceIdx = faceIdx/*GlobalManager.DEFAULT_FACE_IDX*/;
        userData_.level = 1;
        userData_.exp = 0;
        //userData_.userType = UserType.User;
        userData_.createDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        userData_.lastLoginDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        userData_.resourceData = resourceData;
        userData_.itemData = itemData;
        userData_.jokerData = jokerData;
        userData_.profileData = profileData;

        userData_.staminLastEventDate = DateTime.Now;
        userData_.epElixirEndEventDate = DateTime.Now;
        userData_.soElixirEndEventDate = DateTime.Now;

        userData_.ADRemainCount = GlobalManager.AD_SHOW_COUNT;
        userData_.ADLastTime = DateTime.Now - new TimeSpan(1, 0, 0);

        //achievement & mission...
        Dictionary<int, AchieveInfo> achieveListDic = AchieveManager.Instance.GetAchieveListDic();
        if (achieveListDic != null)
        {
            AchieveInfo info = null;
            foreach (var infoPair in achieveListDic)
            {
                info = infoPair.Value;
                //initialize...
                if (info.iDx % 100 == 0)
                {
                    userData_.achieveValue.Add(new AchieveValue(info.iDx, 0, 0, false));
                }
            }
        }
    }

    internal void InitMailData(/*string nick,*/ out MailData mailData, out MailData mailData2)
    {
        mailData = new MailData();
        //mailData.nick = nick;
        mailData.sendID = "SYSTEM";
        //mailData.senderNick = "SYSTEM";
        mailData.subject = LocalizationKey.JOIN_GIFT_SUBJECT.ToString();
        mailData.content = LocalizationKey.JOIN_GIFT_CONTENT.ToString();
        mailData.itemIdx1 = (int)ElementType.Gold;
        mailData.itemCount1 = GlobalManager.JOIN_GIFT_GOLD;
        mailData.itemGot = false;

        mailData2 = new MailData();
        //mailData2.nick = nick;
        mailData2.sendID = "SYSTEM";
        //mailData2.senderNick = "SYSTEM";
        mailData2.subject = LocalizationKey.JOIN_GIFT_SUBJECT.ToString();
        mailData2.content = LocalizationKey.JOIN_GIFT_CONTENT.ToString();
        mailData2.itemIdx1 = (int)ElementType.MoonRuby;
        mailData2.itemCount1 = GlobalManager.JOIN_GIFT_RUBY;
        mailData2.itemGot = false;
    }

    internal void LogItemInsert(string serverSession, string userID, string nickName, int itemIdx, string itemName, int itemCount, LogItemType type)
    {
        //item log
        LogItemData data = new LogItemData(userID, nickName, DateTime.Now.ToString(), itemIdx, itemName, itemCount, type);
        PacketsHandlerLog.SLogReqItemInsert(serverSession, new PCKItemLog(PacketTypeList.SLogReqItemInsert.ToString(), data));

        if (LOG_ITEM_COUNT > 100) LOG_ITEM_COUNT = 0;
        else LOG_ITEM_COUNT++;

    }

    internal void LogAccountInsert(string serverSession, string userID, string nickName, LogAccountType type)
    {
        LogAccountData data = new LogAccountData(userID, nickName, DateTime.Now.ToString(), type);
        PacketsHandlerLog.SLogReqAccountInsert(serverSession, new PCKAccountLog(PacketTypeList.SLogReqAccountInsert.ToString(), data));

        if (LOG_ACCOUNT_COUNT > 100) LOG_ACCOUNT_COUNT = 0;
        else LOG_ACCOUNT_COUNT++;
    }

    internal void LogLevelInsert(string serverSession, string userID, string nickName, int lastLevel, int level)
    {
        LogLevelData levelData = new LogLevelData(userID, nickName, DateTime.Now.ToString(), lastLevel, level);
        PacketsHandlerLog.SLogReqLevelInsert(serverSession, new PCKLevelLog(PacketTypeList.SLogReqLevelInsert.ToString(), levelData));

        if (LOG_LEVEL_COUNT > 100) LOG_LEVEL_COUNT = 0;
        else LOG_LEVEL_COUNT++;
    }

    internal void LogPlayInsert(string serverSession, string userID, string nickName, GameType gameType, PlayMode playMode, PlayTime playTime, int stake, bool disClose, bool isLock, LogPlayType type, string gameTime, string result)
    {
        LogPlayData data = new LogPlayData(userID, nickName, DateTime.Now.ToString(), gameType, playMode, playTime, stake, disClose, isLock, type, gameTime, result);
        PacketsHandlerLog.SLogReqPlayInsert(serverSession, new PCKPlayLog(PacketTypeList.SLogReqPlayInsert.ToString(), data));

        if (LOG_PLAY_COUNT > 100) LOG_PLAY_COUNT = 0;
        else LOG_PLAY_COUNT++;
    }

    internal void LogProfileInsert(string serverSession, string userID, string nickName, int winCount, int loseCount, int drawCount, int rankWin, int rankLose, int rankDraw)
    {
        LogProfileData data = new LogProfileData(userID, nickName, DateTime.Now.ToString(), winCount, loseCount, drawCount, rankWin, rankLose, rankDraw);
        PacketsHandlerLog.SLogReqProfileInsert(serverSession, new PCKProfileLog(PacketTypeList.SLogReqProfileInsert.ToString(), data));

        if (LOG_PROFILE_COUNT > 100) LOG_PROFILE_COUNT = 0;
        else LOG_PROFILE_COUNT++;
    }

}
