﻿using System;
using System.Threading;
using System.Diagnostics;
using MoonGlow;
using log4net;

public class LogThread
{
    private volatile bool _shouldStop = false;
    private LogQueue _logQueue = null;
    private int _logQueueCount = 0;
    private ILog _logger = null;
    private Thread _logThread = null;
    private IPacketCommand packetcmd = null;

    private static DateTime today = DateTime.Today; 

    public LogThread(ILog logger_)
    {
        /// injections..
        _logger = logger_;
        _logQueue = QueueManager.Instance.LogQueue;
        // thread create
        _logThread = new Thread(LogWork);
        _logThread.Start();
    }

    public void LogWork()
    {
        Stopwatch sw = new Stopwatch();

        while ( MoonGlow.GameServer._isCloud == true && !_shouldStop)
        {
            _logQueueCount = _logQueue.Count;
            //Console.WriteLine("**The Database queue count is: " + _dbQueueCount);

            if ((_logQueueCount > 0/*_queue.Count > 0*/) && (_logQueue.TryDequeue(out packetcmd)))
            {
                try
                {
                    //Console.WriteLine(packetcmd.packet.GetType().ToString());
                    if (today != DateTime.Today)
                    {
                        if (GameServer.azureStorage.ResetTable())  today = DateTime.Today;
                    }

                    sw = Stopwatch.StartNew();

                    packetcmd.packet.Execute(packetcmd.session);
                    
                    sw.Stop();
                    Console.WriteLine("Log ExecuteTime : " + sw.Elapsed);
                }
                catch (Exception ex)
                {
                    //  _logger.Debug(ex.Message);
                    //Console.WriteLine("!!!LogThread Work packetExecuteError: " + ex.Message);
                    //Console.WriteLine("!!!Log Error Packets: " + packetcmd.packet.GetType().ToString());
                    if (_logger.IsErrorEnabled)
                    {
                        string log = string.Format("LogThread packet execute exception ({0}), packet: {1}", ex.Message, packetcmd.packet.GetType().ToString());
                        _logger.Error(log);
                    }
                }
            }
            else
            {
                Thread.Sleep(1);
            }
        }
    }

    public void RequestStop()
    {
        _shouldStop = true;
    }

    public bool isAlive { get { return _logThread.IsAlive; } }
}


