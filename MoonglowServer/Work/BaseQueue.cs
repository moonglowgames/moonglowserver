﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


public interface IPacketCommand 
{
    IPacket packet { get; set; }
    //int idx{get;set;}
    string session { get; set; }
}

public class PacketCommand : IPacketCommand
{
    public IPacket packet { get; set; }
    public string session { get; set; }
}

public class ExecuteQueue:ConcurrentQueue<IPacketCommand> { }
public class DBQueue : ConcurrentQueue<IPacketCommand> { }
public class RedisDBQueue : ConcurrentQueue<IPacketCommand> { }
public class LogQueue : ConcurrentQueue<IPacketCommand> { }