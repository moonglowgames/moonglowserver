﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow.Work
{
    class RedisManager
    {
        public static RedisManager _instance = null;
        //private int _currentExpireSessionTickCount = 0;
        private int _currentCheckServerTickCount = 0;
        private const int expireSessionIntervalTick = 1000; // 100 msec 
        private const int serverCheckIntervalTick = 1*60*1000; // 1 min

        public static RedisManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RedisManager();
                }

                return _instance;
            }
        }

        //internal bool IsExpireUserInfoRun(int nowtick_)
        //{
        //    bool retvalue = false;
        //    if ((_currentExpireSessionTickCount + expireSessionIntervalTick) < nowtick_)
        //    {
        //        _currentExpireSessionTickCount = nowtick_;
        //        retvalue = true;
        //    }

        //    return retvalue;
        //}

        //public void ExpireUserInfoRun()
        //{
        //    UserInfo userInfo = null;
        //    foreach (var infoPair in SessionIdManager.Instance)
        //    {
        //        userInfo = infoPair.Value;
        //        if (userInfo != null)
        //        {
        //            if (userInfo.userServerState == UserServerStatus.DisConnect)
        //            {
        //               PacketsHandlerDB.SMReqExpireSession(userInfo.serverSession, userInfo.session);
        //            }

        //            if (userInfo.isPause == true)
        //            {
        //                // PacketsHandlerDB.SMReqExpireSession(userInfo.serverSession, userInfo.session);
        //                PacketsHandlerDB.SSReqClientPause(userInfo.serverSession, userInfo.session);
        //            }
        //        }
        //    }
        //}

        internal bool isCheckServerListRun(int nowtick_)
        {
            bool retvalue = false;
            if ((_currentCheckServerTickCount + serverCheckIntervalTick) < nowtick_)
            {
                _currentCheckServerTickCount = nowtick_;
                retvalue = true;
            }

            return retvalue;
        }

        internal void CheckServerListRun()
        {
            GameServer._gameServerList = ServerStateRedisConnect.Instance.GetGameServerList();

            //check all game server state
            if (!GameServer._isLoginServer)
            {
                bool loginServerState = ServerStateRedisConnect.Instance.GetGlobalServerState().Result;
                if (loginServerState)
                {
                    bool isAllServerDisable = true;
                    foreach (var gameServer in GameServer._gameServerList)
                    {
                        if (gameServer.enable)
                        {
                            isAllServerDisable = false;
                            break;
                        }
                    }

                    if (isAllServerDisable)
                    {
                        GameServer._enable = true;
                        //need noti login server? maybe do not need
                    }

                } 
            }

            int connections = SessionIdManager.Instance.Count;
            if (!ServerStateRedisConnect.Instance.CheckServerList(/*GameServer._connectIP*/GameServer._DNS, GameServer._port, GameServer._type, GameServer._version, connections, GameServer._enable).Result)
            {
                //Console.WriteLine("!!!Error: Add Server to Redis Server List Fail.");
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = "Add server to Redis";
                    GameServer._logger.Error(log);
                }
            }

        }
    }

}
