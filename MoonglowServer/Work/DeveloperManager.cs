﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class DeveloperManager : Singleton<DeveloperManager>
{
    public static List<string> _developerList = new List<string>();
    public static List<string> _developerManagerList = new List<string>();
    public static List<string> _operatorList = new List<string>();
    public static List<string> _operatorManagerList = new List<string>();

    internal void GetDevList(UserType userType)
    {
        PacketsHandlerInner.SDReqDevList(userType);
    }
}
