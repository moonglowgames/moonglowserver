﻿using System;
using System.Threading;
using System.Diagnostics;

using MoonGlow.Room;
using MoonGlow;
using MoonGlow.Data;
using log4net;

public class ExecuteThread
{
    private volatile bool _shouldStop = false;
    private ExecuteQueue _workQueue = null;//new ExecuteQueue();
    private int _workQueueCount = 0;
    private  ILog _logger = null;
    private Thread workerThread = null;
    private IPacketCommand packetcmd = null;

    public ExecuteThread(ILog logger_)
    {
        // injections..
        _logger = logger_;
        _workQueue = QueueManager.Instance.WorkQueue;
        // thread create
        workerThread = new Thread(DoWork);
        workerThread.Start();
    }

    public void InitData()
    {
        var cardmanager = CardManager.Instance;
        var rewordmanger = RewardManager.Instance;
        var levelexpmanger = LevelExpManager.Instance;
        var conditionmanager = ConditionManager.Instance;
        var shopManager = ItemManager.Instance;
        var jokerManager = SpellManager.Instance;
        var newsManager = NewsManager.Instance;
        var achieveManager = AchieveManager.Instance;
        var missionManager = MissionManager.Instance;
        var chanelManager = ChannelManager.Instance;
        var chanelRewardManager = ChannelRewardManager.Instance;
        var newsLocalizationManager = LocalizationManager.Instance;
        var localizationManager = LocalizationManager.Instance;
        var banNickManager = BanNickManager.Instance;
    }

    public void DoWork()
    {
        InitData();
        //test area
        //string userID = Guid.NewGuid().ToString("N");
        //userIDCount = Guid.NewGuid().ToString().Length;
        //SqlConnect.Instance.Test();           
        //Console.WriteLine(DateTime.Now.Date + new TimeSpan(1,0,0,0)/*.ToString("yyMMdd")*/);       
        //Console.WriteLine(GlobalManager.Instance.GeneratePassword());        
        //AzureSendGrid.Instance.Test("xyanan@moonglowgames.com", "1234AbCd");
        //InnerPacketsManager.Instance.GetSDReqUpdateFriendFight("aa", "bb", Result.Draw);
        //int rankWin = 1;int totalRankCount = 3;
        //string  winRate1 = Math.Round((decimal)rankWin * 100 / totalRankCount, 2).ToString("F2");

        //double value1 = .086;
        //Console.WriteLine(value1.ToString("#0.##%", CultureInfo.InvariantCulture));
        //double value2 = 0;
        //Console.WriteLine(value2.ToString("#0.##%", CultureInfo.InvariantCulture));
        //double value3 = .0863;
        //Console.WriteLine(value3.ToString("#0.##%", CultureInfo.InvariantCulture));
        //double value4 = 1;
        //Console.WriteLine(value4.ToString("#0.##%", CultureInfo.InvariantCulture));
        //GlobalManager.Instance.SendEmail("qustxyn@gmail.com", "Nan","abcdefg##");
        //bool result = BanNickManager.Instance.IsNickBan(string.Empty);
        //GameServer.documentDBConnect.Test();
        //test above

        Stopwatch sw = new Stopwatch();
        while (!_shouldStop)
        {
            //Console.WriteLine("worker thread: working...");
            _workQueueCount = _workQueue.Count;

            if ((_workQueueCount > 0/*_queue.Count > 0*/) && (_workQueue.TryDequeue(out packetcmd)))
            {
                try
                {
                    if (packetcmd.packet.GetType() == typeof(SSReqCheckEvent)
                        || packetcmd.packet.GetType() == typeof(SSReqClientPause) 
                        )
                    {
                        Console.Write(packetcmd.packet.GetType().ToString());
                    }
                    else 
                    {
                        Console.WriteLine(packetcmd.packet.GetType().ToString());
                    }

                    sw = Stopwatch.StartNew();

                    packetcmd.packet.Execute(packetcmd.session);

                    sw.Stop();
                    if (packetcmd.packet.GetType() == typeof(SSReqCheckEvent) 
                        || packetcmd.packet.GetType() == typeof(SSReqClientPause)
                        //|| packetcmd.packet.GetType() == typeof(SMReqExpireUserInfo)
                        //|| packetcmd.packet.GetType() == typeof(SMReqAddRedis)
                        )
                    {
                        Console.Write(" ExecuteTime : " + sw.Elapsed +"\r");
                    }
                    else
                    {
                        Console.WriteLine(" ExecuteTime : " + sw.Elapsed);
                    }
                }
                catch (Exception ex)
                {
                    //  _logger.Debug(ex.Message);
                    //Console.WriteLine("!!!Execute packetExecuteError: " + ex.Message);
                    if (_logger.IsErrorEnabled)
                    {
                        string log = "Execute thread packet execure exception (" + ex.Message + ")";
                        _logger.Error(log);
                    }
                }
            }
            else
            {
                bool isRunning = false;
                //Console.WriteLine("delete : {0}  Count : {1}",packetcmd,_queue.Count);
                //Gets the number of milliseconds elapsed since the system started
                if (MoonGlow.GameServer._isLoginServer == false)
                {
                    int nowtick = System.Environment.TickCount;// &Int32.MaxValue;

                    if (RoomManager.Instance.isRun(nowtick))
                    {
                        RoomManager.Instance.Run(nowtick);
                        isRunning = true;
                    }

                    UserInfo userInfo = null;
                    try
                    {
                        foreach (var infoPair in SessionIdManager.Instance)
                        {
                            userInfo = infoPair.Value;
                            if (userInfo.isReadDone)
                            {
                                if (userInfo.IsItemTimeRun(nowtick))
                                {
                                    userInfo.ItemTimeRun();
                                    isRunning = true;
                                }

                                if (userInfo.IsPingRun(nowtick))
                                {
                                    userInfo.PingRun();
                                    isRunning = true;
                                }

                                if (userInfo.IsExpireUserInfoRun(nowtick))
                                {
                                    userInfo.ExpireUserInfoRun();
                                    isRunning = true;
                                }

                                if (userInfo.IsCheckSessionRun(nowtick))
                                {
                                    userInfo.CheckSessionRun();
                                    isRunning = true;
                                }
                            }
                            
                        }
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine(string.Format("!!!Error: User: {0} Inner PacketsExecute Error: ", userInfo) + e.Message);
                        if (_logger.IsErrorEnabled)
                        {
                            string log = string.Format("InnerPackets execute exception ({0}), userInfo: {1}", e.Message, userInfo) ;
                            _logger.Error(log);
                        }
                    }
                }

                if(isRunning == false)
                {
                    Thread.Sleep(1);
                }
            }
        }
    }

    public void RequestStop()
    {
        _shouldStop = true;
    }

    public bool isAlive { get { return workerThread.IsAlive; } }
}
    

