﻿using System;
using System.Threading;
using System.Diagnostics;
using MoonGlow;
using log4net;

public class RedisDBThread
{
    private volatile bool _shouldStop = false;
    private RedisDBQueue _memDBQueue = null;
    private int _memDBQueueCount = 0;
    private ILog _logger = null;
    private Thread _redisDBThread = null;
    private IPacketCommand packetcmd = null;

    public RedisDBThread(ILog logger_)
    {
        // injections..
        _logger = logger_;
        _memDBQueue = QueueManager.Instance.MemDBQueue;
        // thread create
        _redisDBThread = new Thread(RedisDBWork);
        _redisDBThread.Start();
    }

    public void RedisDBWork()
    {
        if (ServerStateRedisConnect.Instance.CheckServerList(/*GameServer._connectIP,*/  GameServer._DNS, GameServer._port, GameServer._type, GameServer._version, 0, GameServer._enable).Result)
        {
            Console.WriteLine("*** Redis: Add Server to Redis Server List successfully.");
        }

        if (GameServer._isLoginServer)
        {
            GameServer._gameServerList = ServerStateRedisConnect.Instance.GetGameServerList();
        }

        Stopwatch sw = new Stopwatch();
        while (!_shouldStop)
        {
            _memDBQueueCount = _memDBQueue.Count;
            //Console.WriteLine("**The MemDB queue count is: " + _memDBQueueCount);
            //Thread.Sleep(1000);

            if ((_memDBQueueCount > 0/*_queue.Count > 0*/) && (_memDBQueue.TryDequeue(out packetcmd)))
            {
                try
                {
                    if ( packetcmd.packet.GetType() == typeof(SMReqExpireUserInfo)
                        //|| packetcmd.packet.GetType() == typeof(SMReqAddRedis)
                        )
                    {
                        Console.Write(packetcmd.packet.GetType().ToString());
                    }
                    else
                    {
                        Console.WriteLine(packetcmd.packet.GetType().ToString());
                    }
                    sw = Stopwatch.StartNew();
                    packetcmd.packet.Execute(packetcmd.session);
                    sw.Stop();
                    if (packetcmd.packet.GetType() == typeof(SMReqExpireUserInfo))
                    {
                        Console.Write("Redis ExecuteTime : " + sw.Elapsed + "\r");
                    }
                    else
                    {
                        Console.WriteLine("Redis ExecuteTime : " + sw.Elapsed);
                    }
                    //Console.WriteLine("ExecuteTime : " + sw.Elapsed);
                }
                catch (Exception ex)
                {
                    //  _logger.Debug(ex.Message);
                    //Console.WriteLine("!!!RedisDBWork packetExecuteError: " + ex.Message + "Packet: " + packetcmd);
                    if (_logger.IsErrorEnabled)
                    {
                        string log = string.Format("RedisDB Thread packet execute error ({0}), packet: {1}", ex.Message, packetcmd.packet.GetType().ToString());
                        _logger.Error(log);
                    }
                }
            }
            else
            {
                bool isRunning = false;
                int nowtick = Environment.TickCount;// &Int32.MaxValue;

                if (MoonGlow.Work.RedisManager.Instance.isCheckServerListRun(nowtick))
                {
                    MoonGlow.Work.RedisManager.Instance.CheckServerListRun();
                    isRunning = true;
                }

                if (isRunning == false) Thread.Sleep(1);
            }
        }
    }

    public void RequestStop()
    {
        _shouldStop = true;
    }

    public bool isAlive { get { return _redisDBThread.IsAlive; } }
}