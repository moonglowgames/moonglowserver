﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class AzureStorageConnect
{
    public static string connectString = string.Empty;
    private bool isConnect = false;

    CloudStorageAccount storageAccount = null;
    CloudTableClient tableClient = null;
    public CloudTable accountTable = null;
    public CloudTable levelTable = null;
    public CloudTable itemTable = null;
    public CloudTable playTable = null;
    public CloudTable profileTable = null;

    //string today = string.Empty;
    public string LoginDataName = "AccountData";
    public string LevelDataName = "LevelData";
    public string ItemDataName = "ItemData";
    public string PlayDataName = "PlayData";
    public string ProfileDataName = "ProfileData";

    public AzureStorageConnect()
    {
        storageAccount = CloudStorageAccount.Parse(connectString);
        tableClient = storageAccount.CreateCloudTableClient();

        if (tableClient != null) isConnect = true;

        if (isConnect)
        {
            Console.WriteLine("OK Azure Log table Connected! ");
        }
        else
        {
            //Console.WriteLine("FAIL Azure Logtable Not Connected!!! ");
            if (GameServer._logger.IsFatalEnabled)
            {
                string log = "Azure Logtable Not Connected!!!";
                GameServer._logger.Fatal(log);
            }
        }
    }

    internal void AccountDataInsert(LogAccountData data)
    {
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");

            //reconnect..
            return;
        }

        TableOperation insertOperation = TableOperation.Insert(data);
        accountTable.Execute(insertOperation);
        Console.WriteLine("Insert into account table successfully~");
    }

    internal void LevelDataInsert(LogLevelData data)
    {
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");

            //reconnect..
            return;
        }

        TableOperation insertOperation = TableOperation.Insert(data);
        levelTable.Execute(insertOperation);
        Console.WriteLine("Insert into level table successfully~");
    }

    internal void ItemDataInsert(LogItemData data)
    {
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");

            //reconnect..
            return;
        }

        TableOperation insertOperation = TableOperation.Insert(data);
        itemTable.Execute(insertOperation);
        Console.WriteLine("Insert into item table successfully~");
    }

    internal void PlayDataInsert(LogPlayData data)
    {
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");

            //reconnect..
            return;
        }

        TableOperation insertOperation = TableOperation.Insert(data);
        playTable.Execute(insertOperation);
        Console.WriteLine("Insert into play table successfully~");
    }

    internal void ProfileDataInsert(LogProfileData data)
    {
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");

            //reconnect..
            return;
        }

        TableOperation insertOperation = TableOperation.Insert(data);
        profileTable.Execute(insertOperation);
        Console.WriteLine("Insert into profile table successfully~");
    }

    internal bool ResetTable()
    {
        bool retvalue = false;
        string today = "T" + DateTime.Today.ToString("yyMMdd");
        LoginDataName = today + "AccountData";
        LevelDataName = today + "LevelData";
        ItemDataName = today + "ItemData";
        PlayDataName = today + "PlayData";
        ProfileDataName = today + "ProfileData";

        accountTable = tableClient.GetTableReference(LoginDataName);
        levelTable = tableClient.GetTableReference(LevelDataName);
        itemTable = tableClient.GetTableReference(ItemDataName);
        playTable = tableClient.GetTableReference(PlayDataName);
        profileTable = tableClient.GetTableReference(ProfileDataName);

        accountTable.CreateIfNotExists();
        levelTable.CreateIfNotExists();
        itemTable.CreateIfNotExists();
        playTable.CreateIfNotExists();
        profileTable.CreateIfNotExists();
        if (accountTable.Exists() && levelTable.Exists() && itemTable.Exists() && playTable.Exists() && profileTable.Exists())
        {
            retvalue = true;
        }

        return retvalue;
    }
}
