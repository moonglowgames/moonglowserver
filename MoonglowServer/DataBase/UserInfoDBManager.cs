﻿using LitJson;
using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ProfileData
{
    public int addHP { get; set; }
    public int addSP { get; set; }
    public int winCount { get; set; }
    public int loseCount { get; set; }
    public int drawCount { get; set; }
    public int conWinCount { get; set; }
    public int CoConWinCount { get; set; }
    public int JoConWinCount { get; set; }
    public int ChConWinCount { get; set; }
    public int ZaConWinCount { get; set; }
    public int ArConWinCount { get; set; }
    public int TaConWinCount { get; set; }
    public int KrConWinCount { get; set; }
    public int YaConWinCount { get; set; }
    public int friendCount { get; set; }
    public int checkInDays { get; set; }
    public int conCheckInDays { get; set; }

    public int chanelIdx { get; set; }
    public int rankPoint { get; set; }
    public int rankWin { get; set; }
    public int rankLose { get; set; }
    public int rankDraw { get; set; }
    public ChatChannel chatChannel { get; set; }

    //public int etherealInkUseCount { get; set; }
    //public int saqqaraTabletUseCount { get; set; }
    //public int rosemaryOilUseCount { get; set; }
    //public int sLElixirUseCount { get; set; }
    //public int epElixirUseCount { get; set; }
    //public int soElixirUseCount { get; set; }
}

public class ResourceData
{
    public int Stamina {get;set;}
    public int maxStamina { get; set; }
    public int Gold {get; set;}
    public int Ruby {get; set;}
    public int Ink { get; set; }
    public int JokerLine { get; set; }
}

public class ItemData
{
    public int ePElixir { get; set; }
    public int sOElixir { get; set; }
    public int rosemaryOil { get; set; }
    public int sLElixir { get; set; }
    public int etherealInk { get; set; }
    public int saqqaraTablet { get; set; }
    //pet & badge & jokerFace List
    public List<Pet> petList { get; set; }
    public List<Badge> badgeList { get; set; }
    public List<int> jokerFaceList { get; set; }
    public List<CardSkin> cardSkinList { get; set; }
    public List<int> iconSet { get; set; }
    public ItemData() 
    {
        petList = new List<Pet>();
        badgeList = new List<Badge>();
        cardSkinList = new List<CardSkin>();
        jokerFaceList = new List<int>();
        iconSet = new List<int>();
    }
}

public class JokerData
{
    public int faceIdx { get; set; }
    public List<int> spellIdxList { get; set; }
    public JokerData()
    {
        spellIdxList = new List<int>();
    }
}

public class UserData
{
    //id is used for azure document db
    public string id { get; set; }
    public string account { get; set; }
    public string nick { get; set; }
    //public string pw { get; set; }
    public int faceIdx { get; set; }
    public int level { get; set; }
    public int exp { get; set; }
    public ResourceData resourceData { get; set; }
    public ItemData itemData { get; set; }
    public JokerData jokerData { get; set; }
    public ProfileData profileData { get; set; }
    public DateTime staminLastEventDate { get; set; }
    public DateTime soElixirEndEventDate { get; set; }
    public DateTime epElixirEndEventDate { get; set; }
    public DateTime ADLastTime { get; set; }
    public int ADRemainCount { get; set; }

    //achievement & mission
    public List<AchieveValue> achieveValue { get; set; }
    public List<MissionValue> missionValue { get; set; }
    //public UserType userType { get; set; }
    public string createDate { get; set; }
    public string lastLoginDate{ get; set; }
    //public string lasetLogoutDate { get; set; }
    //public bool ban { get; set; }
    public UserData()
    {
        //achievement & mission...
        achieveValue = new List<AchieveValue>();
        missionValue = new List<MissionValue>();

        itemData = new ItemData();
        resourceData = new ResourceData();
        jokerData = new JokerData();
        profileData = new ProfileData();
    }
}

public class UserInfoDataBaseManager
{
    public static UserInfoDataBaseManager _instance = null;
    public static UserInfoDataBaseManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new UserInfoDataBaseManager();
            }
            return _instance;
        }
    }
    public static UserData _userData = null;

    //public bool SaveToDataBase(/*string nick_, int level_, int faceIdx_, int rankPoint_, */UserData userData_)
    //{
    //    bool retvalue = false;
    //    if (userData_ != null)
    //    {
    //        string jsonstring = JsonMapper.ToJson(userData_);
    //        if (GameServer.SqlType == SQLType.MySQL)
    //        {
    //            if (MySqlConnect.Instance.UpdateUserInfo(userData_.id, userData_.nick, userData_.level, userData_.faceIdx, userData_.profileData.rankPoint, jsonstring))
    //            {
    //                retvalue = true;
    //            }
    //        }
    //        else
    //        {
    //            if (MSSqlConnect.Instance.UpdateUserInfo(userData_.id, userData_.nick, userData_.level, userData_.faceIdx, userData_.profileData.rankPoint, jsonstring))
    //            {
    //                retvalue = true;
    //            }
    //        }

    //        //if (GameServer.documentDBConnect.UpdateUserInfo(userData_))
    //        //{
    //        //    //do something...
    //        //}

    //    }
    //    return retvalue;
    //    // save to  sql
    //}

}
