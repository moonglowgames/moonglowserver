﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class MailData
{
    public int idx { get; set; }
    public string userSession { get; set; }
    //public string nick { get; set; }
    public string sendID { get; set; }
    //public string senderNick { get; set; }
    public string subject { get; set; }
    public string content { get; set; }
    public int itemIdx1 { get; set; }
    public int itemCount1 { get; set; }
    //public int itemIdx2 { get; set; }
    //public int itemCount2 { get; set; }
    //public int itemIdx3{ get; set; }
    //public int itemCount3 { get; set; }
    public bool itemGot { get; set; }
    //public DateTime sendDate { get; set; }
    public DateTime itemGotDate { get; set; }
    public bool del { get; set; }
    public MailData() {}
}
public class MailDBManager
{
    public static MailDBManager _instance = null;
    public static MailDBManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new MailDBManager();
            }
            return _instance;
        }
    }  

    public void GetMailListFromDB(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

       PacketsHandlerInner.SDReqGetMailList(serverSession, userSession);
    }

    public void SetSendSCNotiMailList(string serverSession, string userSession)
    {
        List<Mail> mailList = null;
        GetUserMailList(out mailList, serverSession, userSession);

        if (mailList != null)
        {
            //Send SCNotiMailList
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo == null)
            {
                GlobalManager.Instance.NullUserInfo(serverSession);
                return;
            }
            PacketsHandlerServer.SendSCNotiMailList(userInfo.serverSession, new MailList(PacketTypeList.SCNotiMailList.ToString(), mailList));
        }
    }

    public void GetUserMailList(out List<Mail> mailList,string serverSession, string userSession)
    {
        mailList = new List<Mail>();
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        if (userInfo._mailList != null)
        {
            string subject = string.Empty;
            string content = string.Empty;
            for (int i = 0; i < userInfo._mailList.Count; i++)
            {
                if (userInfo._mailList[i].del == false)
                {
                    subject = userInfo._mailList[i].subject;
                    content = userInfo._mailList[i].content;

                    subject = (LocalizationManager._dict.ContainsKey(subject)) ?
                                    LocalizationManager.Instance.Get(subject, userInfo.language) : subject;
                    content = (LocalizationManager._dict.ContainsKey(content)) ?
                                    LocalizationManager.Instance.Get(content, userInfo.language) : content;

                    mailList.Add(new Mail(i + 1, subject, content, userInfo._mailList[i].itemIdx1, userInfo._mailList[i].itemCount1, userInfo._mailList[i].itemGot));
                }
            }
        }
    }

    internal void NewMail(MailData mailData)
    {
        UserInfo userInfo = GameServer.GetUserInfo(mailData.userSession);
        if(userInfo != null)
        {
            PacketsHandlerInner.SDReqNewMail(userInfo.serverSession, mailData);
        }
    }
}