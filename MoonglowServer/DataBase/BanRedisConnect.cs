﻿using StackExchange.Redis;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using MoonGlow;

class BanRedisConnect
{
    public static string connectString = string.Empty;
    public static int defaultDB = 0;
    public IDatabase db = null;
    public static BanRedisConnect _instance = null;
    public static BanRedisConnect Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new BanRedisConnect();
            }
            return _instance;
        }
    }

    public ConnectionMultiplexer redis = null;

    public BanRedisConnect()
    {
        try
        {
            var options = ConfigurationOptions.Parse(connectString);
            options.DefaultDatabase = defaultDB;

            redis = ConnectionMultiplexer.Connect(options);
            db = redis.GetDatabase();

            Console.WriteLine("BanRedisConnect connected...");
        }
        catch
        {
            //Console.WriteLine("!!!BanRedisConnect not connected");
            if (GameServer._logger.IsFatalEnabled)
            {
                string log = "BanRedisConnect not connected!!!";
                GameServer._logger.Fatal(log);
            }
        }
    }

    private bool IsConnect()
    {
        bool retvalue = false;
        if (redis != null && db != null)
        {
            retvalue = redis.IsConnected;
        }
        return retvalue;
    }

    async public Task<bool> AddRedis(string userID, TimeSpan timeSpan)
    {
        bool retvalue = false;
        if (IsConnect())
        {
            string key = userID;

            await db.SetAddAsync(key, string.Empty);

            retvalue = await db.KeyExpireAsync(key, timeSpan);
            //retvalue = true;
        }
        return retvalue;
    }

    async public Task<bool> CheckUserBan(string userID)
    {
        return IsConnect() ? await db.KeyExistsAsync(userID) : false;
    }

}
