﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;
using LitJson;

public class DocumentDBConnect
{
    public static string EndpointUrl = string.Empty/*"https://mgcd-kr-docdb1.documents.azure.com:443/"*/;
    public static string AuthorizationKey = string.Empty/*"xnp7khhgRlGVdHklWYthBogKbTd9WLipSEQx1kVkNO1N+VE7sKwPqIlvFia1PAtUvyTyPslOYj30nzLW3I7KlA=="*/;

    public bool isConnect = false;
    private DocumentClient client = null;
    private Database database = null;
    private DocumentCollection collection = null;
    private Document document = null;
    private string dateBaseID = "UserInfoDB";
    private string collectionID = "UserInfoCollection";

    //private static DocumentDBConnect _instance = null;
    //public static DocumentDBConnect Instance
    //{
    //    get
    //    {
    //        if (_instance == null)
    //        {
    //            _instance = new DocumentDBConnect();
    //        }
    //        return _instance;
    //    }
    //}

    public DocumentDBConnect()
    {
        try
        {
            Connect().Wait();
            if(client != null) isConnect = true;
            Console.WriteLine("\nDocumentDB Connected...");
        }
        catch (Exception e)
        {
            Exception baseException = e.GetBaseException();
            Console.WriteLine("Error: {0}, Message: {1}", e.Message, baseException.Message);
        }
    }

    private async Task Connect()
    {
        client = new DocumentClient(new Uri(EndpointUrl), AuthorizationKey);

        // Try to retrieve a Database if exists, else create the Database
        database = await RetrieveOrCreateDatabaseAsync();

        // Try to retrieve a Document Collection, else create the Document Collection
        collection = await RetrieveOrCreateCollectionAsync(database.SelfLink);
    }

    private async Task<DocumentCollection> RetrieveOrCreateCollectionAsync(string databaseSelfLink)
    {
        // Try to retrieve the collection (Microsoft.Azure.Documents.DocumentCollection) whose Id is equal to collectionId
        var collection = client.CreateDocumentCollectionQuery(databaseSelfLink).Where(c => c.Id == collectionID).ToArray().FirstOrDefault();

        // If the previous call didn't return a Collection, it is necessary to create it
        if (collection == null)
        {
            collection = await client.CreateDocumentCollectionAsync(databaseSelfLink, new DocumentCollection { Id = collectionID });
        }

        return collection;
    }

    private async Task<Database> RetrieveOrCreateDatabaseAsync()
    {
        // Try to retrieve the database (Microsoft.Azure.Documents.Database) whose Id is equal to databaseId            
        var database = client.CreateDatabaseQuery().Where(db => db.Id == dateBaseID).AsEnumerable().FirstOrDefault();

        // If the previous call didn't return a Database, it is necessary to create it
        if (database == null)
        {
            database = await client.CreateDatabaseAsync(new Database { Id = dateBaseID });
            Console.WriteLine("Created Database: id - {0} and selfLink - {1}", database.Id, database.SelfLink);
        }

        return database;
    }

    public string RetrieveDocuments(string documentID)
    {
        string retvalue = string.Empty;
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");

            //reconnect..
            return retvalue;
        }

        document = client.CreateDocumentQuery("dbs/" + database.Id + "/colls/" + collection.Id).Where(d => d.Id == documentID).AsEnumerable().FirstOrDefault();

        if (document != null)
        {
            //Console.WriteLine(document.ToString());
            retvalue = document.ToString();
        }

        return retvalue;
    }

    public async Task<bool> CreateDocumentsAsync(UserData userData_)
    {
        bool result = false;

        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");

            //reconnect..
            return result;
        }

        document = client.CreateDocumentQuery("dbs/" + database.Id + "/colls/" + collection.Id).Where(d => d.Id == userData_.id).AsEnumerable().FirstOrDefault();
        // If the document does not exist, create a new document
        if (document == null)
        {
            // id based routing for the first argument, "dbs/FamilyRegistry/colls/FamilyCollection"
            document = await client.CreateDocumentAsync("dbs/" + database.Id + "/colls/" + collection.Id, userData_);
        }

        if (document != null) result = true;
        return result;
    }

    public async Task<bool> UpdateDocumentsAsync(UserData userData_)
    {
        bool result = false;
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");

            //reconnect..
            return result;
        }

        document = client.CreateDocumentQuery("dbs/" + database.Id + "/colls/" + collection.Id).Where(d => d.Id == userData_.id).AsEnumerable().FirstOrDefault();
        if (document != null)
        {
            //the self-link of the document to be updated. E.g.dbs / db_rid / colls / col_rid / docs / doc_rid /
            document = await client.ReplaceDocumentAsync("dbs/" + database.Id + "/colls/" + collection.Id + "/docs/" + userData_.id, userData_);
        }

        if (document != null) result = true;
        return result;
    }

    internal bool Register(string userID, string account, string nick, int faceIdx)
    {
        bool retvalue = false;
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");

            //reconnect..
            return retvalue;
        }

        UserData userData = null;
        GlobalManager.Instance.InitUserData(userID, account, nick, faceIdx, out userData);

        retvalue = CreateDocumentsAsync(userData).Result;

        return retvalue;
    }

    internal void GetUserInfo(string documentID, out string userInfoJson)
    {
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");
            userInfoJson = string.Empty;
            //reconnect..

            return;
        }

        userInfoJson = RetrieveDocuments(documentID);
    }

    internal bool UpdateUserInfo(UserData userData_)
    {
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");
            //reconnect..

            return false;
        }

        return UpdateDocumentsAsync(userData_).Result;
    }

    internal void GetUserLevel(string documentID, out int userLevel)
    {
        userLevel = 0;
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");
            //reconnect..

            return;
        }

        try
        {
            var query = new SqlQuerySpec("SELECT VALUE c.level FROM c where c.id=@userID", new SqlParameterCollection(new SqlParameter[] { new SqlParameter { Name = "@userID", Value = documentID } }));
            userLevel = client.CreateDocumentQuery<int>("dbs/" + database.Id + "/colls/" + collection.Id, query).AsEnumerable().FirstOrDefault();
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Error: documentDB GetUserLevel error!!! " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "documentDB GetUserLevel exception : (" + e.Message + " ), userID: " + documentID;
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
    }

    internal void GetUserNick(string documentID, out string userNick)
    {
        userNick = string.Empty;
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");
            //reconnect..
            return;
        }
        try
        {
            var query = new SqlQuerySpec("SELECT VALUE c.nick FROM c where c.id=@userID", new SqlParameterCollection(new SqlParameter[] { new SqlParameter { Name = "@userID", Value = documentID } }));
            userNick = client.CreateDocumentQuery<string>("dbs/" + database.Id + "/colls/" + collection.Id, query).AsEnumerable().FirstOrDefault();
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Error: documentDB GetUserNick error!!! " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "documentDB GetUserNick exception : (" + e.Message + " ), userID: " + documentID;
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
    }

    internal bool EnableUserID(string userID)
    {
        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");

            //reconnect..
            return false;
        }

        bool retvalue = string.IsNullOrEmpty(RetrieveDocuments(userID)) ? true : false;

        return retvalue;
    }

    internal void GetUserRankInfo(string documentID, out int rankWin, out int rankLose, out int rankDraw)
    {
        List<int> results = new List<int>();
        rankWin = 0;
        rankLose = 0;
        rankDraw = 0;

        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");
            //reconnect..
            return;
        }
        try
        {
            var query = new SqlQuerySpec("SELECT VALUE c.profileData FROM UserData c where c.id=@userID", new SqlParameterCollection(new SqlParameter[] { new SqlParameter { Name = "@userID", Value = documentID } }));
            ProfileData userProfile = client.CreateDocumentQuery<ProfileData>("dbs/" + database.Id + "/colls/" + collection.Id, query).AsEnumerable().FirstOrDefault();

            rankWin = userProfile.rankWin;
            rankLose = userProfile.rankLose;
            rankDraw = userProfile.rankDraw;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Error: documentDB GetUserRankInfo error!!! " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "documentDB GetUserRankInfo exception : (" + e.Message + " ), userID: " + documentID;
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
    }

    internal void GetPlayerProfile(string playerNick, out int faceIdx, out int lvl, out int winCount, out int loseCount, out List<Badge> badgeList, out List<Pet> petList)
    {
        faceIdx = 0;
        lvl = 0;
        winCount = 0;
        loseCount = 0;
        badgeList = new List<Badge>();
        petList = new List<Pet>();

        if (!isConnect)
        {
            Console.WriteLine("Data not Connected! ");
            //reconnect..
            return;
        }
        try
        {
            var query = new SqlQuerySpec("SELECT VALUE c.id FROM c where c.nick=@nick", new SqlParameterCollection(new SqlParameter[] { new SqlParameter { Name = "@nick", Value = playerNick} }));
            string userID = client.CreateDocumentQuery<string>("dbs/" + database.Id + "/colls/" + collection.Id, query).AsEnumerable().FirstOrDefault();

            document = client.CreateDocumentQuery("dbs/" + database.Id + "/colls/" + collection.Id).Where(d => d.Id == userID).AsEnumerable().FirstOrDefault();

            if (document != null)
            {
                string jsonString = document.ToString();
                if (!string.IsNullOrEmpty(jsonString))
                {
                    UserData playerUserData = JsonMapper.ToObject<UserData>(jsonString);
                    faceIdx = playerUserData.faceIdx;
                    lvl = playerUserData.level;
                    winCount = playerUserData.profileData.winCount;
                    loseCount = playerUserData.profileData.loseCount;
                    badgeList = playerUserData.itemData.badgeList;
                    petList = playerUserData.itemData.petList;
                    //iconSet
                }
            }
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Error: documentDB GetPlayerProfile error!!! " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "documentDB GetPlayerProfile exception : (" + e.Message + " ), userNick: " + playerNick;
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
    }

    internal void Test()
    {
        //1.
        //string resultString = CheckDocuments("CLI091682");
        //if (!string.IsNullOrEmpty(resultString))
        //{
        //    Console.WriteLine(resultString);
        //}

        //2.
        //Console.WriteLine(CheckDocuments("CLI091682"));
        //UserData userData = null;
        //GlobalOperateManager.Instance.InitUserData("CLI091682", out userData);
        //bool result = UpdateDocumentsAsync(userData).Result;
        //Console.WriteLine(CheckDocuments("CLI091682"));

        //3.
        //int level = 0;
        //GetUserLevel("CLI056027", out level);
        //Console.WriteLine("Level is: " + level);

        //4.
        //string nick = string.Empty;
        //GetUserNick("CLI056027", out nick);
        //Console.WriteLine("Nick is: " + nick);

        //5.
        //LoginState state = LoginState.Unknown;
        //CheckUserID("CLI056028", out state);

        //6.
        //int rankWin = 0;
        //int rankLose = 0;
        //int rankDraw = 0;
        //GetUserRankInfo("CLI015642", out rankWin, out rankLose, out rankDraw);

        //7.
        int faceIdx = 0;
        int lvl = 0;
        int winCount = 0;
        int loseCount = 0;
        List<Badge> badgeList = null;
        List<Pet> petList = null;
        GetPlayerProfile("xu_pc", out faceIdx, out lvl, out winCount, out loseCount, out badgeList, out petList);
    }
}
