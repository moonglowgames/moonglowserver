﻿using MoonGlow;
using StackExchange.Redis;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

class RankConnect
{
    public static string connectString = string.Empty;
    public static int defaultDB = 0;
    public IDatabase db = null;
    public static RankConnect _instance = null;
    public static RankConnect Instance
    { 
        get
        {
            if(_instance == null)
            {
                _instance = new RankConnect();
            }
            return _instance;
        }
    }

    public ConnectionMultiplexer redis = null;

    public RankConnect()
    {
        try
        {
            var options = ConfigurationOptions.Parse(connectString);
            options.DefaultDatabase = defaultDB;

            redis = ConnectionMultiplexer.Connect(options);
            db = redis.GetDatabase();
            
            Console.WriteLine("RankRedis connected...");
        }
        catch(Exception e)
        {
            //Console.WriteLine("!!!RankRedis not connected");
            if (GameServer._logger.IsFatalEnabled)
            {
                string log = "UpdateIAPState to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Fatal(log);
            }
        }
    }

    private bool IsConnect()
    {
        bool retvalue = false;
        if (redis != null && db != null)
        {
            retvalue = redis.IsConnected;
        }
        return retvalue;
    }

    async public Task<bool> AddRank(RedisKey rankChanel, RedisValue userID, double score)
    {
        Console.WriteLine("AddRank ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        //bool x = false;
        //if (db != null)
        //{
        //    x = await db.SortedSetAddAsync(rankChanel, userID, score);
        //}
        //return x;
        return (IsConnect()) ? await db.SortedSetAddAsync(rankChanel, userID, score):false;
    }

    async public Task<bool> RemoveRank(RedisKey rankChanel, RedisValue userID)
    {
        Console.WriteLine("RemoveRank ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        //bool x = false;
        //if (db != null)
        //{
        //    x = await db.SortedSetRemoveAsync(rankChanel, userID);
        //}
        //return x;
        return (IsConnect()) ? await db.SortedSetRemoveAsync(rankChanel, userID) : false;
    }

    async public Task<long?> GetRank(RedisKey rankChanel, RedisValue userID)
    {
        Console.WriteLine("GetRank ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        //long? x = 0;
        //if (db != null)
        //{
        //   x = await db.SortedSetRankAsync(rankChanel, userID, Order.Descending);
        //}
        //return x;
        return (IsConnect()) ? await db.SortedSetRankAsync(rankChanel, userID, Order.Descending) : 0;
    }

    async public Task<SortedSetEntry[]> GetRankRange(RedisKey rankChanel, long start, long stop)
    {
        Console.WriteLine("GetRankRange ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        //SortedSetEntry[] setEntry = null;
        //if (db != null)
        //{
        //    setEntry = await db.SortedSetRangeByRankWithScoresAsync(rankChanel, start, stop, Order.Descending);
        //}
        //return setEntry;
        return (IsConnect()) ? await db.SortedSetRangeByRankWithScoresAsync(rankChanel, start, stop, Order.Descending) : null;
    }

    async public Task<long> GetRankCardinality(RedisKey rankChanel)
    {
        Console.WriteLine("GetRankCardinality ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        //long x = 0;
        //if (db != null)
        //{
        //    x = await db.SortedSetLengthAsync(rankChanel);
        //}
        //return x;
        return (IsConnect()) ? await db.SortedSetLengthAsync(rankChanel) : 0;
    }

    async public Task<bool> RemoveChanel(string key)
    {
        Console.WriteLine("RemoveChanel ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        //bool x = false;
        //if (db != null)
        //{
        //    x = await db.KeyDeleteAsync(key);
        //}
        //return x;
        return (IsConnect()) ? await db.KeyDeleteAsync(key) : false;
    }

}
