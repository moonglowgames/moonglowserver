﻿using MoonGlow;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ServerStateRedisConnect
{
    public static string connectString = string.Empty;
    public static int defaultDB = 0;
    public IDatabase db = null;
    public ISubscriber sub = null;
    public static ServerStateRedisConnect _instance = null;
    public static ServerStateRedisConnect Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ServerStateRedisConnect();
            }
            return _instance;
        }
    }

    public ConnectionMultiplexer redis = null;
    public ConnectionMultiplexer listRedis = null;

    public ServerStateRedisConnect()
    {
        try
        {
            var options = ConfigurationOptions.Parse(connectString);
            options.DefaultDatabase = defaultDB;
            options.AllowAdmin = true;
            redis = ConnectionMultiplexer.Connect(options);

            db = redis.GetDatabase();
            sub = redis.GetSubscriber();

            Console.WriteLine("GlobalServerState Redis connected...");
        }
        catch
        {
            //Console.WriteLine("!!!GlobalServerState Redis not connected");
            if (GameServer._logger.IsFatalEnabled)
            {
                string log = "GlobalServerState Redis not connected!!!";
                /*Logger*/
                GameServer._logger.Fatal(log);
            }
        }
    }

    private bool IsConnect()
    {
        bool retvalue = false;
        if (redis != null && db != null && sub != null)
        {
            retvalue = redis.IsConnected;
        }  
        return retvalue;
    }

    public void SubscibeChannel()
    {
        if (IsConnect())
        {
            sub.Unsubscribe("Server State");
            sub.Subscribe("Server State", (channel, message) => {
                PacketsHandlerInner.MSNotiServerState(string.Empty, (string)message);
            });

            sub.Unsubscribe("ReFresh Server List");
            sub.Subscribe("ReFresh Server List", (channel, message) => {
                string serverSession = message;
                PacketsHandlerInner.SMReqRefreshServerList(serverSession);
            });

            sub.Unsubscribe("ReFresh Channel List");
            sub.Subscribe("ReFresh Channel List", (channel, message) => {
                string serverSession = message;
                PacketsHandlerInner.MDNotiRefreshChannelInfo(serverSession);
            });

            //Version
            sub.Unsubscribe("ReFresh Version List");
            sub.Subscribe("ReFresh Version List", (channel, message) => {
                string serverSession = message;
                PacketsHandlerInner.MDNotiRefreshClientVersion(serverSession);
            });

            sub.Unsubscribe("Update Enable");
            sub.Subscribe("Update Enable", (channel, message) => {
                string[] value = message.ToString().Split('+');
                if (value.Length == 4)
                {
                    string serverSession = value[0];
                    //string _connectIP = value[1];
                    string _connectDNS = value[1];
                    int _port = int.Parse(value[2]);
                    bool _enable = Boolean.Parse(value[3]);
                    PacketsHandlerInner.SMNotiUpdateEnable(serverSession, _connectDNS, _port, _enable);
                }
            });

            //Update Limit
            sub.Unsubscribe("Update Limit");
            sub.Subscribe("Update Limit", (channel, message) => {
                string[] value = message.ToString().Split('+');
                if (value.Length == 2)
                {
                    string serverSession = value[0];
                    int count = int.Parse(value[1]);
                    PacketsHandlerInner.MSNotiUpdateLimit(serverSession, count);
                }
            });

            //Fresh Localization
            sub.Unsubscribe("ReFresh Localization");
            sub.Subscribe("ReFresh Localization", (channel, message) => {
            string serverSession = message;
            PacketsHandlerInner.MDNotiUpdateLocalization(serverSession);
            });

            //ReFresh News
            sub.Unsubscribe("ReFresh News");
            sub.Subscribe("ReFresh News", (channel, message) => {
                string serverSession = message;
                PacketsHandlerInner.MDNotiUpdateNews(serverSession);
            });

            //ReFresh Mission
            sub.Unsubscribe("ReFresh Mission");
            sub.Subscribe("ReFresh Mission", (channel, message) => {
                string serverSession = message;
                PacketsHandlerInner.MDNotiUpdateMission(serverSession);
            });

            //New Friend Gift
            sub.Unsubscribe("New Friend Gift");
            sub.Subscribe("New Friend Gift", (channel, message) => {
                string userID = message;
                PacketsHandlerInner.MSNotiNewFriendGift(string.Empty, userID);
            });

            //Alarm Shop
            sub.Unsubscribe("Alarm Shop");
            sub.Subscribe("Alarm Shop", (channel, message) => {
                string[] value = message.ToString().Split(',');
                if (value.Length == 2)
                {
                    string serverSession = value[0];
                    string alarmString = value[1];
                    PacketsHandlerInner.MSNotiAlarmShop(serverSession, alarmString);
                }
            });

            //Alarm News
            sub.Unsubscribe("Alarm News");
            sub.Subscribe("Alarm News", (channel, message) => {
                string[] value = message.ToString().Split(',');
                if (value.Length == 2)
                {
                    string serverSession = value[0];
                    string alarmString = value[1];
                    PacketsHandlerInner.MSNotiAlarmNews(serverSession, alarmString);
                }
            });
        }
    }

    async public Task<bool> SetGlobalServerState(string state)
    {
        //Console.WriteLine("ServerGlobalState ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);

        bool retvalue = false;
        if (IsConnect())
        {
            string key = ServerManager.GLOBAL_SERVER_NAME;
            bool isExist = await db.KeyExistsAsync(key);
            string stateField = ServerManager.GLOBAL_SERVER_STATE;
            string limitField = ServerManager.GLOBAL_SERVER_LIMIT;
            if (!isExist)
            {
                HashEntry[] hashFields = { new HashEntry(stateField, state), new HashEntry(limitField, ServerManager.SERVER_CONNECTION_LIMIT) };
                await db.HashSetAsync(key, hashFields);
            }
            else
            {
                await db.HashSetAsync(key, stateField, state);//true: new, false: update
            }
            retvalue = true;
        }

        return retvalue;
    }

    async public Task<bool> GetGlobalServerState()
    {
        bool retvalue = false;
       
        if (IsConnect())
        {
            string key = ServerManager.GLOBAL_SERVER_NAME;
            string stateField = ServerManager.GLOBAL_SERVER_STATE;

            //Task<RedisValue>
            RedisValue x = await db.HashGetAsync(key, stateField);
            if (x.ToString() == "start") retvalue = true;
        }
        //Task<RedisValue> HashGetAsync
        return retvalue;
    }

    async public Task<long> BCGlobalServerState(string serverState)
    {
        long x = 0;
        if (IsConnect())
        {
            x = await sub.PublishAsync("Server State", serverState);
        }
        return x;
    }

    async internal Task<bool> CheckServerList(/*string iP*/ string DNS, int port, string type, string version, int connections, bool enable)
    {
        bool retvalue = false;
        try
        {
            if (IsConnect())
            {
                string serverKey = string.Format("Server:{0}({1})", /*iP*/DNS, port);
                //bool isExist = await db.KeyExistsAsync(serverKey);

                string urlField = ServerManager.GLOBAL_SERVER_URL;
                string typeField = ServerManager.GLOBAL_SERVER_TYPE;
                string versionField = ServerManager.GLOBAL_SERVER_VERSION;
                string conField = ServerManager.GLOBAL_SERVER_CONNECTIONS;
                string enableField = ServerManager.GLOBAL_SERVER_ENABLE;
                //if (!isExist)
                //{
                    HashEntry[] hashFields = { new HashEntry(urlField, string.Format("{0}:{1}", DNS/*iP*/, port)), new HashEntry(typeField, type), new HashEntry(versionField, version), new HashEntry(conField, connections), new HashEntry(enableField, enable) };
                    await db.HashSetAsync(serverKey, hashFields);
                //}
                //else
                //{
                //    HashEntry[] hashFields = { new HashEntry(conField, connections), new HashEntry(enableField, enable) };
                //    await db.HashSetAsync(serverKey, hashFields);
                //}

                retvalue = await db.KeyExpireAsync(serverKey, GlobalManager.EXPIRE_SERVER_TIMESPAN);

            }
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Error: CheckServerList, error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "redis CheckServerList exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        
        return  retvalue;
    }

    internal List<ServerInfo> GetGameServerList()
    {
        List<ServerInfo> gameServerList = new List<ServerInfo>();
        if (IsConnect())
        {
            try
            {
                //db.get
                IServer server = null;
                System.Net.EndPoint[] endPoints = redis.GetEndPoints();
                server = redis.GetServer(endPoints[0]);

                RedisValue[] hashFields = { ServerManager.GLOBAL_SERVER_URL, ServerManager.GLOBAL_SERVER_TYPE, ServerManager.GLOBAL_SERVER_VERSION, ServerManager.GLOBAL_SERVER_CONNECTIONS, ServerManager.GLOBAL_SERVER_ENABLE };
                RedisValue[] result = null;
                string serverUrl = string.Empty;
                string serverType = string.Empty;
                string version = string.Empty;
                int connections = 0;
                bool enable = false;
                foreach (var key in server.Keys(defaultDB, pattern: "*Server:*"))
                {
                    result = db.HashGet(key, hashFields);
                    if (result.Length == 5)
                    {
                        serverUrl = result[0];
                        serverType = result[1];
                        version = result[2];
                        connections = int.Parse(result[3]);
                        enable = (result[4] == "1") ? true : false;
                        string[] _version = ServerManager.SERVER_VERSION.Split('.');
                        string[] version_ = version.Split('.');
                        if (_version.Length == 4 && version_.Length == 4)
                        {
                            if (serverType == "Game" && _version[1] == version_[1] && connections < ServerManager.SERVER_CONNECTION_LIMIT)
                            {
                                gameServerList.Add(new ServerInfo(serverUrl, serverType, version, connections, enable));
                            }
                        }
                        
                    }
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine("!!!Error: " + e.Message);
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = "redis GetGameServerList exception : (" + e.Message + " )";
                    /*Logger*/
                    GameServer._logger.Error(log);
                }
            }
            
        }
        //return gameServerDic;
        return gameServerList;
    }

    async internal Task<bool> UpdateServerEnable(string DNS, int port, bool enable)
    {
        bool retvalue = false;
        if (IsConnect())
        {
            string serverKey = string.Format("Server:{0}({1})", DNS, port);
            bool isExist = await db.KeyExistsAsync(serverKey);
            string enableField = ServerManager.GLOBAL_SERVER_ENABLE;
            if (isExist)
            {
                HashEntry[] hashFields = { new HashEntry(enableField, enable) };
                await db.HashSetAsync(serverKey, hashFields);
                retvalue = true;
            }

        }
        return retvalue;
    }

    async public Task BCReFreshServerList(string serverSession)
    {
        if (IsConnect())
        {
            await sub.PublishAsync("ReFresh Server List", serverSession);
        }
    }

    //BCReFreshChannelInfo
    async public Task BCReFreshChannelInfo(string serverSession)
    {
        if (IsConnect())
        {
            await sub.PublishAsync("ReFresh Channel List", serverSession);
        }
    }

    async public Task BCReFreshClientVersion(string serverSession)
    {
        if (IsConnect())
        {
            await sub.PublishAsync("ReFresh Version List", serverSession);
        }
    }

    async public Task BCUpdateServerEnable(string serverSession_, String connectDNS_, int port_, bool enable_)
    {
        if (IsConnect())
        {
            string message = string.Format("{0}+{1}+{2}+{3}", serverSession_, connectDNS_, port_.ToString(), enable_.ToString());
            await sub.PublishAsync("Update Enable", message);
        }
    }

    internal List<ServerInfo> GetAllServerInfo()
    {
        List<ServerInfo> serverList = new List<ServerInfo>();
        if (IsConnect())
        {
            try
            {
                //db.get
                IServer server = null;
                System.Net.EndPoint[] endPoints = redis.GetEndPoints();
                server = redis.GetServer(endPoints[0]);

                string urlField = ServerManager.GLOBAL_SERVER_URL;
                string typeField = ServerManager.GLOBAL_SERVER_TYPE;
                string versionField = ServerManager.GLOBAL_SERVER_VERSION;
                string conField = ServerManager.GLOBAL_SERVER_CONNECTIONS;
                string enableField = ServerManager.GLOBAL_SERVER_ENABLE;

                RedisValue[] hashFields = { urlField, typeField, versionField, conField, enableField };
                RedisValue[] result = null;
                string serverUrl = string.Empty;
                string serverType = string.Empty;
                string version = string.Empty;
                int connections = 0;
                bool enable = false;
                foreach (var key in server.Keys(defaultDB, pattern: "*Server:*"))
                {
                    //Console.WriteLine(key);
                    result = db.HashGet(key, hashFields);
                    if (result.Length == 5)
                    {
                        serverUrl = result[0];
                        serverType = result[1];
                        version = result[2];
                        connections = (serverUrl ==/* GameServer._connectIP*/GameServer._DNS + ":" + GameServer._port)? SessionIdManager.Instance.Count : int.Parse(result[3]);
                        enable = (result[4] == "1") ? true : false;


                        serverList.Add(new ServerInfo(serverUrl, serverType, version, connections, enable));
                    }
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine("!!!Error: " + e.Message);
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = "redis GetAllServerInfo exception : (" + e.Message + " )";
                    /*Logger*/
                    GameServer._logger.Error(log);
                }
            }
        }
        return serverList;
    }

    async public Task BCUpdateServerLimit(string serverSession, int count)
    {
        //ServerStateRedisConnect.Instance.UpdateServerLimit(count);
        if (IsConnect())
        {
            string key = ServerManager.GLOBAL_SERVER_NAME;
            bool isExist = await db.KeyExistsAsync(key);
            string stateField = ServerManager.GLOBAL_SERVER_STATE;
            string limitField = ServerManager.GLOBAL_SERVER_LIMIT;
            if (!isExist)
            {
                HashEntry[] hashFields = { new HashEntry(stateField, "stop"), new HashEntry(limitField, count) };
                await db.HashSetAsync(key, hashFields);
            }
            else
            {
                await db.HashSetAsync(key, limitField, count);//true: new, false: update
            }

            string message = string.Format("{0}+{1}", serverSession, count);
            await sub.PublishAsync("Update Limit", message);
        }
    }

    //BCReFreshLocalization
    async public Task BCReFreshLocalization(string serverSession)
    {
        if (IsConnect())
        {
            await sub.PublishAsync("ReFresh Localization", serverSession);
        }
    }

    //BCReFreshNews
    async public Task BCReFreshNews(string serverSession)
    {
        if (IsConnect())
        {
            await sub.PublishAsync("ReFresh News", serverSession);
        }
    }

    //BCReFreshMission
    async public Task BCReFreshMission(string serverSession)
    {
        if (IsConnect())
        {
            await sub.PublishAsync("ReFresh Mission", serverSession);
        }
    }

    //BCNewFriendGift
    async public Task BCNewFriendGift(string userID)
    {
        if (IsConnect())
        {
            await sub.PublishAsync("New Friend Gift", userID);
        }
    }

    //BCAlarmShop
    async public Task BCAlarmShop(string serverSession, string alarmString)
    {
        if (IsConnect())
        {
            string msg = serverSession + "," + alarmString;
            await sub.PublishAsync("Alarm Shop", msg);
        }
    }

    //BCAlarmNews
    async public Task BCAlarmNews(string serverSession, string alarmString)
    {
        if (IsConnect())
        {
            string msg = serverSession + "," + alarmString;
            await sub.PublishAsync("Alarm News", msg);
        }
    }
}
