﻿using System;
using StackExchange.Redis;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using MoonGlow;

public class RedisConnect
{
    public static string connectString = string.Empty;
    public IDatabase db = null;
    public ISubscriber sub = null;
    public static RedisConnect _instance = null;
    public static RedisConnect Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new RedisConnect();
            }
            return _instance;
        }
    }

    public ConnectionMultiplexer redis = null;

    public RedisConnect()
    {
        try
        {
            var options = ConfigurationOptions.Parse(connectString);
            options.AllowAdmin  =  true;
            redis = ConnectionMultiplexer.Connect(options);

            db = redis.GetDatabase();
            sub = redis.GetSubscriber();

            Console.WriteLine("Session Redis connected...");
        }
        catch(Exception e)
        {
            //Console.WriteLine("!!!Session Redis not connected");
            if (GameServer._logger.IsFatalEnabled)
            {
                string log = "UpdateIAPState to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Fatal(log);
            }
        }
    }

    private bool IsConnect()
    {
        bool retvalue = false;
        if (redis != null && db != null && sub != null)
        {
            retvalue = redis.IsConnected;
        }

        return retvalue;
    }

    public void SubscribeChannel()
    {
        if (IsConnect())
        {
            sub.UnsubscribeAsync("messages");
            sub.SubscribeAsync("messages", (channel, message) =>
            {
                //Console.WriteLine("cmd message: " + (string)message);
                PacketsHandlerInner.MSNotiBCServerNoti(string.Empty, (string)message);
            });

            sub.UnsubscribeAsync("freshUserType");
            sub.SubscribeAsync("freshUserType", (channel, message) =>
            {
                //Console.WriteLine("cmd message: " + (string)message);
                string[] value = message.ToString().Split('+');
                if (value.Length == 2)
                {
                    UserType usertType = (UserType)Enum.Parse(typeof(UserType), value[1]);
                    PacketsHandlerInner.MSNotiBCReFreshUserType(value[0], usertType);
                }
            });

            sub.UnsubscribeAsync("forceClose");
            sub.SubscribeAsync("forceClose", (channel, message) =>
            {
                //Console.WriteLine("cmd message: " + (string)message);
                string[] result = message.ToString().Split(',');
                if (result.Length == 2)
                {
                    string gameServerUrl = result[0];
                    string userSession = result[1];
                    PacketsHandlerInner.MSNotiBCForceClose(string.Empty, gameServerUrl, userSession);
                }
            });

            //UpdateUserBan
            sub.UnsubscribeAsync("BanUser");
            sub.SubscribeAsync("BanUser", (channel, message) =>
            {
                string userID = message;
                PacketsHandlerInner.SMNotiBanUser(string.Empty, userID);
            });

        }
    }

    async public Task<bool> AddRedis(string key, string session, string serverUrl)
    {
        //Console.WriteLine("AddRedis ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);

        //return (db != null)? await db.StringSetAsync(key, session):false;
        bool retvalue = false;
        if (IsConnect())
        {
            bool noExist = (await db.KeyExistsAsync(key))? await db.KeyDeleteAsync(key): true;

            if (noExist)
            {
                string field1 = GlobalManager.GLOBAL_USER_SESSION;
                string field2 = GlobalManager.GLOBAL_USER_SERVER;
                HashEntry[] hashFields = { new HashEntry(field1, session), new HashEntry(field2, serverUrl) };
                await db.HashSetAsync(key, hashFields);

                retvalue = await db.KeyExpireAsync(key, GlobalManager.EXPIRE_SESSION_TIMESPAN);
                //retvalue = true;
            }
        }
        return retvalue;
    }

    async public Task<List<string>> GetRedis(string key)
    {
        //Console.WriteLine("GetRedis ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        //string x = string.Empty;
        //if (db != null) x = await db.StringGetAsync(key);

        //return x;

        List<string> retvalue = new List<string>();
        if (IsConnect())
        {
            string field1 = GlobalManager.GLOBAL_USER_SESSION;
            string field2 = GlobalManager.GLOBAL_USER_SERVER;

            RedisValue session = await db.HashGetAsync(key, field1);
            RedisValue serverIP = await db.HashGetAsync(key, field2);

            retvalue.Add(session);
            retvalue.Add(serverIP);
        }

        return retvalue;
    }

    async public Task<bool> RemoveRedis(string key)
    {
        //Console.WriteLine("RemoveRedis ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);

        return (IsConnect()) ? await db.KeyDeleteAsync(key):false;
    }

    async public Task<long> BCServerNoti(string serverSession, RedisValue msg)
    {
        //Console.WriteLine("MessageBC ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);

        long x = 0;
        if (IsConnect())
        {
            x = await sub.PublishAsync("messages", msg);
            Console.WriteLine("clients count: " + x);
        }
        return x;
    }

    async public Task<bool> CheckSession(string userID/*session*/)
    {
        return (IsConnect()) ? await db.KeyExistsAsync(userID) :false;
    }

    async internal Task<long> BCReFreshUserType(string serverSession, UserType userType)
    {
        long x = 0;
        if (IsConnect())
        {
            string message = string.Format("{0}+{1}", serverSession, userType.ToString());
            x = await sub.PublishAsync("freshUserType", message);
            //Console.WriteLine("clients count: " + x);
        }
        return x;
    }

    async internal Task<bool> ExpireRedis(string key)
    {
        bool retvalue = false;
        if (IsConnect())
        {
            retvalue = await db.KeyExpireAsync(key, GlobalManager.EXPIRE_SESSION_TIMESPAN);
        }
        return retvalue;
    }

    async internal Task BCForceClose(string userID)
    {
        if (IsConnect())
        {
            if (await db.KeyExistsAsync(userID))
            {
                string userSession = await db.HashGetAsync(userID, GlobalManager.GLOBAL_USER_SESSION);
                string gameServerUrl = await db.HashGetAsync(userID, GlobalManager.GLOBAL_USER_SERVER);

                string message = gameServerUrl + "," + userSession;
                await sub.PublishAsync("forceClose", message);

                //delete session
                await db.KeyDeleteAsync(userID);
                //Console.WriteLine("clients count: " + x);
            }
        }
    }

    async internal Task BCBanUser(string userID)
    {
        if (IsConnect())
        {
            if (await db.KeyExistsAsync(userID))
            {
                await sub.PublishAsync("BanUser", userID);
            }
        }
    }
}
