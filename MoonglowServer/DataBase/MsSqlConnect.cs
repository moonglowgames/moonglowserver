﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MoonGlow.Data;
using System.Globalization;
using MoonGlow;
using System.Threading.Tasks;

public class MSSqlConnect
{
    public static string connectString = "";
    public bool isConnect = false;
    private SqlConnection conn = null;
    //private static MSSqlConnect _instance = null;
    //public static MSSqlConnect Instance
    //{
    //    get
    //    {
    //        if (_instance == null)
    //        {
    //            _instance = new MSSqlConnect();
    //        }
    //        return _instance;
    //    }
    //}

    public MSSqlConnect()
    {
        conn = new SqlConnection(connectString);
        try
        {
            conn.Open();
            isConnect = true;
            Console.WriteLine("\nMSSQL Connected...");
        }
        catch
        {
            Console.WriteLine("\nMSSQL not Connect");
        }
        finally
        {
            conn.Close();
        }
    }

    internal void GetAllDevList()
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }
        //Console.WriteLine("GetOperatorList ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        DeveloperManager._developerList = new List<string>();
        DeveloperManager._developerManagerList = new List<string>();
        DeveloperManager._operatorList = new List<string>();
        DeveloperManager._operatorManagerList = new List<string>();
        try
        {
            //conn open...
            conn.Open();

            string sql = "developer_get_all";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[developer]");

            UserType userType = 0;
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                userType = (UserType)(int.Parse(r[2].ToString()));
                //r[0]:idx, r[1]:account, r[2]:user_type
                if (userType == UserType.Developer)
                {
                    DeveloperManager._developerList.Add(r[1].ToString());
                }
                if (userType == UserType.DeveloperManager)
                {
                    DeveloperManager._developerManagerList.Add(r[1].ToString());
                }
                if (userType == UserType.Operator)
                {
                    DeveloperManager._operatorList.Add(r[1].ToString());
                }
                if (userType == UserType.OperatorManager)
                {
                    DeveloperManager._operatorManagerList.Add(r[1].ToString());
                }
            }

            Console.WriteLine("*** DB: Get All Dev. List from MSSQL Successfully!");
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Get All Dev. List from SQL Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "Get All Dev. List from SQL Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
    }

    internal void GetOperatorList()
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }
        
        //Console.WriteLine("GetOperatorList ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        DeveloperManager._operatorList = new List<string>();
        try
        {
            //conn open...
            conn.Open();

            string sql = "developer_get";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@userType_", (int)UserType.Operator);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[developer]");

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                //r[0]:account
                DeveloperManager._operatorList.Add(r[0].ToString());
            }

            Console.WriteLine("*** DB: Get Operator List from MSSQL Successfully!");
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Get Operator List from MSSQL Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "Get Operator List from MSSQL Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
    }

    internal void GetOperatorManagerList()
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        //Console.WriteLine("GetOperatorManagerList ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        DeveloperManager._operatorManagerList = new List<string>();

        try
        {
            //conn open...
            conn.Open();

            string sql = "developer_get";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@userType_", (int)UserType.OperatorManager);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[developer]");

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                //r[0]:account
                DeveloperManager._operatorManagerList.Add(r[0].ToString());
            }

            Console.WriteLine("*** DB: Get OperatorManager List from MSSQL Successfully!");
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Get OperatorManager List from MSSQL: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "Get OperatorManager List from MSSQL Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
    }

    internal void GetDeveloperManagerList()
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        //Console.WriteLine("GetDeveloperManagerList ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        DeveloperManager._developerManagerList = new List<string>();
        try
        {
            //conn open...
            conn.Open();

            string sql = "developer_get";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@userType_", (int)UserType.DeveloperManager);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[developer]");

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                //r[0]:account
                DeveloperManager._developerManagerList.Add(r[0].ToString());
            }

            Console.WriteLine("*** DB: Get DeveloperManager List from MSSQL Successfully!");
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Get DeveloperManager List from MSSQL Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "Get DeveloperManager List from MSSQL Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
    }

    internal /*async*/ void GetDeveloperList()
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        //Console.WriteLine("GetDeveloperList ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        DeveloperManager._developerList = new List<string>();
        try
        {
            //conn open...
            //await Task.Run(() =>
            //{
                conn.Open();

                string sql = "developer_get";
                SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@userType_", (int)UserType.Developer);
                DataSet ds = new DataSet();
                adapter.Fill(ds, "[developer]");
            

                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    //r[0]:account
                    DeveloperManager._developerList.Add(r[0].ToString());
                }

            //});

            Console.WriteLine("*** DB: Get Developer List from MSSQL Successfully!");
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Get Developer List from MSSQL Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "Get Developer List from MSSQL Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
    }

    internal void RegisterCheck(string accountType, string account, string nick, out LoginState state)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");
            state = LoginState.Fail;
            //reconnect..
            return;
        }

        //Console.WriteLine("CheckRegisterCheck ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        state = LoginState.Unknown;
        try
        {
            //conn open...
            conn.Open();

            string sql = "user_register_check";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@accountType_", accountType);
            cmd.Parameters.AddWithValue("@account_", account);
            int idCount = Convert.ToInt32(cmd.ExecuteScalar());

            string sql2 = "user_check_nick";
            SqlCommand cmd2 = new SqlCommand(sql2, conn);
            cmd2.CommandType = CommandType.StoredProcedure;
            cmd2.Parameters.AddWithValue("@nick_", nick);
            int nickCount = Convert.ToInt32(cmd2.ExecuteScalar());

            
            if (idCount == 0 && nickCount == 0)
            {
                state = (accountType == AccountType.Guest.ToString()) ? LoginState.GuestOK : LoginState.OK;
            }
            else if (idCount > 0 && nickCount == 0) { state = LoginState.IDExist; }
            else if (idCount == 0 && nickCount > 0) { state = LoginState.NickExist; }
            else if (idCount > 0 && nickCount > 0) { state = LoginState.IDAndNickExist; }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

    internal bool Register(string userID, string accountType, string account, string userPW, string nick, int faceIdx, string deviceID)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        //Console.WriteLine("RegisterDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        if (account != null || userPW != null)
        {
            //int faceIdx = GlobalManager.DEFAULT_FACE_IDX;

            //UserData userData = null;
            //GlobalOperateManager.Instance.InitUserData(userID, nick, userPW, out userData);

            MailData mailData = null;
            MailData mailData2 = null;
            GlobalManager.Instance.InitMailData(/*nick,*/ out mailData, out mailData2);

            //string userDataString = JsonMapper.ToJson(userData);
            try
            {
                conn.Open();
                string sql = "user_register";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userID_", userID);
                cmd.Parameters.AddWithValue("@accountType_", accountType);
                cmd.Parameters.AddWithValue("@account_", account);
                cmd.Parameters.AddWithValue("@password_", userPW);
                cmd.Parameters.AddWithValue("@nick_", nick);
                cmd.Parameters.AddWithValue("@faceIdx_", faceIdx);
                cmd.Parameters.AddWithValue("@deviceID_", deviceID);
                //cmd.Parameters.AddWithValue("@userInfo_", userDataString);
                cmd.ExecuteNonQuery();

                string sql2 = "mailbox_new";
                SqlCommand cmd2 = new SqlCommand(sql2, conn);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@userID_", userID);
                //cmd2.Parameters.AddWithValue("@userNick_", mailData.nick);
                cmd2.Parameters.AddWithValue("@senderID_", mailData.sendID);
                //cmd2.Parameters.AddWithValue("@senderNick_", mailData.senderNick);
                cmd2.Parameters.AddWithValue("@subject_", mailData.subject);
                cmd2.Parameters.AddWithValue("@content_", mailData.content);
                cmd2.Parameters.AddWithValue("@itemIdx1_", mailData.itemIdx1);
                cmd2.Parameters.AddWithValue("@itemCount1_", mailData.itemCount1);
                cmd2.ExecuteNonQuery();

                string sql3 = "mailbox_new";
                SqlCommand cmd3 = new SqlCommand(sql3, conn);
                cmd3.CommandType = CommandType.StoredProcedure;
                cmd3.Parameters.AddWithValue("@userID_", userID);
                //cmd3.Parameters.AddWithValue("@userNick_", mailData2.nick);
                cmd3.Parameters.AddWithValue("@senderID_", mailData2.sendID);
                //cmd3.Parameters.AddWithValue("@senderNick_", mailData2.senderNick);
                cmd3.Parameters.AddWithValue("@subject_", mailData2.subject);
                cmd3.Parameters.AddWithValue("@content_", mailData2.content);
                cmd3.Parameters.AddWithValue("@itemIdx1_", mailData2.itemIdx1);
                cmd3.Parameters.AddWithValue("@itemCount1_", mailData2.itemCount1);
                cmd3.ExecuteNonQuery();

                retvalue = true;
            }
            catch (Exception e)
            {
                //Console.WriteLine("!!!Register to DB Error: " + e.Message);
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = "Register to DB Exception : (" + e.Message + " )";
                    /*Logger*/
                    GameServer._logger.Error(log);
                }
            }
            finally
            {
                conn.Close();
            }
        }
        return retvalue;
    }

    internal void LoginCheck(string accountType, string account, string password, out string userID, out string deviceID, out LoginState state)
    {
        deviceID = string.Empty;
        userID = string.Empty;
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");
            state = LoginState.Fail;
            //reconnect..
            return;
        }

        //Console.WriteLine("UserIDCheck ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        state = LoginState.Unknown;
        try
        {
            //conn open...
            conn.Open();

            string sql = "user_login_check";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@accountType_", accountType);
            adapter.SelectCommand.Parameters.AddWithValue("@account_", account);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[user]");

            int idCount = ds.Tables[0].Rows.Count; //Convert.ToInt32(cmd.ExecuteScalar());
            if (idCount == 0) { state = LoginState.NoID; }
            else
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    //r[0]: user_id, r[1]:pw, r[2]: unique_id r[3]: ban
                    if (r[3].ToString() == "1")
                    {
                        Console.WriteLine(r[3]);
                        state = LoginState.Ban;
                    }
                    else if (r[1].ToString() != password)
                    {
                        state = LoginState.MissPassword;
                    }
                    else
                    {
                        state = LoginState.OK;
                    }
                    userID = r[0].ToString();
                    deviceID = r[2].ToString();
                }
            }

            
            //else { state = LoginState.OK; }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

    public bool UpdateUserData(string id_, string nick_, int level_, int faceIdx_, /*float*/int rankPoint_, string lastLoginDate_/*, string jsonString_*/)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }
        if (id_ == string.Empty)
        {
            Console.WriteLine("UserID is Empty!");
            return false ;
        }
        //Console.WriteLine("UpdateUserInfoDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);

        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "user_update";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", id_);
            cmd.Parameters.AddWithValue("@nick_", nick_);
            cmd.Parameters.AddWithValue("@level_", level_);
            cmd.Parameters.AddWithValue("@faceIdx_", faceIdx_);
            cmd.Parameters.AddWithValue("@rankPoint_", rankPoint_);
            cmd.Parameters.AddWithValue("@lastLoginDate_", lastLoginDate_);
            cmd.Parameters.AddWithValue("@lastLogoutDate_", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            //cmd.Parameters.AddWithValue("@userInfo_", jsonString_);
            cmd.ExecuteNonQuery();

            Console.WriteLine("SQL_STATE:" + cmd.Connection.State.ToString());
            Console.WriteLine("UeserInfo updated successfully.");

            retvalue = true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
            Console.WriteLine("UeserInfo updated failed.");
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    public bool NewMail(string userID_, MailData mailData)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        //Console.WriteLine("UpdateMailDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "mailbox_new";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID_);
            //cmd.Parameters.AddWithValue("@userNick_", mailData.nick);
            cmd.Parameters.AddWithValue("@senderID_", mailData.sendID);
            //cmd.Parameters.AddWithValue("@senderNick_", mailData.senderNick);
            cmd.Parameters.AddWithValue("@subject_", mailData.subject);
            cmd.Parameters.AddWithValue("@content_", mailData.content);
            cmd.Parameters.AddWithValue("@itemIdx1_", mailData.itemIdx1);
            cmd.Parameters.AddWithValue("@itemCount1_", mailData.itemCount1);
            cmd.ExecuteNonQuery();
            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Add Email to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "Add Email to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    public void GetMailList(string userID, out List<MailData> mailList)
    {
        mailList = new List<MailData>();
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        //Console.WriteLine("GetMailDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);

        try
        {
            //conn open...
            conn.Open();
            string sql = "mailbox_get";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@userID_", userID);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "mailbox");

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                /* r[0]: idx,             r[1]: user_id,         r[2]: sender_id,          
                 * r[3]: subject,         r[4]: content,         r[5]: item_idx_1,     
                 * r[6]: item_count_1,    r[7]: item_idx_2,      r[8]: item_count_2,  
                 * r[9]: item_idx_3,      r[10]: item_count_3,   r[11]: item_got,      
                 * r[12]: send_date,      r[13]: item_got_date,  r[14]: deleted
                 */
                if (int.Parse(r[14].ToString()) == 0)
                {
                    MailData mailData = new MailData();
                    mailData.idx = int.Parse(r[0].ToString());
                    //mailData.nick = r[2].ToString();
                    mailData.sendID = r[2].ToString();
                    //mailData.senderNick = r[4].ToString();
                    mailData.subject = r[3].ToString();
                    mailData.content = r[4].ToString();
                    mailData.itemIdx1 = int.Parse(r[5].ToString());
                    mailData.itemCount1 = int.Parse(r[6].ToString());
                    //mailData.itemIdx2 = int.Parse(r[9].ToString());
                    //mailData.itemCount2 = int.Parse(r[10].ToString());
                    ////mailData.itemIdx3 = int.Parse(r[11].ToString());
                    //mailData.itemCount3 = int.Parse(r[12].ToString());
                    mailData.itemGot = (int.Parse(r[11].ToString()) == 0) ? false : true;
                    //mailData.sendDate = Convert.ToDateTime(r[14].ToString());
                    if(!string.IsNullOrEmpty(r[13].ToString()))
                        mailData.itemGotDate = Convert.ToDateTime(r[13].ToString());
                    //mailData.del = (int.Parse(r[16].ToString()) == 0) ? false : true;
                    mailData.del = false;
                    mailList.Add(mailData);
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

    public bool MailUpdate(MailData mailData)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }
        //Console.WriteLine("UpdateMailDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "mailbox_update";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idx_", mailData.idx);
            cmd.Parameters.AddWithValue("@itemGot_", mailData.itemGot);
            cmd.Parameters.AddWithValue("@itemGotDate_", mailData.itemGotDate);
            cmd.Parameters.AddWithValue("@del_", mailData.del);
            cmd.ExecuteNonQuery();

            Console.WriteLine("SQL_STATE:" + cmd.Connection.State.ToString());
            Console.WriteLine("Mail updated successfully.");
            retvalue = true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
            Console.WriteLine("Mail updated failed.");
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    public void GetMyFriends(string userID, out List<FriendData> myFriends)
    {
        myFriends = new List<FriendData>();
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect...
            return;
        }

        //Console.WriteLine("GetMyFriendDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        try
        {
            //conn open...
            conn.Open();
            string sql = "friend_get";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@userID_", userID);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "friend");

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                // r[0]: friend_id,       r[1]: friend_nick,     r[2]: friend_face,        
                // r[3]: friend_level,    r[4]: friend_type,     r[5]: friend_last_login_date,    
                // r[6]: request_date,    r[7]: item_send_date,  r[8]: item_get_enable
                // r[9]: win,             r[10]: lose            r[11]: draw
                if ((FriendType)int.Parse(r[4].ToString()) == FriendType.Friend)
                {
                    FriendData friendData = new FriendData();
                    friendData.friendID = r[0].ToString();
                    friendData.friendNick = r[1].ToString();
                    friendData.faceIdx = int.Parse(r[2].ToString());
                    friendData.level = int.Parse(r[3].ToString());
                    friendData.friendType = (FriendType)int.Parse(r[4].ToString());
                    friendData.friendLastLoginDate = r[5].ToString();
                    friendData.itemSendDate = r[7].ToString();
                    friendData.enableGetItem = (int.Parse(r[8].ToString()) == 0) ? false : true;
                    friendData.winCount = int.Parse(r[9].ToString());
                    friendData.loseCount = int.Parse(r[10].ToString());
                    friendData.drawCount = int.Parse(r[11].ToString());

                    myFriends.Add(friendData);
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

    public void GetRequestFriends(string userID, out List<FriendData> requestFriends, out List<FriendData> delFriends)
    {
        //Console.WriteLine("GetRequestFriendDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);

        requestFriends = new List<FriendData>();
        delFriends = new List<FriendData>();
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        try
        {
            //conn open...
            conn.Open();
            string sql = "friend_get";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@userID_", userID);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "friend");

            TimeSpan intervalSpan = new TimeSpan(3, 0, 0, 0);
            DateTime requestDate = new DateTime();
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                // r[0]: friend_id,       r[1]: friend_nick,     r[2]: friend_face,        
                // r[3]: friend_level,    r[4]: friend_type,     r[5]: friend_last_login_date,    
                // r[6]: request_date,    r[7]: item_send_date,  r[8]: item_get_enable

                if ((FriendType)int.Parse(r[4].ToString()) == FriendType.Accepting || (FriendType)int.Parse(r[4].ToString()) == FriendType.Requesting)
                {
                    FriendData friendData = new FriendData();
                    friendData.friendID = r[0].ToString();
                    friendData.friendNick = r[1].ToString();
                    friendData.faceIdx = int.Parse(r[2].ToString());
                    friendData.level = int.Parse(r[3].ToString());
                    friendData.friendType = (FriendType)int.Parse(r[4].ToString());
                    friendData.friendLastLoginDate = r[5].ToString();
                    friendData.itemSendDate = r[7].ToString();
                    friendData.enableGetItem = (int.Parse(r[8].ToString()) == 0) ? false : true;

                    if (GlobalManager.Instance.ConvertToDateTime(r[6].ToString(), out requestDate))
                    {
                        if (friendData.friendType == FriendType.Requesting)
                        {
                            if (DateTime.Now >= requestDate + intervalSpan)
                            {
                                //delete
                                delFriends.Add(friendData);
                            }
                            else
                            {
                                //add
                                requestFriends.Add(friendData);
                            }
                        }
                        if (friendData.friendType == FriendType.Accepting)
                        {
                            //add
                            requestFriends.Add(friendData);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

    internal bool UpdateUserBan(string userNick, bool ban, out string userID)
    {
        userID = string.Empty;
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }
        //Console.WriteLine("UpdateMailDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "user_update_ban";
            //SqlCommand cmd = new SqlCommand(sql, conn);
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@nick_", userNick);
            //cmd.Parameters.AddWithValue("@ban_", ban);
            //cmd.ExecuteNonQuery();
            //int nickCount = Convert.ToInt32(cmd.ExecuteScalar());

            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@nick_", userNick);
            adapter.SelectCommand.Parameters.AddWithValue("@ban_", ban);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[user]");

            //if (nickCount == 0)  isNickExist = false; //nick exist
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                userID = r[0].ToString();
            }

            retvalue = true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
            Console.WriteLine("User ban updated failed.");
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    public bool GetFriendCount(string userID, out int count)
    {
        count = 0;
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        bool retvalue = false;
        //Console.WriteLine("GetFriendCountDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);

        try
        {
            //conn open...
            conn.Open();
            string sql = "friend_get_count";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            count = Convert.ToInt32(cmd.ExecuteScalar());

            retvalue = true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    public bool NewFriend(string userID, /*string userNick,*/ string friendID/*, string friendNick*/)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }
        
        //Console.WriteLine("AddFriendDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "friend_new";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            cmd.Parameters.AddWithValue("@friendID_", friendID);
            cmd.Parameters.AddWithValue("@friendType_", FriendType.Requesting);
            cmd.ExecuteNonQuery();

            SqlCommand cmd2 = new SqlCommand(sql, conn);
            cmd2.CommandType = CommandType.StoredProcedure;
            cmd2.Parameters.AddWithValue("@userID_", friendID);
            cmd2.Parameters.AddWithValue("@friendID_", userID);
            cmd2.Parameters.AddWithValue("@friendType_", FriendType.Accepting);
            cmd2.ExecuteNonQuery();
            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Add Friend to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "Add Friend to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal bool UpdateFriendType(string userID, string friendID)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }
        
        //Console.WriteLine("UpdateFriendDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "friend_accept";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            cmd.Parameters.AddWithValue("@friendID_", friendID);
            cmd.Parameters.AddWithValue("@itemIdx_", (int)ElementType.Stamina);
            cmd.Parameters.AddWithValue("@itemCount_", GlobalManager.GIFT_STAMINA_COUNT);
            //cmd.Parameters.AddWithValue("@friendType_", FriendType.Friend);
            cmd.ExecuteNonQuery();

            SqlCommand cmd2 = new SqlCommand(sql, conn);
            cmd2.CommandType = CommandType.StoredProcedure;
            cmd2.Parameters.AddWithValue("@userID_", friendID);
            cmd2.Parameters.AddWithValue("@friendID_", userID);
            cmd2.Parameters.AddWithValue("@itemIdx_", (int)ElementType.Stamina);
            cmd2.Parameters.AddWithValue("@itemCount_", GlobalManager.GIFT_STAMINA_COUNT);
            //cmd2.Parameters.AddWithValue("@friendType_", FriendType.Friend);
            cmd2.ExecuteNonQuery();
            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Add Friend to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "Add Friend to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal bool DeleteFriend(string userID, string friendID)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "friend_del";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            cmd.Parameters.AddWithValue("@friendID_", friendID);
            cmd.ExecuteNonQuery();

            SqlCommand cmd2 = new SqlCommand(sql, conn);
            cmd2.CommandType = CommandType.StoredProcedure;
            cmd2.Parameters.AddWithValue("@userID_", friendID);
            cmd2.Parameters.AddWithValue("@friendID_", userID);
            cmd2.ExecuteNonQuery();
            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Delete Friend to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "Delete Friend from DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal bool UpdateGiftSendDate(string userID, string friendID, DateTime itemSendDate)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "friend_update_date";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            cmd.Parameters.AddWithValue("@friendID_", friendID);
            cmd.Parameters.AddWithValue("@itemSendDate_", itemSendDate);
            cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Update Friend to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "UpdateGiftSendDate to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal bool UpdateFriendSendGift(string userID, string friendID, DateTime sendDate)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "friend_update_date";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            cmd.Parameters.AddWithValue("@friendID_", friendID);
            cmd.Parameters.AddWithValue("@itemSendDate_", sendDate);
            cmd.ExecuteNonQuery();

            string sql2 = "friend_update_enable";
            SqlCommand cmd2 = new SqlCommand(sql2, conn);
            cmd2.CommandType = CommandType.StoredProcedure;
            cmd2.Parameters.AddWithValue("@userID_", friendID);
            cmd2.Parameters.AddWithValue("@friendID_", userID);
            cmd2.Parameters.AddWithValue("@enableGetItem_", true);
            cmd2.ExecuteNonQuery();

            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Update Friend to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "UpdateFriendSendGift to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal bool UpdateFriendGetGift(string userID, string friendID)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "friend_update_enable";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            cmd.Parameters.AddWithValue("@friendID_", friendID);
            cmd.Parameters.AddWithValue("@enableGetItem_", false);
            cmd.ExecuteNonQuery();
            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Update Friend to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "UpdateFriendGetGift to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal void GetMyRankHis(string userID, out List<PCKRankInfo> rankHisList)
    {
        //Console.WriteLine("GetMyRank ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);

        rankHisList = new List<PCKRankInfo>();
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        try
        {
            //conn open...
            conn.Open();
            string sql = "rank_his_get";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@userID_", userID);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "rank_his");

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                /* r[0]: idx,         r[1]: user_id,       r[2]: chanel_name,    
                *  r[3]: rank_count,  r[4]: rank_percent,  r[5]: rank_point,  
                *  r[6]: rank_win     r[7]: rank_lose,     r[8]: rank_draw     
                *  r[9]: item_exist,  r[10]: item_got,     r[11]: item_got_date 
                */
                string channelName = r[2].ToString();
                int rankCount = int.Parse(r[3].ToString());
                int rankPercent = int.Parse(r[4].ToString());
                int rankPoint = int.Parse(r[5].ToString());
                int rankWin = int.Parse(r[6].ToString());
                int rankLose = int.Parse(r[7].ToString());
                int rankDraw = int.Parse(r[8].ToString());
                int totalRankFight = rankWin + rankLose /*+ rankDraw*/;
                decimal winRate = 0;
                if (totalRankFight != 0)  winRate = (decimal)rankWin / totalRankFight;
                string winRateStr = winRate.ToString("#0.##%", CultureInfo.InvariantCulture);

                bool itemExist = (int.Parse(r[9].ToString()) == 0) ? false : true;
                bool itemGot = (int.Parse(r[10].ToString()) == 0) ? false : true;
                rankHisList.Add(new PCKRankInfo(channelName, rankCount, rankPercent, rankPoint, rankWin, rankLose, winRateStr, itemExist, itemGot));
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

    internal bool AddNewRankHis(string userID, string channelName, int rankCount, int rankPercent, /*float*/int rankPoint, int rankWin, int rankLose, int rankDraw, bool itemExist)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }
        
        //Console.WriteLine("AddNewRankHisDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "rank_his_new";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            cmd.Parameters.AddWithValue("@channelName_", channelName);
            cmd.Parameters.AddWithValue("@rankCount_", rankCount);
            cmd.Parameters.AddWithValue("@rankPercent_", rankPercent);
            cmd.Parameters.AddWithValue("@rankPoint_", rankPoint);
            cmd.Parameters.AddWithValue("@rankWin_", rankWin);
            cmd.Parameters.AddWithValue("@rankLose_", rankLose);
            cmd.Parameters.AddWithValue("@rankDraw_", rankDraw);
            cmd.Parameters.AddWithValue("@itemExist_", itemExist);

            cmd.ExecuteNonQuery();

            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Add NewRankHis to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "AddNewRankHis to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal bool UpdateRank(string userID, string channelName, bool itemGot, DateTime itemGotDate)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        //Console.WriteLine("UpdateRankHisDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "rank_his_update";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            cmd.Parameters.AddWithValue("@channelName_", channelName);
            cmd.Parameters.AddWithValue("@itemGot_", itemGot);
            cmd.Parameters.AddWithValue("@itemGotDate_", itemGotDate);

            cmd.ExecuteNonQuery();

            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Update RankHis to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "UpdateRank to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal bool AddChannelUser(string channelName, string userID)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        //Console.WriteLine("AddChannelUserDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        try
        {
            //conn open...
            conn.Open();
            string sql = "channel_user_new";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            cmd.Parameters.AddWithValue("@channelName_", channelName);

            cmd.ExecuteNonQuery();

            retvalue = true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal bool UpdateChannelInfo(int channelIdx, bool active)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }
        
        //Console.WriteLine("UpdateChannelInfoDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "channel_info_update";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@channelIdx_", channelIdx);
            cmd.Parameters.AddWithValue("@active_", active);

            cmd.ExecuteNonQuery();

            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Update ChannelInfo to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "UpdateChannelInfo to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal bool RemoveChannelUser(string channelName)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "channel_user_del";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@channelName_", channelName);
            cmd.ExecuteNonQuery();

            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Remove ChannelUser from DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "RemoveChannelUser from DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal bool UpdateChannelInfoDate(int channelIdx, DateTime dateTime)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        //Console.WriteLine("UpdateChannelInfoDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "channel_info_update_date";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@channelIdx_", channelIdx);
            cmd.Parameters.AddWithValue("@endDate_", dateTime);

            cmd.ExecuteNonQuery();

            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Update ChannelInfo to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "UpdateChannelInfoDate to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal void GetChannelUserList(string channelName, List<ChanelUser> channelUserList)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        //Console.WriteLine("GetMyRank ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);

        try
        {
            //conn open...
            conn.Open();
            string sql = "channel_user_get";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@channelName_", channelName);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "channel_user");

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                //r[0]: chanel_name, r[1]: user_id, r[2]: rank_point
                //Console.WriteLine(r[0] + " " + r[1] + " " + r[2]);

                ChanelUser channelUser = new ChanelUser(r[0].ToString(), r[1].ToString(), int.Parse(r[2].ToString()));

                channelUserList.Add(channelUser);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

    internal void ReadChannelRewardInfo()
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        //Console.WriteLine("ReadChannelRewardInfo ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);

        try
        {
            //conn open...
            conn.Open();
            string sql = "channel_get_reward";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            adapter.Fill(ds, "channel_reward");

            ChannelRewardManager._items = new List<ChanelRewardInfo>();
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                //r[0]:idx,          r[1]:rank_idx,      r[2]:rank,           r[3]:rank_type,    
                //r[4]:item_idx1,    r[5]:item_count1,   r[6]:item_idx2,      r[7]:item_count2,
                //r[8]:item_idx3,    r[9]:item_count3,   r[10]:active 
                bool active = int.Parse(r[10].ToString()) == 1 ? true : false;

                if (active == true)
                {
                    ChanelRewardInfo info = new ChanelRewardInfo();
                    info.rankIdx = int.Parse(r[1].ToString());
                    info.rank = int.Parse(r[2].ToString());
                    info.rankType = (RankType)Enum.Parse(typeof(RankType), r[3].ToString());
                    info.itemIdx1 = int.Parse(r[4].ToString());
                    info.itemCount1 = int.Parse(r[5].ToString());
                    info.itemIdx2 = int.Parse(r[6].ToString());
                    info.itemCount2 = int.Parse(r[7].ToString());
                    info.itemIdx3 = int.Parse(r[8].ToString());
                    info.itemCount3 = int.Parse(r[9].ToString());

                    ChannelRewardManager._items.Add(info);
                }
            }
            Console.WriteLine("*** DB: Read ChannelRewardInfo from MSSQL Successfully!");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

    internal void ReadChannelInfo()
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        //Console.WriteLine("ReadChannelInfo ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        try
        {
            //conn open...
            conn.Open();
            string sql = "channel_get_info";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            adapter.Fill(ds, "channel_info");

            ChannelManager._items = new List<ChanelInfo>();
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                //r[0]:idx,         r[1]:chanel_idx,       r[2]:type,           r[3]:name,        r[4]:description, 
                //r[5]:min_level,   r[6]:max_level,        r[7]:start_date      r[8]:end_date,    r[9]：max_count
                //r[10]:active, 
                bool active = int.Parse(r[10].ToString()) == 1 ? true : false;

                if (active == true)
                {
                    ChanelInfo info = new ChanelInfo();
                    info.chanelIdx = int.Parse(r[1].ToString());
                    info.chanelType = (ChanelType)Enum.Parse(typeof(ChanelType), r[2].ToString());
                    info.name = r[3].ToString();
                    info.description = r[4].ToString();
                    info.minLevel = int.Parse(r[5].ToString());
                    info.maxLevel = int.Parse(r[6].ToString());
                    info.startDate = r[7].ToString();
                    info.endDate = r[8].ToString();
                    info.maxCount = int.Parse(r[9].ToString());

                    ChannelManager._items.Add(info);
                }
            }

            Console.WriteLine("*** DB: Read ChannelInfo from MSSQL Successfully!");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

    internal bool SetDeveloper(string userNick, UserType userType, out string account)
    {
        account = string.Empty;
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        bool retvalue = false;
        
        try
        {
            //conn open...
            conn.Open();

            string sql = "user_get_account_with_nick";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@nick_", userNick);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[user]");
            //int nickCount = Convert.ToInt32(cmd.ExecuteScalar());

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                //r[0]: account
                account = r[0].ToString();
            }

            if (!string.IsNullOrEmpty(account))
            {
                string sql2 = "developer_update";
                //SqlCommand cmd2 = new SqlCommand(sql2, conn);
                //cmd2.CommandType = CommandType.StoredProcedure;
                //cmd2.Parameters.AddWithValue("@nick_", userNick);
                SqlDataAdapter adapter2 = new SqlDataAdapter(sql2, conn);
                adapter2.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter2.SelectCommand.Parameters.AddWithValue("@userType_", (int)userType);
                adapter2.SelectCommand.Parameters.AddWithValue("@account_", account);
                DataSet ds2 = new DataSet();
                adapter2.Fill(ds2, "[developer]");
                //if (Convert.ToInt32(cmd2.ExecuteScalar()) == 0)
                //{
                //    //new 

                //}
                //else
                //{
                //    //update
                //}

                bool exist = false;
                foreach (DataRow r in ds2.Tables[0].Rows)
                {
                    //r[0]: idx
                    Console.WriteLine("index is: " + r[0]);
                    exist = !string.IsNullOrEmpty(r[0].ToString());
                }

                if (!exist)
                {
                    string sql3 = "developer_new";
                    SqlCommand cmd3 = new SqlCommand(sql3, conn);
                    cmd3.CommandType = CommandType.StoredProcedure;
                    cmd3.Parameters.AddWithValue("@account_", account);
                    cmd3.Parameters.AddWithValue("@userType_", (int)userType);
                    cmd3.ExecuteNonQuery();
                }

                retvalue = true;
            }
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Set Developer to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "SetDeveloper to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }

        return retvalue;
    }

    internal void GetClientVersions()
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        try
        {
            //conn open...
            conn.Open();

            string sql = "version_get";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@server_", ServerManager.SERVER_VERSION);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[version]");
            //int nickCount = Convert.ToInt32(cmd.ExecuteScalar());

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                //r[0]: ios,  r[1]: android, r[2]: windows,  r[3]: web,  r[4]: mac,  r[5]: pc
                ServerManager.CURRENT_IOS_VERSION = r[0].ToString();
                ServerManager.CURRENT_ANDROID_VERSION = r[1].ToString();
                ServerManager.CURRENT_WINDOWS_VERSION = r[2].ToString();
                ServerManager.CURRENT_WEB_VERSION = r[3].ToString();
                ServerManager.CURRENT_MAC_VERSION = r[4].ToString();
                ServerManager.CURRENT_PC_VERSION = r[5].ToString();

                Console.WriteLine("*** DB: Get Client Versions from MSSQL successfully!");
            }
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Get Client Versions from DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "GetClientVersions from DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
    }

    internal void UpdatedeviceID(string userID, /*string account, */string deviceID)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        //Console.WriteLine("UpdateRankHisDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        try
        {
            conn.Open();
            string sql = "user_update_deviceID";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            //cmd.Parameters.AddWithValue("@account_", account);//UPDATE ACCOUNT ALSO BECAUSE OF GUEST (ACCOUNT = DEVICE)
            cmd.Parameters.AddWithValue("@deviceID_", deviceID);

            cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Update RankHis to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "UpdatedeviceID to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
    }

    internal void UpdatePassword(string account, string oldPassword, string newPassword, bool isFind, out LoginState state, out string userID)
    {
        state = LoginState.Unknown;
        userID = string.Empty;
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        try
        {
            conn.Open();
            
            string sql = "user_login_check";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@accountType_", AccountType.Email.ToString());
            adapter.SelectCommand.Parameters.AddWithValue("@account_", account);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[user]");
            int idCount = ds.Tables[0].Rows.Count; //Convert.ToInt32(cmd.ExecuteScalar());
            if (idCount == 0)  state = LoginState.NoID;
            else
            {
                if (!isFind)
                {
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            //r[0]: user_id,  r[1]: pw, r[2]: device_id
                            if (r[1].ToString() != oldPassword) { state = LoginState.MissPassword;}
                            userID = r[0].ToString();
                        }
                    }

                }
            }

            if (state == LoginState.Unknown)
            {
                string sql2 = "user_update_pw";
                SqlCommand cmd = new SqlCommand(sql2, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@account_", account);
                cmd.Parameters.AddWithValue("@pw_", newPassword);

                cmd.ExecuteNonQuery();

                state = LoginState.OK;
            }
           
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Update password to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "UpdatePassword to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
    }

    internal bool UpdateFriendFight(string userID, string friendID, string result)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        bool retvalue = false;
        //Console.WriteLine("UpdateRankHisDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        try
        {
            conn.Open();
            string sql = "friend_update_fight";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            cmd.Parameters.AddWithValue("@friendID_", friendID);
            cmd.Parameters.AddWithValue("@result_", result);

            cmd.ExecuteNonQuery();
            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Update Friend Fight Result to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "UpdateFriendFight to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal void ChangeNick(string userID, string nick, out ChangeNickResult result)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");
            result = ChangeNickResult.Fail;
            //reconnect..
            return;
        }

        result = ChangeNickResult.Unknown;
        try
        {
            //conn open...
            conn.Open();

            string sql = "user_check_nick";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nick_", nick);
            int nickCount = Convert.ToInt32(cmd.ExecuteScalar());

            if (nickCount > 0) result = ChangeNickResult.NickExist;
            else
            {
                string sql2 = "user_update_nick";
                SqlCommand cmd2 = new SqlCommand(sql2, conn);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@userID_", userID);
                cmd2.Parameters.AddWithValue("@nick_", nick);
                cmd2.ExecuteNonQuery();

                result = ChangeNickResult.OK;
            }

        }
        catch (Exception ex)
        {
            result = ChangeNickResult.Fail;
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

    internal bool AddIAP(string userID_, string userNick_, IAPType iAPType_, string guid_, int itemIdx_)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        //Console.WriteLine("AddChannelUserDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        try
        {
            //conn open...
            conn.Open();
            string sql = "IAP_new";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID_);
            cmd.Parameters.AddWithValue("@nick_", userNick_);
            cmd.Parameters.AddWithValue("@type_", iAPType_.ToString());
            cmd.Parameters.AddWithValue("@guid_", guid_);
            cmd.Parameters.AddWithValue("@itemIdx_", itemIdx_);

            cmd.ExecuteNonQuery();

            retvalue = true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal bool UpdateIAPState(string userID, string guid, IAPResult result, out int itemIdx, out bool alreadyDone)
    {
        itemIdx = 0;
        alreadyDone = false;
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        //Console.WriteLine("UpdateChannelInfoDB ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "IAP_check_done";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@userID_", userID);
            adapter.SelectCommand.Parameters.AddWithValue("@guid_", guid);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[IAP]");
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                //r[0]: done
                if (r[0].ToString() == "1") alreadyDone = true;//already done
            }

            if (!alreadyDone)
            {
                string sql2 = "IAP_update_state";
                SqlDataAdapter adapter2 = new SqlDataAdapter(sql2, conn);
                adapter2.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter2.SelectCommand.Parameters.AddWithValue("@userID_", userID);
                adapter2.SelectCommand.Parameters.AddWithValue("@guid_", guid);
                adapter2.SelectCommand.Parameters.AddWithValue("@clientState_", result.ToString());
                adapter2.SelectCommand.Parameters.AddWithValue("@done_", (result != IAPResult.OK)? true : false);
                 
                DataSet ds2 = new DataSet();
                adapter2.Fill(ds2, "[IAP]");

                foreach (DataRow r in ds2.Tables[0].Rows)
                {
                    //r[0]: item_idx
                    itemIdx = int.Parse(r[0].ToString());
                }
            }
            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Update IAP State to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "UpdateIAPState to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal bool UpdateIAPDone(string userID, string guid)
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return false;
        }

        bool retvalue = false;
        try
        {
            conn.Open();
            string sql = "IAP_update_done";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userID_", userID);
            cmd.Parameters.AddWithValue("@guid_", guid);
            cmd.Parameters.AddWithValue("@done_", true);
            cmd.ExecuteNonQuery();
            retvalue = true;
        }
        catch (Exception e)
        {
            //Console.WriteLine("!!!Update IAP Done to DB Error: " + e.Message);
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "UpdateIAPState to DB Exception : (" + e.Message + " )";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
        finally
        {
            conn.Close();
        }
        return retvalue;
    }

    internal void ReadLocalization()
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        //Console.WriteLine("ReadChannelInfo ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        try
        {
            //conn open...
            conn.Open();
            string sql = "localization_get";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[localization]");

            LocalizationManager._dict = new Dictionary<string, string[]>();
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                //r[0]: idx,    r[1]:key,       r[2]:english,  r[3]:korean,  
                //r[4]:chinese, r[5]:japanese,  r[6]:french,   r[7]:german 
                if (!LocalizationManager._dict.ContainsKey(r[1].ToString()))
                {
                    LocalizationManager._dict.Add(r[1].ToString(), new string[] { r[2].ToString(), r[3].ToString() });
                }
            }

            Console.WriteLine("*** DB: Read Localization from MSSQL Successfully!");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

    internal void ReadNews()
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        //Console.WriteLine("ReadChannelInfo ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        try
        {
            //conn open...
            conn.Open();
            string sql = "news_get";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[news]");

            NewsManager._items = new List<NewsInfo>();
            NewsInfo info = null;
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                //r[0]: idx,         r[1]:type,       r[2]:title,  
                //r[3]: start_date,  r[4]: end_date,  r[5]: enable 
                //r[6]: english      r[7]: korean     r[8]: chinese,  r[9]: japanese,  r[10]: french,  r[11]: german
                if (int.Parse(r[5].ToString()) == 1)
                {
                    info = new NewsInfo(int.Parse(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[6].ToString(), r[7].ToString(), r[8].ToString(), r[9].ToString(), r[10].ToString(), r[11].ToString());
                    NewsManager._items.Add(info);
                }
            }

            Console.WriteLine("*** DB: Read News from MSSQL Successfully!");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

    internal void ReadMission()
    {
        if (!isConnect)
        {
            Console.WriteLine("SQL not Connected! ");

            //reconnect..
            return;
        }

        //Console.WriteLine("ReadChannelInfo ThreadID:{0}/{1}", Thread.CurrentThread.ManagedThreadId, Process.GetCurrentProcess().Threads.Count);
        try
        {
            //conn open...
            conn.Open();
            string sql = "mission_get";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataSet ds = new DataSet();
            adapter.Fill(ds, "[mission]");

            MissionManager._items = new List<MissionInfo>();
            MissionInfo info = null;
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                //r[0]: idx,            r[1]: [mission_idx],   r[2]: [achieve_type],  r[3]: [mission_type], 
                //r[4]: [title],        r[5]: [description],   r[6]: [start_date],    r[7]: [timespan],
                //r[8]: [expire_date],  r[9]: [target_value],  r[10]: [item_idx1],    r[11]: [item_count1], 
                //r[12]: [item_idx2],   r[13]: [item_count2],  r[14]: [game_type],    r[15]: [disable]
                if (int.Parse(r[15].ToString()) == 0)
                {
                    int missioniDx = int.Parse(r[1].ToString());
                    AchieveType achieveType = (AchieveType)Enum.Parse(typeof(AchieveType), r[2].ToString());
                    MissionType missionType = (MissionType)Enum.Parse(typeof(MissionType), r[3].ToString());
                    string title = r[4].ToString();
                    string description = r[5].ToString();
                    string startDate = r[6].ToString();
                    string timeSpan = r[7].ToString();
                    string expireDate = r[8].ToString();
                    int targetValue = int.Parse(r[9].ToString());
                    int itemIdx1 = int.Parse(r[10].ToString());
                    int itemCount1 = int.Parse(r[11].ToString());
                    int itemIdx2 = int.Parse(r[12].ToString());
                    int itemCount2 = int.Parse(r[13].ToString());
                    GameType gameType= (GameType)Enum.Parse(typeof(GameType), r[14].ToString());

                    info = new MissionInfo(missioniDx, achieveType, missionType, title, description, startDate, timeSpan, expireDate, targetValue, itemIdx1, itemCount1, itemIdx2, itemCount2, gameType);
                    MissionManager._items.Add(info);
                }
            }

            Console.WriteLine("*** DB: Read Mission from MSSQL Successfully!");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
    }

}
