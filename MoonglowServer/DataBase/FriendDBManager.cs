﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class FriendData
{
    public string friendID { get; set; }
    public string friendNick { get; set; }
    public int faceIdx { get; set; }
    public int level { get; set; }
    public FriendType friendType { get; set; }
    public string friendLastLoginDate { get; set; }
    public string itemSendDate { get; set; }
    public bool enableGetItem { get; set; }
    public int winCount { get; set; }
    public int loseCount { get; set; }
    public int drawCount { get; set; }
    public FriendData() { }
}
public class FriendDBManager
{
    public static FriendDBManager _instance = null;
    public static FriendDBManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new FriendDBManager();
            }
            return _instance;
        }
    }

    public void GetFriendListFromDB(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        PacketsHandlerInner.SDReqGetFriendList(userInfo.serverSession, userSession);
    }

    internal void AddFriendToDB(string userSession, string userID, /*string userNick,*/ string friendID/*, string friendNick*/)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);//null;
        if (userInfo != null)
        {
            PacketsHandlerInner.SDReqNewFriend(userInfo.serverSession, userID, /*userNick,*/ friendID/*, friendNick*/);
        }
    }

    internal void UpdateDBFriendType(string userSession, string userID_, string friendID_, int otherFriendCount_)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);//null;
        if (userInfo != null)
        {
            PacketsHandlerInner.SDReqUpdateFriendType(userInfo.serverSession, userSession, userID_, friendID_, otherFriendCount_);
        }
    }

    internal void DeleteFriend(string userSession, string friendID, FriendType friendType)
    {
         UserInfo userInfo = GameServer.GetUserInfo(userSession);
         if (userInfo != null)
         {
             PacketsHandlerInner.SDReqDeleteFriend(userInfo.serverSession, userSession, friendID, friendType);
         }
    }

    internal void GetRequestFriends(string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);//null;
         if (userInfo != null)
         {
             PacketsHandlerInner.SDReqGetRequestFriends(userInfo.serverSession, userSession);
         }
    }

    internal void GetMyFriends(string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            PacketsHandlerInner.SDReqGetMyFriends(userInfo.serverSession, userSession);
        }
    }

    internal void UpdateGiftSendDate(string userSession, string friendID, string sendDate)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);//null;
         if (userInfo != null)
         {
             PacketsHandlerInner.SDReqGiftSendDate(userInfo.serverSession, userSession, friendID, sendDate);
         }
    }

    internal void UpdateSendGift(string userSession, string friendID, string itemSendDate)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            PacketsHandlerInner.SDReqUpdateSendFriendItem(userInfo.serverSession, userSession, friendID, itemSendDate);
        }
   }

    internal void UpdateGetGift(string userSession, string friendID)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);//null;
        if (userInfo != null)
        {
            PacketsHandlerInner.SDReqUpdateGetFriendItem(userInfo.serverSession, userSession, friendID);
        }
    }
}
