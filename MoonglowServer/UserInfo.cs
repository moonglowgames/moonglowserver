﻿using MoonGlow.Data;
using MoonGlow.Room;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Threading;

namespace MoonGlow
{
    public enum UserServerStatus { Unknown , Connect, Login, SessionLogin,  Home , Room , DisConnect , Logout }

    //player profile
    public class UserProfile
    {
        public int addHP { get; set; }
        public int addSP { get; set; }
        public int winCount { get; set; }
        public int loseCount { get; set; }
        public int drawCount { get; set; }
        public int conWinCount { get; set; }
        public int CoConWinCount { get; set; }
        public int JoConWinCount { get; set; }
        public int ChConWinCount { get; set; }
        public int ZaConWinCount { get; set; }
        public int ArConWinCount { get; set; }
        public int TaConWinCount { get; set; }
        public int KrConWinCount { get; set; }
        public int YaConWinCount { get; set; }
        public int chanelIdx { get; set; }
        public /*float*/int rankPoint { get; set; }
        public /*float*/int rankWin { get; set; }
        public /*float*/int rankLose { get; set; }
        public /*float*/int rankDraw { get; set; }

        public int friendsCount { get; set; }
        public int checkInDays { get; set; }
        public int conCheckInDays { get; set; }
        //{ EtherealInk = 101, SaqqaraTablet = 102, RosemaryOil = 103, SLElixir = 104, EPElixir = 105, SOElixir = 106}
        //public int etherealInkUseCount { get; set; }
        //public int saqqaraTabletUseCount { get; set; }
        //public int rosemaryOilUseCount { get; set; }
        //public int sLElixirUseCount { get; set; }
        //public int epElixirUseCount { get; set; }
        //public int soElixirUseCount { get; set; }
        public ChatChannel chatChannel { get; set; }
    }

    //achievement & mission...
    public class AchieveValue
    {
        public int achieveIdx { get; set; }
        public int currentValue { get; set; }
        public int achieveLevel { get; set; }
        public bool isDone { get; set; }
        public AchieveValue() { }
        public AchieveValue(int achieveIdx_, int currentValue_, int currentLevel_, bool isDone_)
        {
            achieveIdx = achieveIdx_;
            currentValue = currentValue_;
            achieveLevel = currentLevel_;
            isDone = isDone_;
        }
    }
    public class MissionValue
    {
        public int missionIdx { get; set; }
        public int currentValue { get; set; }
        public DateTime expireDate { get; set; }
        public MissionState state { get; set; }
        public DateTime completeDate { get; set; }
        public MissionValue() { }
        public MissionValue(int missionIdx_, int currentValue_, DateTime expireDate_, MissionState state_, DateTime completeDate_)
        {
            missionIdx = missionIdx_;
            currentValue = currentValue_;
            expireDate = expireDate_;
            state = state_;
            completeDate = completeDate_;
        }

    }

    //cardSkin
    public class CardSkin
    {
        public int skinIdx { get; set; }
        public bool isSelect { get; set; }
        public CardSkin() { }
        public CardSkin(int skinIdx_, bool isSelect_)
        {
            skinIdx = skinIdx_;
            isSelect = isSelect_;
        }
    }
    //pet & badge...
    public class Pet
    {
        public int petIdx { get; set; }
        public bool isSelect { get; set; }
        public Pet() { }
        public Pet(int petIdx_,bool isSelect_)
        {
            petIdx = petIdx_;
            isSelect = isSelect_;
        }
    }
    public class Badge
    {
        public int badgeIdx { get; set; }
        public bool isSelect { get; set; }
        public Badge() { }
        public Badge(int badgeIdx_, bool isSelect_)
        {
            badgeIdx = badgeIdx_;
            isSelect = isSelect_;
        }
    }

    public class UserJoker
    {
        public int faceIdx { get; set; }
        public List<int> spellIdxList { get; set; }
    }
    public class UserItem 
    {
        public int ePElixir {get;set;}
        public int sOElixir {get;set;}
        public int sLElixir {get;set;}
        public int rosemaryOil {get;set;} //staminaUp
        public int etherealInk { get; set; } //joker Ink reset 
        public int saqqaraTablet { get; set; }//Change Nick Name

        //pet and badge list
        public List<Pet> petList { get; set; }
        public List<Badge> badgeList { get; set; }
        //joker face index list
        public List<int> jokerFaceList { get; set; }
        public List<CardSkin> cardSkinList { get; set; }
        public List<int> iconSet { get; set; }
    }

    public class UserDataBase
    {
        public string account { get; set; }
        public string nickName { get; set; }
        //public string pw { get; set; }
        public int FaceIdx { get; set; }
        public int Level { get; set; }
        //public int NextLevelExp { get; set; }
        public int Exp { get; set; }
        public int Gold { get; set; }
        public int Stamina { get; set; }
        public int maxStamina { get; set; }
        public int Ruby { get; set; }
        public int Ink { get; set; }
        public int JokerLine { get; set; }
        //public UserType userType { get; set; }
        public DateTime staminaLastEventDate { get; set; }
        public DateTime soElixirEndEventDate { get; set; }
        public DateTime epElixirEndEventDate { get; set; }

        public DateTime ADLastTime { get; set; }
        public int ADRemainCount { get; set; }

        public string createDate { get; set; }
        public string lastLoginDate { get; set; }
        //public bool ban { get; set; }
       
        //achievement & mission
        public List<AchieveValue> achieveValue { get; set; }
        public List<MissionValue> missionValue { get; set; }

        public UserItem userItem { get; set; }
        public UserJoker userJoker { get; set; }
        public UserProfile userProfile { get; set; }
    }

    public class UserInfo
    {
        public UserServerStatus userServerState { get; private set; }
        public string session { get; private set; } //userSession = loginSession
        public string serverSession { get; set; }
        public string userID { get; set; }
        public string roomID { get; set; }
        //public string lastLoginDate { get; set; }
        public UserDataBase userDataBase { get; set; }

        public StaminaTime staminaTime;
        public EPElixirTime epElixirTime;
        public SOElixirTime soElixirTime;
        public bool isReadDone = false;
        public bool needNotiGotoScene = false;

        public int tryInkCount = 0;
        public int tmpJokerFace = 0;
        public List<int> tmpSpellIdxList = null;
        public List<SpellType> spellTypeList = null;

        public bool isJokerTry = false;
        public SceneType sceneType = SceneType.Unknown;
        //public SpellType spellType = SpellType.Unknown;
        public List<MailData> _mailList = null;
        public List<FriendData> _recommendFriends = null;
        public List<FriendData> _allFriends = null;
        public List<FriendData> _myFriends = null;
        public List<FriendData> _requestFriends = null;
        public bool enableGetItem = false;
        public List<FriendData> _delRequestFriends = null;
        public Dictionary<int, ChanelInfo> _chanelDic = null;
        public List<MyRank> _rankHistroy = null;

        public PlayerItemInfo _playerItemInfo = null;

        public DateTime userInfoRemoveDate = new DateTime();
        public DateTime pauseExpireDate = new DateTime();
        internal RoomList _requestRoomList = null;
       
        internal int connectTimes = 0;
        internal int pingCount = 0;
        internal int pingTimes = 0;
        internal Room.Room tmpRoomInfo = null;
        internal bool isPause = false;
        internal Language language = Language.English;
        internal string ADGuid = string.Empty;
        internal PayType ADType = PayType.None;
        internal int ADRewardCount = 0;

        private int _currentItemTimeTickCount = 0;
        private int _currentPingTickCount = 0;
        private int _currentExpireTickCount = 0;
        private int _currentSessionTickCount = 0;
        internal string creatorSession = string.Empty;
        internal bool IAPDoing = false;

        public UserInfo(string userSession_, string serverSession_)
        {
            SetSession(userSession_, serverSession_);
            InitUserDataBase();

            staminaTime = new StaminaTime();
            epElixirTime = new EPElixirTime();
            soElixirTime = new SOElixirTime();
        }
      
        void InitUserDataBase()
        {
            userDataBase = new UserDataBase();
            userDataBase.userItem = new UserItem();
            userDataBase.userItem.petList = new List<Pet>();
            userDataBase.userItem.badgeList = new List<Badge>();
            userDataBase.userItem.jokerFaceList = new List<int>();
            userDataBase.userItem.cardSkinList = new List<CardSkin>();
            userDataBase.userItem.iconSet = new List<int>();

            //initial joker...
            userDataBase.userJoker = new UserJoker();
            userDataBase.userJoker.spellIdxList = new List<int>();

            //achievement & mission...
            userDataBase.achieveValue = new List<AchieveValue>();
            userDataBase.missionValue = new List<MissionValue>();

            //profile...
            userDataBase.userProfile = new UserProfile();
        }

        internal void SetSendChatIconSet()
        {
            if (userDataBase.userItem.iconSet != null && userDataBase.userItem.iconSet.Count > 0)
            {
                ChatIconSet iconSet = new ChatIconSet(PacketTypeList.SCNotiChatIconSet.ToString(), userDataBase.userItem.iconSet);
                PacketsHandlerServer.SendSCNotiChatIconSet(serverSession, iconSet);
            }
        }

        internal void CheckDailyCheckInAndGift()
        {
            string subject = LocalizationKey.DAILY_GIFT_SUBJECT.ToString();
            string content = LocalizationKey.DAILY_GIFT_CONTENT.ToString();
            bool dailyGiftOk = false;

            if (string.IsNullOrEmpty(userDataBase.lastLoginDate))
            {
                if (!string.IsNullOrEmpty(userDataBase.createDate))
                {
                    DateTime createDateTime = new DateTime();
                    if (GlobalManager.Instance.ConvertToDateTime(userDataBase.createDate, out createDateTime))
                    {
                        if (DateTime.Today > createDateTime.Date + new TimeSpan(1, 0, 0, 0))
                        {
                            userDataBase.userProfile.conCheckInDays = 1;
                            userDataBase.userProfile.checkInDays++;
                            dailyGiftOk = true;
                        }
                        else if (DateTime.Today == createDateTime.Date + new TimeSpan(1, 0, 0, 0))
                        {
                            userDataBase.userProfile.conCheckInDays++;
                            userDataBase.userProfile.checkInDays++;
                            dailyGiftOk = true;
                        }
                    }
                }
                else
                {
                    //error user's createdate is empty
                }
            }
            else
            {
                DateTime loginDateTime = new DateTime();
                if (GlobalManager.Instance.ConvertToDateTime(userDataBase.lastLoginDate, out loginDateTime))
                {
                    if (DateTime.Today > loginDateTime.Date + new TimeSpan(1, 0, 0, 0))
                    {
                        userDataBase.userProfile.conCheckInDays = 1;
                        userDataBase.userProfile.checkInDays++;
                        dailyGiftOk = true;
                    }
                    else if (DateTime.Today == loginDateTime.Date + new TimeSpan(1, 0, 0, 0))
                    {
                        userDataBase.userProfile.conCheckInDays++;
                        userDataBase.userProfile.checkInDays++;
                        dailyGiftOk = true;
                    }
                }
            }

            if (dailyGiftOk)
            {
                //add new mail
                MailData mailData = new MailData();
                mailData.userSession = session;
                //mailData.nick = userDataBase.nickName;
                mailData.sendID = "SYSTEM";
                //mailData.senderNick = "SYSTEM";
                mailData.subject = subject;
                mailData.content = content;
                mailData.itemIdx1 = (int)ElementType.MoonRuby;
                mailData.itemCount1 = GlobalManager.DAY_GIFT;
                mailData.itemGot = false;

                //save to mail db, add new mail...
                MailDBManager.Instance.NewMail(mailData);
                //PacketsHandlerInner.SDReqNewMail(serverSession, mailData);
                //SaveUserInfo();
            }
        }

        public void SetSession(string userSession_, string serverSession_)
        {
            userServerState = UserServerStatus.Connect;
            session = userSession_;
            serverSession = serverSession_;
        }

        //public void SetUserID(string userID_)
        //{
        //    userServerState = UserServerStatus.Login;
        //    userID = userID_;
        //    nickName = userID; 
        //}

        internal void AddUserItem(int itemIdx, int itemCount)
        {
            if (itemIdx == (int)ElementType.Gold)//Glod = 2, maybe need to change later..
            {
                userDataBase.Gold += itemCount;

                //send SCNotiUserElement
                SetSendElementInfoList();
            }
            else if (itemIdx == (int)ElementType.MoonRuby)//Ruby = 3
            {
                userDataBase.Ruby += itemCount;

                //send SCNotiUserElement
                SetSendElementInfoList();
            }
            else
            {
                TabType tabType = ItemManager.Instance.GetTabType(itemIdx);
                switch (tabType)
                {
                    case TabType.Pet:
                        bool petExist = false;
                        foreach (var pet in userDataBase.userItem.petList)
                        {
                            if (pet.petIdx == itemIdx)
                            {
                                petExist = true;
                                break;
                            }
                        }
                        if (petExist == false)
                        {
                            userDataBase.userItem.petList.Add(new Pet(itemIdx, false));
                        }
                        break;
                    case TabType.Badge:
                        bool badgeExist = false;
                        foreach (var badge in userDataBase.userItem.badgeList)
                        {
                            if ((badge.badgeIdx /100 == itemIdx/100) && (badge.badgeIdx / 10 % 10 == itemIdx / 10 % 10))//same badge group
                            {
                                if (badge.badgeIdx < itemIdx)
                                {
                                    badge.badgeIdx = itemIdx;
                                }
                                badgeExist = true;
                                break;
                            }
                        }
                        if (badgeExist == false)
                        {
                            userDataBase.userItem.badgeList.Add(new Badge(itemIdx, false));
                        }
                        break;
                    case TabType.Items://items: 1
                        //add to userInfo
                        ChangeUserItemAmount(itemIdx, itemCount);
                        if (sceneType == SceneType.Home || sceneType == SceneType.Mail || sceneType == SceneType.RankHistory)
                        {
                            //send itemInfoList again...
                            //SetSendItemInfoList();
                        }
                        break;
                    case TabType.CardSkin:
                        bool skinExist = false;
                        foreach (var skin in userDataBase.userItem.cardSkinList)
                        {
                            if (skin.skinIdx == itemIdx)
                            {
                                petExist = true;
                                break;
                            }
                        }
                        if (skinExist == false)
                        {
                            userDataBase.userItem.cardSkinList.Add(new CardSkin(itemIdx, false));
                        }
                        break;
                    default: break;
                }
            }

            if (GameServer._isCloud == true)
            {
                //item log
                ItemElementInfo itemInfo = ItemManager.Instance.GetElement(itemIdx);
                if (itemInfo != null)
                {
                    GlobalManager.Instance.LogItemInsert(serverSession, userID, userDataBase.nickName, itemIdx, itemInfo.name, itemCount, LogItemType.Reward);
                }
            }
        }

        public void SetSessionLogin()
        {
            userServerState = UserServerStatus.SessionLogin;
        }
        // Home entered
        public void SetHome()
        {
            userServerState = UserServerStatus.Home;
        }

        public void SetRoom(string roomID_)
        {
            userServerState = UserServerStatus.Room;
            roomID = roomID_;
        }

        internal void OnClientPause()
        {
            isPause = true;
            pauseExpireDate = DateTime.Now + new TimeSpan(0, 0, 20);//allow 20s' pause, otherwise set disconnect 
        }

        public void SetDisConnect()
        {
            userServerState = UserServerStatus.DisConnect;
            // room exist or not
        }

        public void SetLogOut()
        {
            userServerState = UserServerStatus.Logout;
            // room must exit;
            //roomID = string.Empty;
        }

        public void ChangeUserItemAmount(int itemIdx_, int count_)
        {
            switch (itemIdx_)
            {
                //case (int)(UserItems.EtherealInk):
                //    userDataBase.userItem.etherealInk += count_;
                //    break;
                case (int)(UserItems.EPElixir):
                    userDataBase.userItem.ePElixir += count_;
                    break;
                case (int)(UserItems.SOElixir):
                    userDataBase.userItem.sOElixir += count_;
                    break;
                case (int)(UserItems.RosemaryOil):
                    userDataBase.userItem.rosemaryOil += count_;
                    break;
                case (int)(UserItems.SaqqaraTablet):
                    userDataBase.userItem.saqqaraTablet += count_;
                    break;
                case (int)(UserItems.SLElixir):
                    userDataBase.userItem.sLElixir += count_;
                    break;
            }
        }

        //internal void SetSendItemInfoList()
        //{
        //    List<ItemInfo> itemList = new List<ItemInfo>();
        //    itemList.Add(new ItemInfo((int)UserItems.EPElixir, userDataBase.userItem.ePElixir));
        //    itemList.Add(new ItemInfo((int)UserItems.SOElixir, userDataBase.userItem.sOElixir));
        //    itemList.Add(new ItemInfo((int)UserItems.SLElixir, userDataBase.userItem.sLElixir));
        //    itemList.Add(new ItemInfo((int)UserItems.RosemaryOil, userDataBase.userItem.rosemaryOil));
        //    itemList.Add(new ItemInfo((int)UserItems.EtherealInk, userDataBase.userItem.etherealInk));
        //    itemList.Add(new ItemInfo((int)UserItems.SaqqaraTablet, userDataBase.userItem.saqqaraTablet));

        //    PacketsHandlerServer.SendSCNotiItemInfo(serverSession, new ItemList(PacketTypeList.SCNotiItemInfo.ToString(), itemList));
        //}

        internal void SetRankPoint(/*float*/int rankPoint)
        {
            userDataBase.userProfile.rankPoint += rankPoint;
            List<ChanelInfo> chanelInfoList = null;
            ChannelManager.Instance.GetChanelInfoList(out chanelInfoList);
            string chanelName = string.Empty;
            foreach (var info in chanelInfoList)
            {
                if (info.chanelIdx == userDataBase.userProfile.chanelIdx)
                {
                    chanelName = info.name;
                    break;
                }
            }

            PacketsHandlerInner.SMReqUpdateRank(serverSession, chanelName, session, userID);
        }

        internal void SetJokerFaceList(int elementIdx_)
        {
            userDataBase.userItem.jokerFaceList.Add(elementIdx_);
        }

        internal void SetSpellIdxList(ref List<int> spellIdxList, SpellType spellType)
        {
            SpellInfo spellInfo = null;
            if (isJokerTry == false)
            {
                foreach (var spellIdx in userDataBase.userJoker.spellIdxList)
                {
                    spellInfo = SpellManager.Instance.GetSpellInfo(spellIdx);
                    if (spellInfo != null && (spellInfo.spellType == spellType))
                    {
                        spellIdxList.Add(spellIdx);
                    }
                }
            }
            else if (isJokerTry == true && tmpSpellIdxList != null)
            {
                foreach (var spellIdx in this.tmpSpellIdxList)
                {
                    spellInfo = SpellManager.Instance.GetSpellInfo(spellIdx);
                    if (spellInfo != null && (spellInfo.spellType == spellType))
                    {
                        spellIdxList.Add(spellIdx);
                    }
                }
            }
        }

        internal void SetSendElementInfoList()
        {
            List<ElementInfo> elementList = new List<ElementInfo>();
            elementList.Add(new ElementInfo(ElementType.Stamina, userDataBase.Stamina, userDataBase.maxStamina));
            elementList.Add(new ElementInfo(ElementType.Gold, userDataBase.Gold));
            elementList.Add(new ElementInfo(ElementType.MoonRuby, userDataBase.Ruby));

            MoonGlow.PacketsHandlerServer.SendSCNotiUserElement(serverSession, new ElementList(PacketTypeList.SCNotiUserElement.ToString(), elementList));
        }

        internal void CheckStaminaEvent()
        {
            if(staminaTime ==null) staminaTime = new StaminaTime();
            staminaTime.CheckEvent(session);
        }

        internal void SetStaminaEvent()
        {
            staminaTime.SetEvent(this);
        }

        public void SaveUserData(out UserData userData)
        {
            ResourceData resourceData = new ResourceData();
            resourceData.Gold = userDataBase.Gold;//need update
            resourceData.Ruby = userDataBase.Ruby;
            resourceData.Stamina = userDataBase.Stamina;
            resourceData.maxStamina = userDataBase.maxStamina;
            resourceData.Ink = userDataBase.Ink;
            resourceData.JokerLine = userDataBase.JokerLine;

            ItemData itemData = new ItemData();
            itemData.etherealInk = userDataBase.userItem.etherealInk;
            itemData.ePElixir = userDataBase.userItem.ePElixir;
            itemData.sLElixir = userDataBase.userItem.sLElixir;
            itemData.rosemaryOil = userDataBase.userItem.rosemaryOil;
            itemData.sOElixir = userDataBase.userItem.sOElixir;
            itemData.saqqaraTablet = userDataBase.userItem.saqqaraTablet;
            //save PetList & Badge List...
            itemData.petList = userDataBase.userItem.petList;
            itemData.badgeList = userDataBase.userItem.badgeList;
            //joker face index list
            itemData.jokerFaceList = userDataBase.userItem.jokerFaceList;
            itemData.cardSkinList = userDataBase.userItem.cardSkinList;
            //iconSet
            itemData.iconSet = userDataBase.userItem.iconSet;

            JokerData jokerData = new JokerData();
            jokerData.spellIdxList = userDataBase.userJoker.spellIdxList;
            jokerData.faceIdx = userDataBase.userJoker.faceIdx;

            ProfileData profileData = new ProfileData();
            profileData.addSP = userDataBase.userProfile.addSP;
            profileData.addHP = userDataBase.userProfile.addHP;
            profileData.winCount = userDataBase.userProfile.winCount;
            profileData.loseCount = userDataBase.userProfile.loseCount;
            profileData.drawCount = userDataBase.userProfile.drawCount;

            profileData.conWinCount = userDataBase.userProfile.conWinCount;
            profileData.CoConWinCount = userDataBase.userProfile.CoConWinCount;
            profileData.JoConWinCount = userDataBase.userProfile.JoConWinCount;
            profileData.ChConWinCount = userDataBase.userProfile.ChConWinCount;
            profileData.ZaConWinCount = userDataBase.userProfile.ZaConWinCount;
            profileData.ArConWinCount = userDataBase.userProfile.ArConWinCount;
            profileData.TaConWinCount = userDataBase.userProfile.TaConWinCount;
            profileData.KrConWinCount = userDataBase.userProfile.KrConWinCount;
            profileData.YaConWinCount = userDataBase.userProfile.YaConWinCount;

            profileData.friendCount = userDataBase.userProfile.friendsCount;
            profileData.checkInDays = userDataBase.userProfile.checkInDays;
            profileData.conCheckInDays = userDataBase.userProfile.conCheckInDays;
            profileData.chanelIdx = userDataBase.userProfile.chanelIdx;
            profileData.rankPoint = userDataBase.userProfile.rankPoint;

            profileData.rankWin = userDataBase.userProfile.rankWin;
            profileData.rankLose = userDataBase.userProfile.rankLose;
            profileData.rankDraw = userDataBase.userProfile.rankDraw;

            //item use count
            //profileData.etherealInkUseCount = userDataBase.userProfile.etherealInkUseCount;
            //profileData.sLElixirUseCount = userDataBase.userProfile.sLElixirUseCount;
            //profileData.rosemaryOilUseCount = userDataBase.userProfile.rosemaryOilUseCount;
            //profileData.saqqaraTabletUseCount = userDataBase.userProfile.saqqaraTabletUseCount;
            //profileData.epElixirUseCount = userDataBase.userProfile.epElixirUseCount;
            //profileData.soElixirUseCount = userDataBase.userProfile.soElixirUseCount;
            profileData.chatChannel = userDataBase.userProfile.chatChannel;

            /*UserData*/ userData = new UserData();
            userData.id = userID;
            userData.account = userDataBase.account;
            userData.nick = userDataBase.nickName;
            //userData.pw = userDataBase.pw;
            userData.faceIdx = userDataBase.FaceIdx;
            userData.level = userDataBase.Level;
            userData.exp = userDataBase.Exp;
            userData.resourceData = resourceData;
            userData.itemData = itemData;
            userData.jokerData = jokerData;
            userData.profileData = profileData;
            userData.staminLastEventDate = userDataBase.staminaLastEventDate;
            userData.soElixirEndEventDate = userDataBase.soElixirEndEventDate;
            userData.epElixirEndEventDate = userDataBase.epElixirEndEventDate;

            userData.ADLastTime = userDataBase.ADLastTime;
            userData.ADRemainCount = userDataBase.ADRemainCount;

            userData.achieveValue = userDataBase.achieveValue;
            userData.missionValue = userDataBase.missionValue;
            //userData.userType = userDataBase.userType;
            userData.createDate = userDataBase.createDate;
            userData.lastLoginDate = userDataBase.lastLoginDate;
            //userData.lasetLogoutDate = userDataBase.lastLogoutDate;
            //userData.ban = userDataBase.ban;
        }
        internal void SaveUserInfo()
        {
            UserData userData = null;
            SaveUserData(out userData);
            //make SDReqUpdateUserInfo
            PacketsHandlerInner.SDReqUpdateUserInfo(serverSession, userData);
        }

        internal int badgeIdx
        {
            get
            {
                int badgeIdx = 0;
                if (userDataBase.userItem.badgeList != null)
                {
                    foreach (var badge in userDataBase.userItem.badgeList)
                    {
                        if (badge.isSelect)
                        {
                            badgeIdx = badge.badgeIdx;
                            break;
                        }
                    }
                }
                return badgeIdx;
            }
        }

        internal void SaveToSql()
        {
            PacketsHandlerInner.SDReqSaveToSql(serverSession, userID, userDataBase.nickName, userDataBase.FaceIdx, userDataBase.Level, userDataBase.userProfile.rankPoint, userDataBase.lastLoginDate);
        }

        internal void SetJokerLine(LogItemType logItemType_)
        {
            int lastJokerLine = userDataBase.JokerLine;

            if (lastJokerLine != 1 && userDataBase.Level < 11) userDataBase.JokerLine = 1; 

            else if ( lastJokerLine != 2 &&　userDataBase.Level >= 11 && userDataBase.Level < 21) userDataBase.JokerLine = 2;
            else if (lastJokerLine != 3 && userDataBase.Level >= 21) userDataBase.JokerLine = 3;

            //SetTmpSpellIdxList();
            if (GameServer._isCloud == true)
            {
                //item log
                GlobalManager.Instance.LogItemInsert(serverSession, userID, userDataBase.nickName, (int)ElementType.JokerLine, ElementType.JokerLine.ToString(), userDataBase.JokerLine - lastJokerLine, logItemType_);
            }
        }

        internal bool UseUserItem(int itemIdx_)
        {
            bool retvalue = true;
            switch (itemIdx_)
            {
                case (int)(UserItems.EtherealInk):
                    //Get all ink count  from Joker and clear all magic description, reset joker
                    // if (userDataBase.userItem.etherealInk <= 0) { return false; }
                    userDataBase.userJoker.faceIdx = 0;
                    isJokerTry = false;
                    tmpJokerFace = 0;
                    tmpSpellIdxList = new List<int>();

                    if (userDataBase.JokerLine > 0)
                    {
                        if ((userDataBase.userJoker.spellIdxList != null) && (userDataBase.userJoker.spellIdxList.Count > 0))
                        {
                            //SpellInfo spellInfo = null;
                            int extractInk = GlobalManager.DEFAULT_INIT_INK + userDataBase.Level - userDataBase.Ink;

                            userDataBase.userJoker.faceIdx = 0;
                            userDataBase.userJoker.spellIdxList = new List<int>();
                            isJokerTry = false;
                            tmpSpellIdxList = null;
                            spellTypeList = null;

                            //reset tmp spell list
                            userDataBase.Ink = GlobalManager.DEFAULT_INIT_INK + userDataBase.Level -1;
                            tryInkCount = 0;
                            //userDataBase.userProfile.etherealInkUseCount++;

                            //check achieve
                            //CheckAchieveValue(AchieveType.EtherealInkUseCount, 1);
                            //CheckAchieveValue(AchieveType.ItemUseCount, 1);

                            //check mission
                            //CheckMissionValue(GameType.Unknown, AchieveType.EtherealInkUseCount, 1);

                            //retvalue = true;

                            if (extractInk != 0 && GameServer._isCloud == true)
                            {
                                //item log
                                GlobalManager.Instance.LogItemInsert(serverSession, userID, userDataBase.nickName, (int)ElementType.Ink, ElementType.Ink.ToString(), extractInk, LogItemType.Return);
                            }
                        }
                    }
                    else
                    {
                        //No jokerLine enable...
                        retvalue = false;
                    }

                    break;
                case (int)(UserItems.SaqqaraTablet): break;
                case (int)(UserItems.RosemaryOil):
                    //Charge stamina full
                    if (userDataBase.userItem.rosemaryOil <= 0 || userDataBase.Stamina >= GlobalManager.FULL_STAMINA)
                        retvalue = false;
                    else
                    {
                        userDataBase.Stamina = GlobalManager.FULL_STAMINA;

                        //SCNotiUserElement
                        SetSendElementInfoList();
                        //send myInfo again
                        SetSendMyInfo();

                        //userDataBase.userProfile.rosemaryOilUseCount++;

                        //check achieve
                        CheckAchieveValue(GameType.Unknown, AchieveType.RosemaryOilUseCount, 1);
                        CheckAchieveValue(GameType.Unknown, AchieveType.ItemUseCount, 1);

                        //check mission
                        CheckMissionValue(GameType.Unknown, AchieveType.RosemaryOilUseCount, 1);

                        retvalue = true;
                    }
                    break;
                case (int)(UserItems.SLElixir):
                    if (userDataBase.userItem.sLElixir <= 0 || userDataBase.Stamina == 200)
                        retvalue = false;
                    else
                    {
                        // stamina up to 200
                        userDataBase.Stamina = 200;

                        //SCNotiUserElement
                        SetSendElementInfoList();
                        //send myInfo again
                        SetSendMyInfo();

                        //userDataBase.userProfile.sLElixirUseCount++;

                        //check achieve
                        CheckAchieveValue(GameType.Unknown, AchieveType.SLElixirUseCount,1);
                        CheckAchieveValue(GameType.Unknown, AchieveType.ItemUseCount, 1);

                        //check mission
                        CheckMissionValue(GameType.Unknown, AchieveType.SLElixirUseCount, 1);

                        retvalue = true;
                    }
                    break;
                case (int)(UserItems.EPElixir):
                    if (userDataBase.userItem.ePElixir <= 0 || this.epElixirTime.isSet == true)
                        retvalue = false;
                    else
                    {
                        //+1HP during 2hrs
                        SetEPElixirEvent();

                        //send myInfo again
                        SetSendMyInfo();
                        //send SCNotiPlayerstatus to everyone again in Play scece, later when enble to use item in Play scene...

                        //userDataBase.userProfile.epElixirUseCount++;
                        
                        //check achieve
                        CheckAchieveValue(GameType.Unknown, AchieveType.EPElixirUseCount, 1);
                        CheckAchieveValue(GameType.Unknown, AchieveType.ItemUseCount, 1);

                        //check mission
                        CheckMissionValue(GameType.Unknown, AchieveType.EPElixirUseCount, 1);

                        retvalue = true;
                    }
                    break;
                case (int)(UserItems.SOElixir):
                    if (userDataBase.userItem.sOElixir <= 0 || this.soElixirTime.isSet == true)
                        retvalue = false;
                    else
                    {
                        //+1SP during 2hrs
                        SetSOElixirEvent();

                        //send myInfo again
                        SetSendMyInfo();
                        //send SCNotiPlayerstatus to everyone again in Play scece, later when enble to use item in Play scene...

                        //userDataBase.userProfile.soElixirUseCount++;

                        //check achieve
                        CheckAchieveValue(GameType.Unknown, AchieveType.SOElixirUseCount, 1);
                        CheckAchieveValue(GameType.Unknown, AchieveType.ItemUseCount, 1);

                        //check mission
                        CheckMissionValue(GameType.Unknown, AchieveType.SOElixirUseCount, 1);

                        retvalue = true;
                    }
                    break;
                default: break;
            }
            
            return retvalue;
        }

        public void SetSendSpellIdxList()
        {
            List<int> spellIdxList = new List<int>();

            int totalInk = GlobalManager.DEFAULT_INIT_INK + userDataBase.Level - 1;
            //int jokerFaceOption = 0;
            //List<SpellType> spellTypeList = new List<SpellType>();
            int jokerFace = (tmpJokerFace == 0)? userDataBase.userJoker.faceIdx: tmpJokerFace;
            if (jokerFace == 0)
            {
                PacketsHandlerServer.SendSCNotiSpellList(this.serverSession, new SpellIdxList(PacketTypeList.SCNotiSpellList.ToString(), /*spellType_,*/ userDataBase.Ink, -this.tryInkCount, totalInk, spellIdxList));
                return;
            }

            ItemManager.Instance.GetJokerFaceType(jokerFace, out this.spellTypeList);

            foreach (var spellType in this.spellTypeList)
            {
                SetSpellIdxList(ref spellIdxList, spellType);
            }

            //if (spellIdxList != null)
            //{
                PacketsHandlerServer.SendSCNotiSpellList(this.serverSession, new SpellIdxList(PacketTypeList.SCNotiSpellList.ToString(), /*spellType_,*/ userDataBase.Ink, -this.tryInkCount, totalInk, spellIdxList));
            //}
            
            //List<int> spellIdxList = null;
            //SetSpellIdxList2(out spellIdxList, userDataBase.userJoker.faceIdx);
            //if (spellIdxList != null)
            //{
            //    PacketsHandlerServer.SendSCNotiSpellList(this.serverSession, new SpellIdxList(PacketTypeList.SCNotiSpellList.ToString(), spellType_, userDataBase.Ink, this.tryInkCount, spellIdxList));
            //}
        }

        public void SetSendJokerInfo()
        {
            //List<int> spellIdxList = null;
            //SetUserSpellIdxList(out spellIdxList);
            //if (spellIdxList != null)
            //{
            //if(isJokerTry)
            if(tmpJokerFace != 0)
                PacketsHandlerServer.SendSCNotiJokerInfo(this.serverSession, tmpJokerFace, this.isJokerTry/*, userDataBase.userItem.etherealInk*/);
            else
                PacketsHandlerServer.SendSCNotiJokerInfo(this.serverSession, userDataBase.userJoker.faceIdx, this.isJokerTry/*, userDataBase.userItem.etherealInk*/);
            //}
        }

        public void SetSendMyInfo()
        {
            List<int> petList = SetPetList();

            int badgeIdx = SetBadge();
            int cardSkinIdx = SetCardSkin();

            List<ItemAbility> itemAbilityList = null;
            SetItemAbilityList(out itemAbilityList);
           
            List<PetAbility> petAbilityList = null;
            SetPetAbilityList(out petAbilityList);

            List<ProfileInfo> profileList = null;
            SetProfileList(out profileList);

            TimeSpan nextStaminaTime = new TimeSpan();
            if (userDataBase.Stamina < 100 && (staminaTime.endEventDate > DateTime.Now))
            {
                nextStaminaTime = staminaTime.endEventDate - DateTime.Now;
            }

            int winCount = userDataBase.userProfile.winCount;
            int totalCount = userDataBase.userProfile.winCount + userDataBase.userProfile.loseCount /*+ userDataBase.userProfile.drawCount*/;
            decimal winRate = (totalCount != 0)? ((decimal)winCount / totalCount) : 0;
            string winRateStr = winRate.ToString("#0.##%", CultureInfo.InvariantCulture);

            int upLevelExp = LevelExpManager.Instance.GetUpLevelExp(userDataBase.Level);
            int downLevelExp = LevelExpManager.Instance.GetDownLevelExp(userDataBase.Level);
            MyInfo myInfo = new MyInfo(PacketTypeList.SCNotiMyInfo.ToString(), userDataBase.FaceIdx, userDataBase.Exp, upLevelExp, downLevelExp,
                                       userDataBase.Stamina, userDataBase.maxStamina, nextStaminaTime.ToString(), winCount, totalCount, winRateStr,
                                       userDataBase.Level, userDataBase.nickName, userID, petList, badgeIdx, cardSkinIdx, itemAbilityList, petAbilityList, profileList);
            PacketsHandlerServer.SendSCNotiMyInfo(serverSession, myInfo);
        }

        public void SetEPElixirEvent()
        {
            epElixirTime.SetEvent(this);
        }

        internal void CheckEPElixirEvent()
        {
            epElixirTime.isSet = true;
            epElixirTime.endEventDate = userDataBase.epElixirEndEventDate;
            epElixirTime.CheckEvent(session);
        }

        internal void CheckSOElixirEvent()
        {
            soElixirTime.isSet = true;
            soElixirTime.endEventDate = userDataBase.soElixirEndEventDate;
            soElixirTime.CheckEvent(session);
        }
        public void SetSOElixirEvent()
        {
            soElixirTime.SetEvent(this);
        }

        internal List<int> SetPetList()
        {
            List<int> petList = new List<int>();
            if (userDataBase.userItem.petList != null)
            {
                foreach (var pet in userDataBase.userItem.petList)
                {
                    if (pet.isSelect) petList.Add(pet.petIdx);
                }
            }
            
            return petList;
        }

        private int SetBadge()
        {
            int badgeIdx = 0;
            if (userDataBase.userItem.badgeList != null)
            {
                foreach (var badge in userDataBase.userItem.badgeList)
                {
                    if (badge.isSelect) badgeIdx = badge.badgeIdx;
                }
            }

            return badgeIdx;
        }

        private int SetCardSkin()
        {
            int cardSkinIdx = 0;
            if (userDataBase.userItem.cardSkinList != null)
            {
                foreach (var cardSkin in userDataBase.userItem.cardSkinList)
                {
                    if (cardSkin.isSelect) cardSkinIdx = cardSkin.skinIdx;
                }
            }

            return cardSkinIdx;
        }

        internal void SetItemAbilityList(out List<ItemAbility> itemAbilityList)
        {
            itemAbilityList = new List<ItemAbility>();
            //if (epElixirTime.isSet == true)
            {
                TimeSpan epElixirEndSpan = new TimeSpan();
                string epElixirEndSpanStr = string.Empty;

                if (epElixirTime.endEventDate > DateTime.Now)
                {
                    epElixirEndSpan = epElixirTime.endEventDate - DateTime.Now;
                }

                epElixirEndSpanStr = string.Format("{0:00}:{1:00}:{2:00}", epElixirEndSpan.Hours, epElixirEndSpan.Minutes, epElixirEndSpan.Seconds);
                itemAbilityList.Add(new ItemAbility("+1 HP", "[" + epElixirEndSpanStr + "]"));
            }
            //if (soElixirTime.isSet == true)
            {
                TimeSpan sOElixirEndSpan = new TimeSpan();
                string sOElixirEndSpanStr = string.Empty;
                if (soElixirTime.endEventDate > DateTime.Now)
                {
                    sOElixirEndSpan = soElixirTime.endEventDate - DateTime.Now;
                }

                sOElixirEndSpanStr = string.Format("{0:00}:{1:00}:{2:00}", sOElixirEndSpan.Hours, sOElixirEndSpan.Minutes, sOElixirEndSpan.Seconds);
                itemAbilityList.Add(new ItemAbility("+1 SP", "[" + sOElixirEndSpanStr + "]"));
            }
        }
        
        internal void SetPetAbilityList(out List<PetAbility> petAbilityList)
        {
            petAbilityList = new List<PetAbility>();
            petAbilityList.Add(new PetAbility("PetCardAblity will be shown here..."));
        }

        internal void SetProfileList(out List<ProfileInfo> profileList)
        {
            profileList = new List<ProfileInfo>();
            profileList.Add(new ProfileInfo("winCount: ", this.userDataBase.userProfile.winCount.ToString()));
            profileList.Add(new ProfileInfo("loseCount: ", this.userDataBase.userProfile.loseCount.ToString()));
            profileList.Add(new ProfileInfo("drawCount: ", this.userDataBase.userProfile.drawCount.ToString()));
            profileList.Add(new ProfileInfo("friendCount: ", this.userDataBase.userProfile.friendsCount.ToString()));
        }

        internal void SetInvItems(out List<InventoryInfo> inventoryList)
        {
            int count = 0;
            inventoryList = new List<InventoryInfo>();

            List<ItemElementInfo> itemElementList = null;
            ItemManager.Instance.GetItemElementList(out itemElementList, TabType.Items);
            if (itemElementList != null)
            {
                foreach (ItemElementInfo itemInfo in itemElementList)
                {
                    switch (itemInfo.iDx)
                    {
                        case (int)UserItems.EPElixir:
                            count = userDataBase.userItem.ePElixir;
                            break;
                        //case (int)UserItems.EtherealInk:
                        //    count = userDataBase.userItem.etherealInk;
                        //    break;
                        case (int)UserItems.RosemaryOil:
                            count = userDataBase.userItem.rosemaryOil;
                            break;
                        case (int)UserItems.SaqqaraTablet:
                            count = userDataBase.userItem.saqqaraTablet;
                            break;
                        case (int)UserItems.SLElixir:
                            count = userDataBase.userItem.sLElixir;
                            break;
                        case (int)UserItems.SOElixir:
                            count = userDataBase.userItem.sOElixir;
                            break;
                        default: break;
                    }
                    inventoryList.Add(new InventoryInfo(itemInfo.iDx, count, false));
                }
            }
        }

        internal void SetInvCharacter(out List<InventoryInfo> inventoryList)
        {
            int count = 0;
            bool select = false;
            inventoryList = new List<InventoryInfo>();

            List<ItemElementInfo> itemElementList = null;
            ItemManager.Instance.GetItemElementList(out itemElementList, TabType.Character);
            if (itemElementList != null)
            {
                foreach (ItemElementInfo itemInfo in itemElementList)
                {
                    //initial faceIdx is 0-5
                    //itemInfo.elementIDx is 501-502...
                    if (userDataBase.Level >= itemInfo.rqrLevel)
                    {
                        count = 1;
                        if (itemInfo.iDx == userDataBase.FaceIdx) { select = true; }
                        else { select = false; }
                        inventoryList.Add(new InventoryInfo(itemInfo.iDx, count, select));
                    }
                }
            }
        }

        internal void SetInvCardSkin(out List<InventoryInfo> inventoryList)
        {
            inventoryList = new List<InventoryInfo>();
            bool select = false;
            inventoryList = new List<InventoryInfo>();
            if (userDataBase.userItem.cardSkinList != null)
            {
                ItemElementInfo cardSkinInfo = null;
                foreach (var cardSkin in userDataBase.userItem.cardSkinList)
                {
                    select = cardSkin.isSelect;
                    cardSkinInfo = ItemManager.Instance.GetElement(cardSkin.skinIdx);
                    if (cardSkinInfo != null)
                    {
                        inventoryList.Add(new InventoryInfo(cardSkin.skinIdx, 1, select));
                    }
                }
            }
        }
        internal void SetInvPet(out List<InventoryInfo> inventoryList)
        {
            //int count = 0;
            bool select = false;
            inventoryList = new List<InventoryInfo>();
            if (userDataBase.userItem.petList != null)
            {
                ItemElementInfo petInfo = null;
                foreach (var pet in userDataBase.userItem.petList)
                {
                    select = pet.isSelect;
                    petInfo = ItemManager.Instance.GetElement(pet.petIdx);
                    if (petInfo != null)
                    {
                        //if (userDataBase.Level >= petInfo.rqrLevel) 
                        //{ count = 1; }
                        //else { count = 0; }
                        inventoryList.Add(new InventoryInfo(pet.petIdx, 1, select));
                    }
                }
            }
        }

        internal void SetInvBadge(out List<InventoryInfo> inventoryList)
        {
            int count = 0;
            bool select = false;
            inventoryList = new List<InventoryInfo>();
            if (userDataBase.userItem.badgeList != null)
            {
                ItemElementInfo badgeInfo = null;
                foreach (var badge in userDataBase.userItem.badgeList)
                {
                    select = badge.isSelect;
                    badgeInfo = ItemManager.Instance.GetElement(badge.badgeIdx);
                    if (badgeInfo != null)
                    {
                        if (userDataBase.Level >= badgeInfo.rqrLevel)
                        { count = 1; }
                        else { count = 0; }
                        inventoryList.Add(new InventoryInfo(badge.badgeIdx, count, select));
                    }
                }
            }
        }

        internal void SetInvJokerFace(out List<InventoryInfo> inventoryList)
        {
            int count = 0;
            bool select = false;
            inventoryList = new List<InventoryInfo>();
            //...
            if (userDataBase.userItem.jokerFaceList != null)
            {
                foreach (var faceIdx in userDataBase.userItem.jokerFaceList)
                {
                    if (isJokerTry)
                    {
                        if (tmpJokerFace == faceIdx)  select = true; 
                        else select = false;
                    }
                    else
                    {
                        if (userDataBase.userJoker.faceIdx == faceIdx)  select = true; 
                        else select = false; 
                    }
                    inventoryList.Add(new InventoryInfo(faceIdx, count, select));
                }
            }
        }

        internal bool SetUserCharacter(int characterIdx_)
        {
            bool retvalue = false;
            ItemElementInfo characterInfo = null;
            characterInfo = ItemManager.Instance.GetElement(characterIdx_);
            if (characterInfo != null)
            {
                if (userDataBase.Level >= characterInfo.rqrLevel)
                {
                    userDataBase.FaceIdx = characterIdx_;
                    retvalue = true;
                }
                else
                {
                    PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.LowLevel);
                }
            }
            else
            {
                //Console.WriteLine("!!!Error: characterInfo is null, character index: " + characterIdx_);
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = string.Format("CharacterInfo is null, characterIdx is: {0}", characterIdx_);
                    GameServer._logger.Error(log);
                }
            }
            return retvalue;
        }

        internal bool SetUserCardSkin(int skinIdx_, bool use_)
        {
            bool retvalue = false;
            int cardSkinUseCount = GetCardSkinUseCount();

            ItemElementInfo cardSkinInfo = null;
            cardSkinInfo = ItemManager.Instance.GetElement(skinIdx_);
            if (cardSkinInfo != null)
            {
                bool rqrSkinExist = false;

                if (userDataBase.Level >= cardSkinInfo.rqrLevel)
                {
                    if (userDataBase.userItem.cardSkinList != null)
                    {
                        foreach (var cardSkin in userDataBase.userItem.cardSkinList)
                        {
                            if (cardSkin.skinIdx == skinIdx_)
                            {
                                rqrSkinExist = true;
                                break;
                            }
                            
                        }

                        if (rqrSkinExist == true)
                        {
                            foreach (var cardSkin in userDataBase.userItem.cardSkinList)
                            {
                                if (cardSkin.skinIdx == skinIdx_)
                                {
                                    if (use_ == true) cardSkin.isSelect = true;
                                    else cardSkin.isSelect = false;
                                    //cardSkin.isSelect = true;
                                    //retvalue = true;
                                    //break;
                                }
                                else
                                {
                                    cardSkin.isSelect = false;
                                    //retvalue = false;
                                }
                            }
                            retvalue = true;
                        }

                    }
                }
                else
                {
                    PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.LowLevel);
                }
            }
            else
            {
                //Console.WriteLine("!!!Error: cardSkinInfo is null, card index: " + skinIdx_);
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = string.Format("CardSkinInfo is null, cardIdx is: {0}", skinIdx_);
                    GameServer._logger.Error(log);
                }

            }
            return retvalue;
        }

        internal bool SetUserPet(int petIdx_, bool use_)
        {
            bool retvalue = false;
            int petUseCount = GetPetUseCount();
            if (use_ == true && petUseCount >= GlobalManager.MAX_PET)
            {
                //Console.WriteLine("!!!Error: UserPet has reached max count: " + petUseCount);
                PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.MaxPetCountAchieved);

                return false;
            }

            ItemElementInfo petInfo = null;
            petInfo = ItemManager.Instance.GetElement(petIdx_);
            if (petInfo != null)
            {
                if (userDataBase.Level >= petInfo.rqrLevel)
                {
                    if (userDataBase.userItem.petList != null)
                    {
                        foreach (var pet in userDataBase.userItem.petList)
                        {
                            if (pet.petIdx == petIdx_)
                            {
                                if (use_ == true) pet.isSelect = true;
                                else pet.isSelect = false;

                                retvalue = true;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Error: Set pet failed, higher level required...");
                    PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.LowLevel);

                }
            }
            else
            {
                //Console.WriteLine("!!!Error: petInfo is null, pet index: " + petIdx_);
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = string.Format("PetInfo is null, petIdx is: {0}", petIdx_);
                    GameServer._logger.Error(log);
                }
            }
            return retvalue;
        }

        internal bool SetUserBadge(int badgeIdx_, bool use_)
        {
            bool retvalue = false;
            int badgeUseCount = GetBadgeUseCount();

            ItemElementInfo badgeInfo = null;
            badgeInfo = ItemManager.Instance.GetElement(badgeIdx_);
            if (badgeInfo != null)
            {
                bool rqrBadgeExist = false;
                if (userDataBase.Level >= badgeInfo.rqrLevel)
                {
                    if (userDataBase.userItem.badgeList != null)
                    {
                        foreach (var badge in userDataBase.userItem.badgeList)
                        {
                            if (badge.badgeIdx == badgeIdx_)
                            {
                                rqrBadgeExist = true;
                                break;
                            }
                        }

                        if (rqrBadgeExist == true)
                        {
                            foreach (var badge in userDataBase.userItem.badgeList)
                            {
                                if (badge.badgeIdx == badgeIdx_)
                                {
                                    if (use_ == true) badge.isSelect = true;
                                    else badge.isSelect = false;
                                    //retvalue = true;
                                }
                                else
                                {
                                    badge.isSelect = false;
                                    //retvalue = false;
                                }
                            }
                            retvalue = true;
                        }

                    }
                }
                else
                {
                    //Console.WriteLine("Error: Set badge failed, higher level required...");
                    PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.LowLevel);

                }
            }
            else
            {
                //Console.WriteLine("!!!Error: badgeInfo is null, badge index: " + badgeIdx_);
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = string.Format("BadgeInfo is null, badgeIdx is: {0}", badgeIdx_);
                    GameServer._logger.Error(log);
                }
            }
            return retvalue;
        }

        internal bool SetUserJokerFace(int faceIdx_)
        {
            bool retvalue = false;

            if (userDataBase.userJoker.spellIdxList == null || userDataBase.userJoker.spellIdxList.Count == 0)
            {
                isJokerTry = true;

                //test first...
                if (userDataBase.userItem.jokerFaceList.Contains(faceIdx_))
                {
                    //userDataBase.userJoker.faceIdx = faceIdx_;
                    tmpJokerFace = faceIdx_;
                    tmpSpellIdxList = new List<int>();
                    tryInkCount = 0;
                    retvalue = true;
                }
            }
            else
            {
                //use item to reset joker..
            }

            return retvalue;
        }

        public int GetBadgeUseCount()
        {
            int retvalue = 0;
            if (userDataBase.userItem.badgeList != null)
            {
                foreach (var badge in userDataBase.userItem.badgeList)
                {
                    if (badge.isSelect == true) retvalue++;
                }
            }

            return retvalue;
        }

        public int GetPetUseCount()
        {
            int retvalue = 0;
            if (userDataBase.userItem.petList != null)
            {
                foreach (var pet in userDataBase.userItem.petList)
                {
                    if (pet.isSelect) retvalue++;
                }
            }

            return retvalue;
        }

        public int GetCardSkinUseCount()
        {
            int retvalue = 0;
            if (userDataBase.userItem.cardSkinList != null)
            {
                foreach (var cardSkin in userDataBase.userItem.cardSkinList)
                {
                    if (cardSkin.isSelect == true) retvalue++;
                }
            }
            return retvalue;
        }

        internal void SetUpgradeSpells(int currentSpellIdx, out SpellResult result)
        {
            //this.isTry = true;
            result = SpellResult.Unknown;
            if(tmpSpellIdxList == null) SetTmpSpellIdxList();

            SpellInfo spellInfo = SpellManager.Instance.GetSpellInfo(currentSpellIdx);
            if (spellInfo != null/* && tmpSpellIdxList != null*/)
            {
                if (tmpSpellIdxList.Contains(currentSpellIdx))
                {
                    for (int i = 0; i < tmpSpellIdxList.Count; i++)
                    {
                        if (tmpSpellIdxList[i] == currentSpellIdx)
                        {
                            if (spellInfo.nextSpellIdx != 0 && spellInfo.nextReqInk <= (userDataBase.Ink - this.tryInkCount))
                            {
                                tmpSpellIdxList[i] = spellInfo.nextSpellIdx;
                                this.tryInkCount += spellInfo.nextReqInk;
                                result = SpellResult.OK;
                            }
                            else
                            {
                                if (spellInfo.nextSpellIdx == 0)
                                {
                                    result = SpellResult.MaxSpellLevel;
                                }
                                else if (spellInfo.nextReqInk > (userDataBase.Ink - this.tryInkCount))
                                {
                                    result = SpellResult.LackInk;
                                }
                            }
                            break;
                        }
                    }
                }
                else
                {
                    foreach (var spellIdx in tmpSpellIdxList)
                    {
                        if ((spellIdx / 100) == (currentSpellIdx / 100))
                        {
                            if (spellIdx >= currentSpellIdx)
                            {
                                //client send a wrong index such as: 102 > 101
                                result = SpellResult.Fail;
                            }
                            break;
                        }
                    }
                    if (result != SpellResult.Fail)
                    {
                        //x01..
                        if (tmpSpellIdxList.Count < userDataBase.JokerLine)
                        {
                            if (spellInfo.nextReqInk > (userDataBase.Ink - this.tryInkCount))
                            {
                                result = SpellResult.LackInk;
                            }
                            else
                            {
                                tmpSpellIdxList.Add(spellInfo.nextSpellIdx);
                                this.tryInkCount += spellInfo.nextReqInk;
                                result = SpellResult.OK;
                            }
                        }
                        else { /*DO NOTHING...*/ result = SpellResult.Fail; }
                    }
                }
            }
        }

        internal void SetDowngradeSpells(int currentSpellIdx, out SpellResult result)
        {
            result = SpellResult.Fail;
            if(tmpSpellIdxList == null) SetTmpSpellIdxList();
            //bool include = false;
            SpellInfo spellInfo = SpellManager.Instance.GetSpellInfo(currentSpellIdx);
            if (spellInfo != null/* && tmpSpellIdxList != null*/)
            {
                if (tmpSpellIdxList.Contains(currentSpellIdx))
                {
                    for (int i = 0; i < tmpSpellIdxList.Count; i++)
                    {
                        //down to userdatebase.spell
                        if (tmpSpellIdxList[i] == currentSpellIdx)
                        {
                            //if (userDataBase.userJoker.spellIdxList != null)
                            //{
                                //foreach (var userSpellIdx in userDataBase.userJoker.spellIdxList)
                                //{
                                    //if (currentSpellIdx / 100 == userSpellIdx / 100)
                                    //{
                                        if (/*spellInfo.lastSpellIdx >= userSpellIdx &&*/ spellInfo.lastSpellIdx != 0)
                                        {
                                            tmpSpellIdxList[i] = spellInfo.lastSpellIdx;
                                            this.tryInkCount += spellInfo.lastReqInk;
                                            result = SpellResult.OK;
                                        }

                                        //include = true;
                                        //break;
                                    //}
                                //}
                           // }

                            //if (!include)
                            //{
                                //if (spellInfo.lastSpellIdx != 0)
                                //{
                                    if (spellInfo.lastSpellIdx % 100 == 0)
                                    {
                                        tmpSpellIdxList.RemoveAt(i);
                                    }
                                    //else
                                    //{
                                    //    tmpSpellIdxList[i] = spellInfo.lastSpellIdx;
                                    //}

                                    //this.tryInkCount += spellInfo.lastReqInk;
                                    //result = SpellResult.OK;
                                //}
                           // }
                            
                            //if (userDataBase.userJoker.spellIdxList.Contains && spellInfo.lastSpellIdx != 0)
                            break;
                        }
                    }
                }
            }
           
        }

        internal void UpdateFriendCount()
        {
            if (_myFriends != null)
            {
                userDataBase.userProfile.friendsCount = _myFriends.Count;

                //check achieve
                CheckAchieveValue(GameType.Unknown, AchieveType.Friends, userDataBase.userProfile.friendsCount);
                //check mission
                CheckMissionValue(GameType.Unknown, AchieveType.Friends, userDataBase.userProfile.friendsCount);
            }
        }

        internal bool ApplyUserJoker(/*out SpellResult result*/)
        {
            //result = SpellResult.Unknown;

            if (tmpJokerFace == 0 && tmpSpellIdxList == null) return false;

            isJokerTry = false;

            if (tmpJokerFace != 0) userDataBase.userJoker.faceIdx = tmpJokerFace; 

            //if (tmpSpellIdxList.Count > 0)
            //{
                userDataBase.userJoker.spellIdxList = new List<int>();
                foreach (var spellIdx in tmpSpellIdxList)
                {
                    userDataBase.userJoker.spellIdxList.Add(spellIdx);
                }
                tmpSpellIdxList = null;

                userDataBase.Ink -= this.tryInkCount;
                int itemCount = -this.tryInkCount;
                tryInkCount = 0;
                //result = SpellResult.OK;

                if (GameServer._isCloud == true)
                {
                    //item log
                    GlobalManager.Instance.LogItemInsert(serverSession, userID, userDataBase.nickName, (int)ElementType.Ink, ElementType.Ink.ToString(), itemCount, LogItemType.Use);
                }
            //}
            //else
            //{
            //    result = SpellResult.NoUpgrade;
            //}
            return true;
        }

        internal void CancelUserJoker()
        {
            //this.isTry = false;
            isJokerTry = false;
            this.tryInkCount = 0;
            tmpJokerFace = 0;
            //reset temp spell list
            //SetTmpSpellIdxList();
            tmpSpellIdxList = null;
        }

        internal void SetTmpSpellIdxList()
        {
            this.tmpSpellIdxList = new List<int>();
            if (userDataBase.userJoker.spellIdxList != null && userDataBase.userJoker.spellIdxList.Count > 0)
            {
                foreach (var spellIdx in userDataBase.userJoker.spellIdxList)
                {
                    if(!tmpSpellIdxList.Contains(spellIdx))
                        tmpSpellIdxList.Add(spellIdx);
                }
            }
        }

        //cmd area
        internal void CMDMail(string[] cmdArray)
        {
            //cmdArray[0] = @email
            //cmdArray[1] = itemIdx
            //cmdArray[2] = itemCount
            //cmdArray[3] = nick
            string msg = string.Empty;
            string content = string.Empty;

            //short form
            if (cmdArray.Length == 1 && cmdArray[0].ToLower() == "@mail")
            {
                msg = "Get a new mail successfully! ";
                //send a new mail...
                content = "SaqqaraTablet Item";
                SetSendNewMail("SYSTEM", /*"SYSTEM",*/ "Developer Required Mail", content, (int)UserItems.SaqqaraTablet, 1/*, 0, 0*/);
            }
            else
            {
                int number;
                if (!Int32.TryParse(cmdArray[2], out number) || number <= 0)
                {
                    msg = "item_count should be positive integer!";
                }
                else if (cmdArray.Length == 3)
                {
                    msg = userDataBase.nickName + " get a new mail successfully! ";
                    content = ((UserItems)(int.Parse(cmdArray[1]))).ToString();

                    //send a new mail
                    SetSendNewMail("SYSTEM", /*"SYSTEM",*/ "Developer Required Mail", content, int.Parse(cmdArray[1]), int.Parse(cmdArray[2])/*, 0, 0*/);
                }
                else if (cmdArray.Length != 4)
                {
                    msg = @"Cmd format incorrect! Enter ""@help"" for help.)";
                }
                else
                {
                    bool isUserOnline = false;
                    foreach (var infoPair in SessionIdManager.Instance)
                    {
                        UserInfo userInfo = infoPair.Value;
                        if (userInfo.userDataBase.nickName == cmdArray[3] && userInfo.userServerState != UserServerStatus.DisConnect)
                        {
                            isUserOnline = true;
                            msg = cmdArray[3] + " get a new mail successfully! ";
                            content = ((UserItems)(int.Parse(cmdArray[1]))).ToString();

                            //send a new mail
                            //userInfo = infoPair.Value;
                            userInfo.SetSendNewMail(userID,/* userDataBase.nickName,*/ "Developer Required Mail", content, int.Parse(cmdArray[1]), int.Parse(cmdArray[2])/*, 0, 0*/);
                            break;
                        }
                    }

                    if (isUserOnline == false)
                    {
                        msg = string.Format("{0} is offline, set ruby failed!", cmdArray[3]);
                    }
                }

            }
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);

        }

        internal void CMDRuby(string[] cmdArray)
        {
            //cmdArray[0] = @ruby
            //cmdArray[1] = @count
            //cmdArray[2] = @nick
            string msg = string.Empty;
            int count;
            if (!Int32.TryParse(cmdArray[1], out count) || count <= 0) { msg = "count should be positive integer!"; }
            else
            {
                if (cmdArray.Length == 2)
                {
                    SetElementCount(count, ref msg, "ruby");
                }
                else if (cmdArray.Length != 3) { msg = @"Cmd format incorrect! Enter ""@help"" for help."; }
                else
                {
                    bool isUserOnline = false;
                    foreach (var infoPair in SessionIdManager.Instance)
                    {
                        UserInfo userInfo = infoPair.Value;
                        if (userInfo.userDataBase.nickName == cmdArray[2] && userInfo.userServerState != UserServerStatus.DisConnect)
                        {
                            isUserOnline = true;

                            userInfo.SetElementCount(count, ref msg, "ruby");
                            break;
                        }
                    }

                    if (isUserOnline == false)
                    {
                        msg = string.Format("{0} is offline, set ruby failed!", cmdArray[2]);
                    }
                }
            }

            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }

        internal void CMDGold(string[] cmdArray)
        {
            //cmdArray[0] = @ruby
            //cmdArray[1] = @count
            //cmdArray[2] = @nick
            string msg = string.Empty;
            int count;
            if (!Int32.TryParse(cmdArray[1], out count) || count <= 0) { msg = "count should be positive integer!"; }
            else
            {
                if (cmdArray.Length == 2)
                {
                    SetElementCount(count, ref msg, "gold");
                }
                else if (cmdArray.Length != 3) { msg = @"Cmd format incorrect! Enter ""@help"" for help."; }
                else
                {
                    bool isUserOnline = false;
                    foreach (var infoPair in SessionIdManager.Instance)
                    {
                        UserInfo userInfo = infoPair.Value;
                        if (userInfo.userDataBase.nickName == cmdArray[2] && userInfo.userServerState != UserServerStatus.DisConnect)
                        {
                            isUserOnline = true;
                            userInfo.SetElementCount(count, ref msg, "gold");
                            break;
                        }
                    }

                    if (isUserOnline == false)
                    {
                        msg = string.Format("{0} is offline, set ruby failed!", cmdArray[2]);
                    }
                }
            }
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }

        private void SetElementCount(int count, ref string msg, string elementName)
        {
            if (count <= 0) { msg = "count should be positive integer!"; }
            else
            {
                int itemIdx = 0;
                string itemName = string.Empty;
                switch (elementName)
                {
                    case "ruby":
                        itemIdx = (int)ElementType.MoonRuby;
                        itemName = ElementType.MoonRuby.ToString();
                        userDataBase.Ruby += count;
                        break;
                    case "gold":
                        itemIdx = (int)ElementType.Gold;
                        itemName = ElementType.Gold.ToString();
                        userDataBase.Gold += count;
                        break;
                    case "ink":
                        itemIdx = (int)ElementType.Ink;
                        itemName = ElementType.Ink.ToString();
                        userDataBase.Ink += count;
                        break;
                    case "stamina":
                        itemIdx = (int)ElementType.Stamina;
                        itemName = ElementType.Stamina.ToString();
                        userDataBase.Stamina += count;
                        break;
                    default: break;
                }
                msg = string.Format("{0} get {1} {2} successfully! ", userDataBase.nickName, count, elementName);

                //send scnoti ElementInfo list again
                SetSendElementInfoList();
                
                //save userinfo
                //save items...
                //SaveUserInfo();

                if (GameServer._isCloud == true)
                {
                    //item log
                    GlobalManager.Instance.LogItemInsert(serverSession, userID, userDataBase.nickName, itemIdx, itemName, count, LogItemType.Reward);
                }
            }
        }

        internal void CMDStamina(string[] cmdArray)
        {
            //cmdArray[0] = @stamina
            //cmdArray[1] = @count
            //cmdArray[2] = @nick
            string msg = string.Empty;
            int count;
            if (!Int32.TryParse(cmdArray[1], out count ) || count < 0) { msg = "count should be positive integer!"; }
            else
            {
                if (cmdArray.Length == 2)
                {
                    SetElementCount(count, ref msg, "stamina");
                }
                else if (cmdArray.Length != 3) { msg = @"Cmd format incorrect! Enter ""@help"" for help."; }
                else
                {
                    bool isUserOnline = false;
                    foreach (var infoPair in SessionIdManager.Instance)
                    {
                        UserInfo userInfo = infoPair.Value;
                        if (userInfo.userDataBase.nickName == cmdArray[2] && userInfo.userServerState != UserServerStatus.DisConnect)
                        {
                            isUserOnline = true;
                            userInfo.SetElementCount(count, ref msg, "stamina");
                            break;
                        }
                    }

                    if (isUserOnline == false)
                    {
                        msg = string.Format("{0} is offline, set ruby failed!", cmdArray[2]);
                    }
                }
            }
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);

        }

        internal void CMDItem(string[] cmdArray)
        {
            //cmdArray[0] = @item
            //cmdArray[1] = @nick
            //cmdArray[2] = @item_index
            //cmdArray[3] = @item_count
            string msg = string.Empty;
            if (cmdArray.Length != 4) { msg = @"Cmd format incorrect! Enter ""@help"" for help.)"; }
            else
            {
                if (userDataBase.nickName == cmdArray[1])
                {
                    if (int.Parse(cmdArray[2]) != (int)UserItems.EPElixir && int.Parse(cmdArray[2]) != (int)UserItems.EtherealInk &&
                        int.Parse(cmdArray[2]) != (int)UserItems.RosemaryOil && int.Parse(cmdArray[2]) != (int)UserItems.SaqqaraTablet &&
                        int.Parse(cmdArray[2]) != (int)UserItems.SLElixir && int.Parse(cmdArray[2]) != (int)UserItems.SOElixir)
                    {
                        msg = "item_index is incorrect!";
                        PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                        return;
                    }

                    int number;
                    if (!Int32.TryParse(cmdArray[3], out number) || number <= 0) { msg = "item_count should be positive integer!"; }
                    else
                    {
                        msg = string.Format("Get {0} {1} successfully! ", number, (UserItems)(int.Parse(cmdArray[3])));
                        ChangeUserItemAmount(int.Parse(cmdArray[2]), int.Parse(cmdArray[3]));
                        //Send ItemInfoList again...
                        //SetSendItemInfoList();

                        if (GameServer._isCloud == true)
                        {
                            //item log
                            string itemName = ((UserItems)int.Parse(cmdArray[2])).ToString();
                            GlobalManager.Instance.LogItemInsert(serverSession, userID, userDataBase.nickName, int.Parse(cmdArray[2]), itemName, number, LogItemType.Reward);
                        }
                    }
                }
                else { msg = "Nickname is incorrect!"; }
            }
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }

        internal void CMDInk(string[] cmdArray)
        {
            //cmdArray[0] = @ink
            //cmdArray[1] = @count
            //cmdArray[2] = @nick
            string msg = string.Empty;
            int count;
            if (!Int32.TryParse(cmdArray[1], out count) || count <= 0) { msg = "count should be positive integer!"; }
            else
            {
                if (cmdArray.Length == 2)
                {
                    SetElementCount(count, ref msg, "ink");
                }
                else if (cmdArray.Length != 3) { msg = @"Cmd format incorrect! Enter ""@help"" for help."; }
                else
                {
                    bool isUserOnline = false;
                    foreach (var info in SessionIdManager.Instance)
                    {
                        UserInfo userInfo = info.Value;
                        if (userInfo.userDataBase.nickName == cmdArray[2] && userInfo.userServerState != UserServerStatus.DisConnect)
                        {
                            isUserOnline = true;
                            userInfo.SetElementCount(count, ref msg, "ink");

                            break;
                        }
                    }

                    if (isUserOnline == false)
                    {
                        msg = string.Format("{0} is offline, set ruby failed!", cmdArray[2]);
                    }
                }
            }

            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }

        internal void CMDLevel(string[] cmdArray)
        {
            //cmdArray[0] = @level
            //cmdArray[1] = @count
            //cmdArray[2] = @nick
            string msg = string.Empty;
            int level;
            if (!Int32.TryParse(cmdArray[1], out level) || level <= 0) { msg = "count should be positive integer!"; }
            else
            {
                if (cmdArray.Length == 2)
                {
                    SetLevel(level, ref msg);//mine
                }
                else if (cmdArray.Length != 3) { msg = @"Cmd format incorrect! Enter ""@help"" for help."; }
                else
                {
                    bool isUserOnline = false;
                    foreach (var info in SessionIdManager.Instance)
                    {
                        UserInfo userInfo = info.Value;
                        if (userInfo.userDataBase.nickName == cmdArray[2] && userInfo.userServerState != UserServerStatus.DisConnect)
                        {
                            isUserOnline = true;
                            userInfo.SetLevel(level, ref msg);//for other

                            break;
                        }
                    }

                    if (isUserOnline == false)
                    {
                        //test first
                        msg = string.Format("{0} is offline, set level failed!", cmdArray[2]);
                    }

                    //if (isUserOnline == false)
                    //{
                    //    //save user DB
                    //    //PacketsHandlerDB.SDReqUpdateUserLevel(serverSession, cmdArray[2], level);
                    //    return;
                    //}
                }
            }

            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);

            //SendSCNotiUserInfo
            int upLevelExp = LevelExpManager.Instance.GetUpLevelExp(userDataBase.Level);
            //int downLevelExp = LevelExpManager.Instance.GetDownLevelExp(userDataBase.Level);
            PacketsHandlerServer.SendSCNotiUserInfo(serverSession, userDataBase.FaceIdx, userDataBase.nickName, badgeIdx, userDataBase.Level, userDataBase.Exp, upLevelExp);
        }

        private void SetLevel(int level, ref string msg)
        {
            if (level <= 0) { msg = "count should be positive integer!"; }
            else if (level > 30) { msg = "level count can't be larger than 30!"; }
            else
            {
                int lastLevel = userDataBase.Level;
                userDataBase.Level = level;
                if (userDataBase.Level == lastLevel)
                {
                    msg = "Level was not changed!";
                    return;
                } 

                userDataBase.Exp = 0; //LevelExpManager.Instance.GetCurrentLevelExp(level);

                CheckAchieveValue(GameType.Unknown, AchieveType.Level, userDataBase.Level);
                CheckMissionValue(GameType.Unknown, AchieveType.Level, userDataBase.Level);

                msg = string.Format("Set {0}'s level to be {1} successfully! ", userDataBase.nickName, level);

                if (this.sceneType == SceneType.Home || this.sceneType == SceneType.Room || this.sceneType == SceneType.Mail)
                {
                    //send scnotiuserinfo again...
                    int upLevelExp = LevelExpManager.Instance.GetUpLevelExp(level);

                    PacketsHandlerServer.SendSCNotiUserInfo(serverSession, userDataBase.FaceIdx, userDataBase.nickName, badgeIdx, userDataBase.Level, userDataBase.Exp, upLevelExp);
                }
                else if (this.sceneType == SceneType.MyPage)
                {
                    //send scnotimyinfo again
                    SetSendMyInfo();
                }

                userDataBase.Ink += userDataBase.Level - lastLevel;
                if (userDataBase.Ink < 0) userDataBase.Ink = 0;

                LogItemType logItemType = LogItemType.Unknown;
                if (userDataBase.Level < lastLevel)
                {
                    logItemType = LogItemType.LevelDown;
                    SetJokerLine(logItemType);
                    CheckUserJoker();
                    CheckUserPet();
                    CheckUserBadge();
                }
                else
                {
                    logItemType = LogItemType.LevelUp;
                }

                SetJokerLine(logItemType);
                //save userinfo
                //save level(exclude), exp, ink or joker line
                SaveUserInfo();

                if (GameServer._isCloud == true)
                {
                    //level log
                    GlobalManager.Instance.LogLevelInsert(serverSession, userID, userDataBase.nickName, lastLevel, userDataBase.Level);

                    //item log
                    GlobalManager.Instance.LogItemInsert(serverSession, userID, userDataBase.nickName, (int)ElementType.Ink, ElementType.Ink.ToString(), userDataBase.Level - lastLevel, logItemType);
                }
            }
        }

        internal void CMDWin(string[] cmdArray)
        {
            //cmdArray[0] = @win
            //cmdArray[1] = @times
            //cmdArray[2] = @gametype
            string msg = string.Empty;
            GameType gameType = GlobalManager.DEFAULT_GAME_TYPE;
            PlayMode playMode = GlobalManager.DEFAULT_GAME_MODE;

            if (cmdArray.Length != 3 && cmdArray.Length != 2) { msg = @"Cmd format incorrect! Enter ""@help"" for help."; }
            else
            {
                int number;
                if (!Int32.TryParse(cmdArray[1], out number) || number <= 0) { msg = "times should be positive integer!"; }
                else
                {
                    bool operateSuccess = true;
                    if (cmdArray.Length == 3)
                    {
                        switch (cmdArray[2].ToLower())
                        {
                            case "co": gameType = GameType.Colmaret; break;
                            case "jo": gameType = GameType.Joha; break;
                            case "ch": gameType = GameType.Cheshire; break;
                            case "za": gameType = GameType.Zatoo; break;
                            case "ar": gameType = GameType.Armdree; break;
                            case "ta": gameType = GameType.Tarassen; break;
                            case "kr": gameType = GameType.Krazan; break;
                            case "ya": gameType = GameType.Yanan; break;
                            default:
                                msg = string.Format("game type: {0} is not exist!", cmdArray[2]);
                                operateSuccess = false;
                                break;
                        }
                    }

                    userDataBase.userProfile.winCount += number;
                    userDataBase.userProfile.conWinCount += number;
                    //check achievement..
                    switch (gameType)
                    {
                        case GameType.Armdree:
                            userDataBase.userProfile.ArConWinCount += number;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.ArConWinCount);
                            break;
                        case GameType.Cheshire:
                            userDataBase.userProfile.ChConWinCount += number;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.ChConWinCount);
                            break;
                        case GameType.Colmaret:
                            userDataBase.userProfile.CoConWinCount += number;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.CoConWinCount);
                            break;
                        case GameType.Joha:
                            userDataBase.userProfile.JoConWinCount += number;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.JoConWinCount);
                            break;
                        case GameType.Krazan:
                            userDataBase.userProfile.KrConWinCount += number;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.KrConWinCount);
                            break;
                        case GameType.Tarassen:
                            userDataBase.userProfile.TaConWinCount += number;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.TaConWinCount);
                            break;
                        case GameType.Yanan:
                            userDataBase.userProfile.YaConWinCount += number;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.YaConWinCount);
                            break;
                        case GameType.Zatoo:
                            userDataBase.userProfile.ZaConWinCount += number;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.ZaConWinCount);
                            break;
                        default: break;
                    }
                    CheckAchieveValue(GameType.Unknown, AchieveType.Win, number);
                    CheckAchieveValue(gameType, AchieveType.Win, number);
                    CheckAchieveValue(GameType.Unknown, AchieveType.ConsecutiveWin, userDataBase.userProfile.conWinCount);

                    //check mission..
                    MoonGlow.Room.Room room = null;
                    if (!string.IsNullOrEmpty(roomID))
                    {
                        room = GameServer.GetRoom(session);
                        if (room != null)
                        {
                            gameType = room.gameType;
                            playMode = room.playMode;
                        }
                    }
                    //special gameType
                    CheckMissionValue(gameType, AchieveType.Win, number);

                    //update userProfile..
                    int rankPoint = 0;
                    //int winCount = 0;
                    //int totalCount = 0;
                    //int winRate = 0;

                    //for (int i = 0; i < number; i++)
                    //{
                    //    userDataBase.userProfile.winCount++;

                            if (userDataBase.userProfile.chanelIdx != 0)
                            {
                                rankPoint = RewardManager.Instance.GetRankPoint(gameType, playMode);

                    //        winCount = userDataBase.userProfile.winCount;
                    //        totalCount = userDataBase.userProfile.winCount + userDataBase.userProfile.loseCount;
                    //        if (totalCount != 0) winRate = winCount * 100 / totalCount;

                    //        SetRankPoint(rankPoint + 0 - 10 + winRate);
                                SetRankPoint(rankPoint*number);
                                userDataBase.userProfile.rankWin += number;
                            }
                    //}

                    //check level
                    bool isLevelUp = false;
                    int upLevelExp = LevelExpManager.Instance.GetUpLevelExp(userDataBase.Level);

                    int lastLevel = userDataBase.Level;
                    if (userDataBase.Level < 30)
                    {
                        int remainExp = userDataBase.Exp + number;

                        while (remainExp >= upLevelExp && userDataBase.Level < 30)
                        {
                            userDataBase.Level++;
                            isLevelUp = true;
                            userDataBase.Exp = 0;

                            remainExp = remainExp - upLevelExp;
                            upLevelExp = LevelExpManager.Instance.GetUpLevelExp(userDataBase.Level);
                        }
                        userDataBase.Exp = remainExp;

                        if (userDataBase.Level >= 30) { userDataBase.Level = 30; userDataBase.Exp = 0; }
                    }

                    if (isLevelUp == true)
                    {
                        PacketsHandlerServer.SendSCNotiUserInfo(serverSession, userDataBase.FaceIdx, userDataBase.nickName, badgeIdx, userDataBase.Level, userDataBase.Exp, upLevelExp);
                            
                        userDataBase.Ink += userDataBase.Level - lastLevel;
                        SetJokerLine(LogItemType.LevelUp);//include item log

                        //check achievement..
                        CheckAchieveValue(GameType.Unknown, AchieveType.Level, userDataBase.Level);
                        //check mission -level..
                        CheckMissionValue(GameType.Unknown, AchieveType.Level, userDataBase.Level);

                        if (GameServer._isCloud == true)
                        {
                            //level log
                            GlobalManager.Instance.LogLevelInsert(serverSession, userID, userDataBase.nickName, lastLevel, userDataBase.Level);

                            //item log
                            GlobalManager.Instance.LogItemInsert(serverSession, userID, userDataBase.nickName, (int)ElementType.Ink, ElementType.Ink.ToString(), userDataBase.Level - lastLevel, LogItemType.LevelUp);
                        }
                    }

                    if (operateSuccess == true) msg = string.Format("Add win times:{0} successfully!", number);

                    //save userinfo
                    //save wincount, level(exclude), exp, joker line or ink...
                    SaveUserInfo();

                    if (GameServer._isCloud == true)
                    {
                        //play log
                        GlobalManager.Instance.LogPlayInsert(serverSession, userID, userDataBase.nickName, gameType, GlobalManager.DEFAULT_GAME_MODE, GlobalManager.DEFAULT_GAME_TIME, GlobalManager.DEFAULT_GAME_STAKE, GlobalManager.DEFAULT_GAME_DISCLOSE, GlobalManager.DEFAULT_GAME_LOCK, LogPlayType.End, string.Empty, Result.Win.ToString());
                        //log profile
                        GlobalManager.Instance.LogProfileInsert(serverSession, userID, userDataBase.nickName, userDataBase.userProfile.winCount, userDataBase.userProfile.loseCount, userDataBase.userProfile.drawCount, userDataBase.userProfile.rankWin, userDataBase.userProfile.rankLose, userDataBase.userProfile.rankDraw);
                    }
                }
            }
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }

        internal void CMDLose(string[] cmdArray)
        {
            //cmdArray[0] = @lose
            //cmdArray[1] = @times
            //cmdArray[2] = @gametype
            string msg = string.Empty;
            GameType gameType = GlobalManager.DEFAULT_GAME_TYPE;
            PlayMode playMode = GlobalManager.DEFAULT_GAME_MODE;

            if (cmdArray.Length != 2 && cmdArray.Length != 3) { msg = @"Cmd format incorrect! Enter ""@help"" for help."; }
            else
            {
                int number;
                if (!Int32.TryParse(cmdArray[1], out number) || number <= 0) { msg = "times should be positive integer!"; }
                else
                {
                    bool operateSuccess = true;
                    if (cmdArray.Length == 3)
                    {
                        switch (cmdArray[2].ToLower())
                        {
                            case "co": gameType = GameType.Colmaret; break;
                            case "jo": gameType = GameType.Joha; break;
                            case "ch": gameType = GameType.Cheshire; break;
                            case "za": gameType = GameType.Zatoo; break;
                            case "ar": gameType = GameType.Armdree; break;
                            case "ta": gameType = GameType.Tarassen; break;
                            case "kr": gameType = GameType.Krazan; break;
                            case "ya": gameType = GameType.Yanan; break;
                            default:
                                msg = string.Format("game type: {0} is not exist!", cmdArray[2]);
                                operateSuccess = false;
                                break;
                        }
                    }

                    userDataBase.userProfile.conWinCount = 0;
                    //check achievement..
                    switch (gameType)
                    {
                        case GameType.Armdree:
                            userDataBase.userProfile.ArConWinCount = 0;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.ArConWinCount);
                            break;
                        case GameType.Cheshire:
                            userDataBase.userProfile.ChConWinCount = 0;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.ChConWinCount);
                            break;
                        case GameType.Colmaret:
                            userDataBase.userProfile.CoConWinCount = 0;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.CoConWinCount);
                            break;
                        case GameType.Joha:
                            userDataBase.userProfile.JoConWinCount = 0;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.JoConWinCount);
                            break;
                        case GameType.Krazan:
                            userDataBase.userProfile.KrConWinCount = 0;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.KrConWinCount);
                            break;
                        case GameType.Tarassen:
                            userDataBase.userProfile.TaConWinCount = 0;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.TaConWinCount);
                            break;
                        case GameType.Yanan:
                            userDataBase.userProfile.YaConWinCount = 0;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.YaConWinCount);
                            break;
                        case GameType.Zatoo:
                            userDataBase.userProfile.ZaConWinCount = 0;
                            //check achievement..
                            CheckAchieveValue(gameType, AchieveType.ConsecutiveWin, userDataBase.userProfile.ZaConWinCount);
                            break;
                        default: break;
                    }
                    CheckAchieveValue(GameType.Unknown, AchieveType.Lose, number);
                    CheckAchieveValue(gameType, AchieveType.Lose, number);
                    CheckAchieveValue(GameType.Unknown, AchieveType.ConsecutiveWin, userDataBase.userProfile.conWinCount);
                       
                    //check mission..
                    MoonGlow.Room.Room room = null;
                    if (!string.IsNullOrEmpty(roomID))
                    {
                        room = GameServer.GetRoom(session);
                        if (room != null)
                        {
                            gameType = room.gameType;
                            playMode = room.playMode;
                        }
                    }
                    CheckMissionValue(gameType, AchieveType.Lose, number);

                    //update userProfile..

                    //int rankPoint = 1;
                    //int winCount = 0;
                    //int totalCount = 0;
                    //int winRate = 0;
                    //for (int i = 0; i < number; i++)
                    //{
                    //    //userDataBase.userProfile.loseCount += number;
                    //    userDataBase.userProfile.loseCount ++;
                        //if (userDataBase.userProfile.chanelIdx != 0)
                        //{
                        //    rankPoint = RewardManager.Instance.GetRankPoint(gameType, playMode);
                        //    winCount = userDataBase.userProfile.winCount;
                        //    totalCount = userDataBase.userProfile.winCount + userDataBase.userProfile.loseCount;
                        //    if (totalCount != 0) winRate = winCount * 100 / totalCount;

                        //    SetRankPoint((rankPoint + 0 - 10 + winRate) * 0.1f);
                        //}
                    //}

                    userDataBase.userProfile.loseCount+= number;
                    if (userDataBase.userProfile.chanelIdx != 0) userDataBase.userProfile.rankLose += number; 

                    //check level
                    bool isLevelDown = false;
                    int downLevelExp = LevelExpManager.Instance.GetDownLevelExp(userDataBase.Level);
                    int lastLevel = userDataBase.Level;
                    int remainExp = 0;
                    if (userDataBase.Level > 1)
                    {
                        remainExp = userDataBase.Exp - number;
                        while (remainExp <= downLevelExp && userDataBase.Level > 1)
                        {
                            userDataBase.Level--;
                            isLevelDown = true;
                            userDataBase.Exp = 0;

                            remainExp = remainExp - downLevelExp;
                            downLevelExp = LevelExpManager.Instance.GetDownLevelExp(userDataBase.Level);
                        }
                        if (userDataBase.Level < 1) { userDataBase.Level = 1; userDataBase.Exp = 0; }
                        downLevelExp = LevelExpManager.Instance.GetDownLevelExp(userDataBase.Level);
                    }
                    else if (userDataBase.Exp > downLevelExp) remainExp = userDataBase.Exp - number;

                    userDataBase.Exp = remainExp < downLevelExp ? downLevelExp : remainExp;

                    if (isLevelDown == true)
                    {
                        //later when downlevel work...
                        int upLevelExp = LevelExpManager.Instance.GetUpLevelExp(userDataBase.Level);
                        PacketsHandlerServer.SendSCNotiUserInfo(serverSession, userDataBase.FaceIdx, userDataBase.nickName, badgeIdx, userDataBase.Level, userDataBase.Exp, upLevelExp);
                        SetJokerLine(LogItemType.LevelDown);
                        userDataBase.Ink += userDataBase.Level - lastLevel;

                        CheckUserJoker();
                        CheckUserPet();
                        CheckUserBadge();

                        if (GameServer._isCloud == true)
                        {
                            //level log
                            GlobalManager.Instance.LogLevelInsert(serverSession, userID, userDataBase.nickName, lastLevel, userDataBase.Level);

                            //item log
                            GlobalManager.Instance.LogItemInsert(serverSession, userID, userDataBase.nickName, (int)ElementType.Ink, ElementType.Ink.ToString(), userDataBase.Level - lastLevel, LogItemType.LevelDown);
                        }
                    }
                    if (operateSuccess == true) msg = string.Format("Add lose times:{0} successfully!", number);

                    //save userinfo
                    //lose count, exp, level(exclude, not now)
                    SaveUserInfo();

                    if (GameServer._isCloud == true)
                    {
                        //play log
                        GlobalManager.Instance.LogPlayInsert(serverSession, userID, userDataBase.nickName, gameType, GlobalManager.DEFAULT_GAME_MODE, GlobalManager.DEFAULT_GAME_TIME, GlobalManager.DEFAULT_GAME_STAKE, GlobalManager.DEFAULT_GAME_DISCLOSE, GlobalManager.DEFAULT_GAME_LOCK, LogPlayType.End, string.Empty, Result.Lose.ToString());
                        //log profile
                        GlobalManager.Instance.LogProfileInsert(serverSession, userID, userDataBase.nickName, userDataBase.userProfile.winCount, userDataBase.userProfile.loseCount, userDataBase.userProfile.drawCount, userDataBase.userProfile.rankWin, userDataBase.userProfile.rankLose, userDataBase.userProfile.rankDraw);
                    }
                }
            }
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }

        internal void CMDFriend(string[] cmdArray)
        {
            //cmdArray[0] = @friend
            //cmdArray[1] = @friendIdx
            //cmdArray[2] = @sendDateTime
            string msg = string.Empty;
            if (cmdArray.Length != 3) { msg = @"Cmd format incorrect! Enter ""@help"" for help."; }
            else
            {
                if (_myFriends == null)
                {
                    msg = "friend list is null!";
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }
                if (int.Parse(cmdArray[1]) <= 0 || int.Parse(cmdArray[1]) > _myFriends.Count)
                {
                    msg = "friendIdx is incorrect!";
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }

                DateTime sendDate = new DateTime();
                if (GlobalManager.Instance.ConvertToDateTime(cmdArray[2], out sendDate))
                {
                    _myFriends[int.Parse(cmdArray[1]) - 1].itemSendDate = sendDate.ToString();
                    msg = string.Format("Change gift sendDateTime to {0} successfully!", sendDate.ToString());

                    //save to DB
                    FriendDBManager.Instance.UpdateGiftSendDate(session, _myFriends[int.Parse(cmdArray[1]) - 1].friendID, sendDate.ToString());
                }
                else { msg = "sendDate format is incorrect!"; }
            }
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }

        internal void CMDCon(string[] cmdArray)
        {
            //cmdArray[0] = @connections
            //send scnoticonnections
            int connections = 0;
            UserInfo userInfo2 = null;
            foreach (var infoPair in SessionIdManager.Instance)
            {
                userInfo2 = infoPair.Value;
                if (userInfo2.userServerState != UserServerStatus.DisConnect)
                {
                    connections++;
                }
            }
            string msg = string.Format("Connections: {0}", connections.ToString());
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }

        internal void CMDChannel(string[] cmdArray)
        {
            //cmdArray[0] = @chanel
            //cmdArray[1] = @chanelIdx
            //cmdArray[2] = @endDate
            string msg = string.Empty;
            if (cmdArray.Length != 3) { msg = @"Cmd format incorrect! Enter ""@help"" for help."; }
            else
            {
                bool isContain = false;
                foreach (var info in ChannelManager._items)
                {
                    if (info.chanelIdx == int.Parse(cmdArray[1]))
                    {
                        isContain = true;
                    }
                    break;
                }
                if (isContain == false)
                {
                    msg = @"chanelIdx is incorrect!";
                    return;
                }

                DateTime endDate = new DateTime();
                if (GlobalManager.Instance.ConvertToDateTime(cmdArray[2], out endDate))
                {
                    //_myFriends[int.Parse(cmdArray[1]) - 1].itemSendDate = sendDate.ToString();
                    foreach (var info in ChannelManager._items)
                    {
                        if (info.chanelIdx == int.Parse(cmdArray[1]))
                        {
                            msg = string.Format("Change chanel {0} endDate to {1} successfully! ", info.name, endDate.ToString());

                            //save to DB
                            PacketsHandlerInner.SDReqUpdateChanelInfoDate(serverSession, info.chanelIdx, endDate.ToString());

                            //reload chanel _items
                            //MoonGlow.Data.ChanelManager.Instance.ReadChanelInfo();
                            PacketsHandlerInner.SMReqBCReFreshChannelInfo(serverSession);

                            break;
                        }
                    }

                }
                else { msg = "endDate format is incorrect!"; }
            }
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }

        internal void CMDRepair(string[] cmdArray)
        {
            //cmdArray[0] = @repair
            //cmdArray[1] = @rank
            string msg = string.Empty;
            if (cmdArray.Length == 2)
            {
                if (cmdArray[1].ToLower() == "rank")
                {
                    PacketsHandlerInner.SDReqRepairChanel(serverSession);
                    return;
                }
                else { /*other items need to repair...*/}
            }

            msg = @"Cmd format incorrect! Enter ""@help"" for help.";
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            if (GameServer._logger.IsInfoEnabled)
            {
                string log = "RankRedis repaired";
                GameServer._logger.Info(log);
            }
        }

        internal void CMDDevref(string[] cmdArray)
        {
            if (cmdArray.Length == 1 && cmdArray[0] == "@devref")
            {
                PacketsHandlerInner.SMReqBCReFreshUserType(serverSession, UserType.All);
            }
            else
            {
                string msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }
        internal void CMDDev(string[] cmdArray)
        {
            string msg = string.Empty;
            //cmdArray[0] = @dev
            //cmdArray[1] = nick
            //cmdArray[2] = type[Operator: 1, OperatorManager: 2, Developer: 3, DeveloperManager: 4]
            if (cmdArray.Length != 3)
            {
                msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
            else
            {
                UserType userType = (UserType)(int.Parse(cmdArray[2]));

                PacketsHandlerInner.SDReqSetDeveloper(serverSession, cmdArray[1], userType);
            }
        }

        internal void CMDStart(string[] cmdArray)
        {
            if (cmdArray.Length == 1 && cmdArray[0].ToLower() == "@start")
            {
                PacketsHandlerInner.SMReqSetServerGlobalState(serverSession, "start");
            }
            else
            {
                string msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }

        internal void CMDStop(string[] cmdArray)
        {
            if (cmdArray.Length == 1 && cmdArray[0].ToLower() == "@stop")
            {
                PacketsHandlerInner.SMReqSetServerGlobalState(serverSession, "stop");
            }
            else
            {
                string msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }

        internal void CMDDisable(string[] cmdArray)
        {
            //@disable
            //need change- disable index / disable all
            string msg = string.Empty;
            if (cmdArray.Length == 2 && cmdArray[0].ToLower() == "@disable")
            {
                bool enable = false;
                if (cmdArray[1].ToLower() == "all" || cmdArray[1].ToLower() == "all")
                {
                    //get all server state
                    PacketsHandlerInner.SMReqBCUpdateAllServerEnable(serverSession, enable);
                    return;
                }

                if (GameServer._allServerList == null)
                {
                    msg = "Pls check server state first.";
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }

                int serverIndex = 0;
                if (!Int32.TryParse(cmdArray[1], out serverIndex) || serverIndex <= 0 || serverIndex > GameServer._allServerList.Count)
                {
                    msg = string.Format("index should be positive integer and smaller than {0}", GameServer._allServerList.Count);
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }

                string[] address = GameServer._allServerList[serverIndex - 1].url.Split(':');
                if (address.Length != 2)
                {
                    msg = string.Format("Server IP: {0} is strange, pls check this server.", GameServer._allServerList[serverIndex - 1].url);
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }

                PacketsHandlerInner.SMReqBCUpdateServerEnable(serverSession, address[0], int.Parse(address[1]), enable);
                return;
            }
            else
            {
                msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }

        internal void CMDEnable(string[] cmdArray)
        {
            //@disable
            //need change- disable index / disable all
            string msg = string.Empty;
            if (cmdArray.Length == 2 && cmdArray[0].ToLower() == "@enable")
            {
                bool enable = true;
                if (cmdArray[1].ToLower() == "all" || cmdArray[1].ToLower() == "all")
                {
                    //get all server state
                    PacketsHandlerInner.SMReqBCUpdateAllServerEnable(serverSession, enable);
                    return;
                }

                if (GameServer._allServerList == null)
                {
                    msg = "Pls check server state first.";
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }

                int serverIndex = 0;
                if (!Int32.TryParse(cmdArray[1], out serverIndex) || serverIndex <= 0 || serverIndex > GameServer._allServerList.Count)
                {
                    msg = string.Format("index should be positive integer and smaller than {0}", GameServer._allServerList.Count);
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }

                string[] address = GameServer._allServerList[serverIndex - 1].url.Split(':');
                if (address.Length != 2)
                {
                    msg = string.Format("Server IP: {0} is strange, pls check this server.", GameServer._allServerList[serverIndex - 1].url);
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }

                PacketsHandlerInner.SMReqBCUpdateServerEnable(serverSession, address[0], int.Parse(address[1]), enable);
                return;
            }
            else
            {
                msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }

        internal void CMDState(string[] cmdArray)
        {
            //@state
            if (cmdArray.Length == 1 && cmdArray[0].ToLower() == "@state")
            {
                //get all server state: name ip connections state
                PacketsHandlerInner.SMReqGetAllServerInfo(serverSession);
            }
            else
            {
                string msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }

        internal void CMDMe(string[] cmdArray)
        {
            //@me
            if (cmdArray.Length == 1 && cmdArray[0].ToLower() == "@me")
            {
                //index ip:xxx.xxx.xxx.xxx:9000 type:Game, ver:1.11.6.2, con:x, state:enable
                PacketsHandlerInner.SMReqGetThisServerInfo(serverSession);
            }
            else
            {
                string msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }

        internal void CMDMove(string[] cmdArray)
        {
            //@move index
            string msg = string.Empty;
            if (cmdArray.Length == 2 && cmdArray[0].ToLower() == "@move")
            {
                if (GameServer._allServerList == null)
                {
                    msg = "Pls check server state first.";
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }

                int serverIndex = 0;
                if (!Int32.TryParse(cmdArray[1], out serverIndex) || serverIndex <= 0 || serverIndex > GameServer._allServerList.Count)
                {
                    msg = string.Format("index should be positive integer and smaller than {0}", GameServer._allServerList.Count);
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }

                string[] address = GameServer._allServerList[serverIndex - 1].url.Split(':');
                if (address.Length != 2)
                {
                    msg = string.Format("Server IP: {0} is strange, pls check this server.", GameServer._allServerList[serverIndex - 1].url);
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }

                //change game server
                string _connectUrl = address[0] + ":" + address[1];
                PacketsHandlerInner.SMReqChangeServer(serverSession, session, _connectUrl);
            }
            else
            {
                msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }

        internal void CMDNoti(string[] cmdArray, string message)
        {
            string msg = string.Empty;
            if (cmdArray.Length > 1)
            {
                if (cmdArray[0].ToLower() == "@noti")
                {
                    //message
                    //int cmdLength = cmdArray[0].Length;
                    PacketsHandlerInner.SMReqBCServerNoti(serverSession, message);

                    msg = "Published a message successfully.";
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }

                int serverIndex = 0;
                if (Int32.TryParse(cmdArray[0].ToLower().Substring("@noti".Length), out serverIndex))
                {
                    if (GameServer._allServerList == null)
                    {
                        msg = "Pls check server state first.";
                        PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                        return;
                    }

                    if (serverIndex <= 0 || serverIndex > GameServer._allServerList.Count)
                    {
                        msg = string.Format("index should be positive integer and smaller than {0}", GameServer._allServerList.Count);
                        PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                        return;
                    }

                    string[] address = GameServer._allServerList[serverIndex - 1].url.Split(':');
                    if (address.Length != 2)
                    {
                        msg = string.Format("Server IP: {0} is strange, pls check this server.", GameServer._allServerList[serverIndex - 1].url);
                        PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                        return;
                    }

                    PacketsHandlerInner.SMReqBCServerNoti(serverSession, message);
                    msg = "Published a message successfully.";
                }
                else { msg = @"Cmd format incorrect! Enter ""@help"" for help."; }
            }
            else { msg = @"Cmd format incorrect! Enter ""@help"" for help."; }

            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }

        internal void CMDRefresh(string[] cmdArray)
        {
            //@refresh server, channel, version, localization, news
            if (cmdArray.Length == 2)
            {
                if (cmdArray[1].ToLower() == "server")
                {
                    PacketsHandlerInner.SMReqBCReFreshServerList(serverSession);
                    return;
                }

                if (cmdArray[1].ToLower() == "league")
                {
                    PacketsHandlerInner.SMReqBCReFreshChannelInfo(serverSession);
                    return;
                }

                if (cmdArray[1].ToLower() == "version")
                {
                    //GameServer.GetClientVersions();
                    PacketsHandlerInner.SMReqBCReFreshClientVersions(serverSession);
                    return;
                }

                if (cmdArray[1].ToLower() == "localization")
                {
                    PacketsHandlerInner.SMReqBCReFreshLocalization(serverSession);
                    return;
                }

                if (cmdArray[1].ToLower() == "news")
                {
                    PacketsHandlerInner.SMReqBCReFreshNews(serverSession);
                    return;
                }

                //mission
                if (cmdArray[1].ToLower() == "mission")
                {
                    PacketsHandlerInner.SMReqBCReFreshMission(serverSession);
                    return;
                }
            }
            else
            {
                string msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }

        }

        internal void CMDAlarm(string[] cmdArray)
        {
            //@alarm news(shop) [now, 2011-01-01, 2011-01-01T00:39:00]
            string msg = string.Empty;
            if (cmdArray.Length == 3 && cmdArray[0].ToLower() == "@alarm" && (cmdArray[1].ToLower() == "shop" || cmdArray[1].ToLower() == "news"))
            {
                DateTime alarmDate = new DateTime();
                string alarmString = string.Empty;
                if (cmdArray[2].ToLower() == "now")
                {
                    alarmDate = DateTime.Now;
                }
                else if (!GlobalManager.Instance.ConvertToDateTime(cmdArray[2], out alarmDate))
                {
                    msg = string.Format("Datetime: '{0}' is not in the proper  format.", cmdArray[2]);
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                    return;
                }

                alarmString = alarmDate.ToString("yyyy-MM-dd HH:mm:ss");

                //not sure if need broadcast or not
                //MenuAlarm menuAlarm = null;
                if (cmdArray[1].ToLower() == "shop")
                {
                    PacketsHandlerInner.SMReqBCAlarmShop(serverSession, alarmString);
                }
                if (cmdArray[1].ToLower() == "news")
                {
                    PacketsHandlerInner.SMReqBCAlarmNews(serverSession, alarmString);
                }
            }
            else
            {
                msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }

        }

        internal void CMDLimit(string[] cmdArray)
        {
            //@limit #
            string msg = string.Empty;
            if (cmdArray.Length == 2 && cmdArray[0].ToLower() == "@limit")
            {
                int count = 0;
                if (!Int32.TryParse(cmdArray[1], out count) || count <= 0) msg = "Minutes should be positive integer!";
                else
                {
                    PacketsHandlerInner.SMReqBCUpdateLimit(serverSession, count);
                    return;
                }
            }
            else  msg = @"Cmd format incorrect! Enter ""@help"" for help.";
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);

        }

        internal void CMDBan(string[] cmdArray)
        {
            //@ban nick minutes / @ban(@release) nick
            //later...
            string msg = string.Empty;
            bool ban = true;

            if (cmdArray.Length == 2 && cmdArray[0].ToLower() == "@ban")
            {
                string userNick = cmdArray[1];

                PacketsHandlerInner.SDReqUpdateUserBan(serverSession, userNick, ban, 0);
            }
            else if (cmdArray.Length == 3 && cmdArray[0].ToLower() == "@ban")
            {
                string userNick = cmdArray[1];
                int minutes = 0;
                if (!Int32.TryParse(cmdArray[2], out minutes) || minutes <= 0)
                {
                    msg = "Minutes should be positive integer!";
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                }
                else
                {
                    PacketsHandlerInner.SDReqUpdateUserBan(serverSession, userNick, ban, minutes);
                }
                //else PacketsHandlerInner.SMReqBanUser(serverSession, userID, minutes); //ADD TO REDIS -BAN USER LSIT
            }
            else
            {
                msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }

        internal void CMDRelease(string[] cmdArray)
        {
            //@ban nick minutes / @ban(@release) nick
            //later...
            string msg = string.Empty;
            if (cmdArray.Length == 2 && cmdArray[0].ToLower() == "@release")
            {
                bool ban = false;
                string userNick = cmdArray[1];
                PacketsHandlerInner.SDReqUpdateUserBan(serverSession, userNick, ban, 0);
            }
            else
            {
                msg = @"Cmd format incorrect! Enter ""@help"" for help.";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }

        internal void CMDEffect(string[] cmdArray)
        {
            string msg = string.Empty;
            if (cmdArray.Length == 2 && cmdArray[0].ToLower() == "@effect")
            {
                if (cmdArray[1].ToLower() == "ruby")
                {
                    PacketsHandlerServer.SendSCNotiGetADItem(serverSession, (int)ElementType.MoonRuby, 1, EffectPosition.PlayCenter);
                    msg = "send packet: SCNotiGetADItem - Ruby successfully.";
                }
                else if (cmdArray[1].ToLower() == "gold")
                {
                    PacketsHandlerServer.SendSCNotiGetADItem(serverSession, (int)ElementType.Gold, 1, EffectPosition.PlayCenter);
                    msg = "send packet: SCNotiGetADItem - Gold successfully.";
                }
                else
                {
                    msg = "only ruby or gold effect can be asked.";
                }
            }
            else
            {
                msg = @"Cmd format incorrect! Enter ""@help"" for help.";
            }

            if (!string.IsNullOrEmpty(msg))
            {
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }

        internal void CMDWhisper(string[] cmdArray, string chatMsg)
        {
            //#nick msg
            string msg = string.Empty;
            if (cmdArray[0][0] == '#')
            {
                bool isUserExit = false;
                string userNick = cmdArray[0].Substring(1);
                //need broadcast?
                foreach (var infoPair in SessionIdManager.Instance)
                {
                    if (infoPair.Value.userDataBase.nickName == userNick)
                    {
                        isUserExit = true;
                        string message = chatMsg.Substring(cmdArray[0].Length + 1);
                        PacketsHandlerServer.SendSCNotiChat(infoPair.Value.serverSession, userDataBase.nickName, 0, 0, message, 0);
                        msg = string.Format("Send whisper to {0} successfully.", userNick);
                        break;
                    }
                }
                if (!isUserExit)
                {
                    msg = string.Format("User: {0} is offline or not exist.", userNick);
                }
            }
            else
            {
                msg = @"Cmd format incorrect! Enter ""@help"" for help.";
            }

            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);

        }
        //cmd above

        public void CheckUserJoker()
        {
            if (userDataBase.userJoker.spellIdxList != null)
            {
                if (userDataBase.userJoker.spellIdxList.Count > userDataBase.JokerLine)
                {
                    //reset joker
                    userDataBase.userJoker.faceIdx = 0;
                    userDataBase.userJoker.spellIdxList = new List<int>();
                    userDataBase.Ink = GlobalManager.DEFAULT_INIT_INK + userDataBase.Level - 1;
                    isJokerTry = false;
                }
                else
                {
                    SpellInfo spellInfo = null;
                    int usedInk = 0;
                    foreach (var spellIdx in userDataBase.userJoker.spellIdxList)
                    {
                        spellInfo = SpellManager.Instance.GetSpellInfo(spellIdx);
                        if (spellInfo != null)
                        {
                            usedInk += spellInfo.reqInk;
                        }
                    }

                    if (usedInk > /*userDataBase.Ink*/userDataBase.Level + GlobalManager.DEFAULT_INIT_INK -1)
                    {
                        //reset joker
                        userDataBase.userJoker.faceIdx = 0;
                        userDataBase.userJoker.spellIdxList = new List<int>();
                        userDataBase.Ink = GlobalManager.DEFAULT_INIT_INK + userDataBase.Level - 1;
                        isJokerTry = false;
                    }
                }
            }
        }

        public void CheckUserPet()
        {
            if (userDataBase.userItem.petList == null) return;
            ItemElementInfo info = null;
            foreach (var pet in userDataBase.userItem.petList)
            {
                if (pet.isSelect)
                {
                    info = ItemManager.Instance.GetElement(pet.petIdx);
                    if(info != null)
                    {
                        if (info.rqrLevel > userDataBase.Level)  pet.isSelect = false;
                    }
                }
            }

            if (sceneType == SceneType.MyPage) SetSendMyInfo(); 
           
        }

        public void CheckUserBadge()
        {
            if (userDataBase.userItem.badgeList == null) return;
            ItemElementInfo info = null;
            foreach (var badge in userDataBase.userItem.badgeList)
            {
                if (badge.isSelect == true)
                {
                    info = ItemManager.Instance.GetElement(badge.badgeIdx);
                    if (info != null)
                    {
                        if (info.rqrLevel > userDataBase.Level) badge.isSelect = false;
                    }
                }
            }

            if (sceneType == SceneType.MyPage)
            {
                SetSendMyInfo();

            } 
            int upLevelExp = MoonGlow.Data.LevelExpManager.Instance.GetUpLevelExp(userDataBase.Level);
            PacketsHandlerServer.SendSCNotiUserInfo(serverSession, userDataBase.FaceIdx, userDataBase.nickName, badgeIdx, userDataBase.Level, userDataBase.Exp, upLevelExp);
        }

        internal void CheckAchieveActive()
        {
            bool needUpdate = false;
            Dictionary<int, AchieveInfo> achieveListDic = AchieveManager.Instance.GetAchieveListDic();
            if (achieveListDic != null)
            {
                bool isUserGot = false;
                AchieveInfo info = null;
                foreach (var infoPair in achieveListDic)
                {
                    info = infoPair.Value;
                    isUserGot = false;
                    foreach (var value in userDataBase.achieveValue)
                    {
                        if (info.iDx / 100 == value.achieveIdx / 100)
                        {
                            //same team
                            isUserGot = true;
                            break;
                        }
                    }
                    if (!isUserGot && (info.iDx % 100 == 0))
                    {
                        userDataBase.achieveValue.Add(new AchieveValue(info.iDx, 0, 0, false));
                        if (!needUpdate) needUpdate = true; 
                    }
                }
            }

            //if (needUpdate)
            //{
            //    SaveUserInfo();
            //}
        }

        internal void GetAchieveList(out List<Achievement> achieveList)
        {
            achieveList = new List<Achievement>();

            Dictionary<int, AchieveInfo> achieveListDic = AchieveManager.Instance.GetAchieveListDic();
            if (achieveListDic != null)
            {
                AchieveInfo info = null;
                foreach (var value in userDataBase.achieveValue)
                {
                    foreach (var infoPair in achieveListDic)
                    {
                        info = infoPair.Value;
                        if (info.iDx == value.achieveIdx)
                        {
                            achieveList.Add(new Achievement(info.iDx, value.currentValue, info.targetValue));
                            break;
                        }
                    }
                }

            }
        }

        internal void CheckAchieveValue(GameType gameType_, AchieveType achieveType_, int number_)
        {
            AchieveType _achieveType = AchieveType.Unknown;
            GameType _gameType = GameType.Unknown;
            foreach (var value in userDataBase.achieveValue)
            {
                _achieveType = AchieveManager.Instance.GetAchieveType(value.achieveIdx);
                _gameType = AchieveManager.Instance.GetGameType(value.achieveIdx);

                if (_gameType == gameType_ && _achieveType == achieveType_)
                {
                    if (value.isDone == false)
                    {
                        SetAchieveValue(/*gameType_, _achieveType*/value.achieveIdx, number_);
                    }
                    //break;//can not brea;. because different achieve group may may have same achieveType
                }
            }
        }

        private void SetAchieveValue(/*GameType gameType_, AchieveType achieveType_*/int achieveIdx_, int number_)
        {
            Dictionary<int,AchieveInfo> achieveListDic = AchieveManager.Instance.GetAchieveListDic();
            if (achieveListDic == null)
            {
                //Console.WriteLine("!Error achieveListDic is null!!!");
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = "AchieveListDic is null";
                    GameServer._logger.Error(log);
                }
                return;
            }

            foreach (var value in userDataBase.achieveValue)
            {
                if (/*AchieveManager.Instance.GetAchieveType(value.achieveIdx) == achieveType_*/ value.achieveIdx == achieveIdx_)
                {
                    AchieveType _achieveType = AchieveManager.Instance.GetAchieveType(value.achieveIdx);
                    //value.currentValue++;
                    switch (/*achieveType_*/_achieveType)
                    {
                        case AchieveType.Level:
                        case AchieveType.Friends:
                        case AchieveType.CheckIn:
                        case AchieveType.Tutorial:
                        case AchieveType.ConsecutiveWin:
                        case AchieveType.ConCheckIn:
                            value.currentValue = number_;

                            break;
                        case AchieveType.Win:
                        case AchieveType.Lose:
                        case AchieveType.ItemUseCount:
                        case AchieveType.EPElixirUseCount:
                        case AchieveType.EtherealInkUseCount:
                        case AchieveType.RosemaryOilUseCount:
                        case AchieveType.SLElixirUseCount:
                        case AchieveType.SOElixirUseCount:
                        case AchieveType.AllJokerFace:
                            value.currentValue += number_;

                            break;
                        default: break;
                    }

                    int lastAchieveIdx = 0;
                    foreach (var achevePair in achieveListDic)
                    {
                        if (achevePair.Value.iDx/100/*achieveType*/ == /*achieveType_*/achieveIdx_/100)//check gametype or idx/100 team also
                        {
                            if (value.currentValue >= achieveListDic[value.achieveIdx].targetValue)
                            {
                                lastAchieveIdx = value.achieveIdx;
                                value.achieveIdx++;
                                if (value.achieveLevel < achieveListDic[value.achieveIdx].currentStar)
                                {
                                    //send email
                                    string subject = LocalizationKey.ACHIEVE_GIFT_SUBJECT.ToString();
                                    string content = string.Empty;
                                    SetContent(out content, achieveListDic[lastAchieveIdx].itemIdx1);
                                    SetSendNewMail("SYSTEM",/* "SYSTEM",*/ subject, content, achieveListDic[lastAchieveIdx].itemIdx1, achieveListDic[lastAchieveIdx].itemCount1);

                                    if (achieveListDic[lastAchieveIdx].itemIdx2 != 0 && achieveListDic[lastAchieveIdx].itemCount2 != 0)
                                    {
                                        SetContent(out content, achieveListDic[lastAchieveIdx].itemIdx2);
                                        SetSendNewMail("SYSTEM", /*"SYSTEM",*/ subject, content, achieveListDic[lastAchieveIdx].itemIdx2, achieveListDic[lastAchieveIdx].itemCount2);
                                    }

                                    value.achieveLevel = achieveListDic[value.achieveIdx].currentStar;
                                }


                                if (achieveListDic[value.achieveIdx].currentStar == achieveListDic[value.achieveIdx].maxStar)
                                {
                                    value.currentValue = achieveListDic[value.achieveIdx].targetValue;
                                    value.isDone = true;
                                    break;
                                }
                            }

                            //continue win and friend
                            if (value.achieveIdx % 100 > 0)
                            {
                                if (value.currentValue < achieveListDic[value.achieveIdx - 1].targetValue) value.achieveIdx--;
                            }
                        }
                    }
                    break;
                }
            }
        }

        public void SetContent(out string content, int itemIdx)
        {
            content = string.Empty;
            TabType tabType = ItemManager.Instance.GetTabType(itemIdx);

            switch (tabType)
            {
                //3
                case TabType.Gold:
                    content = LocalizationKey.QUEST_GOLD_CONTENT.ToString(); break;
                //4
                case TabType.Ruby:
                    content = LocalizationKey.QUEST_RUBY_CONTENT.ToString(); break;
                //items:1
                case TabType.Items:
                    content = "ITEM_" + ((UserItems)(itemIdx)).ToString().ToUpper(); break;
                //badge: 7
                case TabType.Badge:
                    content = LocalizationKey.QUEST_BADGE_CONTENT.ToString(); break;
                //pet: 6
                case TabType.Pet:
                    content = LocalizationKey.QUEST_PET_CONTENT.ToString(); break;
                //cardSkin: 8...
                default:
                    if(itemIdx == (int)ElementType.Gold) content = LocalizationKey.QUEST_GOLD_CONTENT.ToString();
                    if(itemIdx == (int)ElementType.MoonRuby) content = LocalizationKey.QUEST_RUBY_CONTENT.ToString();
                    break;
            }
        }

        internal void GetMissionList(out List<Mission> missionList)
        {
            missionList = new List<Mission>();
            List<MissionInfo> missionInfoList = MissionManager.Instance.GetMissionList();
            if (missionInfoList == null)
            {
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = "MissionInfo List is null";
                    GameServer._logger.Error(log);
                }
                return;
            }
            //check expireTime...
            CheckMissionDate();

            string missionTitle = string.Empty;
            string missionDescription = string.Empty;
            foreach (var value in userDataBase.missionValue)
            {
                foreach (var info in missionInfoList)
                {
                    if (value.missionIdx == info.iDx)
                    {
                        TimeSpan timeSpan = new TimeSpan();
                        if (GlobalManager.Instance.ConvertToTimeSpan(info.timeSpan, out timeSpan))
                        {
                            DateTime startDate = new DateTime();
                            startDate = (value.expireDate - timeSpan).ToUniversalTime();
                            DateTime endDate = value.expireDate.ToUniversalTime();

                            missionTitle = (LocalizationManager._dict.ContainsKey(info.title))?
                                            LocalizationManager.Instance.Get(info.title, language) : info.title;
                            missionDescription = (LocalizationManager._dict.ContainsKey(info.description))?
                                            LocalizationManager.Instance.Get(info.description, language) : info.description;
                            missionList.Add(new Mission(info.missionType,/* info.title*/missionTitle, missionDescription/*info.description*/, 
                                                        startDate.ToString(), /*value.expireDate*/endDate.ToString(), value.currentValue, info.targetValue, 
                                                        info.itemIdx1, info.itemCount1, info.itemIdx2, info.itemCount2, value.state));
                        }
                        break;
                    }
                }
            }
        }

        private void CheckRemoveMission()
        {
            List<int> tmpMissionIdx = new List<int>();
            TimeSpan interval = new TimeSpan(1,0,0,0);
            foreach(var value in userDataBase.missionValue)
            {
                if (value.state == MissionState.Complete)
                {
                    if (DateTime.Now > value.completeDate.Date + interval)
                    {
                        tmpMissionIdx.Add(value.missionIdx);
                    }
                }
                else
                {
                    if (value.state == MissionState.Fail)
                    {
                        if (DateTime.Now > value.expireDate + interval)
                        {
                            tmpMissionIdx.Add(value.missionIdx);
                        }
                    }
                }
            }

            if (tmpMissionIdx.Count > 0)
            {
                bool isRemoved = false;
                for (int i = userDataBase.missionValue.Count - 1; i >= 0; i--)
                {
                    foreach(int missionIdx in tmpMissionIdx)
                    {
                        if (userDataBase.missionValue[i].missionIdx == missionIdx)
                        {
                            userDataBase.missionValue.RemoveAt(i);
                            isRemoved = true;
                            break;
                        }
                    }
                }
                if (isRemoved == true)
                {
                    //save userInfo
                    //save mission
                    SaveUserInfo();
                }
            }
        }

        internal void CheckMissionActive()
        {
            CheckMissionDate();

            List<MissionInfo> missionList = MissionManager.Instance.GetMissionList();
            if (missionList != null) /*Console.WriteLine();*/
            {
                bool isExist = false;
                foreach (var info in missionList)
                {
                    isExist = false;
                    foreach (var value in userDataBase.missionValue)
                    {
                        if (value.missionIdx == info.iDx)
                        {
                            isExist = true;
                            break;
                        }
                    }

                    if (isExist == false)
                    {
                        DateTime expireDate = new DateTime();
                        DateTime completeDate = new DateTime();
                        if (info.missionType == MissionType.Mission)
                        {
                            if (GlobalManager.Instance.ConvertToDateTime(info.expireDate, out expireDate))
                            {
                                //UserData userData = new UserData();
                                //userData.missionValue.Add(new MissionValue(info.iDx, 0, expireDate, MissionState.Normal));
                                userDataBase.missionValue.Add(new MissionValue(info.iDx, 0, expireDate, MissionState.Normal, completeDate));
                            }
                        }
                        else
                        {
                            TimeSpan timeSpan = new TimeSpan();
                            if (GlobalManager.Instance.ConvertToTimeSpan(info.timeSpan, out timeSpan))
                            {
                                //UserData userData = new UserData();
                                expireDate = DateTime.Now + timeSpan;
                                //userData.missionValue.Add(new MissionValue(info.iDx, 0, expireDate, MissionState.Normal));
                                userDataBase.missionValue.Add(new MissionValue(info.iDx, 0, expireDate, MissionState.Normal, completeDate));
                            }
                        }
                    }
                }
                //save userinfo
                //save mission
                //SaveUserInfo();
            }
        }

        private void CheckMissionDate()
        {
            if (userDataBase.missionValue != null)
            {
                for (int i = 0; i < userDataBase.missionValue.Count; i++)
                {
                    if (userDataBase.missionValue[i].state == MissionState.Normal && DateTime.Now >= userDataBase.missionValue[i].expireDate)
                    {
                        userDataBase.missionValue[i].state = MissionState.Fail;
                    }
                }
                //delete auto remove check for closeBeta
                CheckRemoveMission();
            }
        }

        internal void CheckMissionValue(GameType gameType, AchieveType achieveType, int number)
        {
            CheckMissionDate();

            AchieveType _achieveType = AchieveType.Unknown;
            GameType _gameType = GameType.Unknown;
            foreach (var value in userDataBase.missionValue)
            {
                //get gameType via missionIdx
                _achieveType = MissionManager.Instance.GetAchieveType(value.missionIdx);
                _gameType = MissionManager.Instance.GetGameType(value.missionIdx);
                if (_gameType == gameType && _achieveType == achieveType)
                {
                    if (value.state == MissionState.Normal)
                    {
                        SetMissionValue(value.missionIdx, number);
                    }
                }
            }
        }

        private void SetMissionValue(int missionIdx, int number)
        {
            List<MissionInfo> missionInfoList = MissionManager.Instance.GetMissionList();
            if (missionInfoList == null)
            {
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = "MissionInfo List is null";
                    GameServer._logger.Error(log);
                }
                return;
            }

            foreach (var value in userDataBase.missionValue)
            {
                if (value.missionIdx == missionIdx)
                {
                    switch (MissionManager.Instance.GetAchieveType(value.missionIdx))
                    {
                        case AchieveType.Level:
                        case AchieveType.Friends:
                        case AchieveType.CheckIn:
                        case AchieveType.Tutorial:
                        case AchieveType.ConsecutiveWin:
                            value.currentValue = number;
                           
                            break;
                        case AchieveType.Win:
                        case AchieveType.Lose:
                        case AchieveType.ItemUseCount:
                        case AchieveType.EtherealInkUseCount:
                        case AchieveType.SLElixirUseCount:
                        case AchieveType.RosemaryOilUseCount:
                        case AchieveType.EPElixirUseCount:
                        case AchieveType.SOElixirUseCount:
                        case AchieveType.AllJokerFace:
                            value.currentValue += number;

                            break;
                       
                        default: break;
                    }
                    foreach (var info in missionInfoList)
                    {
                        if (info.iDx == missionIdx)
                        {
                            if (value.currentValue >= info.targetValue)
                            {
                                value.currentValue = info.targetValue;
                                value.state = MissionState.Complete;
                                value.completeDate = DateTime.Now;
                                string subject = LocalizationKey.MISSION_GIFT_SUBJECT.ToString();
                                string content = string.Empty;
                                //send a new mail.
                                SetContent(out content, info.itemIdx1);
                                SetSendNewMail("SYSTEM", /*"SYSTEM",*/ subject,content, info.itemIdx1, info.itemCount1);
                                if (info.itemIdx2 != 0 && info.itemCount2 != 0)
                                {
                                    SetContent(out content, info.itemIdx2);
                                    SetSendNewMail("SYSTEM", /*"SYSTEM",*/ subject, content,info.itemIdx2, info.itemCount2);
                                }
                            }
                            break;
                        }
                    }
                    //save userinfo...missionValue not here
                    break;
                }
            }
        }

        public void SetSendNewMail(string sendID, /*string senderNick,*/ string subject, string content, int itemIdx1, int itemCount1/*, int itemIdx2, int itemCount2*/)
        {
            MailData mailData = new MailData();
            mailData.userSession = session;
            //mailData.nick = userDataBase.nickName;
            mailData.sendID = sendID;
            //mailData.senderNick = senderNick;
            mailData.subject = subject;
            mailData.content = content;
            mailData.itemIdx1 = itemIdx1;
            mailData.itemCount1 = itemCount1;
            //mailData.itemIdx2 = itemIdx2;
            //mailData.itemCount2 = itemCount2;
            mailData.itemGot = false;

            //save to mail db, add new mail...
            MailDBManager.Instance.NewMail(mailData);
        }

        /*internal void ResetMissionValue(AchieveType achieveType)
        {
            CheckMissionDate();
            foreach (var value in userDataBase.missionValue)
            {
                _achieveType = achieveManager.instance.getachievetype(value.missionIdx)
                if (value.state == MissionState.Normal && _achieveType == (int)achieveType)
                {
                    value.currentValue = 1;
                    //save userinfo...missionValue
                    SaveUserInfo();
                    break;
                }
            }
        }*/

        internal void InitRecommendFriends()
        {
            _recommendFriends = new List<FriendData>();
            foreach (var infoPair in SessionIdManager.Instance)
            {
                bool recommendEnable = true;
                if (_recommendFriends.Count >= 2)
                {
                    break;
                }
                
                if (infoPair.Value.userServerState != UserServerStatus.DisConnect)
                {
                    foreach (var friendData in _allFriends)
                    {
                        if (friendData.friendID == infoPair.Value.userID)
                        {
                            recommendEnable = false;
                            break;
                        }
                    }

                    if (recommendEnable == true && infoPair.Value.userID != this.userID)
                    {
                        foreach (var recommandFriend in _recommendFriends)
                        {
                            if (recommandFriend.friendID == infoPair.Value.userID)
                            {
                                recommendEnable = false;
                                break;
                            }
                        }
                    }

                    if (recommendEnable == true && infoPair.Value.userID != this.userID)
                    {
                        if ((infoPair.Value.userDataBase.Level == this.userDataBase.Level) &&
                            (infoPair.Value.userDataBase.userProfile.friendsCount < GlobalManager.MAX_FRIEND))
                        {
                            FriendData friendData = new FriendData();
                            friendData.friendID = infoPair.Value.userID;
                            friendData.friendNick = infoPair.Value.userDataBase.nickName;
                            friendData.faceIdx = infoPair.Value.userDataBase.FaceIdx;
                            friendData.level = infoPair.Value.userDataBase.Level;
                            friendData.friendType = FriendType.Recommend;
                            friendData.friendLastLoginDate = infoPair.Value.userDataBase.lastLoginDate;
                            friendData.itemSendDate = string.Empty;
                            friendData.enableGetItem = false;
                            _recommendFriends.Add(friendData);
                        }
                    }
                }
                
            }
        }
        
        internal void GetRecommendFriends(out List<Friend> friendList)
        {
            friendList = new List<Friend>();
            List<int> tmpRemoveFriendIdxList = new List<int>();

            if (_recommendFriends.Count > 0)
            {
                for (int i=0; i<_recommendFriends.Count; i++)
                {
                    foreach (var myFriend in _myFriends)
                    {
                        if (_recommendFriends[i].friendID == myFriend.friendID)
                        {
                            if(!tmpRemoveFriendIdxList.Contains(i))
                                tmpRemoveFriendIdxList.Add(i);

                            break;
                        }
                    }

                    foreach (var requestFriend in _requestFriends)
                    {
                        if (_recommendFriends[i].friendID == requestFriend.friendID)
                        {
                            if(!tmpRemoveFriendIdxList.Contains(i))
                                tmpRemoveFriendIdxList.Add(i);

                            break;
                        }
                    }
                }
            }

            if (tmpRemoveFriendIdxList.Count > 0)
            {
                foreach (var idx in tmpRemoveFriendIdxList)
                {
                    _recommendFriends.RemoveAt(idx);
                }
            }

            if (_recommendFriends.Count > 0)
            {
                for (int i = 0; i < _recommendFriends.Count; i++)
                {
                    friendList.Add(new Friend(i + 1, _recommendFriends[i].friendNick, _recommendFriends[i].faceIdx, _recommendFriends[i].level, _recommendFriends[i].friendLastLoginDate, _recommendFriends[i].friendType, false, false, 0, 0, 0));
                }

                //if (_recommendFriends.Count == 1 || _recommendFriends.Count == 2)
                //{
                //    for (int i = 0; i < _recommendFriends.Count; i++)
                //    {
                //        friendList.Add(new Friend(i + 1, _recommendFriends[i].friendNick, _recommendFriends[i].faceIdx,_recommendFriends[i].level, _recommendFriends[i].friendLastLoginDate, _recommendFriends[i].friendType, false, _recommendFriends[i].enableGetItem));
                //    }
                //}
                //else if (_recommendFriends.Count > 2)
                //{
                //    Random rand = new Random();
                //    int iDx = rand.Next(0, _recommendFriends.Count);
                //    friendList.Add(new Friend(iDx + 1, _recommendFriends[iDx].friendNick, _recommendFriends[iDx].faceIdx, _recommendFriends[iDx].level, _recommendFriends[iDx].friendLastLoginDate, _recommendFriends[iDx].friendType, false, _recommendFriends[iDx].enableGetItem));

                //    while (friendList.Count < 2)
                //    {
                //        int iDx2 = rand.Next(0, _recommendFriends.Count);

                //        if (iDx2 != iDx &&(_recommendFriends[iDx2].friendID != _recommendFriends[iDx].friendID))
                //        {
                //            friendList.Add(new Friend(iDx2 + 1, _recommendFriends[iDx2].friendNick, _recommendFriends[iDx2].faceIdx, _recommendFriends[iDx2].level, _recommendFriends[iDx2].friendLastLoginDate, _recommendFriends[iDx2].friendType, false, _recommendFriends[iDx2].enableGetItem));
                //        }
                //    }
                //}
            }
        }

        internal void GetMyFriends()
        {
            FriendDBManager.Instance.GetMyFriends(session);
        }

        internal void GetRequestFriends()
        {
            FriendDBManager.Instance.GetRequestFriends(session);
        }

        internal void AddRequestFriends(string userSession, int iDx_, out FriendResult friendResult)
        {
            friendResult = FriendResult.OK;
            FriendData friendData = new FriendData();
            friendData.friendID = _recommendFriends[iDx_ - 1].friendID;
            friendData.friendNick = _recommendFriends[iDx_ - 1].friendNick;
            friendData.faceIdx = _recommendFriends[iDx_ - 1].faceIdx;
            friendData.level = _recommendFriends[iDx_ - 1].level;
            friendData.friendLastLoginDate = _recommendFriends[iDx_ - 1].friendLastLoginDate;
            friendData.friendType = FriendType.Requesting;
            friendData.itemSendDate = _recommendFriends[iDx_ - 1].itemSendDate;
            friendData.enableGetItem = _recommendFriends[iDx_ - 1].enableGetItem;
            
            if(_requestFriends ==null) _requestFriends = new List<FriendData>();

            foreach (var friend in _requestFriends)
            {
                if (friend.friendID == _recommendFriends[iDx_ - 1].friendID)
                {
                    friendResult = FriendResult.Fail;
                    _recommendFriends.RemoveAt(iDx_ - 1);
                    return;
                }
            }

            _requestFriends.Add(friendData);
            _recommendFriends.RemoveAt(iDx_ - 1);

            List<Friend> friendList = new List<Friend>();
            for (int i = 0; i < _recommendFriends.Count; i++)
            {
                friendList.Add(new Friend(i + 1, _recommendFriends[i].friendNick, _recommendFriends[i].faceIdx, _recommendFriends[i].level, _recommendFriends[i].friendLastLoginDate, _recommendFriends[i].friendType, false, false, 0, 0, 0));
            }

            //send SCNotifriend list again-recommend
            PacketsHandlerServer.SendSCNotiFriendList(serverSession, new FriendList(PacketTypeList.SCNotiFriendList.ToString(), FriendTabType.RecommendFriends, 20, friendList));

            //add new to db(double) - me and friend
            FriendDBManager.Instance.AddFriendToDB(userSession, userID, /*userDataBase.nickName,*/ friendData.friendID/*, friendData.friendNick*/);
        }

        internal void AddMyFriends(string userSession, int iDx_, int otherFriendCount_, out FriendResult friendResult)
        {
            friendResult = FriendResult.OK;
            FriendData friendData = new FriendData();
            friendData.friendID = _requestFriends[iDx_ - 1].friendID;
            friendData.friendNick = _requestFriends[iDx_ - 1].friendNick;
            friendData.faceIdx = _requestFriends[iDx_ - 1].faceIdx;
            friendData.level = _requestFriends[iDx_ - 1].level;
            friendData.friendLastLoginDate = _requestFriends[iDx_ - 1].friendLastLoginDate;
            friendData.friendType = FriendType.Friend;
            friendData.itemSendDate = _requestFriends[iDx_ - 1].itemSendDate;
            friendData.enableGetItem = _requestFriends[iDx_ - 1].enableGetItem;

            if(_myFriends == null) _myFriends = new List<FriendData>();

            foreach (var friend in _myFriends)
            {
                if (friend.friendID == _requestFriends[iDx_ - 1].friendID)
                {
                    friendResult = FriendResult.Fail;
                    _requestFriends.RemoveAt(iDx_ - 1);
                    return;
                }
            }

            _myFriends.Add(friendData);
            _requestFriends.RemoveAt(iDx_ - 1);

            List<Friend> friendList = new List<Friend>();
            for (int i = 0; i < _requestFriends.Count; i++)
            {
                friendList.Add(new Friend(i + 1, _requestFriends[i].friendNick, _requestFriends[i].faceIdx, _requestFriends[i].level, _requestFriends[i].friendLastLoginDate, _requestFriends[i].friendType, false, false, 0, 0, 0));
            }

            //send SCNotifriend list again-request
            PacketsHandlerServer.SendSCNotiFriendList(serverSession, new FriendList(PacketTypeList.SCNotiFriendList.ToString(), FriendTabType.RequestFriends, 20, friendList));

            //send SCNotiMenuAlarm - friend
            //SetSendMenuAlarmFriends();
            //if _requestFriends.Counnt == 0, also send alarm
            int alarmCount = 0;
            bool allowZero = true;
            SetSendMenuAlarmFriends(alarmCount, allowZero);
            //update friend db(double) - me and friend
            FriendDBManager.Instance.UpdateDBFriendType(userSession, userID, friendData.friendID, otherFriendCount_);
        }

        internal void DeleteFriend(int iDx_)
        {
            FriendData friendData = new FriendData();
            friendData.friendID = _myFriends[iDx_ - 1].friendID;
            friendData.friendNick = _myFriends[iDx_ - 1].friendNick;
            friendData.faceIdx = _myFriends[iDx_ - 1].faceIdx;
            friendData.level = _myFriends[iDx_ - 1].level;
            friendData.friendLastLoginDate = _myFriends[iDx_ - 1].friendLastLoginDate;
            friendData.friendType = FriendType.Friend;
            friendData.itemSendDate = _myFriends[iDx_ - 1].itemSendDate;
            friendData.enableGetItem = _myFriends[iDx_ - 1].enableGetItem;

            _myFriends.RemoveAt(iDx_ - 1);

            //send myfriends again...
            SetSendMyFriends();

            userDataBase.userProfile.friendsCount = _myFriends.Count;
            //check achieve
            CheckAchieveValue(GameType.Unknown, AchieveType.Friends, userDataBase.userProfile.friendsCount);
            //check mission
            CheckMissionValue(GameType.Unknown, AchieveType.Friends, userDataBase.userProfile.friendsCount);

            //update friend db(couple) - me and friend
            FriendDBManager.Instance.DeleteFriend(session, friendData.friendID, friendData.friendType);
        }

        internal void GetGift(int iDx, out FriendResult friendResult)
        {
            friendResult = FriendResult.Unknown;
            if (_myFriends[iDx - 1].enableGetItem == true)
            {
                friendResult = FriendResult.OK;
                //gift count: 3
                userDataBase.Stamina += GlobalManager.GIFT_STAMINA_COUNT;
                //send scnoti userelement again...
                SetSendElementInfoList();

                _myFriends[iDx - 1].enableGetItem = false;

                //send myfriends again...
                SetSendMyFriends();

                //send SCNotiMenuAlarm - friend
                int alarmCount = 0;
                bool allowZero = true;
                SetSendMenuAlarmFriends(alarmCount, allowZero);
                
                //save to DB
                FriendDBManager.Instance.UpdateGetGift(session, _myFriends[iDx - 1].friendID);

                if (GameServer._isCloud == true)
                {
                    //item log
                    GlobalManager.Instance.LogItemInsert(serverSession, userID, userDataBase.nickName, (int)ElementType.Stamina, ElementType.Stamina.ToString(), GlobalManager.GIFT_STAMINA_COUNT, LogItemType.Friend);
                }
            }
            else { friendResult = FriendResult.Fail; }
        }

        public void SetSendMyFriends()
        {
            List<Friend> friendList = new List<Friend>();
            bool enableSendItem = false;
            DateTime itemSendDate = new DateTime();
            TimeSpan intervalSpan = new TimeSpan(1, 0, 0, 0);
            for (int i = 0; i < _myFriends.Count; i++)
            {
                enableSendItem = false;
                itemSendDate = new DateTime();
                intervalSpan = new TimeSpan(1, 0, 0, 0);

                if (_myFriends[i].itemSendDate == string.Empty) { enableSendItem = true; }
                else if (GlobalManager.Instance.ConvertToDateTime(_myFriends[i].itemSendDate, out itemSendDate))
                {
                    if (DateTime.Now.Date >= itemSendDate.Date + intervalSpan)
                    {
                        enableSendItem = true;
                    }
                }

                friendList.Add(new Friend(i + 1, _myFriends[i].friendNick, _myFriends[i].faceIdx, _myFriends[i].level, _myFriends[i].friendLastLoginDate, _myFriends[i].friendType, enableSendItem, _myFriends[i].enableGetItem, _myFriends[i].winCount, _myFriends[i].loseCount, _myFriends[i].drawCount));
            }

            //send myFriends
            PacketsHandlerServer.SendSCNotiFriendList(serverSession, new FriendList(PacketTypeList.SCNotiFriendList.ToString(),FriendTabType.MyFriends, 20, friendList));
        }

        internal void SendGift(int iDx, out FriendResult friendResult)
        {
            friendResult = FriendResult.Unknown;
            bool enable = false;
            DateTime sendDate = new DateTime();
            TimeSpan interval = new TimeSpan(1, 0, 0, 0);
            if (_myFriends[iDx - 1].itemSendDate == string.Empty) { enable = true; }
            else if (GlobalManager.Instance.ConvertToDateTime(_myFriends[iDx - 1].itemSendDate, out sendDate))
            {
                if (DateTime.Now.Date >= sendDate.Date + interval)
                {
                    enable = true;
                }
            }

            if (enable == true)
            {
                friendResult = FriendResult.OK;

                _myFriends[iDx - 1].itemSendDate = DateTime.Now.ToString();

                //send myfriends again...
                SetSendMyFriends();

                //save to DB
                FriendDBManager.Instance.UpdateSendGift(session, _myFriends[iDx - 1].friendID, _myFriends[iDx - 1].itemSendDate);
            }
            else { friendResult = FriendResult.Fail; }
        }

        internal void SendAllGifts(out FriendResult friendResult)
        {
            friendResult = FriendResult.Unknown;
            List<FriendData> tmpFriendList = new List<FriendData>();

            for (int i = 0; i < _myFriends.Count; i++)
            {
                bool enableSendItem = false;
                DateTime itemSendDate = new DateTime();
                TimeSpan intervalSpan = new TimeSpan(1, 0, 0, 0);
                if (_myFriends[i].itemSendDate == string.Empty) { enableSendItem = true; }
                else if (GlobalManager.Instance.ConvertToDateTime(_myFriends[i].itemSendDate, out itemSendDate))
                {
                    if (DateTime.Now.Date >= itemSendDate.Date + intervalSpan)
                    {
                        enableSendItem = true;
                    }
                }

                if (enableSendItem == true)
                {
                    _myFriends[i].itemSendDate = DateTime.Now.ToString();
                    tmpFriendList.Add(_myFriends[i]);
                }
            }

            if (tmpFriendList.Count > 0)
            {
                friendResult = FriendResult.OK;
                SetSendMyFriends();

                //save to DB
                foreach (var friend in tmpFriendList)
                {
                    FriendDBManager.Instance.UpdateSendGift(session, friend.friendID, friend.itemSendDate);
                }
            }
            else { friendResult = FriendResult.Fail; }

        }

        internal void GetAllGifts(out FriendResult friendResult)
        {
            friendResult = FriendResult.Unknown;
            List<FriendData> tmpFriendList = new List<FriendData>();

            for (int i = 0; i < _myFriends.Count; i++)
            {
                if (_myFriends[i].enableGetItem == true)
                {
                    _myFriends[i].enableGetItem = false;
                    //gift count: 3
                    userDataBase.Stamina += GlobalManager.GIFT_STAMINA_COUNT;
                    //send scnoti userelement again...
                    SetSendElementInfoList();

                    tmpFriendList.Add(_myFriends[i]);
                }
            }

            if (tmpFriendList.Count > 0)
            {
                friendResult = FriendResult.OK;
                SetSendMyFriends();

                //send SCNotiMenuAlarm - friend
                int alarmCount = 0;
                bool allowZero = true;
                SetSendMenuAlarmFriends(alarmCount, allowZero);

                //save to DB
                foreach (var friend in tmpFriendList)
                {
                    FriendDBManager.Instance.UpdateGetGift(session, friend.friendID);
                }
            }
            else { friendResult = FriendResult.Fail; }
        }

        internal void GetQuickItemInfo(int itemIdx, out QuickItemInfo quickItemInfo)
        {
            int itemCount = 0;
            string remainTime = string.Empty;
            PayType payType = PayType.None;
            double price = 0;
            SpecialType specialType = SpecialType.Normal;

            ItemElementInfo elementInfo = MoonGlow.Data.ItemManager.Instance.GetElement(itemIdx);
            payType = elementInfo.payType;
            price = elementInfo.salePrice;
            specialType = elementInfo.specialType;
            int remainH = 0;
            int remainM = 0;
            int remainS = 0;

            if (itemIdx == (int)UserItems.EPElixir)
            {
                itemCount = userDataBase.userItem.ePElixir;
                if (epElixirTime.isSet == true)
                {
                    TimeSpan epElixirEndSpan = new TimeSpan();

                    if (epElixirTime.endEventDate > DateTime.Now)
                    {
                        epElixirEndSpan = epElixirTime.endEventDate - DateTime.Now;
                    }
                    remainH = epElixirEndSpan.Hours;
                    remainM = epElixirEndSpan.Minutes;
                    remainS = epElixirEndSpan.Seconds;
                }
            }
            if (itemIdx == (int)UserItems.SOElixir)
            {
                itemCount = userDataBase.userItem.sOElixir;
                if (soElixirTime.isSet == true)
                {
                    TimeSpan soElixirEndSpan = new TimeSpan();

                    if (soElixirTime.endEventDate > DateTime.Now)
                    {
                        soElixirEndSpan = soElixirTime.endEventDate - DateTime.Now;
                    }
                    remainH = soElixirEndSpan.Hours;
                    remainM = soElixirEndSpan.Minutes;
                    remainS = soElixirEndSpan.Seconds;
                }
            }

            remainTime = string.Format("{0:00}:{1:00}:{2:00}", remainH, remainM, remainS);

            quickItemInfo = new QuickItemInfo(itemIdx, itemCount, remainTime, payType, price, specialType);
        }

        internal void OnSessionClose()
        {
            if (userServerState == UserServerStatus.DisConnect) return;

            connectTimes--;
            if (connectTimes > 0)
            {
                return;
            }
            //save userinfo
            //save all
            SaveUserInfo();
            SaveToSql();

            if (GameServer._isCloud == true)
            {
                //Disconnect log
                GlobalManager.Instance.LogAccountInsert(serverSession, userID, userDataBase.nickName, LogAccountType.Disconnect);
            }

            //2. check if there is room should be deleted or not, prevent force exist but empty room still remain
            //change it when AI works later 나중에...
            if (!string.IsNullOrEmpty(roomID))
            {
                Room.Room room = GameServer.GetRoom(session); 
                if (room != null) room.SetDisconnectPlayer(session);
            }

            //3. set expire session   
            SetDisConnect();
            userInfoRemoveDate = DateTime.Now + /*new TimeSpan(0, 15, 0)*/GlobalManager.EXPIRE_SESSION_TIMESPAN;

            SessionIdManager.Instance._sessionDic.Remove(serverSession);
        }

        internal void CheckRoomReEnter()
        {
            bool joinRoom = false;
            Room.Room room = null;
            //ConditionInfo conditionInfo = null;
            if (!string.IsNullOrEmpty(roomID))
            {
                room = GameServer.GetRoom(session);
                if (room != null)
                {
                    //conditionInfo = ConditionManager.Instance.Get(((int)room.gameType - 1));
                    //if (conditionInfo != null)
                    //{
                        PacketsHandlerServer.SendSCNotiEnterRoom(serverSession, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, room.goalHP, room.goalResource/*conditionInfo.goalHP, conditionInfo.goalResource*/);

                        SetRoom(roomID);
                        room.unNormal = false;
                        //remove from _pause...
                        room.CancelPausePlayer(session);
                        room.CancelEarlyOutPlayer(session);

                        foreach (var player in room.GetPlayers())
                        {
                            if (player.session == session)
                            {
                                if(!player.setAI) player.isUser = true;

                                if (room.isPlaying)
                                {
                                    player.noReward = false;
                                    if (!player.setAI) player.autoPlayCount = 0;

                                    room._rule.SendSCNotiContinueGame(session);
                                    //send playerlist
                                    room.SendPlayerList();
                                    //send playerstatus, send all to me 
                                    room.ReConnectPlayerStatus(serverSession);
                                    //send iteminfo, send all to me 
                                    room.ReConnectPlayerItemInfo(serverSession);
                                    room._rule.SendSCNotiPlayerFocus();

                                    //send carddeckinfo, send only to me..
                                    int cardSkinIdx = 900;
                                    if (userDataBase.userItem.cardSkinList != null)
                                    {
                                        foreach (var cardSkin in userDataBase.userItem.cardSkinList)
                                        {
                                            if (cardSkin.isSelect == true)
                                            {
                                                cardSkinIdx = cardSkin.skinIdx;
                                                break;
                                            }
                                        }
                                    }
                                    DeckList deckList = new DeckList(PacketTypeList.SCNotiDeckInfo.ToString(), room._deckInfolist, cardSkinIdx, 0, null);
                                    if (deckList != null)
                                    {
                                        PacketsHandlerServer.SendSCNotiDeckInfo(serverSession, deckList);
                                    }

                                    //send deck to hand only mine
                                    room.ReConnectSCNotiDeckToHand(session);

                                }
                                else
                                {
                                    // player list
                                    room.SendPlayerList();
                                    // player status 
                                    room.SendPlayerStatus();
                                    //send playeriteminfo
                                    room.SendPlayerItemInfo();
                                    room.PlayerEnterReady(session);

                                }

                                joinRoom = true;
                                break;
                            }
                        }
                    //}
                }
            }
            else
            {
                if (tmpRoomInfo != null)
                {
                    if (RoomManager.Instance.CreateRoom("", tmpRoomInfo.gameType, tmpRoomInfo.playMode, tmpRoomInfo.playTime, tmpRoomInfo.stake, tmpRoomInfo.disClose, tmpRoomInfo.isLock, out room))
                    {
                        //success
                        if (room != null)
                        {
                            //conditionInfo = ConditionManager.Instance.Get(((int)room.gameType - 1));
                            //if (conditionInfo != null)
                            //{
                                //PacketsHandlerServer.SendSCNotiEnterRoom(serverSession, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, room.goalHP, room.goalResource/*conditionInfo.goalHP, conditionInfo.goalResource*/);
                                if (!room.singlePlayer && room.Join(session, true))
                                {
                                // send packet
                                //MoonGlow.Data.ConditionInfo conditionInfo = MoonGlow.Data.ConditionManager.Instance.Get(((int)room.gameType - 1));
                                //if (conditionInfo != null)
                                //{
                                //    // player list
                                //    room.SendPlayerList();
                                //    // player status 
                                //    room.SendPlayerStatus();
                                //    //send playeriteminfo
                                //    room.SendPlayerItemInfo();
                                //    room.PlayerEnterReady(session);

                                      joinRoom = true;
                                //    tmpRoomInfo = null;
                                //}

                                room._playStateManager.SetState(PlayState.Ready);
                                //RoomList roomList = RoomManager.Instance.GetAllRoomList(session);
                                //if (roomList != null) PacketsHandlerServer.SendSCNotiRoomList(serverSession, roomList);
                                tmpRoomInfo = null;
                                if (GameServer._isCloud == true)
                                {
                                    //play log
                                    GlobalManager.Instance.LogPlayInsert(serverSession, userID, userDataBase.nickName, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, LogPlayType.Create, string.Empty, string.Empty);
                                }

                                SetHome();
                                PacketsHandlerServer.SendSCNotiGotoScene(serverSession, SceneType.Home);

                            }
                            //}
                        }
                    }
                }
            }
            if (joinRoom == false)
            {
                roomID = null;
                SetHome();
                PacketsHandlerServer.SendSCNotiGotoScene(serverSession, SceneType.Home);
            }

      
        }

        internal bool IsItemTimeRun(int nowtick_)
        {
            if (userDataBase.Stamina >= 100 && userDataBase.userProfile.addHP != 1 && userDataBase.userProfile.addSP != 1) return false;

            bool retvalue = false;
            if ((_currentItemTimeTickCount + GlobalManager.ITEMTIME_TICK_INTERNAL) < nowtick_)
            {
                _currentItemTimeTickCount = nowtick_;
                retvalue = true;
            }

            return retvalue;
        }

        internal void ItemTimeRun()
        {
            PacketsHandlerInner.SSReqCheckEvent(serverSession, session);
        }

        internal bool IsPingRun(int nowtick_)
        {
            if(userServerState == UserServerStatus.DisConnect) return false;

            bool retvalue = false;
            if ((_currentPingTickCount + GlobalManager.PING_TICK_INTERNAL) < nowtick_)
            {
                _currentPingTickCount = nowtick_;
                retvalue = true;
            }

            return retvalue;
        }

        internal void PingRun()
        {
            pingCount++;
            if (pingCount >= 3)
            {
                SetDisConnect();
                userInfoRemoveDate = DateTime.Now + /*new TimeSpan(0, 15, 0)*/GlobalManager.EXPIRE_SESSION_TIMESPAN;
            }
            else
            {
                PacketsHandlerServer.SendSCReqPing(serverSession, pingCount);

                pingTimes++;
                if (pingTimes >= 3)
                {
                    pingTimes = 0;
                    //SendSCNotiRoomList - one time every 3 mins
                    RoomList roomList = RoomManager.Instance.GetAllRoomList(session);
                    //Console.WriteLine("User nick: " + userDataBase.nickName);
                    if (roomList != null) PacketsHandlerServer.SendSCNotiRoomList(serverSession, roomList);
                }
            }
        }
    
        internal bool IsExpireUserInfoRun(int nowtick_)
        {
            if (userServerState != UserServerStatus.DisConnect && isPause == false) return false;

            bool retvalue = false;
            if ((_currentExpireTickCount + GlobalManager.EXPIRE_TICK_INTERNAL) < nowtick_)
            {
                _currentExpireTickCount = nowtick_;
                retvalue = true;
            }

            return retvalue;
        }

        internal void ExpireUserInfoRun()
        {
            if (userServerState == UserServerStatus.DisConnect)
            {
                PacketsHandlerInner.SMReqExpireUserInfo(serverSession, session);
            }

            if (isPause == true)
            {
                // PacketsHandlerDB.SMReqExpireSession(userInfo.serverSession, userInfo.session);
                PacketsHandlerInner.SSReqClientPause(serverSession, session);
            }
        }

        internal bool IsCheckSessionRun(int nowtick_)
        {
            bool retvalue = false;
            if ((_currentSessionTickCount + GlobalManager.SESISON_TICK_INTERNAL) < nowtick_)
            {
                _currentSessionTickCount = nowtick_;
                retvalue = true;
            }

            return retvalue;
        }

        internal void CheckSessionRun()
        {
            PacketsHandlerInner.SMReqExpireRedis(serverSession, userID);
        }

        public void SetSendMenuAlarmFriends(int alarmCount_, bool allowZero_)
        {
            if (_myFriends != null)
            {
                foreach (var myFriend in _myFriends)
                {
                    if (myFriend.enableGetItem)
                    {
                        alarmCount_++;
                    }
                }
            }

            if (_requestFriends != null)
            {
                foreach (var requestFriend in _requestFriends)
                {
                    if (requestFriend.friendType == FriendType.Accepting)
                    {
                        alarmCount_ += _requestFriends.Count;
                    }
                }
            }

            if (allowZero_ || (!allowZero_ && alarmCount_ > 0))
            {
                //MenuAlarm menuAlarm = new MenuAlarm(PacketTypeList.SCNotiMenuAlarm.ToString(), Option.Friend, alarmCount_.ToString());
                //PacketsHandlerServer.SendSCNotiMenuAlarm(serverSession, menuAlarm);
                PacketsHandlerServer.SendSCNotiMenuAlarm(serverSession, Option.Friend, alarmCount_.ToString());
            }
        }

        internal void SetSendMenuAlarmMail(bool allowZero_)
        {
            if (_mailList == null) { return; }

            int alarmCount = 0;
            if (_mailList != null)
            {
                foreach (var _mail in _mailList)
                {
                    if (_mail.del == false)
                    {
                        alarmCount++;
                    }
                } 
            }

            if (allowZero_ || (!allowZero_ && alarmCount > 0))
            {
                //MenuAlarm menuAlarm = new MenuAlarm(PacketTypeList.SCNotiMenuAlarm.ToString(), Option.Mail, alarmCount.ToString());
                PacketsHandlerServer.SendSCNotiMenuAlarm(serverSession, Option.Mail, alarmCount.ToString());
            }
        }

        internal bool IsAdOK()
        {
            bool isOK = false;
            TimeSpan ADInternal = new TimeSpan(1, 0, 0);
            
            //new day
            if (DateTime.Today >= userDataBase.ADLastTime.Date + new TimeSpan(1, 0, 0, 0))
            {
                userDataBase.ADRemainCount = GlobalManager.AD_SHOW_COUNT;
                isOK = true;
            }
            else if (userDataBase.ADRemainCount > 0 && DateTime.Now - userDataBase.ADLastTime >= ADInternal)
            {
                isOK = true;
            }
            return isOK;
        }
    }
}

