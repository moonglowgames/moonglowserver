﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonGlow
{
    //public enum SendMsgType { Me, RoomAll, RoomOthers, ServerAll }

    public class PacketsHandlerServer
    {
        public static void SendToClient( string serverSession, IPacket packet /*, SendMsgType sendMsgType*/ )
        {
            //instead switch..case..
            MoonGlow.GameServer.SendMsg(serverSession, packet);

            //switch (sendMsgType)
            //{
            //    case SendMsgType.Me:
            //        MoonGlow.GameServer.SendMsg(serverSession, packet);
            //        break;
            //    case SendMsgType.RoomAll:

            //        MoonGlow.UserInfo userinfo = MoonGlow.SessionIdManager.Instance[serverSession];
            //        if (userinfo != null)
            //        {
            //            MoonGlow.Room.Room room = MoonGlow.Room.RoomManager.Instance[userinfo.roomID];
            //            if (room != null)
            //            {
            //                List<string> userIdList = null;
            //                room.GetUserIdList(out userIdList);

            //                if (userIdList != null)
            //                {
            //                    // send playserlist
            //                    MoonGlow.GameServer.SendMsgWithUserId(userIdList, packet);
            //                }
            //            }
            //        }
            //        break;
            //    case SendMsgType.RoomOthers:
            //        MoonGlow.UserInfo userinfo2 = MoonGlow.SessionIdManager.Instance[serverSession];
            //        if (userinfo2 != null)
            //        {
            //            MoonGlow.Room.Room room = MoonGlow.Room.RoomManager.Instance[userinfo2.roomID];
            //            string exceptuserid = userinfo2.session;
            //            if (room != null)
            //            {
            //                List<string> userIdList = null;
            //                room.GetUserIdList(exceptuserid, out userIdList);

            //                if (userIdList != null)
            //                {
            //                    // send playserlist
            //                    MoonGlow.GameServer.SendMsgWithUserId(userIdList, packet);
            //                }
            //            }
            //        }
            //        break;
            //    case SendMsgType.ServerAll:
            //        MoonGlow.GameServer.SendBC(packet);
            //        break;
            //    default:
            //        break;
            //}
        }

        internal static void SendSCNotiShowAD(string serverSession, bool isOK, string guid, int ADRemainCount, string ADRemainTime)
        {
            SCNotiShowAD pck = new SCNotiShowAD();
            pck.isOK = isOK;
            pck.guid = guid;
            pck.remainCount = ADRemainCount;
            pck.remainTime = ADRemainTime;

            SendToClient(serverSession, pck);
        }

        internal static void SendSCNotiGetADItem(string serverSession, int itemIdx, int itemCount, EffectPosition pos)
        {
            SCNotiGetADItem pck = new SCNotiGetADItem();
            pck.itemIdx = itemIdx;
            pck.itemCount = itemCount;
            pck.pos = pos;

            SendToClient(serverSession, pck);
        }

        internal static void SendSCRspChangeNick(string serverSession, ChangeNickResult result)
        {
            SCRspChangeNick pck = new SCRspChangeNick();
            pck.result = result;

            SendToClient(serverSession, pck);
        }

        //SCNotiHello
        public static void SendSCNotiHello(string serverSession, string svrname, string ver, ServerState state, string info)
        {
            //send to clients
            SCNotiHello pck = new SCNotiHello();
            pck.svrName = svrname;
            pck.ver = ver;
            pck.state = state;
            pck.info = info;

            SendToClient(serverSession, pck /*, SendMsgType.Me*/);
        }

        internal static void SendSCNotiDownload(string serverSession, ConnectType conType, string downloadUrl)
        {
            SCNotiDownload pck = new SCNotiDownload();
            pck.connectType = conType;
            pck.downloadUrl = downloadUrl;

            SendToClient(serverSession, pck);
        }

        //SCRspLogin
        public static void SendSCNotiLogin(string serverSession, LoginState state, UserType userType, string userID, string userSession, string connectUrl)
        {
            //send to clients
            SCNotiLogin pck = new SCNotiLogin();
            pck.state = state;
            pck.userType = userType;
            pck.userID = userID;
            pck.session = userSession;
            pck.connectUrl = connectUrl;

            SendToClient(serverSession, pck /*, SendMsgType.Me*/ );
        }

        //SendSCNotiLogout
        public static void SendSCNotiLogout(string serverSession, bool result)
        {
            //send to clients
            SCNotiLogout pck = new SCNotiLogout();
            pck.result = result;

            SendToClient(serverSession, pck /*, SendMsgType.Me*/ );
        }

        //SCNotiSessionLogin
        public static void SendSCNotiSessionLogin(string serverSession, bool status, string newsession, UserType userType)
        {
            SCNotiSessionLogin pck = new SCNotiSessionLogin();
            pck.status = status;
            pck.newsession = newsession;
            pck.userType = userType;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiClientFocus
        internal static void SendSCNotiClientFocus(string serverSession)
        {
            SCNotiClientFocus pck = new SCNotiClientFocus();

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiChat 
        public static void SendSCNotiChat(string serverSession, string nick, int userType /*0-user, 1-AI*/, int playerNumber, string msg, int iconIdx)
        {
            //send to server
            SCNotiChat pck = new SCNotiChat();
            pck.nick = nick;
            pck.userType = userType;
            pck.playerNumber = playerNumber;
            pck.chatMsg = msg;
            pck.iconIdx = iconIdx;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        
        //SCNotiGotoScene
        public static void SendSCNotiGotoScene(string serverSession, SceneType scene)
        {
            SCNotiGotoScene pck = new SCNotiGotoScene();
            pck.scene = scene;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        internal static void SendSCNotiPlayerJoinCancel(string serverSession)
        {
            SCNotiPlayerJoinCancel pck = new SCNotiPlayerJoinCancel();

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiUserInfo
        public static void SendSCNotiUserInfo(string serverSession, int faceIndex, string userName, int badgeIdx, int level, int exp, int upLevelExp)
        {
            //send to clients
            SCNotiUserInfo pck = new SCNotiUserInfo();
            pck.faceIndex = faceIndex;
            pck.userName = userName;
            pck.badgeIdx = badgeIdx;
            pck.level = level;
            pck.exp = exp;
            pck.nextLevelExp = upLevelExp;

            SendToClient(serverSession,pck/*,SendMsgType.Me*/);
        }

        //SCNotiItemInfo
        //public static void SendSCNotiItemInfo(string serverSession, ItemList itemList)
        //{
        //    //send to clients
        //    SCNotiItemInfo pck = new SCNotiItemInfo();
        //    pck.itemList = itemList;

        //    SendToClient(serverSession, pck/*,SendMsgType.Me*/);
        //}

        //SCNotiUserElement
        public static void SendSCNotiUserElement(string serverSession, ElementList elementList)
        {
            //send to clients
            SCNotiUserElement pck = new SCNotiUserElement();
            pck.elementList = elementList;

            SendToClient(serverSession, pck/*,SendMsgType.Me*/);
        }

        //SCNotiRoomList
        public static void SendSCNotiRoomList(string serverSession, RoomList roomList_)
        {
            SCNotiRoomList pck = new SCNotiRoomList();
            pck.roomList = roomList_;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiEnterRoom
        public static void SendSCNotiEnterRoom(string serverSession, GameType gameType, PlayMode playMode, PlayTime playTime, int stake, bool disClose, bool isLock, int goalHP, int goalRP)
        {
            //send to clients
            SCNotiEnterRoom pck = new SCNotiEnterRoom();
            pck.gameType = gameType;
            pck.playMode = playMode;
            pck.playTime = playTime;
            pck.stake = stake;
            pck.disClose = disClose;
            pck.isLock = isLock;
            pck.goalHP = goalHP;
            pck.goalRP = goalRP;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCRspBuyElement
        public static void SendSCRspBuyElement(string serverSession, ShopResult result)
        {
            //send to client
            SCRspBuyElement pck = new SCRspBuyElement();
            pck.result = result;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiShopList
        public static void SendSCNotiShopList(string serverSession, ShopList shopsList)
        {
            //send to client
            SCNotiShopList pck = new SCNotiShopList();
            pck.shopList = shopsList;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiMyInfo
        public static void SendSCNotiMyInfo(string serverSession, MyInfo myInfo)
        {
            //send to client
            SCNotiMyInfo pck = new SCNotiMyInfo();
            pck.myInfo = myInfo;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SendSCNotiConnections
        internal static void SendSCNotiConnections(string serverSession, int count)
        {
            //send to client
            SCNotiConnections pck = new SCNotiConnections();
            pck.connections = count;

            SendToClient(serverSession, pck);
        }

        internal static void SendSCNotiChatIconSet(string serverSession, ChatIconSet iconSet)
        {
            SCNotiChatIconSet pck = new SCNotiChatIconSet();
            pck.chatIconSet = iconSet;

            SendToClient(serverSession, pck);
        }

        //SCNotiInventory
        public static void SendSCNotiInventory(string serverSession, InventoryList inventoryList)
        {
            //send to client
            SCNotiInventory pck = new SCNotiInventory();
            pck.inventoryList = inventoryList;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCRspUseItem
        public static void SendSCRspUseItem(string serverSession, bool result)
        {
            //send to client
            SCRspUseItem pck = new SCRspUseItem();
            pck.result = result;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiJokerInfo
        public static void SendSCNotiJokerInfo(string serverSession, int jokerFace, bool isTry/*, int inkItemCount*/)
        {
            //send to client
            SCNotiJokerInfo pck = new SCNotiJokerInfo();
            pck.jokerFace = jokerFace;
            pck.isTry = isTry;
            //pck.inkItemCount = inkItemCount;
            //pck.spellIdxList = spellIdxList;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiSpellList 
        public static void SendSCNotiSpellList(string serverSession, SpellIdxList spellIdxList)
        {
            //send to client
            SCNotiSpellList pck = new SCNotiSpellList();
            pck.spellIdxList = spellIdxList;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCRspUpgradeSpell 
        public static void SendSCRspUpdateSpell(string serverSession, SpellResult upgradeResult)
        {
            //send to client
            SCRspUpdateSpell pck = new SCRspUpdateSpell();
            pck.applyResult = upgradeResult;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCRspApplySpells 
        public static void SendSCRspJokerApply(string serverSession, /*SpellResult applyResult*/bool applyResult)
        {
            //send to client
            //SCRspUpgradeSpell pck = new SCRspUpgradeSpell();
            //pck.applyResult = applyResult;

            SCRspJokerApply pck = new SCRspJokerApply();
            pck.applyResult = applyResult;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiNews 
        public static void SendSCNotiNews(string serverSession, NewsList newList)
        {
            //send to client
            SCNotiNews pck = new SCNotiNews();
            pck.newsList = newList;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiMailList
        public static void SendSCNotiMailList(string serverSession, MailList mailList)
        {
            //send to client
            SCNotiMailList pck = new SCNotiMailList();
            pck.mailList = mailList;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

       //SCRspGetItem
        public static void SendSCRspGetItem(string serverSession, bool getResult)
        {
            //send to client
            SCRspGetItem pck = new SCRspGetItem();
            pck.getResult = getResult;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCRspGetAllItems
        public static void SendSCRspGetAllItems(string serverSession, bool getResult)
        {
            //send to client
            SCRspGetAllItems pck = new SCRspGetAllItems();
            pck.getResult = getResult;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiAchieveList
        public static void SendSCNotiAchieveList(string serverSession, AchieveList achieveList)
        {
            //send to client
            SCNotiAchieveList pck = new SCNotiAchieveList();
            pck.achieveList = achieveList;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiMissionList
        public static void SendSCNotiMissionList(string serverSession, MissionList missionList)
        {
            //send to client
            SCNotiMissionList pck = new SCNotiMissionList();
            pck.missionList = missionList;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiFriendList
        public static void SendSCNotiFriendList(string serverSession, FriendList friendList)
        {
            if (friendList.friendList != null && friendList.friendList.Count > 0)
            {
                DateTime friendLastLoginDate = new DateTime();
                foreach (var friend in friendList.friendList)
                {
                    if (GlobalManager.Instance.ConvertToDateTime(friend.lastLoginDate, out friendLastLoginDate))
                    {
                        friend.lastLoginDate = friendLastLoginDate.ToUniversalTime().ToString();
                    }
                }
            }

            //send to client
            SCNotiFriendList pck = new SCNotiFriendList();
            pck.friendList = friendList;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCRspFriendResut
        public static void SendSCRspFriendResut(string serverSession, PacketTypeList pckType, FriendResult result)
        {
            //send to client
            SCRspFriendResut pck = new SCRspFriendResut();
            pck.pckType = pckType;
            pck.result = result;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiChanelList
        public static void SendSCNotiChanelList(string serverSession, ChanelList chanelList)
        {
            //send to client
            SCNotiChanelList pck = new SCNotiChanelList();
            pck.chanelList = chanelList;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCRspRankResult
        public static void SendSCRspRankResult(string serverSession, ChanelResult result)
        {
            //send to client
            SCRspRankResult pck = new SCRspRankResult();
            pck.result = result;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiRankList
        public static void SendSCNotiRankList(string serverSession, RankList rankList)
        {
            //send to client
            SCNotiRankList pck = new SCNotiRankList();
            pck.rankList = rankList;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiRewardList
        public static void SendSCNotiRewardList(string serverSession, RewardList rewardList)
        {
            //send to client
            SCNotiRewardList pck = new SCNotiRewardList();
            pck.rewardList = rewardList;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiMyRankInfo
        public static void SendSCNotiMyRankInfo(string serverSession, RankHistory rankHistory)
        {
            //send to client
            SCNotiMyRankInfo pck = new SCNotiMyRankInfo();
            pck.rankHistory = rankHistory;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiQuickInventory
        public static void SendSCNotiQuickInventory(string serverSession, QuickInventory quickInventory)
        {
            //send to client
            SCNotiQuickInventory pck = new SCNotiQuickInventory();
            pck.quickInventory = quickInventory;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiPlayerProfile
        public static void SendSCNotiPlayerProfile(string serverSession, PlayerProfile playerProfile)
        {
            //send to client
            SCNotiPlayerProfile pck = new SCNotiPlayerProfile();
            pck.playerProfile = playerProfile;

            SendToClient(serverSession, pck/*, SendMsgType.Me*/);
        }

        //SCNotiServerNoti 
        public static void SendSCNotiServerNoti(Command command, string value, List<string> userSessionList)
        {
            //send to client
            SCNotiServerNoti pck = new SCNotiServerNoti();
            pck.command = command;
            pck.value = value;

            GameServer.SendMsgWithUserSession(userSessionList, pck);
        }
        //SCNotiServerNoti2 
        public static void SendSCNotiServerNoti(string serverSession, Command command, string value)
        {
            //send to client
            SCNotiServerNoti pck = new SCNotiServerNoti();
            pck.command = command;
            pck.value = value;

            SendToClient(serverSession, pck);
        }

        //SendSCRspRegister
        public static void SendSCRspRegister(string serverSession, LoginState state)
        {
            //send to Client
            SCRspRegister pck = new SCRspRegister();
            pck.state = state;

            SendToClient(serverSession, pck);
        }

        //SendSCNotiPlayerStatus
        public static void SendSCNotiPlayerStatus(string serverSession, int playerNumber, int HP, int SP, int moonDustRed, 
                                                    int manaBlue, int rodEnergyGreen, int moonStoneRedPlus, int zenStoneBluePlus, 
                                                    int moonCrystalGreenPlus, int effectIdx, int globalEffectIdx)
        {
            //send to Client
            SCNotiPlayerStatus pck = new SCNotiPlayerStatus();
            pck.playerNumber = playerNumber;
            pck.HP = HP;//addHp = 0 or 1
            pck.SP = SP;//addSp = 0 or 1
            pck.MD = moonDustRed;
            pck.MA = manaBlue;
            pck.RE = rodEnergyGreen;
            pck.MS = moonStoneRedPlus;
            pck.ZS = zenStoneBluePlus;
            pck.MC = moonCrystalGreenPlus;
            pck.effectIdx = effectIdx;
            pck.globalEffectIdx = globalEffectIdx;

            SendToClient(serverSession, pck);
        }

        //SendSCNotiPlayerItemInfo
        public static void SendSCNotiPlayerItemInfo(string serverSession, PlayerItemInfo playerItemInfo)
        {
            //send to Client
            SCNotiPlayerItemInfo pck = new SCNotiPlayerItemInfo();
            pck.playerItemInfo = playerItemInfo;

            SendToClient(serverSession, pck);
        }

        //SendSCNotiDeckInfo
        public static void SendSCNotiDeckInfo(string serverSession, DeckList deckList)
        {
            //send to Client
            SCNotiDeckInfo pck = new SCNotiDeckInfo();
            pck.deckList = deckList;

            SendToClient(serverSession, pck);
        }

        internal static void SendSCNotiPlayerJoin(string serverSession, string askerUserID, int askerFaceIdx, string askerNick, int askerLevel/*, int askerWinCount, int askerLoseCount, string askerWinRateStr*/)
        {
            SCNotiPlayerJoin pck = new SCNotiPlayerJoin();
            pck.askerUserID = askerUserID;
            pck.askerFaceIdx = askerFaceIdx;
            pck.askerNick = askerNick;
            pck.askerLevel = askerLevel;
            //pck.askerWinCount = askerWinCount;
            //pck.askerLoseCount = askerLoseCount;
            //pck.askerWinRate = askerWinRateStr;

            SendToClient(serverSession, pck);
        }

        //SendSCNotiUrl
        internal static void SendSCNotiUrl(string serverSession, UrlType urlType, string url)
        {
            //send to Client
            SCNotiUrl pck = new SCNotiUrl();
            pck.urlType = urlType;
            pck.url = url;

            SendToClient(serverSession, pck);
        }

        //SendSCNotiMessage
        internal static void SendSCNotiMessage(string serverSession, MessageType msgType)
        {
            //send to Client
            SCNotiMessage pck = new SCNotiMessage();
            pck.msgType = msgType;

            SendToClient(serverSession, pck);
        }

        //SendSCReqPing
        internal static void SendSCReqPing(string serverSession, int pingCount)
        {
            //send to Client
            SCReqPing pck = new SCReqPing();
            pck.pingCount = pingCount;
            pck.msg = DateTime.Now.ToString();

            SendToClient(serverSession, pck);
        }

        internal static void SendSCRspPasswordResult(string serverSession, PacketTypeList pckType, LoginState state)
        {
            SCRspPasswordResult pck = new SCRspPasswordResult();
            pck.pckType = pckType;
            pck.state = state;
            SendToClient(serverSession, pck);
        }

        internal static void SendSCNotiReconnect(string serverSession)
        {
            SCNotiReconnect pck = new SCNotiReconnect();
            SendToClient(serverSession, pck);
        }

        internal static void SendSCNotiQuit(string serverSession)
        {
            SCNotiQuit pck = new SCNotiQuit();
            SendToClient(serverSession, pck);
        }

        internal static void SendSCRspIAP(string serverSession, string guid_, int elementIdx_)
        {
            SCRspIAP pck = new SCRspIAP();
            pck.guid = guid_;
            pck.elementIdx = elementIdx_;
            SendToClient(serverSession, pck);
        }

        internal static void SendSCRspIAPResult(string serverSession, string guid, ShopResult shopResult)
        {
            SCRspIAPResult pck = new SCRspIAPResult();
            pck.guid = guid;
            pck.result = shopResult;
            SendToClient(serverSession, pck);
        }

        internal static void SendSCNotiCMD(string serverSession, string msg)
        {
            SCNotiCMD pck = new SCNotiCMD();
            pck.msg = msg;
            SendToClient(serverSession, pck);
        }

        internal static void SendSCNotiMenuAlarm(string serverSession, /*MenuAlarm menuAlarm*/Option option, string alarm)
        {
            SCNotiMenuAlarm pck = new SCNotiMenuAlarm();
            //pck.menuItem = menuAlarm;
            pck.option = option;
            pck.alarm = alarm;
            SendToClient(serverSession, pck);
        }
    }
}
