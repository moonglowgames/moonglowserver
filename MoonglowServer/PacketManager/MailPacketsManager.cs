﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class MailPacketsManager : Singleton<MailPacketsManager>
{
    internal void GetCSReqMailList(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.sceneType = SceneType.Mail;
        if (userInfo._mailList == null)
        {
            MailDBManager.Instance.GetMailListFromDB(serverSession, userSession);
        }

        //Send SCNotiMailList
        MailDBManager.Instance.SetSendSCNotiMailList(serverSession, userSession);
    }

    internal void GetCSReqGetItem(string serverSession, string userSession, int mailIdx)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        bool getResult = false;
        if (userInfo._mailList != null && userInfo._mailList.Count > 0)
        {
            if ((userInfo._mailList.Count >= mailIdx) && (userInfo._mailList[mailIdx - 1].itemGot == false))
            {
                int itemIdx1 = userInfo._mailList[mailIdx - 1].itemIdx1;
                int itemCount1 = userInfo._mailList[mailIdx - 1].itemCount1;

                userInfo.AddUserItem(itemIdx1, itemCount1);

                userInfo._mailList[mailIdx - 1].itemGot = true;
                userInfo._mailList[mailIdx - 1].itemGotDate = DateTime.Now;

                userInfo._mailList[mailIdx - 1].del = true;
                getResult = true;
            }
            else
            {
                if (userInfo._mailList.Count < mailIdx)
                {
                    //Console.WriteLine("!!!Error: Email[{0}] is not exist, Email count is: {1}!!!", mailIdx - 1, userInfo._mailList.Count);
                    if (/*Logger*/GameServer._logger.IsErrorEnabled)
                    {
                        /*Logger*/
                        string log = string.Format("Email[{ 0}] is not exist, Email count is: {1}, user: {2}!!!", mailIdx - 1, userInfo._mailList.Count, userInfo.userID);
                        GameServer._logger.Error(log);
                    }
                }
                else if (userInfo._mailList[mailIdx - 1].itemGot == true)
                {
                    //Console.WriteLine("!!!Error: Email[{0}]'s item has already received!!!", mailIdx - 1);
                    if (/*Logger*/GameServer._logger.IsErrorEnabled)
                    {
                        /*Logger*/
                        string log = string.Format("Email[{0}]'s item has already received, user: {1}!!!", mailIdx - 1, userInfo.userID);
                        GameServer._logger.Error(log);
                    }
                }
            }
        }
        else
        {
            if (userInfo._mailList == null)
            {
                //Console.WriteLine("!!!Error: mailList is null!!!");
                if (/*Logger*/GameServer._logger.IsErrorEnabled)
                {
                    /*Logger*/
                    string log = string.Format("mailList is null, user: {0}!!!", userInfo.userID);
                    GameServer._logger.Error(log);
                }
            }
            else if (userInfo._mailList.Count == 0)
            {
                //Console.WriteLine("!!!Error: mailList is empty!!!");
                if (/*Logger*/GameServer._logger.IsErrorEnabled)
                {
                    /*Logger*/
                    string log = string.Format("mailList is empty, user: {0}!!!", userInfo.userID);
                    GameServer._logger.Error(log);
                }
            }
        }

        //Send SCRspGetItem
        PacketsHandlerServer.SendSCRspGetItem(serverSession, getResult);

        if (getResult == true)
        {
            //send SCNotiMailList again...
            MailDBManager.Instance.SetSendSCNotiMailList(serverSession, userInfo.session);

            //send SCNotiMenuAlarm - mailbox
            bool allowZero = true;
            userInfo.SetSendMenuAlarmMail(allowZero);

            //save to mail db...
            PacketsHandlerInner.SDReqUpdateMailItem(serverSession, userSession, userInfo._mailList[mailIdx - 1].idx);

            //save userInfo
            //save items...
            userInfo.SaveUserInfo();
        }
    }

    internal void CSReqGetAllItems(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        bool getResult = false;
        List<int> tmpMailIdxList = new List<int>();
        if (userInfo != null)
        {
            if (userInfo._mailList != null && userInfo._mailList.Count > 0)
            {
                foreach (var mailData in userInfo._mailList)
                {
                    if (mailData.itemGot == false)
                    {
                        int itemIdx1 = mailData.itemIdx1;
                        int itemCount1 = mailData.itemCount1;
                        userInfo.AddUserItem(itemIdx1, itemCount1);

                        //add to userInfo
                        // userInfo.ChangeUserItemAmount(mailData.itemIdx1, mailData.itemCount1);
                        mailData.itemGot = true;
                        mailData.itemGotDate = DateTime.Now;
                        mailData.del = true;
                        tmpMailIdxList.Add(mailData.idx);
                    }
                }

                if (tmpMailIdxList.Count == 0)
                {
                    //Console.WriteLine("!!!Error: All items have already received!!!");
                    if (/*Logger*/GameServer._logger.IsErrorEnabled)
                    {
                        /*Logger*/
                        string log = string.Format("CSReqGetAllItems-All items have already received, user: {0}!!!", userInfo.userID);
                        GameServer._logger.Error(log);
                    }
                }
                else { getResult = true; }
            }
            else
            {
                if (userInfo._mailList == null)
                {
                    //Console.WriteLine("!!!Error: mailList is null!!!");
                    if (/*Logger*/GameServer._logger.IsErrorEnabled)
                    {
                        /*Logger*/
                        string log = string.Format("Mail liset is null, user: {0}!!!", userInfo.userID);
                        GameServer._logger.Error(log);
                    }
                }
                else if (userInfo._mailList.Count == 0)
                {
                    //Console.WriteLine("!!!Error: mailList is empty!!!");
                    if (/*Logger*/GameServer._logger.IsErrorEnabled)
                    {
                        /*Logger*/
                        string log = string.Format("Mail liset is empty, user: {0}!!!", userInfo.userID);
                        GameServer._logger.Error(log);
                    }
                }
            }
        }
        else
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        //SCRspGetAllItems 
        PacketsHandlerServer.SendSCRspGetAllItems(serverSession, getResult);

        if (getResult == true)
        {
            //send scnotiuserelelemt again...
            userInfo.SetSendElementInfoList();

            //send SCNotiMailList again...
            MailDBManager.Instance.SetSendSCNotiMailList(serverSession, userInfo.session);

            //send SCNotiMenuAlarm - mailbox
            bool allowZero = true;
            userInfo.SetSendMenuAlarmMail(allowZero);

            //send itemInfoList again...
            //userInfo.SetSendItemInfoList();

            //save to mail db...
            foreach (var mailIdx in tmpMailIdxList)
            {
                PacketsHandlerInner.SDReqUpdateMailItem(serverSession, userSession, mailIdx);
            }

            //save userInfo
            //save items
            userInfo.SaveUserInfo();
        }
    }
}
