﻿using LitJson;
using MoonGlow;
using MoonGlow.Data;
using MoonGlow.Room;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

public partial class InnerPacketsManager : Singleton<InnerPacketsManager>
{
    internal void GetMSNotiRemoveRedis(string serverSession, bool isLogout, bool removeResult, string session)
    {
        if (isLogout == true)
        {
            PacketsHandlerServer.SendSCNotiLogout(serverSession, removeResult);
        }

        if (removeResult == true)
        {
            //Console.WriteLine("*** Redis: Remove from MemDB Successfully.");

            //bool isRemoveUserID = true;
            UserInfo userInfo = GameServer.GetUserInfo(session);
            if (userInfo != null)
            {
                if (userInfo.userServerState == UserServerStatus.DisConnect) userInfo.SetLogOut();
            }

            if (!GameServer._enable)
            {
                //reconnect
                UserType userType = UserType.User;
                GlobalManager.Instance.GetUserType(userInfo.userDataBase.account, out userType);
                if (userType == UserType.User) PacketsHandlerServer.SendSCNotiReconnect(userInfo.serverSession);
            }

            GlobalManager.Instance.RemoveUserInfo(session);

        }
        else
        {
            Console.WriteLine("*** Redis: Remove from MemDB value Failed.");
        }
    }

    internal void GetDSRspNewMail(string serverSession, bool addResult, string userSession, string senderID)
    {
        if (addResult == true)
        {
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo != null)
            {
                if (senderID != userInfo.userID)//me send to other
                {
                    MailDBManager.Instance.GetMailListFromDB(serverSession, userSession);
                }
            }
            //Console.WriteLine("*** DB: Add Mail to DB Successfully");
        }
        else
        {
            Console.WriteLine("*** DB: Add Mail to DB Failed");
        }
    }

    internal void GetDSRspGetFriendList(string serverSession, bool getResult, string userSession)
    {
        if (getResult == true)
        {
            //Console.WriteLine("*** DB: Get friendList from DB Successfully");

            //SAVE TO USERINFO
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo != null)
            {
                userInfo._allFriends = new List<FriendData>();
                foreach (var friend in userInfo._myFriends)
                {
                    userInfo._allFriends.Add(friend);
                }
                foreach (var friend in userInfo._requestFriends)
                {
                    userInfo._allFriends.Add(friend);
                }

                //Initial RecommendFriends
                userInfo.InitRecommendFriends();
                userInfo.userDataBase.userProfile.friendsCount = userInfo._myFriends.Count;

                if (userInfo._delRequestFriends.Count > 0)
                {
                    foreach (var friend in userInfo._delRequestFriends)
                    {
                        FriendDBManager.Instance.DeleteFriend(userSession, friend.friendID, friend.friendType);
                    }
                }
            }
        }
        else
        {
            Console.WriteLine("*** DB: Get friendList from DB Failed");
        }
    }

    internal void GetDSRspGetRequestFriends(string serverSession, bool getResult, string userSession)
    {
        if (getResult == true)
        {
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo != null)
            {
                List<Friend> friendList = new List<Friend>();
                for (int i = 0; i < userInfo._requestFriends.Count; i++)
                {
                    friendList.Add(new Friend(i + 1, userInfo._requestFriends[i].friendNick, userInfo._requestFriends[i].faceIdx, userInfo._requestFriends[i].level, userInfo._requestFriends[i].friendLastLoginDate, userInfo._requestFriends[i].friendType, false, false, 0, 0, 0));
                }

                //send request friends
                PacketsHandlerServer.SendSCNotiFriendList(serverSession, new FriendList(PacketTypeList.SCNotiFriendList.ToString(), FriendTabType.RequestFriends, 20, friendList));

                if (userInfo._delRequestFriends.Count > 0)
                {
                    foreach (var friend in userInfo._delRequestFriends)
                    {
                        FriendDBManager.Instance.DeleteFriend(userInfo.session, friend.friendID, friend.friendType);
                    }
                }
            }

            //Console.WriteLine("*** DB: Get Request Friends from DB Successfully");
        }
        else
        {
            Console.WriteLine("*** DB: Get Request Friends from DB Failed");
        }
    }

    internal void GetDSRspUserFriendCount(string serverSession, bool getResult, string session, int friendIdx, int otherFriendCount, PacketTypeList sender)
    {
        if (getResult == true)
        {
            //Console.WriteLine("*** DB: Get friendCount from DB Successfully");

            FriendResult friendResult = FriendResult.Unknown;
            UserInfo userInfo = GameServer.GetUserInfo(session);
            if (userInfo != null)
            {
                if (otherFriendCount < 20)
                {
                    friendResult = FriendResult.OK;
                    if (sender == PacketTypeList.CSReqAcceptFriend)
                    {
                        userInfo.AddMyFriends(session, friendIdx, otherFriendCount, out friendResult);
                    }
                    else if (sender == PacketTypeList.CSReqRequestFriend)
                    {
                        userInfo.AddRequestFriends(session, friendIdx, out friendResult);
                    }
                }
                else
                {
                    friendResult = FriendResult.NotAccept;

                    //refresh recommend friend list
                    userInfo._recommendFriends.RemoveAt(friendIdx - 1);
                    List<Friend> friendList = new List<Friend>();
                    for (int i = 0; i < userInfo._recommendFriends.Count; i++)
                    {
                        friendList.Add(new Friend(i + 1, userInfo._recommendFriends[i].friendNick, userInfo._recommendFriends[i].faceIdx, userInfo._recommendFriends[i].level, userInfo._recommendFriends[i].friendLastLoginDate, userInfo._recommendFriends[i].friendType, false, false, 0, 0, 0));
                    }
                    //send SCNotifriend list again-recommend
                    PacketsHandlerServer.SendSCNotiFriendList(serverSession, new FriendList(PacketTypeList.SCNotiFriendList.ToString(), FriendTabType.RecommendFriends, 20, friendList));
                }
            }
            else { friendResult = FriendResult.Fail; }

            //Send SCRspFriendResut
            PacketsHandlerServer.SendSCRspFriendResut(serverSession, sender, friendResult);

        }
        else
        {
            Console.WriteLine("*** DB: Get friendCount from DB Failed");
        }
    }

    internal void GetMSRspGetChanelList(string serverSession, ChanelInfoList chanelInfoList)
    {
        UserInfo userInfo = GameServer.GetUserInfo(chanelInfoList.userSession);
        if (userInfo != null)
        {
            userInfo._chanelDic = new Dictionary<int, ChanelInfo>();
            List<Chanel> chanelList = new List<Chanel>();
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();

            bool isJoin = false;
            bool enable = false;
            string name = string.Empty;
            string description = string.Empty;
            for (int i = 0; i < chanelInfoList.chanelInfoList.Count; i++)
            {
                isJoin = false;
                enable = false;
                if (userInfo.userDataBase.userProfile.chanelIdx == chanelInfoList.chanelInfoList[i].chanelIdx)
                {
                    isJoin = true;
                }

                if (userInfo.userDataBase.Level >= chanelInfoList.chanelInfoList[i].minLevel && userInfo.userDataBase.Level <= chanelInfoList.chanelInfoList[i].maxLevel)
                {
                    enable = true;
                }

                if (GlobalManager.Instance.ConvertToDateTime(chanelInfoList.chanelInfoList[i].startDate, out startDate) && GlobalManager.Instance.ConvertToDateTime(chanelInfoList.chanelInfoList[i].endDate, out endDate))
                {
                    string _startDate = startDate.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss");
                    string _endDate = endDate.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss");
                    name = (LocalizationManager._dict.ContainsKey(chanelInfoList.chanelInfoList[i].name))?
                            LocalizationManager.Instance.Get(chanelInfoList.chanelInfoList[i].name, userInfo.language): chanelInfoList.chanelInfoList[i].name;
                    description = (LocalizationManager._dict.ContainsKey(chanelInfoList.chanelInfoList[i].description)) ? 
                                   LocalizationManager.Instance.Get(chanelInfoList.chanelInfoList[i].description, userInfo.language) : chanelInfoList.chanelInfoList[i].description;
                    chanelList.Add(new Chanel(i + 1, chanelInfoList.chanelInfoList[i].chanelType, name, description, chanelInfoList.chanelInfoList[i].minLevel, chanelInfoList.chanelInfoList[i].maxLevel, _startDate, _endDate, chanelInfoList.chanelInfoList[i].currentCount, chanelInfoList.chanelInfoList[i].maxCount/*GlobalManager.CHANEL_MAX_COUNT*/, isJoin, enable));

                    if (!userInfo._chanelDic.ContainsKey(i + 1))
                    {
                        userInfo._chanelDic.Add(i + 1, chanelInfoList.chanelInfoList[i]);
                    }
                   
                }
            }

            PacketsHandlerServer.SendSCNotiChanelList(serverSession, new ChanelList(PacketTypeList.SCNotiChanelList.ToString(), chanelList));
        }
    }

    internal void GetDSRspRankRange(string serverSession, RankList rankList)
    {
        if (rankList.rankList == null)
        {
            Console.WriteLine("*** Redis: Get Current Rank List from Redis Failed.");
        }
        else
        {
            //Console.WriteLine("*** Redis: Get Current Rank List from Redis Successfully.");
            if (rankList.rankList.Count != 0)
            {
                //send rankList
                PacketsHandlerServer.SendSCNotiRankList(serverSession, rankList);
            }
        }
    }

    internal void GetDSRspNewRankHis(string serverSession, bool addResult, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null) {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        if (addResult == true)
        {
            //Console.WriteLine("*** DB: Add new rank to DB successfully.");
            if (userInfo.sceneType == SceneType.RankHistory)
            {
                //get rankHis from DB...
                PacketsHandlerInner.SDReqRankHistory(serverSession, userSession);
            }
        }
        else
        {
            Console.WriteLine("*** DB: Add new rank to DB Failed.");
        }
    }

    internal void GetMSRspRepairChanel(string serverSession, bool repairResult)
    {
        string msg = string.Empty;
        if (repairResult == true)
        {
            msg = "Repair Rank Redis Successfully!";
        }
        else
        {
            msg = "Repair Rank Redis Failed!";
        }

        PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
    }

    internal void GetMSNotiBCServerNoti(string message)
    {
        if (GameServer._isLoginServer) return;

        List<string> userSessionList = new List<string>();
        string[] cmdArray = message.Split(' ');

        bool needNoti = false;
        if (cmdArray[0].ToLower() == "@noti")
        {
            needNoti = true;
        }
        else
        {
            int serverIndex = 0;
            if (Int32.TryParse(cmdArray[0].ToLower().Substring("@noti".Length), out serverIndex))
            {
                if (GameServer._allServerList == null) return; 

                if (serverIndex <= 0 || serverIndex > GameServer._allServerList.Count) return; 

                string[] address = GameServer._allServerList[serverIndex - 1].url.Split(':');
                if (address.Length != 2) return; 

                if (/*GameServer._connectIP*/GameServer._DNS == address[0] && GameServer._port == int.Parse(address[1]))
                {
                    needNoti = true;
                }
            }
             
        }

        if (needNoti)
        {
            foreach (var infoPair in SessionIdManager.Instance)
            {
                //if (infoPair.Value.userServerState != UserServerStatus.DisConnect)
                {
                    userSessionList.Add(infoPair.Value.session);
                }
            }

            string value = message.Substring(cmdArray[0].Length + 1, message.Length - cmdArray[0].Length - 1);

            if (!string.IsNullOrEmpty(value))
            {
                PacketsHandlerServer.SendSCNotiServerNoti(Command.Message, value, userSessionList);
            }
        }
    }

    internal void GetSSReqClientPause(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }
        if (DateTime.Now >= userInfo.pauseExpireDate)
        {
            userInfo.isPause = false;

            if (!string.IsNullOrEmpty(userInfo.roomID))
            {
                Room room = GameServer.GetRoom(userSession);
                if (room != null)
                {
                    room.SetPausePlayer(userSession);
                }
            }

            //3. set expire session   
            userInfo.SetDisConnect();
            userInfo.userInfoRemoveDate = DateTime.Now + GlobalManager.EXPIRE_SESSION_TIMESPAN;
        }
    }

    internal void GetDSRspLoginCheck(string serverSession, LoginState state, bool sameDevice, string deviceID, string userID, UserType userType, string nick, Language language)
    {
        if (state == LoginState.OK)
        {
            //check deviceID
            if (!sameDevice)
            {
                //BC - close current client.
                PacketsHandlerInner.SMReqBCForceClose(serverSession, userID);

                PacketsHandlerInner.SDReqUpdatedeviceID(serverSession, userID, deviceID);
            }

            PacketsHandlerInner.SMReqAddRedis(serverSession, userID, userType, nick, language);
        }
        else
        {
            //if (state == LoginState.Ban) PacketsHandlerServer.SendSCNotiBanUser(serverSession, banTime);
            string userSession = serverSession;
            PacketsHandlerServer.SendSCNotiLogin(serverSession, state, userType, userID, userSession, GameServer._DNS + ":9000");
        }
    }

    internal void GetDSRspRegisterCheck(string serverSession, LoginState state, AccountType accountType, string account, string password, string nick, int faceIdx, string deviceID)
    {
        if (state == LoginState.OK || state == LoginState.GuestOK)
        {
            PacketsHandlerInner.SDReqNewUserData(serverSession, accountType, account, password, nick, faceIdx, deviceID);
        }
        else
        {
            PacketsHandlerServer.SendSCRspRegister(serverSession, state);
        }

    }

    internal void GetMSRspSessionCheck(string serverSession, bool result, string userSession, string userID, Language language)
    {
        if (result == true)
        {
            //Console.WriteLine("*** Redis: LogingSession exist in redis.");
            //4. if session exist then read userData
            if (SessionIdManager.Instance.AddSession(userSession, serverSession, language))
            {
                PacketsHandlerInner.SDReqReadUserData(serverSession, userSession, userID);
            }
            //5. else sessionLogin fail, go to login server please..
            else
            {
                //go to login server..
                PacketsHandlerServer.SendSCNotiSessionLogin(serverSession, false, string.Empty, UserType.User);
            }
        }
        else
        {
            //go to login server..
            //...
            Console.WriteLine("*** Redis: Session not exist in redis.");
            PacketsHandlerServer.SendSCNotiSessionLogin(serverSession, false, string.Empty, UserType.User);
        }
    }

    internal void GetMSRspCurrentRank(string serverSession, string userSession, string userID, /*float*/int rankPoint, int rankWin, int rankLose, int rankDraw, string chanelName, int chanelIdx, int totalCount, int currentCount, bool isChanelClose)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        int currentPercent = 0;
        if (totalCount != 0) { currentPercent = currentCount * 100 / totalCount; }

        if (isChanelClose == true)
        {
            //check item exist or not
            bool itemExist = false;
            ChannelRewardManager.Instance.CheckRankItemExist(chanelIdx, currentCount, currentPercent, out itemExist);

            PacketsHandlerInner.SDReqNewRankHis(serverSession, userSession, userID, chanelName, currentCount, currentPercent, rankPoint, rankWin, rankLose, rankDraw, itemExist);
        }
        else
        {
            int totalRankFight = rankWin + rankLose /*+ rankDraw*/;
            decimal winRate = 0;
            if (totalRankFight != 0) winRate = (decimal)rankWin / totalRankFight;
            string winRateStr = winRate.ToString("#0.##%", CultureInfo.InvariantCulture);

            //Send SCNotiMyRankInfo
            string _channelName = (LocalizationManager._dict.ContainsKey(chanelName)) ? 
                                    LocalizationManager.Instance.Get(chanelName, userInfo.language) : chanelName;

            PacketsHandlerServer.SendSCNotiMyRankInfo(serverSession, new RankHistory(PacketTypeList.SCNotiMyRankInfo.ToString(), _channelName, currentCount, currentPercent, rankPoint, rankWin, rankLose, winRateStr, userInfo._rankHistroy));
        }
    }

    internal void GetDSRspRankHistory(string serverSession, PCKRankHisList rankHisList)
    {
        if (rankHisList.rankHisList == null)
        {
            Console.WriteLine("*** DB: Get Rank Histroy From DB Failed.");
        }
        else
        {
            //Console.WriteLine("*** DB: Get Rank Histroy From DB successfully.");

            UserInfo userInfo = GameServer.GetUserInfo(rankHisList.session);
            if (userInfo == null)
            {
                GlobalManager.Instance.NullUserInfo(serverSession);
                return;
            }

            userInfo._rankHistroy = new List<MyRank>();
            int bestRankPoint = 0;
            string bestChanelName = string.Empty;
            List<ChanelInfo> chanelInfoList = null;
            ChannelManager.Instance.GetChanelInfoList(out chanelInfoList);

            foreach (var info in rankHisList.rankHisList)
            {
                if (bestRankPoint <= info.rankPoint)
                {
                    bestRankPoint = info.rankPoint;
                    bestChanelName = info.chanelName;
                }
            }

            for (int i = 0; i < rankHisList.rankHisList.Count; i++)
            {
                bool _isBest = false;
                int _chanelIdx = 0;
                if (rankHisList.rankHisList[i].chanelName == bestChanelName)
                {
                    _isBest = true;
                }

                foreach (var info in chanelInfoList)
                {
                    if (info.name == rankHisList.rankHisList[i].chanelName)
                    {
                        _chanelIdx = info.chanelIdx;
                        break;
                    }
                }

                //Send SCNotiMyRankInfo
                string _channelName = (LocalizationManager._dict.ContainsKey(rankHisList.rankHisList[i].chanelName))?
                                      LocalizationManager.Instance.Get(rankHisList.rankHisList[i].chanelName, userInfo.language) : rankHisList.rankHisList[i].chanelName;

                userInfo._rankHistroy.Add(new MyRank(i + 1, _channelName, rankHisList.rankHisList[i].rankCount, rankHisList.rankHisList[i].rankPercent, rankHisList.rankHisList[i].rankPoint, rankHisList.rankHisList[i].rankWin, rankHisList.rankHisList[i].rankLose, rankHisList.rankHisList[i].winRate, _isBest, rankHisList.rankHisList[i].itemExist, rankHisList.rankHisList[i].itemGot));
            }

            if (userInfo.userDataBase.userProfile.chanelIdx != 0)
            {
                foreach (var info in chanelInfoList)
                {
                    if (userInfo.userDataBase.userProfile.chanelIdx == info.chanelIdx)
                    {
                        PacketsHandlerInner.SMReqCurrentRank(serverSession, userInfo.session, userInfo.userID, info.chanelIdx, info.name, userInfo.userDataBase.userProfile.rankPoint, userInfo.userDataBase.userProfile.rankWin, userInfo.userDataBase.userProfile.rankLose, userInfo.userDataBase.userProfile.rankDraw, false);

                        break;
                    }
                }
            }
            else PacketsHandlerServer.SendSCNotiMyRankInfo(serverSession, new RankHistory(PacketTypeList.SCNotiMyRankInfo.ToString(), string.Empty, 0, 0, 0, 0, 0, "0%", userInfo._rankHistroy));


        }
    }

    internal void GetSMReqCheckUserBan(string serverSession, string userID, string nick, UserType userType, bool sameDevice, string deviceID, LoginState state, Language language)
    {
        bool isBan = BanRedisConnect.Instance.CheckUserBan(userID).Result;
        if (isBan) state = LoginState.Ban;
        
        PacketsHandlerInner.DSRspLoginCheck(serverSession, userID, userType, nick, sameDevice, deviceID, state, language);
    }

    internal void GetMSRspAddRank(string serverSession, ChanelResult result, string userSession, string chanelName, int currentCount, int channelIdx)
    {
        PacketsHandlerServer.SendSCRspRankResult(serverSession, result);

        if (result == ChanelResult.OK)
        {
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo == null)
            {
                GlobalManager.Instance.NullUserInfo(serverSession);
                return;
            }

            //List<ChanelInfo>  
            userInfo.userDataBase.userProfile.chanelIdx = /*userInfo._chanelDic[iDx].chanelIdx*/channelIdx;
            userInfo.userDataBase.userProfile.rankPoint = 0;
            userInfo.userDataBase.userProfile.rankWin = 0;
            userInfo.userDataBase.userProfile.rankLose = 0;
            userInfo.userDataBase.userProfile.rankDraw = 0;

            //send SCNotiChanelList again..
            ChannelManager.Instance.GetChanelList(serverSession, userSession);

            //save userInfo
            //save chanelIdx and rankPoint
            userInfo.SaveUserInfo();

            //add to chanel_user
            PacketsHandlerInner.SDReqAddChanelUser(serverSession, chanelName, userInfo.userID);

        }
        else
        {
            Console.WriteLine("*** Redis: Add Rank to Redis Failed.");
        }
    }

    internal void GetDSRspDeleteFriend(string serverSession, bool deleteResult, string userSession, FriendType friendType, string friendID, int otherFriendCount)
    {
        if (deleteResult == true)
        {
            if (friendType == FriendType.Friend)
            {
                UserInfo userInfo = GameServer.GetUserInfo(userSession);
                if (userInfo != null)
                {
                    userInfo.UpdateFriendCount();
                }

                UserInfo friendInfo = null;
                foreach (var infoPair in SessionIdManager.Instance)
                {
                    friendInfo = infoPair.Value;
                    if (friendInfo != null)
                    {
                        if (friendInfo.userServerState != UserServerStatus.DisConnect && friendInfo.userID == friendID)
                        {
                            userInfo.UpdateFriendCount();
                        }
                        else { /* do nothing...maybe not right..><*/}
                    }
                }
            }

            //Console.WriteLine("*** DB: delete friend from DB Successfully");
        }
        else
        {
            Console.WriteLine("*** DB: delete friend from DB Failed");
        }
    }

    internal void GetDSRspUpdateFriendType(string serverSession, bool updateResult,string userSession, string friendID, int otherFriendCount)
    {
        if (updateResult == true)
        {
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo != null)
            {
                userInfo.UpdateFriendCount();
                
                //update friend_count - mine and other's
                //PacketsHandlerDB.SDReqUpdateFriendCount(serverSession, userSession, userInfo._myFriends.Count, friendID, otherFriendCount++);
            }

            UserInfo friendInfo = null;
            foreach (var infoPair in SessionIdManager.Instance)
            {
                friendInfo = infoPair.Value;
                if (friendInfo != null)
                {
                    if (friendInfo.userServerState != UserServerStatus.DisConnect && friendInfo.userID == friendID)
                    {
                        friendInfo.UpdateFriendCount();
                    }
                    else { /* do nothing...maybe not right..><*/}
                }
            }
            //Console.WriteLine("*** DB: Update Friend to DB Successfully");
        }
        else
        {
            Console.WriteLine("*** DB: Update Friend to DB Failed");
        }
    }

    internal void GetDSRspNewFriend(string serverSession, bool addResult, string userID, string friendID)
    {
        if (addResult == true)
        {
            UserInfo friendInfo = null;
            foreach (var infoPair in SessionIdManager.Instance)
            {
                friendInfo = infoPair.Value;
                if (friendInfo.userID == friendID && friendInfo.userServerState != UserServerStatus.DisConnect)
                {
                    foreach (var recommendFriend in friendInfo._recommendFriends)
                    {
                        if (recommendFriend.friendID == userID)
                        {
                            friendInfo._recommendFriends.Remove(recommendFriend);

                            if (friendInfo.sceneType == SceneType.Friend)
                            {
                                List<Friend> friendList = new List<Friend>();
                                for (int i = 0; i < friendInfo._recommendFriends.Count; i++)
                                {
                                    friendList.Add(new Friend(i + 1, friendInfo._recommendFriends[i].friendNick, friendInfo._recommendFriends[i].faceIdx, friendInfo._recommendFriends[i].level, friendInfo._recommendFriends[i].friendLastLoginDate, friendInfo._recommendFriends[i].friendType, false, false, 0, 0, 0));
                                }

                                //send SCNotifriend list again-recommend
                                PacketsHandlerServer.SendSCNotiFriendList(friendInfo.serverSession, new FriendList(PacketTypeList.SCNotiFriendList.ToString(), FriendTabType.RecommendFriends, 20, friendList));
                            }
                            break;
                        }
                    }

                    //send SCNotiMenuAlarm - friend
                    int alarmCount = 1;
                    bool allowZero = false;
                    friendInfo.SetSendMenuAlarmFriends(alarmCount, allowZero);

                    //in order to send SCNotiMenuAlarm - friend on SessionLogin
                    friendInfo.GetRequestFriends();

                    break;
                }
            }
            //Console.WriteLine("*** DB: Add Friend to DB Successfully");
        }
        else
        {
            Console.WriteLine("*** DB: Add Friend to DB Failed");
        }
    }

    internal void GetDSRspGetMyFriends(string serverSession, bool getResult, string userSession)
    {
        if (getResult == true)
        {
            UserInfo userInfo = GameServer.GetUserInfo(userSession);
            if (userInfo != null && userInfo.sceneType == SceneType.Friend)
            {
                userInfo.SetSendMyFriends();
            }

            //Console.WriteLine("*** DB: Get My Friends from DB Successfully");
        }
        else
        {
            Console.WriteLine("*** DB: Get My Friends from DB Failed");
        }
    }

    internal void GetDSRspReadUserData(string serverSession, bool readResult, string userSession,/*string nick, int level, int faceIdx, int rankPoint, string lastLoginDate, int friendCount, int checkInDays,*/ string jsonString)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }
        if (readResult == true)
        {
            try
            {
                UserInfoDataBaseManager._userData = JsonMapper.ToObject<UserData>(jsonString);
                if(UserInfoDataBaseManager._userData == null)UserInfoDataBaseManager._userData = new UserData();
            }
            catch (Exception ex)
            {
                // logging ex.message
                //Console.WriteLine("!!!UserInfoDataBaseManager._userData Error: " + ex.Message);
                if (/*Logger*/GameServer._logger.IsErrorEnabled)
                {
                    /*Logger*/
                    string log = "UserInfoDataBaseManager._userData Exception : (" + ex.Message + ")";
                    GameServer._logger.Error(log);
                }
            }

            SetUserInfo(/*nick, level, faceIdx, rankPoint, lastLoginDate, friendCount, checkInDays,*/userInfo);
            //Console.WriteLine("*** DB: Read userInfo from DataBase successfully");

            //send SCNotiSessionLogin
            UserType userType = UserType.User;
            GlobalManager.Instance.GetUserType(userInfo.userDataBase.account, out userType);
            string newSession = userInfo.session;
            PacketsHandlerServer.SendSCNotiSessionLogin(serverSession, true, newSession, userType);

            userInfo.CheckDailyCheckInAndGift();
            userInfo.userDataBase.lastLoginDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            userInfo.CheckMissionActive();//have saveuserinfo inside
            userInfo.CheckAchieveActive();
            //check achieve -check in 
            userInfo.CheckAchieveValue(GameType.Unknown, AchieveType.CheckIn, userInfo.userDataBase.userProfile.checkInDays);
            userInfo.CheckAchieveValue(GameType.Unknown, AchieveType.ConCheckIn, userInfo.userDataBase.userProfile.conCheckInDays);
            //check mission -check in 
            //userInfo.CheckMissionValue(GameType.Unknown, AchieveType.CheckIn, userInfo.userDataBase.userProfile.checkInDays);
            userInfo.SaveUserInfo();//login date, mission, achieve

            //set staminaTime event 
            userInfo.SetStaminaEvent();
         
            //check ChanelDate...if joined a chanel
            ChannelManager.Instance.CheckChanelDate(serverSession, userInfo.session, userInfo.userDataBase.userProfile.chanelIdx);

            userInfo.isReadDone = true;
            if (userInfo.needNotiGotoScene)
            {
                userInfo.needNotiGotoScene = false;
                // send packet...
                userInfo.SetHome();
                PacketsHandlerServer.SendSCNotiGotoScene(serverSession, SceneType.Home);
            }

            if (userInfo._mailList == null)
            {
                MailDBManager.Instance.GetMailListFromDB(serverSession, userInfo.session);
            }

            if (userInfo._allFriends == null)
            {
                FriendDBManager.Instance.GetFriendListFromDB(serverSession, userInfo.session);
            }

            //send SCNotiChatIconSet
            userInfo.SetSendChatIconSet();

            if (GameServer._isCloud == true)
            {
                //SessionLogin Log
                GlobalManager.Instance.LogAccountInsert(serverSession, userInfo.userID, userInfo.userDataBase.nickName, LogAccountType.SessionLogin);
            }
        }
        else
        {
            Console.WriteLine("*** DB: Read userInfo from DataBase failed");
        }
    }

    public void SetUserInfo(/*string nick, int level, int faceIdx, int rankPoint, string lastLoginDate, int friendCount, int checkInDays,*/ UserInfo userInfo)
    {
        userInfo.userDataBase.Stamina = UserInfoDataBaseManager._userData.resourceData.Stamina;
        userInfo.userDataBase.maxStamina = UserInfoDataBaseManager._userData.resourceData.maxStamina;
        userInfo.userDataBase.Gold = UserInfoDataBaseManager._userData.resourceData.Gold;
        userInfo.userDataBase.Ruby = UserInfoDataBaseManager._userData.resourceData.Ruby;
        userInfo.userDataBase.Ink = UserInfoDataBaseManager._userData.resourceData.Ink;
        userInfo.userDataBase.JokerLine = UserInfoDataBaseManager._userData.resourceData.JokerLine;

        userInfo.userDataBase.userItem.etherealInk = UserInfoDataBaseManager._userData.itemData.etherealInk;
        userInfo.userDataBase.userItem.ePElixir = UserInfoDataBaseManager._userData.itemData.ePElixir;
        userInfo.userDataBase.userItem.rosemaryOil = UserInfoDataBaseManager._userData.itemData.rosemaryOil;
        userInfo.userDataBase.userItem.sLElixir = UserInfoDataBaseManager._userData.itemData.sLElixir;
        userInfo.userDataBase.userItem.sOElixir = UserInfoDataBaseManager._userData.itemData.sOElixir;
        userInfo.userDataBase.userItem.saqqaraTablet = UserInfoDataBaseManager._userData.itemData.saqqaraTablet;
        userInfo.userDataBase.userItem.petList = UserInfoDataBaseManager._userData.itemData.petList;
        userInfo.userDataBase.userItem.badgeList = UserInfoDataBaseManager._userData.itemData.badgeList;
        userInfo.userDataBase.userItem.cardSkinList = UserInfoDataBaseManager._userData.itemData.cardSkinList;
        userInfo.userDataBase.userItem.jokerFaceList = UserInfoDataBaseManager._userData.itemData.jokerFaceList;
        userInfo.userDataBase.userItem.iconSet = UserInfoDataBaseManager._userData.itemData.iconSet;

        //userJoker list
        userInfo.userDataBase.userJoker.faceIdx = UserInfoDataBaseManager._userData.jokerData.faceIdx;
        userInfo.userDataBase.userJoker.spellIdxList = UserInfoDataBaseManager._userData.jokerData.spellIdxList;
        //userInfo.SetTmpSpellIdxList();

        userInfo.userDataBase.userProfile.addHP = UserInfoDataBaseManager._userData.profileData.addHP;
        userInfo.userDataBase.userProfile.addSP = UserInfoDataBaseManager._userData.profileData.addSP;
        userInfo.userDataBase.userProfile.winCount = UserInfoDataBaseManager._userData.profileData.winCount;
        userInfo.userDataBase.userProfile.loseCount = UserInfoDataBaseManager._userData.profileData.loseCount;
        userInfo.userDataBase.userProfile.drawCount = UserInfoDataBaseManager._userData.profileData.drawCount;
        userInfo.userDataBase.userProfile.conWinCount = UserInfoDataBaseManager._userData.profileData.conWinCount;
        userInfo.userDataBase.userProfile.CoConWinCount = UserInfoDataBaseManager._userData.profileData.CoConWinCount;
        userInfo.userDataBase.userProfile.JoConWinCount = UserInfoDataBaseManager._userData.profileData.JoConWinCount;
        userInfo.userDataBase.userProfile.ChConWinCount = UserInfoDataBaseManager._userData.profileData.ChConWinCount;
        userInfo.userDataBase.userProfile.ZaConWinCount = UserInfoDataBaseManager._userData.profileData.ZaConWinCount;
        userInfo.userDataBase.userProfile.ArConWinCount = UserInfoDataBaseManager._userData.profileData.ArConWinCount;
        userInfo.userDataBase.userProfile.TaConWinCount = UserInfoDataBaseManager._userData.profileData.TaConWinCount;
        userInfo.userDataBase.userProfile.KrConWinCount = UserInfoDataBaseManager._userData.profileData.KrConWinCount;
        userInfo.userDataBase.userProfile.YaConWinCount = UserInfoDataBaseManager._userData.profileData.YaConWinCount;
        userInfo.userDataBase.userProfile.friendsCount = UserInfoDataBaseManager._userData.profileData.friendCount;
        userInfo.userDataBase.userProfile.checkInDays = UserInfoDataBaseManager._userData.profileData.checkInDays;
        userInfo.userDataBase.userProfile.conCheckInDays = UserInfoDataBaseManager._userData.profileData.conCheckInDays;
        if (userInfo.userDataBase.userProfile.conCheckInDays == 0) userInfo.userDataBase.userProfile.conCheckInDays = 1;
        userInfo.userDataBase.userProfile.chanelIdx = UserInfoDataBaseManager._userData.profileData.chanelIdx;
        userInfo.userDataBase.userProfile.rankPoint = UserInfoDataBaseManager._userData.profileData.rankPoint/*rankPoint*/;
        userInfo.userDataBase.userProfile.rankWin = UserInfoDataBaseManager._userData.profileData.rankWin;
        userInfo.userDataBase.userProfile.rankLose = UserInfoDataBaseManager._userData.profileData.rankLose;
        userInfo.userDataBase.userProfile.rankDraw = UserInfoDataBaseManager._userData.profileData.rankDraw;
        //item use count
        //userInfo.userDataBase.userProfile.etherealInkUseCount = UserInfoDataBaseManager._userData.profileData.etherealInkUseCount;
        //userInfo.userDataBase.userProfile.saqqaraTabletUseCount = UserInfoDataBaseManager._userData.profileData.saqqaraTabletUseCount;
        //userInfo.userDataBase.userProfile.sLElixirUseCount = UserInfoDataBaseManager._userData.profileData.sLElixirUseCount;
        //userInfo.userDataBase.userProfile.rosemaryOilUseCount = UserInfoDataBaseManager._userData.profileData.rosemaryOilUseCount;
        //userInfo.userDataBase.userProfile.epElixirUseCount = UserInfoDataBaseManager._userData.profileData.epElixirUseCount;
        //userInfo.userDataBase.userProfile.soElixirUseCount = UserInfoDataBaseManager._userData.profileData.soElixirUseCount;
        userInfo.userDataBase.userProfile.chatChannel = UserInfoDataBaseManager._userData.profileData.chatChannel;

        userInfo.userID = UserInfoDataBaseManager._userData.id;
        userInfo.userDataBase.account = UserInfoDataBaseManager._userData.account;
        userInfo.userDataBase.nickName = UserInfoDataBaseManager._userData.nick/*nick*/;
        //userInfo.userDataBase.pw = UserInfoDataBaseManager._userData.pw;
        userInfo.userDataBase.FaceIdx = UserInfoDataBaseManager._userData.faceIdx/*faceIdx*/;
        userInfo.userDataBase.Level = UserInfoDataBaseManager._userData.level/*level*/;
        userInfo.userDataBase.Exp = UserInfoDataBaseManager._userData.exp;
        userInfo.userDataBase.staminaLastEventDate = UserInfoDataBaseManager._userData.staminLastEventDate;
        userInfo.userDataBase.soElixirEndEventDate = UserInfoDataBaseManager._userData.soElixirEndEventDate;
        userInfo.userDataBase.epElixirEndEventDate = UserInfoDataBaseManager._userData.epElixirEndEventDate;

        userInfo.userDataBase.ADLastTime = UserInfoDataBaseManager._userData.ADLastTime;
        userInfo.userDataBase.ADRemainCount = UserInfoDataBaseManager._userData.ADRemainCount;

        //userInfo.userDataBase.userType = UserInfoDataBaseManager._userData.userType;
        userInfo.userDataBase.createDate = UserInfoDataBaseManager._userData.createDate/*lastLoginDate*/;
        userInfo.userDataBase.lastLoginDate = UserInfoDataBaseManager._userData.lastLoginDate/*lastLoginDate*/;
        //userInfo.userDataBase.lastLogoutDate = UserInfoDataBaseManager._userData.lasetLogoutDate;
        //userInfo.userDataBase.ban = UserInfoDataBaseManager._userData.ban;

        //achievement & mission...
        userInfo.userDataBase.achieveValue = UserInfoDataBaseManager._userData.achieveValue;
        userInfo.userDataBase.missionValue = UserInfoDataBaseManager._userData.missionValue;
    }

    internal void GetDSRspNewUserData(string serverSession, AccountType accountType, bool createResult, string userID, string nick)
    {
        string userSession = serverSession;
        if (createResult == true)
        {
            if(accountType == AccountType.Guest) PacketsHandlerServer.SendSCRspRegister(serverSession, LoginState.GuestOK);
            else PacketsHandlerServer.SendSCRspRegister(serverSession, LoginState.OK);

            if (GameServer._isCloud == true)
            {
                //register log
                GlobalManager.Instance.LogAccountInsert(serverSession, userID, nick, LogAccountType.Register);
            }
        }
        else
        {
            //PacketsHandlerServer.SendSCNotiLogin(serverSession, LoginState.Fail, userType, userSession, string.Empty);
            PacketsHandlerServer.SendSCRspRegister(serverSession, LoginState.Fail);
            Console.WriteLine("*** DB: New registered to database failed.");
        }
    }

    internal void GetMSRspAddRedis(string serverSession, bool addResult, string userSession, string userID, UserType userType, string nick, string gameServerUrl)
    {
        if (addResult == true)
        {
            PacketsHandlerServer.SendSCNotiLogin(serverSession, LoginState.OK, userType, userID, userSession, gameServerUrl);
            if (GameServer._isCloud == true)
            {
                //login log
                GlobalManager.Instance.LogAccountInsert(serverSession, userID, nick, LogAccountType.Login);
            }
        }
        else
        {
            Console.WriteLine("*** Redis: Add to MemDB Failed.");
            PacketsHandlerServer.SendSCNotiLogin(serverSession, LoginState.Fail, userType, userID, userSession, string.Empty);
        }
    }

    //MSRspSetServerGlobalState
    internal void GetMSRspSetServerGlobalState(string serverSession, bool setResult, string serverState)
    {
        string msg = string.Format("Change Global Server state to [{0}] {1}!", serverState, (setResult == true) ? "successfully": "fail"); 
        PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);

        //BC all login servers-change global state
        if (setResult == true)
        {
            PacketsHandlerInner.SMReqBCGlobalServerState(serverSession, serverState);
            if (GameServer._logger.IsInfoEnabled)
            {
                string log = string.Format("Change Global Server state to[{0}].", serverState);
                GameServer._logger.Info(log);
            }
        }
    }

    //MSNotiBCServerState
    internal void GetMSNotiBCServerState(string serverSession, string serverState)
    {
        if (GameServer._isLoginServer)
        {
            GameServer._isServerStart = (serverState=="start")? true : false;

            Console.WriteLine(string.Format("Server State: {0}", GameServer._isServerStart ? "start" : "stop"));
        }
    }

    internal void GetDSRspSetDeveloper(string serverSession, string userNick, UserType userType,  bool setResult)
    {
        string msg = string.Format(@"Set ""{0}"" to ""{1}"" {2}.", userNick, userType.ToString(), setResult ? "Successfully" : "Fail");
        PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);

        if (setResult == true) PacketsHandlerInner.SMReqBCReFreshUserType(serverSession, userType);
    }

    internal void GetMSNotiBCReFreshUserType(string serverSession, UserType userType)
    {
        DeveloperManager.Instance.GetDevList(userType);

        //string msg = string.Format("{0} List Refreshed.", userType.ToString());
        //Console.WriteLine(msg);
        if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
        {
            string msg = "Developer List Refreshed.";
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }
    }

    internal void GetMSNotiBCForceClose(string gameServerUrl, string userSession)
    {
        if ((/*GameServer._connectIP*/GameServer._DNS + ":" + GameServer._port) != gameServerUrl) return;
       
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            //save userinfo
            //save all, include lastlogoutdate
            userInfo.SaveUserInfo();
            userInfo.SaveToSql();

            //send quit packets
            PacketsHandlerServer.SendSCNotiQuit(userInfo.serverSession);

            if (SessionIdManager.Instance._sessionDic.ContainsKey(userInfo.serverSession))
            {
                SessionIdManager.Instance._sessionDic.Remove(userInfo.serverSession);
            }

        }

        GlobalManager.Instance.RemoveUserInfo(userSession);
    }

    internal void GetDSRspUpdatePassword(string serverSession, PacketTypeList pckType, LoginState state)
    {
        PacketsHandlerServer.SendSCRspPasswordResult(serverSession, pckType, state);
    }

    internal void GetMSRspUpdateServerEnable(string serverSession)
    {
        UserInfo userInfo = null;
        Room room = null;
        bool isPlaying = false;
        foreach (var infoPair in SessionIdManager.Instance)
        {
            isPlaying = false;
            userInfo = infoPair.Value;
            if (!string.IsNullOrEmpty(userInfo.roomID))
            {
                room = GameServer.GetRoom(userInfo.session);
                if(room != null && room.isPlaying)
                {
                    //reconnect when game end
                    isPlaying = true;
                }
            }

            if (!isPlaying)
            {
                //get userType 
                UserType userType = UserType.User;
                GlobalManager.Instance.GetUserType(userInfo.userDataBase.account, out userType);
                if (userType == UserType.User)
                {
                    //change game server
                    PacketsHandlerServer.SendSCNotiGotoScene(serverSession, SceneType.Home);
                    PacketsHandlerInner.SMReqRemoveRedis(serverSession, userInfo.userID, userInfo.session);
                }

            }
        }
    }

    internal void GetMSNotiUpdateLimit(string serverSession, int count)
    {
        ServerManager.SERVER_CONNECTION_LIMIT = count;
        if (SessionIdManager.Instance._sessionDic != null)
        {
            if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
            {
                string msg = string.Format("Update Connections Limit To {0} Successfully.", count);
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);

                if (GameServer._logger.IsInfoEnabled)
                {
                    string log = string.Format("Update connections limit To {0}", count);
                    GameServer._logger.Info(log);
                }
            }
        }
    }

    internal void GetMSNotiNewFriendGift(string serverSession, string userID)
    {
        if (!GameServer._isLoginServer)
        {
            UserInfo info = null;
            if (SessionIdManager.Instance != null)
            {
                foreach (var infoPair in SessionIdManager.Instance)
                {
                    info = infoPair.Value;
                    if (info.userID == userID)
                    {
                        //send SCNotiMenuAlarm - friend gift
                        int alarmCount = 1;
                        bool allowZero = false;
                        info.SetSendMenuAlarmFriends(alarmCount, allowZero);

                        //in order to send SCNotiMenuAlarm - friend on SessionLogin
                        info.GetMyFriends();

                        break;
                    }
                }
            }
        }
    }

    internal void GetMSNotiAlarmShop(string serverSession, string alarmString)
    {
        if (GameServer._isLoginServer)  { return; }

        if (SessionIdManager.Instance != null)
        {
            UserInfo userInfo = null;
            foreach (var infoPair in SessionIdManager.Instance)
            {
                userInfo = infoPair.Value;
                PacketsHandlerServer.SendSCNotiMenuAlarm(serverSession, Option.Shop, alarmString);
                if (userInfo.serverSession == serverSession)
                {
                    string msg = string.Format("Send [Menu: {0}] alarm successfully.", Option.Shop.ToString());
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                }
            }
        }
    }

    internal void GetMSNotiAlarmNews(string serverSession, string alarmString)
    {
        if (GameServer._isLoginServer) { return; }

        if (SessionIdManager.Instance != null)
        {
            UserInfo userInfo = null;
            foreach (var infoPair in SessionIdManager.Instance)
            {
                userInfo = infoPair.Value;
                PacketsHandlerServer.SendSCNotiMenuAlarm(serverSession, Option.News, alarmString);
                if (userInfo.serverSession == serverSession)
                {
                    string msg = string.Format("Send [Menu: {0}] alarm successfully.", Option.News.ToString());
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                }
            }
        }
    }
}