﻿using MoonGlow;
using MoonGlow.Data;
using MoonGlow.Room;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ChatPacketsManager : Singleton<ChatPacketsManager>
{
    internal void GetCSNotiChat(string serverSession, string userSession, string chatMsg, int iconIdx)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        UserOperate(userInfo.session, chatMsg, iconIdx);
    }

    private void UserOperate(string session, string chatMsg, int iconIdx)
    {
        //Console.WriteLine("CSNotiChat Running:" + this.chatMsg + " session :" + RedisConnect.GetRedis(serverSession).Result);
        UserInfo userInfo = GameServer.GetUserInfo(session);//null;
        if (userInfo != null)
        {
            string[] cmdArray = chatMsg.Split(' ');
            string msg = string.Empty;

            if (cmdArray[0].ToLower().Contains("@"))
            {
                return;
            }

            bool inRoom = false;
            string senderNick = userInfo.userDataBase.nickName;
            if (!string.IsNullOrEmpty(userInfo.roomID))
            {
                int playerNumber = 0;
                Room room = GameServer.GetRoom(userInfo.session);
                //room.GetUserSessionList(out userSessionList);
                if (room != null)
                {
                    inRoom = true;
                    foreach (var player in room.GetPlayers())
                    {
                        if (player.session == session)
                        {
                            //pck.playerNumber = player.playerNumber;
                            //break;
                            playerNumber = player.playerNumber;
                            break;
                        }

                    }

                    bool isPhrase = false;
                    if (iconIdx != 0)
                    {
                        ChatType chatType = ChatManager.Instance.GetChatType(iconIdx);
                        if (chatType == ChatType.Phrase)
                        {
                            isPhrase = true;
                            //chatMsg = ChatLocalizationManager.Instance.GetPhrase(iconIdx, userInfo.language);
                            //iconIdx = 0;
                        }
                    }

                    UserInfo playerInfo = null;
                    foreach (var player in room.GetPlayers())
                    {
                        playerInfo = GameServer.GetUserInfo(player.session);
                        if (playerInfo != null)
                        {
                            if (isPhrase)
                            {
                                chatMsg = ChatLocalizationManager.Instance.GetPhrase(iconIdx, playerInfo.language);
                                PacketsHandlerServer.SendSCNotiChat(playerInfo.serverSession, senderNick, 0, playerNumber, chatMsg, 0);
                            }
                            else
                            {
                                PacketsHandlerServer.SendSCNotiChat(playerInfo.serverSession, senderNick, 0, playerNumber, chatMsg, iconIdx);
                            }
                        }
                    }

                    //test for joy 
                    if (room.singlePlayer)
                    {
                        if (isPhrase)
                        {
                            int evePlayerNum = 0;
                            foreach (var player in room.GetPlayers())
                            {
                                if (player.playerName.ToLower() == "eve")
                                {
                                    evePlayerNum = player.playerNumber;
                                    break;
                                }
                            }
                            if (iconIdx == 10 || iconIdx == 11)
                            {
                                chatMsg = ChatLocalizationManager.Instance.GetPhrase(iconIdx, userInfo.language);
                                PacketsHandlerServer.SendSCNotiChat(userInfo.serverSession, "Eve", 1, evePlayerNum, chatMsg, 0);
                            }
                            else
                            {
                                PacketsHandlerServer.SendSCNotiChat(userInfo.serverSession, "Eve", 1, evePlayerNum, string.Empty, 101);
                            }
                        }
                    }
                    //test for joy above 

                }//if room null
            }
            //else
            if (!inRoom)
            {
                if (iconIdx != 0 || string.IsNullOrEmpty(chatMsg))
                {
                    //ChatType chatType = ChatManager.Instance.GetChatType(iconIdx);
                    //if (chatType == ChatType.Phrase)
                    //{
                    //    chatMsg = ChatLocalizationManager.Instance.GetPhrase(iconIdx, userInfo.language);
                    //    iconIdx = 0;
                    //}
                    return;
                }

                SCNotiChat pck = new SCNotiChat();
                pck.nick = senderNick/*userInfo.userDataBase.nickName*/;
                pck.chatMsg = chatMsg;
                pck.iconIdx = iconIdx;
                pck.userType = 0;//0 IS USER, 1 IS AI, later...

                List<string> userSessionList = new List<string>();

                //MoonGlow.GameServer.SendBC(pck);
                //except room players...
                UserInfo info = null;
                foreach (var infoPair in SessionIdManager.Instance)
                {
                    info = infoPair.Value;
                    if (string.IsNullOrEmpty(info.roomID)
                        && (info.userDataBase.userProfile.chatChannel == userInfo.userDataBase.userProfile.chatChannel
                            || userInfo.userDataBase.userProfile.chatChannel == ChatChannel.All)
                        /*&& info.userServerState != UserServerStatus.DisConnect*/)
                    {
                        userSessionList.Add(info.session);
                    }
                }

                GameServer.SendMsgWithUserSession(userSessionList, pck);
            }
        }
    }

    private void OperatorManagerOperate(string session, string chatMsg)
    {
        OperatorOperate(session, chatMsg);
    }

    private void OperatorOperate(string session, string chatMsg)
    {
        UserInfo userInfo = GameServer.GetUserInfo(session);//null;
        if (userInfo != null)
        {
            string[] cmdArray = chatMsg.Split(' ');
            string msg = string.Empty;

            if (cmdArray[0].Contains("@noti"))
            {
                userInfo.CMDNoti(cmdArray, chatMsg);
                return;
            }
            if (cmdArray[0][0] == '#')
            {
                userInfo.CMDWhisper(cmdArray, chatMsg);
                return;
            }
            switch (cmdArray[0].ToLower())
            {
                case "@mail": userInfo.CMDMail(cmdArray); break;
                case "@ruby": userInfo.CMDRuby(cmdArray); break;
                case "@gold": userInfo.CMDGold(cmdArray); break;
                case "@item": userInfo.CMDItem(cmdArray); break;

                case "@pet": break;
                case "@badge": break;
                case "@jokerface": break;
                case "@spell": break;
                case "@ink": userInfo.CMDInk(cmdArray); break;
                case "@stamina": userInfo.CMDStamina(cmdArray); break;
                case "@level": userInfo.CMDLevel(cmdArray); break;
                case "@con": userInfo.CMDCon(cmdArray); break;
                case "@win": userInfo.CMDWin(cmdArray); break;
                case "@lose": userInfo.CMDLose(cmdArray); break;
                //case "@friend": userInfo.CMDFriend(cmdArray); break;

                //case "@devref": userInfo.CMDDevref(cmdArray); break;
                //case "@dev": userInfo.CMDDev(cmdArray); break;
                case "@repair": userInfo.CMDRepair(cmdArray); break;
                //case "@channel": userInfo.ChannelOperate(cmdArray); break;

                case "@start": userInfo.CMDStart(cmdArray); break;
                case "@stop": userInfo.CMDStop(cmdArray); break;
                case "@disable": userInfo.CMDDisable(cmdArray); break;
                case "@state": userInfo.CMDState(cmdArray); break;
                case "@me": userInfo.CMDMe(cmdArray); break;
                case "@enable": userInfo.CMDEnable(cmdArray); break;
                case "@move": userInfo.CMDMove(cmdArray); break;
                //case "@noti":
                    //case "@noti1":
                    //case "@noti2":
                    //userInfo.CMDNoti(cmdArray, chatMsg);
                    //break;
                case "@refresh":
                    //@refresh server
                    //@refresh channel
                    //@refresh version
                    userInfo.CMDRefresh(cmdArray);
                    break;
                case "@limit": userInfo.CMDLimit(cmdArray); break;

                //@ban nick minutes / @ban(@release) nick
                case "@ban": userInfo.CMDBan(cmdArray); break;
                case "@release": userInfo.CMDRelease(cmdArray); break;
                case "@help":
                    msg = //"\n" +
                          "@mail item_idx item_count\n" +
                          //"@mail item_idx item_count nick\n" +
                          "@item item_idx item_count\n" +
                          "@ruby (@gold, @ink, @stamina, @level) count\n" +
                          //"@ruby (@gold, @ink, @stamina, @level) count nick\n" +
                          "@win (@lose) times\n" +
                          "@win (@lose) times game_type [Co, Jo, Ch, Za, Ar, Ta, Kr, Ya]\n" +
                          //"@friend friend_index send_DateTime\n" +
                          //"@dev nick type [Oper:1, OperM:2, Dev:3, DevM:4]\n" +
                          "@repair rank\n" +
                          "@limit numbers\n" +
                          "@refresh server\n" +
                          "@start (@stop, @con, @state, @me)\n" +
                          "@disable (@enable) index | @disable (@enable) all\n" +
                          "@move index\n" +
                          "@ban (@release) nick | @ban nick minutes\n" +
                          "@noti message | noti index message\n" + 
                          "#nick msg";
                    ///*@noti1 banTime | @noti2*/
                    PacketsHandlerServer.SendSCNotiCMD(userInfo.serverSession, msg);

                    break;
                default:
                    msg = "Error Command!";
                    PacketsHandlerServer.SendSCNotiCMD(userInfo.serverSession, msg);
                    break;
            }
        }
    }

    private void DeveloperManagerOperate(string session, string chatMsg)
    {
        DeveloperOperate(session, chatMsg);
    }

    private void DeveloperOperate(string session, string chatMsg)
    {
        UserInfo userInfo = GameServer.GetUserInfo(session);//null;
        if (userInfo != null)
        {
            string[] cmdArray = chatMsg.Split(' ');
            string msg = string.Empty;

            if (cmdArray[0].ToLower().Contains("@noti"))
            {
                userInfo.CMDNoti(cmdArray, chatMsg);
                return;
            }
            if (cmdArray[0][0] == '#')
            {
                userInfo.CMDWhisper(cmdArray, chatMsg);
                return; 
            }
            switch (cmdArray[0].ToLower())
            {
                case "@mail": userInfo.CMDMail(cmdArray); break;
                case "@ruby": userInfo.CMDRuby(cmdArray); break;
                case "@gold": userInfo.CMDGold(cmdArray); break;
                case "@item": userInfo.CMDItem(cmdArray); break;

                case "@pet": break;
                case "@badge": break;
                case "@jokerface": break;
                case "@spell": break;
                case "@ink": userInfo.CMDInk(cmdArray); break;
                case "@stamina": userInfo.CMDStamina(cmdArray); break;
                case "@level": userInfo.CMDLevel(cmdArray); break;
                case "@con": userInfo.CMDCon(cmdArray); break;
                case "@win": userInfo.CMDWin(cmdArray); break;
                case "@lose": userInfo.CMDLose(cmdArray); break;
                case "@friend": userInfo.CMDFriend(cmdArray); break;

                case "@devref": userInfo.CMDDevref(cmdArray); break;
                case "@dev": userInfo.CMDDev(cmdArray); break;
                case "@repair": userInfo.CMDRepair(cmdArray); break;
                case "@channel": userInfo.CMDChannel(cmdArray); break;
                
                case "@start": userInfo.CMDStart(cmdArray); break;
                case "@stop": userInfo.CMDStop(cmdArray); break;
                case "@disable": userInfo.CMDDisable(cmdArray); break;
                case "@state": userInfo.CMDState(cmdArray); break;
                case "@me": userInfo.CMDMe(cmdArray); break;
                case "@enable": userInfo.CMDEnable(cmdArray); break;
                case "@move": userInfo.CMDMove(cmdArray); break;
                //case  "@noti":
                //case "@noti1":
                //case "@noti2":
                     //userInfo.CMDNoti(cmdArray, chatMsg);
                    //break;
                case "@refresh":
                    //@refresh server
                    //@refresh channel
                    //@refresh version
                    //@refresh localization
                    //@refresh news
                    //@refresh mission
                    userInfo.CMDRefresh(cmdArray);
                    break;
                case "@alarm":
                    //@alarm news datetime
                    //@alarm shop dataetime
                    userInfo.CMDAlarm(cmdArray);
                    break;
                case "@limit": userInfo.CMDLimit(cmdArray); break;

                //@ban nick minutes / @ban(@release) nick
                case "@ban": userInfo.CMDBan(cmdArray); break;
                case "@release": userInfo.CMDRelease(cmdArray); break;
                case "@effect":userInfo.CMDEffect(cmdArray);break;
                case "@help":
                    msg = //"\n" +
                          "@mail item_idx item_count\n" +
                          "@mail item_idx item_count nick\n" +
                          "@item item_idx item_count\n" + 
                          "@ruby (@gold, @ink, @stamina, @level) count\n" +
                          "@ruby (@gold, @ink, @stamina, @level) count nick\n" +
                          "@win (@lose) times\n" +
                          "@win (@lose) times game_type [Co, Jo, Ch, Za, Ar, Ta, Kr, Ya]\n" +
                          "@friend friend_index send_DateTime\n" +
                          "@dev nick type [Oper:1, OperM:2, Dev:3, DevM:4]\n" +
                          "@repair rank\n" + 
                          "@limit numbers\n" +
                          "@refresh server (league, version, localization, news, mission)\n" +
                          "@alarm news (shop) [now, 2016-12-31, 2016-12-31T01:59:59]\n" +
                          "@start (@stop, @con, @devref, @state, @me)\n" +
                          "@disable (@enable) index | @disable (@enable) all\n" +
                          "@move index\n" +
                          "@ban (@release) nick | @ban nick minutes\n" +
                          "@effect ruby (gold)\n" +
                          "@noti message | noti index message\n" +
                          "#nick msg";
                    ///*@noti1 banTime | @noti2*/
                    PacketsHandlerServer.SendSCNotiCMD(userInfo.serverSession, msg);

                    break;
                default:
                    msg = "Error Command!";
                    PacketsHandlerServer.SendSCNotiCMD(userInfo.serverSession, msg);
                    break;
            }
        }
    }

    internal void GetCSReqCMD(string serverSession, string userSession, string msg)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        if (DeveloperManager._developerList.Contains(userInfo.userDataBase.account))
        {
            DeveloperOperate(userInfo.session, msg);
        }
        else if (DeveloperManager._developerManagerList.Contains(userInfo.userDataBase.account))
        {
            DeveloperManagerOperate(userInfo.session, msg);
        }
        else if (DeveloperManager._operatorList.Contains(userInfo.userDataBase.account))
        {
            OperatorOperate(userInfo.session, msg);
        }
        else if (DeveloperManager._operatorManagerList.Contains(userInfo.userDataBase.account))
        {
            OperatorManagerOperate(userInfo.session, msg);
        }
    }

    internal void GetCSNotiChangeChatChannel(string serverSession, string userSession, ChatChannel chatChannel)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }
        userInfo.userDataBase.userProfile.chatChannel = chatChannel;
    }
}
