﻿using MoonGlow;
using MoonGlow.Data;
using MoonGlow.Room;
using System;

public class HomePacketsManager : Singleton<HomePacketsManager>
{
    internal void GetCSReqRoomList(string serverSession, ReqRoomList reqRoomList)
    {
        string userSession = reqRoomList.session;
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        RoomList roomList = RoomManager.Instance.GetReqRoomList(reqRoomList);
        if (roomList != null) PacketsHandlerServer.SendSCNotiRoomList(serverSession, roomList); 

        //send scnoticonnections
        int connections = 0;
        UserInfo userInfo2 = null;
        foreach (var infoPair in SessionIdManager.Instance)
        {
            userInfo2 = infoPair.Value;
            if (userInfo2.userServerState != UserServerStatus.DisConnect)
            {
                connections++;
            }
        }
        PacketsHandlerServer.SendSCNotiConnections(serverSession, connections);
    }

    internal void GetCSReqRoomCreate(string serverSession, string userSession, GameType gameType, PlayMode playMode, PlayTime playTime, int stake, bool singlePlay, bool disClose, bool isLock)
    {
        if (singlePlay) stake = 0;
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        if (!string.IsNullOrEmpty(userInfo.roomID)) return;

        if (!singlePlay && userInfo.userDataBase.Stamina < 10)
        {
            PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.LackStamina);
            return; //lack stamina
        }
        if (userInfo.userDataBase.Gold < stake)
        {
            PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.LackGold);
            return; //lack gold
        }
        //userInfo.CheckRoomID();

        Room room = null;
        if (RoomManager.Instance.CreateRoom("", gameType, playMode, playTime, stake, disClose, isLock, out room))
        {
            //success
        }
        else
        {
            //fail
        }

        if (room != null)
        {
            if (room.Join(userSession, true))
            {
                // send packet
                //PacketsHandlerServer.SendSCNotiEnterRoom(serverSession, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, room.goalHP, room.goalResource/*conditionInfo.goalHP, conditionInfo.goalResource*/);
                //if(room.AI)
                if (singlePlay)
                {
                    //send SendSCNotiEnterRoom
                    //create AIPlayer
                    room.AIJoin(Guid.NewGuid().ToString("N"));
                    room.PlayerEnterReady(userSession);
                    room.singlePlayer = true;
                    PacketsHandlerServer.SendSCNotiEnterRoom(serverSession, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, room.goalHP, room.goalResource/*conditionInfo.goalHP, conditionInfo.goalResource*/);
                }
                else
                {
                    //create room and send roomlist directly
                    //send room list
                    room._playStateManager.SetState(PlayState.Ready);
                    RoomList roomList = RoomManager.Instance.GetAllRoomList(userSession);
                    if (roomList != null) PacketsHandlerServer.SendSCNotiRoomList(serverSession, roomList);
                }

                if (GameServer._isCloud == true)
                {
                    //play log
                    GlobalManager.Instance.LogPlayInsert(serverSession, userInfo.userID, userInfo.userDataBase.nickName, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, LogPlayType.Create, string.Empty, string.Empty);
                }
            }
            else
            {
                // fail to join
            }
        }
    }

    internal void GetCSNotiChangeLanguage(string serverSession, string userSession, Language language)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.language = language;
    }

    internal void GetCSReqReadTutorial(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.CheckAchieveValue(GameType.Unknown, AchieveType.Tutorial, 1);
    }

    internal void GetCSNotiEnterRoom(string serverSession, string userSession)
    {
        // 1. find joined room ( 1. get SessinManager[serverSession], get userinfo.roomid . get roommanager[ room id ] )
        // 2. noti playerList in the room => BC
        // 3. noti player Status in the room => BC to others and other's info send to me
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            userInfo.sceneType = SceneType.Room;
            Room room = GameServer.GetRoom(userSession);
            if (room != null)
            {
                room.unNormal = false;

                // player list
                room.SendPlayerList();
                // player status 
                room.SendPlayerStatus();
                //send playeriteminfo
                room.SendPlayerItemInfo();
                //room.PlayerEnterReady(userSession);
            }
        }
    }

    internal void GetCSNotiSceneEnter(string serverSession, string userSession)
    {
        MoonGlow.UserInfo userInfo = MoonGlow.GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            userInfo.sceneType = SceneType.Home;
            //SendSCNotiUserInfo
            int upLevelExp = MoonGlow.Data.LevelExpManager.Instance.GetUpLevelExp(userInfo.userDataBase.Level);
            //int downLevelExp = MoonGlow.Data.LevelExpManager.Instance.GetDownLevelExp(userInfo.userDataBase.Level);

            PacketsHandlerServer.SendSCNotiUserInfo(serverSession, userInfo.userDataBase.FaceIdx, userInfo.userDataBase.nickName, userInfo.badgeIdx, userInfo.userDataBase.Level, userInfo.userDataBase.Exp, upLevelExp);

            //SendSCNotiItemInfo
            //userInfo.SetSendItemInfoList();

            //SendSCNotiUserElement
            userInfo.SetSendElementInfoList();

            //SendSCNotiRoomList
            RoomList roomList = RoomManager.Instance.GetAllRoomList(userSession);
            if (roomList != null) PacketsHandlerServer.SendSCNotiRoomList(serverSession, roomList); 

            //send scnoticonnections
            int connections = 0;
            UserInfo userInfo2 = null;
            foreach (var infoPair in SessionIdManager.Instance)
            {
                userInfo2 = infoPair.Value;
                if (userInfo2.userServerState != UserServerStatus.DisConnect)
                {
                    connections++;
                }
            }
            PacketsHandlerServer.SendSCNotiConnections(serverSession, connections);
        }
    }

    internal void GetCSReqJoinRoom(string serverSession, string userSession, string roomName)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        if (!string.IsNullOrEmpty(userInfo.roomID))
        {
            //already created a room, cannot join others
            PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.RoomCreator);
            return;
        }

        //userInfo.roomID = null;
        RoomManager.Instance.JoinRoom(serverSession, userSession, roomName);        
    }

    internal void GetCSReqQuickStart(string serverSession, string userSession_, GameType gameType_, PlayMode playMode_, PlayTime playTime_, int stake_, bool disClose_, bool isLock_)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession_);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }


        //just test, we need blank room manager later, maybe...   
        //userInfo.CheckRoomID();
        if (!string.IsNullOrEmpty(userInfo.roomID)) return;
        //RoomManager.Instance.QuickStart(serverSession, userSession_, GlobalManager.DEFAULT_GAME_TYPE, GlobalManager.DEFAULT_GAME_MODE, GlobalManager.DEFAULT_GAME_TIME, GlobalManager.DEFAULT_GAME_STAKE, GlobalManager.DEFAULT_GAME_DISCLOSE, GlobalManager.DEFAULT_GAME_LOCK);
        RoomManager.Instance.QuickStart(serverSession, userSession_, gameType_, playMode_, playTime_, stake_, disClose_, isLock_);        
    }
}