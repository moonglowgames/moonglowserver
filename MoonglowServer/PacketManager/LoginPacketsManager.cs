﻿using MoonGlow;
using MoonGlow.Data;
using MoonGlow.Room;
using System;

public class LoginPacketsManager : Singleton<LoginPacketsManager>
{
    internal void GetCSReqRegister(string serverSession, AccountType accountType, string account, string password, string nick, int faceIdx, string deviceID)
    {
        if (string.IsNullOrEmpty(account) ||(accountType != AccountType.Guest && string.IsNullOrEmpty(password))|| string.IsNullOrEmpty(nick))
        {
            LoginState state = LoginState.Fail;
            PacketsHandlerServer.SendSCRspRegister(serverSession, state);
            return;
        }

        //check nick
        if (nick.ToLower().Contains("moonglow") || nick.Contains("문글로우") || BanNickManager.Instance.IsNickBan(nick))
        {
            //fail
            LoginState state = LoginState.NickExist;
            PacketsHandlerServer.SendSCRspRegister(serverSession, state);
            return;
        }

        PacketsHandlerInner.SDReqRegisterCheck(serverSession, accountType, account, password, nick, faceIdx, deviceID);
    }

    internal void GetCSReqFindPassword(string serverSession, string account)
    {
        string newPassword = GlobalManager.Instance.GeneratePassword();//temporary password
        PacketsHandlerInner.SDReqUpdatePassword(serverSession, account, string.Empty, newPassword, true);
    }

    internal void GetCSReqChangePassword(string serverSession, string account, string oldPassword, string newPassword)
    {
        PacketsHandlerInner.SDReqUpdatePassword(serverSession, account, oldPassword, newPassword, false);
    }

    internal void GetCSReqLogin(string serverSession, AccountType accountType, string account, string password, string deviceID, ConnectType conType, string conVer, Language language)
    {
        string[] clientVer = conVer.Split('.');
        UserType userType = UserType.User;
        GlobalManager.Instance.GetUserType(account, out userType);
        if (!ServerManager.Instance.isServerStart(serverSession, userType, language)) return;

        if (ServerManager.Instance.isClientVersionMatch(serverSession, userType, conType, clientVer))
        {
            //ok
            //state = LoginState.OK;
            PacketsHandlerInner.SDReqLoginCheck(serverSession, accountType, account, password, deviceID, language);
        }
        else
        {
            string userSession = serverSession;
            PacketsHandlerServer.SendSCNotiLogin(serverSession, LoginState.OldVersion, userType, string.Empty, userSession, string.Empty);

            //check language
            string msg = (LocalizationManager._dict.ContainsKey(LocalizationKey.LOW_VERSION.ToString()))?
                         LocalizationManager.Instance.Get(LocalizationKey.LOW_VERSION.ToString(), language) : LocalizationKey.LOW_VERSION.ToString();
            PacketsHandlerServer.SendSCNotiServerNoti(serverSession, Command.Message, msg);

            //send SCNotiDownload
            string downloadUrl = string.Empty;
            switch (conType)
            {
                case ConnectType.Android:
                    downloadUrl = "market://details?id=com.MoonGlow.MoonGlow";
                    break;
                case ConnectType.iOS:
                    downloadUrl = "itms-apps://itunes.apple.com/app/id1122872372";
                    break;
                case ConnectType.WinStore:
                    downloadUrl = "";
                    break;
                default: break;
            }

            if (!string.IsNullOrEmpty(downloadUrl))
            {
               PacketsHandlerServer.SendSCNotiDownload(serverSession, conType, downloadUrl);
            }
        }
    }
  
    internal void GetCSReqGotoScene(string serverSession, string userSession, SceneType scene)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        if (userInfo.isReadDone)
        {
            //Console.WriteLine("CSReqGotoSCene Running ID:" + RedisConnect.Instance.GetRedis(serverSession).Result + " Scene:" + scene);

            //Send SCNotiGotoScene
            userInfo.SetHome();
            PacketsHandlerServer.SendSCNotiGotoScene(serverSession, SceneType.Home);
        }
        else { userInfo.needNotiGotoScene = true; }
    }

    internal void GetCSReqClientFocus(string serverSession, string userSession)
    {
        PacketsHandlerServer.SendSCNotiClientFocus(serverSession);
        if (GameServer._isLoginServer == true) { return; }
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.isPause = false;
        userInfo.pingCount = 0;
        if (userInfo.userServerState != UserServerStatus.DisConnect) { return; }

        if (!SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
        {
            SessionIdManager.Instance._sessionDic.Add(serverSession, userSession);
        }

        userInfo.CheckRoomReEnter();
    }

    internal void GetCSReqClientPause(string serverSession, string userSession)
    {
        if (GameServer._isLoginServer == true) { return; }
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        //1. save userInfo
        if (SessionIdManager.Instance._sessionDic != null)
        {
            if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
            {
                if (userInfo.isPause == true) return;
                if (userInfo.userServerState == UserServerStatus.DisConnect) { return; };
                userInfo.OnClientPause();
            }
        }
    }

    internal void GetCSReqLogout(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        //normal logout
        //1.save userinfo
        if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
        {
            SessionIdManager.Instance._sessionDic.Remove(serverSession);
        }

        //save userinfo
        //save all, include lastlogoutdate
        userInfo.SaveUserInfo();
        userInfo.SaveToSql();

        if (GameServer._isCloud == true)
        {
            //Logout log
            GlobalManager.Instance.LogAccountInsert(serverSession, userInfo.userID, userInfo.userDataBase.nickName, LogAccountType.Logout);
        }

        //2. check if there is room should be deleted or not, prevent force exist but empty room still remain
        //change it when AI works later 나중에...
        if (!string.IsNullOrEmpty(userInfo.roomID))
        {
            Room room = GameServer.GetRoom(userSession);
            if (room != null) room.SetDisconnectPlayer(userSession);
        }

        PacketsHandlerInner.SMReqRemoveRedis(serverSession, userInfo.userID, userSession);
    }

    internal void GetCSReqSessionLogin(string serverSession, string userID, string userSession, Language language)
    {
        //1. check if user session exist in redis
        //2. send SCNotiSessionLogin
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            //reconnect
            userInfo.pingCount = 0;
            userInfo.connectTimes++;
            userInfo.SetSessionLogin();
            userInfo.isPause = false;
            userInfo.language = language;

            userInfo.CheckDailyCheckInAndGift();
            userInfo.userDataBase.lastLoginDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //check achieve -check in 
            userInfo.CheckAchieveValue(GameType.Unknown, AchieveType.CheckIn, userInfo.userDataBase.userProfile.checkInDays);
            userInfo.CheckAchieveValue(GameType.Unknown, AchieveType.ConCheckIn, userInfo.userDataBase.userProfile.conCheckInDays);
            userInfo.SaveUserInfo();//login date, achieve

            if (userInfo.serverSession != serverSession && SessionIdManager.Instance._sessionDic != null)
            {
                //remove old serverSession from dic(prevent rubbish)..and add new
                foreach (var sessionPair in SessionIdManager.Instance._sessionDic)
                {
                    if (sessionPair.Value == userSession)
                    {
                        SessionIdManager.Instance._sessionDic.Remove(sessionPair.Key);
                        break;
                    }
                }

                if (!SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
                {
                    SessionIdManager.Instance._sessionDic.Add(serverSession, userSession);
                }
            }

            userInfo.serverSession = serverSession;
            userInfo.CheckRoomReEnter();

            //check mailbox
            //send SCNotiMenuAlarm - mailbox
            bool allowZeroMail = false;
            userInfo.SetSendMenuAlarmMail(allowZeroMail);

            //check friend - request friends & gift
            //send SCNotiMenuAlarm - friend
            int alarmCount = 0;
            bool allowZeroFriend = false;
            userInfo.SetSendMenuAlarmFriends(alarmCount, allowZeroFriend);
            //send SCNotiChatIconSet
            userInfo.SetSendChatIconSet();
        }
        else
        {
            PacketsHandlerInner.SMReqLogingSessionCheck(serverSession, userID, userSession, language);
        }
    }

    internal void GetCSReqUrl(string serverSession, string userSession, UrlType urlType)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        string url = string.Empty;
        switch (urlType)
        {
            case UrlType.AppStore:
                break;
            case UrlType.GooglePlay:
                break;
            case UrlType.HomePage:
                url = (userInfo.language == Language.Korean) ? "http://cafe.naver.com/moonglow2015.cafe" : "http://moonglowgames.com/index.html";
                break;
            case UrlType.Manual:
                url = "http://mgcdkrstor1.blob.core.windows.net/manual/MGTCG_MANUAL_20160215.pdf";
                break;
            case UrlType.MSStore:
                break;
            case UrlType.Facebook:
                url = "https://www.facebook.com/moonglowgames2015";
                break;
            default: break;
        }

        PacketsHandlerServer.SendSCNotiUrl(serverSession, urlType, url);
    }

    internal void GetCSRspPang(string serverSession, string userSession, int pingCount)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.pingCount = 0;//pingCount-1;
    }
}
