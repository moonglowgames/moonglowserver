﻿using MoonGlow;
using MoonGlow.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LitJson;

public class ShopPacketsManager : Singleton<ShopPacketsManager>
{
    internal void GetCSReqShopList(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.sceneType = SceneType.Shop;

        List<ShopListInfo> shopList = null;
        ItemManager.Instance.GetShopList(out shopList);

        ShopList shopsList = new ShopList(PacketTypeList.SCNotiShopList.ToString(), shopList);
        PacketsHandlerServer.SendSCNotiShopList(serverSession, shopsList);
    }

    internal void GetCSReqBuyElement(string serverSession, string userSession, int elementIdx_, int count_)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        ItemElementInfo elementInfo = ItemManager.Instance.GetElement(elementIdx_);
       
        if (elementInfo != null)
        {
            if (elementInfo.payType == PayType.AD_GOLD || elementInfo.payType == PayType.AD_RUBY)
            {
                TimeSpan ADInternal = new TimeSpan(1, 0, 0);
                TimeSpan ADRemainTime = new TimeSpan(1, 0, 0);
                bool isOK = userInfo.IsAdOK();
                if (isOK)
                {
                    ADRemainTime = new TimeSpan(0, 0, 0);
                    userInfo.ADGuid = Guid.NewGuid().ToString("N");
                    userInfo.ADType = elementInfo.payType;
                    userInfo.ADRewardCount = elementInfo.quantity;

                    userInfo.userDataBase.ADRemainCount--;
                    userInfo.userDataBase.ADLastTime = DateTime.Now;
                }
                else
                {
                    if (userInfo.userDataBase.ADRemainCount <= 0)
                    {
                        ADRemainTime = DateTime.Today + new TimeSpan(1, 0, 0, 0) - DateTime.Now;
                    }
                    else 
                    {
                        ADRemainTime = ADInternal - (DateTime.Now - userInfo.userDataBase.ADLastTime);
                    }
                }

                PacketsHandlerServer.SendSCNotiShowAD(serverSession, isOK, userInfo.ADGuid, userInfo.userDataBase.ADRemainCount, string.Format("{0:00}:{1:00}:{2:00}", ADRemainTime.Hours, ADRemainTime.Minutes, ADRemainTime.Seconds));
                return;
            }

            ShopResult result = ShopResult.Unknown;
            if (elementInfo.tabType == TabType.Joker && userInfo.userDataBase.userItem.jokerFaceList.Contains(elementIdx_))
            {
                result = ShopResult.AlreadyExist;
            }
            else if (elementInfo.tabType == TabType.CardSkin)
            {
                if (userInfo.userDataBase.userItem.cardSkinList != null)
                {
                    foreach (var cardSkin in userInfo.userDataBase.userItem.cardSkinList)
                    {
                        if (cardSkin.skinIdx == elementIdx_)
                        {
                            result = ShopResult.AlreadyExist;
                            break;
                        }
                    }
                }
            }
            else if (elementInfo.tabType == TabType.Pet)
            {
                if (userInfo.userDataBase.userItem.petList != null)
                {
                    foreach (var pet in userInfo.userDataBase.userItem.petList)
                    {
                        if (pet.petIdx == elementIdx_)
                        {
                            result = ShopResult.AlreadyExist;
                            break;
                        }
                    }
                }
            }

            if(result != ShopResult.AlreadyExist)
            {
                double price = elementInfo.price * count_;
                switch (elementInfo.payType)
                {
                    case PayType.Cash:
                        //Connect to Google Play or App Store
                        result = ShopResult.OK;
                        break;
                    case PayType.Gold:
                        if (userInfo.userDataBase.Gold < price) { result = ShopResult.LackGold; }
                        else
                        {
                            userInfo.userDataBase.Gold -= Convert.ToInt32(price);
                            result = ShopResult.OK;
                        }
                        break;
                    case PayType.Ruby:
                        if (userInfo.userDataBase.Ruby < price) { result = ShopResult.LackRuby; }
                        else
                        {
                            userInfo.userDataBase.Ruby -= Convert.ToInt32(price);
                            result = ShopResult.OK;
                        }
                        break;
                    default: break;
                }
            }
            PacketsHandlerServer.SendSCRspBuyElement(serverSession, result);

            if (result == ShopResult.OK)
            {
                int itemIdx = elementIdx_;
                int quantity = elementInfo.quantity * count_;
                switch (elementInfo.tabType)
                {
                    case TabType.Items:
                        userInfo.ChangeUserItemAmount(elementIdx_, quantity );

                        //send SCNotiItemInfo again
                        //userInfo.SetSendItemInfoList();

                        break;
                    case TabType.Joker:
                        userInfo.SetJokerFaceList(elementIdx_);
                        //check achieve
                        userInfo.CheckAchieveValue(GameType.Unknown, AchieveType.AllJokerFace, 1);
                        //check mission
                        userInfo.CheckMissionValue(GameType.Unknown,AchieveType.AllJokerFace, 1);

                        break;
                    case TabType.Gold:
                        itemIdx = (int)ElementType.Gold;
                        userInfo.userDataBase.Gold += quantity;

                        break;
                    case TabType.Ruby:
                        itemIdx = (int)ElementType.MoonRuby;
                        userInfo.userDataBase.Ruby += quantity;

                        break;
                    case TabType.Pet:
                        if(userInfo.userDataBase.userItem.petList == null) userInfo.userDataBase.userItem.petList = new List<Pet>();
                        userInfo.userDataBase.userItem.petList.Add(new Pet(elementIdx_, false));

                        break;
                    case TabType.CardSkin:
                        
                        if(userInfo.userDataBase.userItem.cardSkinList == null) userInfo.userDataBase.userItem.cardSkinList = new List<CardSkin>();
                        userInfo.userDataBase.userItem.cardSkinList.Add(new CardSkin(elementIdx_, false));

                        break;
                    default: break;
                }
                //send SCNotiItemInfo again_ruby
                userInfo.SetSendElementInfoList();
                //save userInfo...
                //save items..
                userInfo.SaveUserInfo();

                if (GameServer._isCloud == true)
                {
                    //item log
                    //Gold = 2, Ruby = 3, Stamina = 1, Ink = 6
                    GlobalManager.Instance.LogItemInsert(serverSession, userInfo.userID, userInfo.userDataBase.nickName, itemIdx, elementInfo.name, quantity, LogItemType.Purchase);
                }
            }
        }
        else
        {
            if (elementInfo == null)
            {
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = string.Format("Shop ElementInfo is null, elementIdx: {0}", elementIdx_);
                    GameServer._logger.Error(log);
                }
            }
            //Console.WriteLine("!!!Error: ShopElementInfo is null");
        }
    }

    internal void GetCSNotiFinishAD(string serverSession, string userSession, string guid)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        //check guid
        if (!string.IsNullOrEmpty(userInfo.ADGuid) && userInfo.ADType != PayType.None)
        {
            if (guid == userInfo.ADGuid)
            {
                //userInfo.userDataBase.ADRemainCount--;
                //userInfo.userDataBase.ADLastTime = DateTime.Now;

                int itemIdx = 0;
                if (userInfo.ADType == PayType.AD_GOLD)
                {
                    userInfo.userDataBase.Gold += userInfo.ADRewardCount;
                    itemIdx = (int)ElementType.Gold;
                }
                else if (userInfo.ADType == PayType.AD_RUBY)
                {
                    userInfo.userDataBase.Ruby += userInfo.ADRewardCount;
                    itemIdx = (int)ElementType.MoonRuby;
                }

                //send elementinfo
                //SendSCNotiUserElement
                userInfo.SetSendElementInfoList();
                //send SCNotiGetADItem
                PacketsHandlerServer.SendSCNotiGetADItem(serverSession, itemIdx, userInfo.ADRewardCount, EffectPosition.PlayCenter);

                userInfo.ADGuid = string.Empty;
                userInfo.ADRewardCount = 0;
                userInfo.ADType = PayType.None;
                userInfo.SaveUserInfo();
            }
        }
    }

    internal void GetCSReqIAP(string serverSession, string userSession, IAPType iAPType_, int elementIdx)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }
        ItemElementInfo info = ItemManager.Instance.Get(elementIdx);
        if (info == null || info.payType != PayType.Cash || userInfo.IAPDoing)
        {
            return;
        }

        userInfo.IAPDoing = true;

        PacketsHandlerInner.SDReqAddIAP(serverSession, userInfo.userID, userInfo.userDataBase.nickName, iAPType_, elementIdx);
    }

    internal void GetCSReqIAPResult(string serverSession, string userSession_, string guid_, IAPResult result_)
    {
        //sdreq client_State
        PacketsHandlerInner.SDReqIAPResult(serverSession, userSession_, guid_, result_);
    }

}
