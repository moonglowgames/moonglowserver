﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class PlayerProfilePacketsManager : Singleton<PlayerProfilePacketsManager>
{
    internal void GetCSReqPlayerProfile(string serverSession, string userSession, string playerNick)
    {
        if (string.IsNullOrEmpty(playerNick)) { return; }

        if (playerNick.ToLower() == "eve") return;
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        int faceIdx = GlobalManager.DEFAULT_FACE_IDX;
        int level = 0;
        int winCount = 0;
        int loseCount = 0;
        int badgeIdx = 0;
        List<int> petIdxList = null;
        UserInfo playerInfo = null;

        foreach (var infoPair in SessionIdManager.Instance)
        {
            playerInfo = infoPair.Value;
            if (playerInfo.userServerState != UserServerStatus.DisConnect && playerInfo.userDataBase.nickName == playerNick)
            {
                faceIdx = playerInfo.userDataBase.FaceIdx;
                level = playerInfo.userDataBase.Level;
                winCount = playerInfo.userDataBase.userProfile.winCount;
                loseCount = playerInfo.userDataBase.userProfile.loseCount;
                badgeIdx = playerInfo.badgeIdx;

                petIdxList = new List<int>();

                if (playerInfo.userDataBase.userItem.petList != null)
                {
                    foreach (var pet in playerInfo.userDataBase.userItem.petList)
                    {
                        if (pet.isSelect)
                        {
                            petIdxList.Add(pet.petIdx);
                        }
                    }
                }

                PlayerProfile playerProfile = new PlayerProfile(PacketTypeList.SCNotiPlayerProfile.ToString(), faceIdx, level, playerNick, winCount, loseCount, badgeIdx, petIdxList);
                PacketsHandlerServer.SendSCNotiPlayerProfile(serverSession, playerProfile);
                return;
            }
        }

        //get info from documentDB
        PacketsHandlerInner.SDReqGetPlayerProfile(serverSession, playerNick);
    }
}
