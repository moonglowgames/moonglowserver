﻿using MoonGlow;
using MoonGlow.Data;
using MoonGlow.Room;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class PlayPacketsManager : Singleton<PlayPacketsManager>
{
    internal void GetCSReqUseCard(string serverSession, string userSession, int targetNumber, int handPosition)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        MoonGlow.Room.Room room = MoonGlow.GameServer.GetRoom(userSession);

        if (room != null)
        {
            //1. someone ( = currentplayer ) use card to target
            //2. card affect to target...
            //3. get a card from deck
            //4. check play again if ( true) turn myself..
            //5. turn next player
            if (room.GetPlayers().Count != room.MaxPlayer) return;
            room.ResetAutoDiscardCount();
            room.UseCard(userInfo.session, targetNumber, handPosition);
        }
    }

    internal void GetCSReqQuickInventory(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        List<QuickItemInfo> quickItemList = new List<QuickItemInfo>();
        QuickItemInfo quickItemInfo1 = null;
        QuickItemInfo quickItemInfo2 = null;

        userInfo.GetQuickItemInfo((int)UserItems.EPElixir, out quickItemInfo1);
        quickItemList.Add(quickItemInfo1);

        userInfo.GetQuickItemInfo((int)UserItems.SOElixir, out quickItemInfo2);
        quickItemList.Add(quickItemInfo2);

        QuickInventory quickInventory = new QuickInventory(PacketTypeList.SCNotiQuickInventory.ToString(), quickItemList);
        PacketsHandlerServer.SendSCNotiQuickInventory(serverSession, quickInventory);
    }

    internal void GetCSRspPlayerJoin(string serverSession, string creatorSession, string askerUserID, bool rspResult)
    {
        UserInfo askerInfo = null;
        UserInfo creatorInfo = null;
        foreach (var infoPair in SessionIdManager.Instance)
        {
            askerInfo = infoPair.Value;
            if (askerInfo.userID == askerUserID) break;
        }

        if (!string.IsNullOrEmpty(askerInfo.roomID))
        {
            //PacketsHandlerServer.SendSCNotiMessage(askerInfo.serverSession, MessageType.AlreadyPlay);

            GameServer.GetUserInfo(creatorSession);
            if (creatorInfo != null) PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.AlreadyPlay);
            return;
        }

        creatorInfo = GameServer.GetUserInfo(creatorSession);  
        Room room = GameServer.GetRoom(creatorSession);
        if (creatorInfo == null)
        {
            //remove room
            if (room != null)
            {
                RoomManager.Instance.TryRemove(room.roomID, out room);
            }
            //tell asker
            PacketsHandlerServer.SendSCNotiMessage(askerInfo.serverSession, MessageType.NoRoom);
            askerInfo.SetHome();
            PacketsHandlerServer.SendSCNotiGotoScene(askerInfo.serverSession, SceneType.Home);
            return;
        }

        //check creator's gold again
        if (room != null)
        {
            if (creatorInfo.userDataBase.Gold < room.stake)
            {
                //remove room
                RoomManager.Instance.TryRemove(room.roomID, out room);
                //go to home
                creatorInfo.roomID = null;
                creatorInfo.SetHome();
                PacketsHandlerServer.SendSCNotiGotoScene(creatorInfo.serverSession, SceneType.Home);
                //tell asker
                PacketsHandlerServer.SendSCNotiMessage(askerInfo.serverSession, MessageType.NoRoom);
                askerInfo.SetHome();
                PacketsHandlerServer.SendSCNotiGotoScene(askerInfo.serverSession, SceneType.Home);
                return;
            }
        }
        if (rspResult)
        {
            //join room
            bool isJoinOk = false;
            //if (room != null)
            //{
                room.isRequested = false;
                if (room.Join(askerInfo.session, false))
                {
                    // send packet
                    isJoinOk = true;
                    UserInfo playerInfo = null;
                    if(room.GetPlayers().Count == room.MaxPlayer)
                    {
                        foreach (var player in room.GetPlayers())
                        {
                            playerInfo = GameServer.GetUserInfo(player.session);
                            if(playerInfo != null)
                            //scnoti enter room..
                            PacketsHandlerServer.SendSCNotiEnterRoom(playerInfo.serverSession, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, room.goalHP, room.goalResource/*conditionInfo.goalHP, conditionInfo.goalResource*/);
                            room.PlayerEnterReady(playerInfo.session);
                        }
                    }
                    else // for 3-4 players
                    {
                        RoomList roomList = null;
                        foreach (var player in room.GetPlayers())
                        {
                            playerInfo = GameServer.GetUserInfo(player.session);
                            roomList = RoomManager.Instance.GetAllRoomList(playerInfo.session);
                            if (roomList != null) PacketsHandlerServer.SendSCNotiRoomList(playerInfo.serverSession, roomList);
                        }
                    }
                //}
            }

            if (!isJoinOk)
            {
                // fail to join, go to Home
                askerInfo.SetHome();
                PacketsHandlerServer.SendSCNotiGotoScene(askerInfo.serverSession, SceneType.Home);
            }
            else
            {
                if (GameServer._isCloud == true)
                {
                    //play log
                    GlobalManager.Instance.LogPlayInsert(serverSession, askerInfo.userID, askerInfo.userDataBase.nickName, room.gameType, room.playMode, room.playTime, room.stake, room.disClose, room.isLock, LogPlayType.Join, string.Empty, string.Empty);
                }
            }
        }
        else
        {
            //scnoti message_reject
            PacketsHandlerServer.SendSCNotiMessage(askerInfo.serverSession, MessageType.JoinReject);
            if (room != null) room.isRequested = false;
        }
     
    }

    internal void GetCSReqCancelJoin(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        if (!string.IsNullOrEmpty(userInfo.creatorSession))
        {
            UserInfo creatorInfo = GameServer.GetUserInfo(userInfo.creatorSession);
            if (creatorInfo != null)
            {
                //player join cancel
                PacketsHandlerServer.SendSCNotiPlayerJoinCancel(creatorInfo.serverSession);
                Room room = GameServer.GetRoom(creatorInfo.session);
                if (room != null) room.isRequested = false;
            }
        }
    }

    internal void GetCSNotiCancelRoom(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        if (string.IsNullOrEmpty(userInfo.roomID))
        {
            PacketsHandlerServer.SendSCNotiMessage(serverSession, MessageType.NoRoom);
            RoomList roomList = RoomManager.Instance.GetAllRoomList(userSession);
            if (roomList != null) PacketsHandlerServer.SendSCNotiRoomList(serverSession, roomList);
            return;
        }
        Room room = GameServer.GetRoom(userSession);
        if (room != null)
        {
            //if not enter room
            if (room.IsCreator(userSession))
            {
                List<Player> playerList = room.GetPlayers();
                for(int i = playerList.Count -1; i >= 0; i--)
                {
                   room.ExitPlayer(playerList[i].session);
                }

                if (room.GetPlayers().Count == 0)
                {
                    //remove room
                    RoomManager.Instance.TryRemove(room.roomID, out room);

                    RoomList roomList = RoomManager.Instance.GetAllRoomList(userSession);
                    if (roomList != null) PacketsHandlerServer.SendSCNotiRoomList(serverSession, roomList);
                }
            }
        }
    }

    internal void GetCSReqWantGame(string serverSession, string userSession, GameAction want)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        Room room = null;
        if (!string.IsNullOrEmpty(userInfo.roomID)) { room = MoonGlow.GameServer.GetRoom(userSession); }
        if (room == null)
        {
            userInfo.SetHome();
            PacketsHandlerServer.SendSCNotiGotoScene(serverSession, SceneType.Home);
            return;
        }

        switch (want)
        {
            //exit during game
            case GameAction.Exit:
                room.PlayerWantExit(serverSession, userSession);
                
                break;
            //action of result window
            case GameAction.Restart:
            /*// 1.
                room = MoonGlow.GameServer.GetRoom(session);
                if (room != null)
                {
                    room.WantRestart = true;
                    // where send playerlsit ?
                }
            break;*/
            //action of result window
            case GameAction.EndGame:
                room.PlayerWantEnd(serverSession, userSession);
                
                break;
            default: break;
        }
    }

    internal void GetCSReqAIPlay(string serverSession, string userSession, bool use)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        Room room = GameServer.GetRoom(userSession);
        if (room == null) return;
        foreach (var player in room.GetPlayers())
        {
            if (player.session == userSession)
            {
                if (use)
                {
                    player.isUser = false;
                    player.setAI = true;
                }
                else
                {
                    player.isUser = true;
                    player.setAI = false;
                }

                player.autoPlayCount = 0;
                break;
            }
        }

        
    }
}
