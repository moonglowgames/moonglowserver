﻿using MoonGlow;
using MoonGlow.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class MyPagePacketsManager : Singleton<MyPagePacketsManager>
{
    internal void GetCSReqMyInfo(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

       userInfo.sceneType = SceneType.MyPage;
       userInfo.SetSendMyInfo();
    }

    internal void GetCSReqInventory(string serverSession , string userSession, TabType tabType)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        List<InventoryInfo> inventoryList = null;
        switch (tabType)
            {
                case TabType.Items:
                    userInfo.SetInvItems(out inventoryList);
                    break;
                case TabType.Character:
                    userInfo.SetInvCharacter(out inventoryList);
                    break;
                case TabType.Pet:
                    userInfo.SetInvPet(out inventoryList);
                    break;
                case TabType.Badge:
                    userInfo.SetInvBadge(out inventoryList);
                    break;
                case TabType.Joker:
                    userInfo.SetInvJokerFace(out inventoryList);
                    break;
                case TabType.CardSkin:
                    userInfo.SetInvCardSkin(out inventoryList);
                    break;
                default: break;
            }
            PacketsHandlerServer.SendSCNotiInventory(serverSession, new InventoryList(PacketTypeList.SCNotiInventory.ToString(), tabType, inventoryList));
    }

    internal void GetCSReqChangeNick(string serverSession, string userSession, string newNick)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        if (userInfo.userDataBase.userItem.saqqaraTablet <= 0)
        {
            PacketsHandlerServer.SendSCRspChangeNick(serverSession, ChangeNickResult.LackItem);
            return;
        }

        PacketsHandlerInner.SDReqChangeNick(serverSession, userSession, newNick);
    }

    internal void GetCSReqUseItem(string serverSession, string userSession, int itemIdx, bool use)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }
        const int DECREASE_ITEM_COUNT = -1;

        ItemElementInfo itemElementInfo = null;
        itemElementInfo = ItemManager.Instance.GetElement(itemIdx);

        bool result = false;
        List<InventoryInfo> inventoryList = null;
        if (itemElementInfo != null)
        {
            switch (itemElementInfo.tabType)
            {
                case TabType.Items:
                    if (use == false) break;

                    result = userInfo.UseUserItem(itemIdx);
                    if (result == true)
                    {
                        userInfo.ChangeUserItemAmount(itemIdx, DECREASE_ITEM_COUNT);

                        if (userInfo.sceneType == SceneType.Joker)
                        {
                            //send SCNotiJokerInfo again 
                            userInfo.SetSendJokerInfo();//face, istry(sometimes), inkItemCount

                            userInfo.SetInvJokerFace(out inventoryList);
                            PacketsHandlerServer.SendSCNotiInventory(serverSession, new InventoryList(PacketTypeList.SCNotiInventory.ToString(), TabType.Joker, inventoryList));

                            userInfo.SetSendSpellIdxList();//spellList
                            userInfo.SaveUserInfo();
                            return;
                        }
                        else
                        {
                            //send SCNotiItemInfo again
                            //userInfo.SetSendItemInfoList();
                            userInfo.SetInvItems(out inventoryList);
                        }
                    }
                    break;
                case TabType.Character:
                    if (use == false) break;

                    result = userInfo.SetUserCharacter(itemIdx);
                    if (result == true)
                    {
                        //send SCNotiUserInfo again
                        int upLevelExp = LevelExpManager.Instance.GetUpLevelExp(userInfo.userDataBase.Level);
                        //int downLevelExp = LevelExpManager.Instance.GetDownLevelExp(userInfo.userDataBase.Level);
                        PacketsHandlerServer.SendSCNotiUserInfo(serverSession, userInfo.userDataBase.FaceIdx, userInfo.userDataBase.nickName, userInfo.badgeIdx, userInfo.userDataBase.Level, userInfo.userDataBase.Exp, upLevelExp);

                        userInfo.SetInvCharacter(out inventoryList);

                        //send SCNotiMyInfo again
                        userInfo.SetSendMyInfo();
                    }
                    break;
                case TabType.Pet:
                    result = userInfo.SetUserPet(itemIdx, use);

                    if (result == true)
                    {
                        userInfo.SetInvPet(out inventoryList);

                        //send SCNotiMyInfo again
                        userInfo.SetSendMyInfo();
                    }

                    break;
                case TabType.Badge:
                    result = userInfo.SetUserBadge(itemIdx, use);

                    if (result == true)
                    {
                        userInfo.SetInvBadge(out inventoryList);

                        //send SCNotiMyInfo again
                        userInfo.SetSendMyInfo();

                        //send userInfo again
                        int upLevelExp = MoonGlow.Data.LevelExpManager.Instance.GetUpLevelExp(userInfo.userDataBase.Level);
                        PacketsHandlerServer.SendSCNotiUserInfo(serverSession, userInfo.userDataBase.FaceIdx, userInfo.userDataBase.nickName, userInfo.badgeIdx, userInfo.userDataBase.Level, userInfo.userDataBase.Exp, upLevelExp);

                    }
                    break;
                case TabType.Joker://Joker Face
                    if (use == false) break;

                    result = userInfo.SetUserJokerFace(itemIdx);

                    if (result == true)
                    {
                        userInfo.SetInvJokerFace(out inventoryList);

                        //send SCNotiJokerInfo again...
                        userInfo.SetSendJokerInfo();//jokerFace, isTry true->false

                        userInfo.spellTypeList = null;

                        //send spelllist again
                        userInfo.SetSendSpellIdxList();
                    }
                    break;
                case TabType.CardSkin:
                    result = userInfo.SetUserCardSkin(itemIdx, use);

                    if (result == true)
                    {
                        userInfo.SetInvCardSkin(out inventoryList);

                        //send SCNotiMyInfo again
                        userInfo.SetSendMyInfo();
                    }
                    break;
                default: break;
            }
            if (result == true)
            {
                if (inventoryList != null)
                {
                    //Send SCNotiInventory again
                    PacketsHandlerServer.SendSCNotiInventory(serverSession, new InventoryList(PacketTypeList.SCNotiInventory.ToString(), itemElementInfo.tabType, inventoryList));
                }
                //save userInfo...
                //save items...
                userInfo.SaveUserInfo();

                if(GameServer._isCloud == true)
                {
                    if(itemIdx != (int)(UserItems.EtherealInk))
                    GlobalManager.Instance.LogItemInsert(serverSession, userInfo.userID, userInfo.userDataBase.nickName, itemIdx, itemElementInfo.name, 1, LogItemType.Use);
                }
            }
            //Send SCRspUseItem
            PacketsHandlerServer.SendSCRspUseItem(serverSession, result);
        }
        else
        {
            //if (itemElementInfo == null) Console.WriteLine("!!!Error: itemElementInfo is null...");
            if (/*Logger*/GameServer._logger.IsErrorEnabled)
            {
                /*Logger*/
                string log = string.Format("itemElementInfo, user: {0}!!!", userInfo.userID);
                GameServer._logger.Error(log);
            }
        }
    }
}
