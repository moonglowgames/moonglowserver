﻿using MoonGlow;
using MoonGlow.Data;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class ServerInfo
{
    public string url { get; set; }
    public string type { get; set; }
    public string ver { get; set; }
    public int con { get; set; }
    public bool enable { get; set; }
    public ServerInfo() { }
    public ServerInfo(string url_, string type_, string ver_, int con_, bool enable_)
    {
        url = url_;
        type = type_;
        ver = ver_;
        con = con_;
        enable = enable_;
    }

}

public partial class InnerPacketsManager : Singleton<InnerPacketsManager>
{
    internal void GetSMReqAddRedis(string serverSession, string userID, UserType userType, string nick, Language language)
    {
        //add login session to redis as userSession
        //if (GameServer._gameServerDic == null || GameServer._gameServerDic.Count == 0)
        if (GameServer._gameServerList == null || GameServer._gameServerList.Count == 0)
        {
            //PacketsHandlerServer.SendSCNotiLogin(serverSession, LoginState.ServerReadyFail, userType, string.Empty, string.Empty);

            //check language
            string msg = (LocalizationManager._dict.ContainsKey(LocalizationKey.SERVER_CHECK.ToString()))?
                         LocalizationManager.Instance.Get(LocalizationKey.SERVER_CHECK.ToString(), language) : LocalizationKey.SERVER_CHECK.ToString();
            PacketsHandlerServer.SendSCNotiServerNoti(serverSession, Command.Message, msg);
            return;
        }

        bool addResult = false;
        bool ipExist = false;
        string userSession = string.Empty;
        string gameServerUrl = string.Empty;

        if (RedisConnect.Instance.CheckSession(userID).Result)
        {
            List<string> value = RedisConnect.Instance.GetRedis(userID).Result;
            if (value.Count == 2)
            {
                userSession = value[0];
                gameServerUrl = value[1];
                if (!string.IsNullOrEmpty(userSession) && !string.IsNullOrEmpty(gameServerUrl) /*&& GameServer._gameServerDic.ContainsKey(gameServerIP)*/)
                {
                    if (GameServer._gameServerList != null)
                    {
                        foreach (var server in GameServer._gameServerList)
                        {
                            if (server.url == gameServerUrl)
                            {
                                if (userType != UserType.User || server.enable)
                                {
                                    ipExist = true;
                                    addResult = true;
                                }

                                break;
                            }
                        }
                    }
                    //ipExist = true;
                }
            }
        }

        //else
        if (ipExist == false)
        {
            //Get userable game server ip from redis "server list"..
            //int maxConnection = 0;
            int minConnection = 10000;

            if (userType == UserType.User)
            {
                foreach (var gameServer in GameServer._gameServerList)
                {
                    if (gameServer.enable && gameServer.con < ServerManager.SERVER_CONNECTION_LIMIT)
                    {

                        if (minConnection >= gameServer.con/*gameServerPair.Value*/)
                        {
                            gameServerUrl = gameServer.url/*gameServerPair.Key*/;
                            minConnection = gameServer.con/*gameServerPair.Value*/;
                        }
                        //if (gameServerPair.Value >= maxConnection)
                        //{
                        //    maxConnection = gameServerPair.Value;
                        //    gameServerIP = gameServerPair.Key;
                        //}
                    }

                }
            }
            else
            {
                foreach (var gameServer in GameServer._gameServerList)
                {
                    if (gameServer.con < ServerManager.SERVER_CONNECTION_LIMIT)
                    {

                        if (minConnection >= gameServer.con/*gameServerPair.Value*/)
                        {
                            gameServerUrl = gameServer.url/*gameServerPair.Key*/;
                            minConnection = gameServer.con/*gameServerPair.Value*/;
                        }
                        //if (gameServerPair.Value >= maxConnection)
                        //{
                        //    maxConnection = gameServerPair.Value;
                        //    gameServerIP = gameServerPair.Key;
                        //}
                    }

                }
            }

            if (string.IsNullOrEmpty(gameServerUrl))
            {
                string msg = (LocalizationManager._dict.ContainsKey(LocalizationKey.SERVER_CHECK.ToString())) ?
                            LocalizationManager.Instance.Get(LocalizationKey.SERVER_CHECK.ToString(), language) : LocalizationKey.SERVER_CHECK.ToString();
                PacketsHandlerServer.SendSCNotiServerNoti(serverSession, Command.Message, msg);
                return;
            }

            if (RedisConnect.Instance.AddRedis(userID, serverSession, gameServerUrl).Result)
            {
                userSession = serverSession;
                addResult = true;
            }
        }

        PacketsHandlerInner.MSRspAddRedis(serverSession, addResult, userSession, userID, userType, nick, gameServerUrl);
    }

    internal void GetSMReqGetChanelList(string serverSession, ChanelInfoList chanelInfoList)
    {
        Task<long> x = null;
        foreach (var chanelInfo in chanelInfoList.chanelInfoList)
        {
            x = RankConnect.Instance.GetRankCardinality(chanelInfo.name);
            chanelInfo.currentCount = Convert.ToInt32(x.Result);
        }

        PacketsHandlerInner.MSRspGetChanelList(serverSession, chanelInfoList);

    }

    internal void GetSMReqCurrentRank(string serverSession, string userSession, string userID, string chanelName, int chanelIdx, int rankPoint, int rankWin, int rankLose, int rankDraw, bool isChanelClose)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        Task<long?> x;
        x = RankConnect.Instance.GetRank(chanelName, userID);

        Task<long> y = null;
        y = RankConnect.Instance.GetRankCardinality(chanelName);

        PacketsHandlerInner.MSRspCurrentRank(serverSession, userSession, userID, chanelIdx, chanelName, Convert.ToInt32(x.Result + 1), Convert.ToInt32(y.Result), rankPoint, rankWin, rankLose, rankDraw, isChanelClose);
    }

    internal void GetSMReqUpdateRank(string serverSession, string userSession, string userID, string chanelName)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        Task<bool> x;
        x = RankConnect.Instance.RemoveRank(chanelName, userID);
        x = RankConnect.Instance.AddRank(chanelName, userInfo.userID, userInfo.userDataBase.userProfile.rankPoint);

        if (x.Result == false) Console.WriteLine("Update rank from Redis Fail!");

    }

    internal void GetDMReqRepairChanel(string serverSession, ChanelUserList chanelUserList)
    {
        bool repairResult = true;
        foreach (var chanelUser in chanelUserList.chanelUserList)
        {
            Task<bool> x;
            x = RankConnect.Instance.RemoveRank(chanelUser.chanelName, chanelUser.userID);
            x = RankConnect.Instance.AddRank(chanelUser.chanelName, chanelUser.userID, chanelUser.rankPoint);
            if (x.Result == false)
            {
                repairResult = false;
            }
        }

        PacketsHandlerInner.MSRspRepairChanel(serverSession, repairResult);
    }

    internal void GetSMReqBCServerNoti(string serverSession, string message)
    {
        RedisConnect.Instance.BCServerNoti(serverSession, message).Wait();
    }

    internal void GetSMReqExpireUserInfo(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);

        Task<bool> x;
        if (userInfo != null)
        {
            if (DateTime.Now >= userInfo.userInfoRemoveDate)
            {
                //save userinfo 
                //save all
                userInfo.SaveUserInfo();
                userInfo.SaveToSql();

                x = RedisConnect.Instance.RemoveRedis(userInfo.userID/*userSession*/);

                PacketsHandlerInner.MSNotiRemoveRedis(serverSession, x.Result, userSession, false);
            }
        }
    }

    internal void GetSMReqSessionCheck(string serverSession, string userID, string userSession, Language language)
    {
        bool checkResult = false;
        if (RedisConnect.Instance.CheckSession(userID).Result)
        {
            List<string> value = RedisConnect.Instance.GetRedis(userID).Result;
            if (value.Count == 2)
            {
                if (value[0] == userSession) checkResult = true;
            }
        }

        PacketsHandlerInner.MSRspLoginSessionCheck(serverSession, checkResult, userSession, userID, language);
    }

    internal void GetSMReqRankRange(string serverSession, string userSession, string chanelName)
    {
        Task<long> y = null;
        y = RankConnect.Instance.GetRankCardinality(chanelName);

        Task<SortedSetEntry[]> x = null;
        if (y.Result < 50)
        {
            x = RankConnect.Instance.GetRankRange(chanelName, 0, y.Result);
        }
        else
        {
            x = RankConnect.Instance.GetRankRange(chanelName, 0, 49);
        }

        PacketsHandlerInner.MDReqRankRange(serverSession, new RankRange(PacketTypeList.MDReqRankRange.ToString(), userSession, chanelName, Convert.ToInt32(y.Result), x.Result));

    }

    internal void GetSMReqAddRank(string serverSession, string userSession, /*float*/int rankPoint, string chanelName, int chanelIdx)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        ChanelResult result = ChanelResult.Unknown;
        Task<long> y = null;
        y = RankConnect.Instance.GetRankCardinality(chanelName);
        int currentCount = Convert.ToInt32(y.Result);
        int channelMaxCount = ChannelManager.Instance.GetChannelMaxCount(chanelIdx);
        if (currentCount >= channelMaxCount/*GlobalManager.CHANEL_MAX_COUNT*/)
        {
            result = ChanelResult.NotAccept;
        }
        else
        {
            Task<bool> x;
            x = RankConnect.Instance.AddRank(chanelName, userInfo.userID, rankPoint);
            if (x.Result == true)
            {
                result = ChanelResult.OK;
            }
            else
            {
                //remove old and add again...
                x = RankConnect.Instance.RemoveRank(chanelName, userInfo.userID);
                x = RankConnect.Instance.AddRank(chanelName, userInfo.userID, rankPoint);
                result = ChanelResult.OK;
            }

        }

        PacketsHandlerInner.MSRspAddRank(serverSession, result, userSession, Convert.ToInt32(y.Result), chanelIdx, chanelName);
    }

    internal void GetSMReqRemoveRedis(string serverSession, string userID, string userSession)
    {
        bool removeResult = RedisConnect.Instance.RemoveRedis(userID).Result;

        PacketsHandlerInner.MSNotiRemoveRedis(serverSession, removeResult, userSession, true);

    }

    //SMReqSetServerGlobalState
    internal void GetSMReqSetServerGlobalState(string serverSession, string state)
    {
        bool result = ServerStateRedisConnect.Instance.SetGlobalServerState(state).Result;

        PacketsHandlerInner.MSRspSetServerGlobalState(serverSession, result, state);
    }

    //SMReqBCServerState
    internal void GetSMReqBCServerState(string serverState)
    {
        ServerStateRedisConnect.Instance.BCGlobalServerState(serverState).Wait();
    }

    internal void GetSMReqBCReFreshUserType(string serverSession, UserType userType)
    {
        RedisConnect.Instance.BCReFreshUserType(serverSession, userType).Wait();
    }

    internal void GetSMReqExpireRedis(string userID)
    {
        if (!RedisConnect.Instance.ExpireRedis(userID).Result)
        {
            //Console.WriteLine("!!!Error: Extend Session Expire Time Failed.");
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = string.Format("Extend Session Expire Time Failed, user: {0}", userID);
                GameServer._logger.Error(log);
            }
        }
    }

    internal void GetSMReqBCForceClose(string userID)
    {
        RedisConnect.Instance.BCForceClose(userID).Wait();
    }

    internal void GetSMReqFreshServerList(string serverSession)
    {
        if (!GameServer._isLoginServer)
        {
            //if (GameServer._enable) return;

            if (SessionIdManager.Instance._sessionDic != null)
            {
                if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
                {
                    string msg = "Server List Refreshed.";
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);

                    if (!GameServer._enable) PacketsHandlerInner.MSRspUpdateServerEnable(serverSession);
                }
            }
            return;
        }

        GameServer._gameServerList = ServerStateRedisConnect.Instance.GetGameServerList();
    }

    internal void GetSMReqBCReFreshServerList(string serverSession)
    {
        ServerStateRedisConnect.Instance.BCReFreshServerList(serverSession).Wait();
    }

    internal void GetSMReqBCReFreshChannelInfo(string serverSession)
    {
        ServerStateRedisConnect.Instance.BCReFreshChannelInfo(serverSession).Wait();

        //need BC?
    }

    internal void GetSMReqBCReFreshClientVersions(string serverSession)
    {
        //need BC?

        ServerStateRedisConnect.Instance.BCReFreshClientVersion(serverSession).Wait();
    }

    internal void GetSMNotiUpdateEnable(string serverSession, string connectDNS, int connectPort, bool enable)
    {
        if (GameServer._isLoginServer == false && /*GameServer._connectIP*/GameServer._DNS == connectDNS && GameServer._port == connectPort)
        {
            bool result = ServerStateRedisConnect.Instance.UpdateServerEnable(connectDNS, connectPort, enable).Result;
            if (result)
            {
                GameServer._enable = enable;
                if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
                {
                    string msg = string.Format("Change Server enable: {0} successfully.", GameServer._enable.ToString().ToLower());
                    PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);

                    if (GameServer._logger.IsInfoEnabled)
                    {
                        string log = string.Format("Change server: {0} enable to {1}", GameServer._DNS + ":" + GameServer._port, GameServer._enable.ToString());
                        GameServer._logger.Info(log);
                    }
                }

                ServerStateRedisConnect.Instance.BCReFreshServerList(serverSession).Wait();
            }
        }
    }

    internal void GetSMReqBCUpdateServerEnable(string serverSession, string connectDNS, int connectPort, bool enable)
    {
        ServerStateRedisConnect.Instance.BCUpdateServerEnable(serverSession, connectDNS, connectPort, enable).Wait();
    }

    internal void GetSMReqBCUpdateAllServerEnable(string serverSession, bool enable)
    {
        GameServer._allServerList = ServerStateRedisConnect.Instance.GetAllServerInfo();

        string connectDNS = string.Empty;
        int connectPort = 0;
        string[] value;
        foreach (var server in GameServer._allServerList)
        {
            value = server.url.Split(':');
            if (value.Length == 2)
            {
                connectDNS = value[0];
                connectPort = int.Parse(value[1]);
                PacketsHandlerInner.SMReqBCUpdateServerEnable(serverSession, connectDNS, connectPort, enable);
            }
        }
    }

    internal void GetSMReqGetAllServerInfo(string serverSession)
    {
        bool loginServerState = ServerStateRedisConnect.Instance.GetGlobalServerState().Result;
        GameServer._allServerList = ServerStateRedisConnect.Instance.GetAllServerInfo();
        string msg = string.Empty;
        for (int i = 0; i < GameServer._allServerList.Count; i++)
        {
            if (msg == string.Empty) msg = string.Format("Login server state: {0}", loginServerState ? "start" : "stop");
            if (msg != string.Empty) msg = msg + "\n";
            msg = msg + string.Format("[{0}] url:{1}, type:{2}, ver:{3}, con:{4}, state:{5}",
                        i + 1, GameServer._allServerList[i].url, GameServer._allServerList[i].type, GameServer._allServerList[i].ver, GameServer._allServerList[i].con, GameServer._allServerList[i].enable ? "enable" : "disable");
        }

        PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
    }

    internal void GetSMReqGetThisServerInfo(string serverSession)
    {
        GameServer._allServerList = ServerStateRedisConnect.Instance.GetAllServerInfo();
        for (int i = 0; i < GameServer._allServerList.Count; i++)
        {
            if (GameServer._allServerList[i].url == /*GameServer._connectIP*/GameServer._DNS + ":" + GameServer._port)
            {
                string msg = string.Format("\n[{0}] url:{1}, type:{2}, ver:{3}, con:{4}, state:{5}",
                       i + 1, GameServer._allServerList[i].url, GameServer._allServerList[i].type, GameServer._allServerList[i].ver, GameServer._allServerList[i].con, GameServer._allServerList[i].enable ? "enable" : "disable");

                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                break;
            }
        }
    }

    internal void GetSMReqChangeServer(string serverSession, string userSession, string connectUrl)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }
        GameServer._allServerList = ServerStateRedisConnect.Instance.GetAllServerInfo();
        bool changeable = false;
        foreach (var server in GameServer._allServerList)
        {
            if (server.url == connectUrl && server.type == "Game")
            {
                changeable = true;
                break;
            }
        }

        string msg = string.Empty;
        if (!changeable)
        {
            msg = string.Format("Game Server {0} is not avaliable!", connectUrl);
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }
        else
        {
            userInfo.SaveUserInfo();
            userInfo.SaveToSql();

            //update redis
            RedisConnect.Instance.RemoveRedis(userInfo.userID).Wait();
            RedisConnect.Instance.AddRedis(userInfo.userID, userSession, connectUrl).Wait();

            GlobalManager.Instance.RemoveUserInfo(userSession);
            if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession)) SessionIdManager.Instance._sessionDic.Remove(serverSession);

            if (!ServerStateRedisConnect.Instance.CheckServerList(/*GameServer._connectIP*/GameServer._DNS, GameServer._port, GameServer._type, GameServer._version, SessionIdManager.Instance.Count, GameServer._enable).Result)
            {
                //Console.WriteLine("!!!Error: Add Server to Redis Server List Fail.");
                if (GameServer._logger.IsErrorEnabled)
                {
                    string log = string.Format("Add Server to Redis Server List Failed");
                    GameServer._logger.Error(log);
                }
            }
            UserType userType = UserType.User;
            GlobalManager.Instance.GetUserType(userInfo.userID, out userType);
            PacketsHandlerServer.SendSCNotiLogin(serverSession, LoginState.OK, userType, userInfo.userID, userSession, connectUrl);
        }
    }

    internal void GetDMReqBCBanUser(string serverSession, string userID, string userNick, bool ban, int minutes)
    {
        string msg = string.Empty;
        if (minutes != 0)
        {
            //add to ban user list
            TimeSpan timeSpan = new TimeSpan(0, minutes, 0);
            if (!BanRedisConnect.Instance.AddRedis(userID, timeSpan).Result)
            {
                //Console.WriteLine("!!!Error: Add user to ban list Failed.");
                msg = string.Format("Add User:{0} to ban list Failed.", userNick);
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
                return;
            }
            else
            {
                msg = string.Format("Add User:{0} to ban list Successfully.", userNick);
                if (GameServer._logger.IsInfoEnabled)
                {
                    string log = string.Format("Add User:{0} to Redis BanList Successfully.", userNick);
                    GameServer._logger.Info(log);
                }

            }
        }
        else
        {
            msg = string.Format("Update User: {0} ban: {1} successfully.", userNick, ban);
            if (GameServer._logger.IsInfoEnabled)
            {
                string log = string.Format("Update User to DB: {0} ban: {1} successfully.", userNick, ban);
                GameServer._logger.Info(log);
            }
        }

        PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        RedisConnect.Instance.BCBanUser(userID).Wait();
    }

    internal void GetSMNotiBanUser(string serverSession, string userID)
    {
        UserInfo userInfo = null;
        if (SessionIdManager.Instance != null)
        {
            foreach (var infoPair in SessionIdManager.Instance)
            {
                if (infoPair.Value.userID == userID)
                {
                    userInfo = infoPair.Value;
                    break;
                }
            }
        }

        if (userInfo != null)
        {
            //update redis
            //send quit
            PacketsHandlerServer.SendSCNotiQuit(userInfo.serverSession);
            //PacketsHandlerServer.SendSCNotiLogin(userInfo.serverSession, LoginState.Ban, UserType.User, userID, userInfo.session, string.Empty);
            RedisConnect.Instance.RemoveRedis(userInfo.userID).Wait();

            GlobalManager.Instance.RemoveUserInfo(userInfo.session);

            if (SessionIdManager.Instance._sessionDic.ContainsKey(userInfo.serverSession)) SessionIdManager.Instance._sessionDic.Remove(userInfo.serverSession);
        }

    }

    internal void GetSMReqBCUpdateLimit(string serverSession, int count)
    {
        ServerStateRedisConnect.Instance.BCUpdateServerLimit(serverSession, count).Wait();
    }

    //GetSMReqBCReFreshLocalization
    internal void GetSMReqBCReFreshLocalization(string serverSession)
    {
        ServerStateRedisConnect.Instance.BCReFreshLocalization(serverSession).Wait();
    }

    //GetSMReqBCReFreshNews
    internal void GetSMReqBCReFreshNews(string serverSession)
    {
        ServerStateRedisConnect.Instance.BCReFreshNews(serverSession).Wait();
    }

    //GetSMReqBCReFreshMission
    internal void GetSMReqBCReFreshMission(string serverSession)
    {
        ServerStateRedisConnect.Instance.BCReFreshMission(serverSession).Wait();
    }

    internal void GetDMRepBCNewFriendGift(string serverSession, string userID)
    {
        ServerStateRedisConnect.Instance.BCNewFriendGift(userID).Wait();
    }

    internal void GetSMReqBCAlarmShop(string serverSession, string alarmString)
    {
        ServerStateRedisConnect.Instance.BCAlarmShop(serverSession, alarmString).Wait();
    }

    internal void GetSMReqBCAlarmNews(string serverSession, string alarmString)
    {
        ServerStateRedisConnect.Instance.BCAlarmNews(serverSession, alarmString).Wait();
    }
}
