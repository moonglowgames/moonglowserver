﻿using MoonGlow;
using MoonGlow.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class NewsPacketsManager : Singleton<NewsPacketsManager>
{
    internal void GetCSReqNews(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.sceneType = SceneType.News;

        List<News> newsList = NewsManager.Instance.GetNewsList(userInfo.language);
        if (newsList != null)
        {
            //Send SCNotiNews
            PacketsHandlerServer.SendSCNotiNews(serverSession, new NewsList(PacketTypeList.SCNotiNews.ToString(), newsList));
        }
    }
} 
