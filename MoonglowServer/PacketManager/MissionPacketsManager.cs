﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class MissionPacketsManager : Singleton<MissionPacketsManager>
{
    internal void GetCSReqMissionList(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.sceneType = SceneType.Mission;
        List<Mission> missionList = null;
        userInfo.GetMissionList(out missionList);
        if (missionList != null)
        {
            PacketsHandlerServer.SendSCNotiMissionList(serverSession, new MissionList(PacketTypeList.SCNotiMissionList.ToString(), missionList));
        }
    }
}