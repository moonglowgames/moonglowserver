﻿using MoonGlow;
using MoonGlow.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class JokerPacketsManager : Singleton<JokerPacketsManager>
{
    internal void GetCSReqJokerInfo(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.sceneType = SceneType.Joker;
        //send SCNotiJokerInfo
        userInfo.SetSendJokerInfo();

        //if (userInfo.userDataBase.userJoker.faceIdx != 0 || userInfo.tmpJokerFace != 0)
        //{
            userInfo.SetSendSpellIdxList();
        //}
    }

    //internal void GetCSReqSpellList(string serverSession, string userSession, SpellType spellType)
    //{
    //    UserInfo userInfo = GameServer.GetUserInfo(userSession);
    //    if (userInfo == null)
    //    {
    //        GlobalManager.Instance.NullUserInfo(serverSession);
    //        return;
    //    }

    //    //userInfo.spellType = spellType;
    //    //send SCNotiSpellIdxList
    //    //userInfo.SetSendSpellIdxList(spellType);        
    //    userInfo.SetSendSpellIdxList();
    //}

    internal void GetCSReqUpgradeSpell(string serverSession, string userSession, int currentSpellIdx)
    {
        //judge if face is consistent with spelltype
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        int jokerFace = (userInfo.tmpJokerFace == 0) ? userInfo.userDataBase.userJoker.faceIdx : userInfo.tmpJokerFace;
        if (jokerFace == 0) return;

        SpellInfo spellInfo = null;
        if (userInfo.spellTypeList == null) ItemManager.Instance.GetJokerFaceType(jokerFace, out userInfo.spellTypeList);
        {
            spellInfo = SpellManager.Instance.GetSpellInfo(currentSpellIdx);
            if (!userInfo.spellTypeList.Contains(spellInfo.spellType)) return;
        }

        SpellResult result = SpellResult.Unknown;
        userInfo.SetUpgradeSpells(currentSpellIdx, out result);
        //send SCRspUpgradeSpell 
        PacketsHandlerServer.SendSCRspUpdateSpell(serverSession, result);

        if (result == SpellResult.OK)
        {
            //send SCNotiJokerInfo again
            if (!userInfo.isJokerTry)
            {
                userInfo.isJokerTry = true;
                userInfo.SetSendJokerInfo();//isTry false->true
            } 

            //send SCNotiSpellList again      
            userInfo.SetSendSpellIdxList();
        }       
    }

    internal void GetCSReqDowngradeSpell(string serverSession, string userSession, int currentSpellIdx)
    {
        //judge if face is consistent with spelltype
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        if (currentSpellIdx % 100 == 0) return;

        int jokerFace = (userInfo.tmpJokerFace == 0) ? userInfo.userDataBase.userJoker.faceIdx : userInfo.tmpJokerFace;
        if (jokerFace == 0) return;

        SpellInfo spellInfo = null;
        if (userInfo.spellTypeList == null) ItemManager.Instance.GetJokerFaceType(jokerFace, out userInfo.spellTypeList);
        {
            spellInfo = SpellManager.Instance.GetSpellInfo(currentSpellIdx);
            if (!userInfo.spellTypeList.Contains(spellInfo.spellType)) return;
        }

        SpellResult result = SpellResult.Unknown;
        userInfo.SetDowngradeSpells(currentSpellIdx, out result);
        //send SCRspUpgradeSpell 
        PacketsHandlerServer.SendSCRspUpdateSpell(serverSession, result);

        if (result == SpellResult.OK)
        {
            //send SCNotiJokerInfo again
            if (!userInfo.isJokerTry)
            {
                userInfo.isJokerTry = true;
                userInfo.SetSendJokerInfo();//isTry false->true
            }

            //send SCNotiSpellList again      
            userInfo.SetSendSpellIdxList();
        }
    }

    internal void GetCSReqJokerApply(string serverSession, string userSession/*, SpellType spellType*/)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        if (userInfo.isJokerTry == false) return;
        bool applyResult = userInfo.ApplyUserJoker();

        //send SCRspApplySpells
        PacketsHandlerServer.SendSCRspJokerApply(serverSession, applyResult);

        if (applyResult/* == SpellResult.OK*/)
        {
            //send SCNotiJokerInfo again
            userInfo.SetSendJokerInfo();//isTry true->false, jokerFace

            //send SCNotiSpellList again  
            //userInfo.SetSendSpellIdxList(spellType);
            userInfo.SetSendSpellIdxList();
            //save userinfo
            //save userjoker 
            userInfo.SaveUserInfo();
        }
    }

    internal void GetCSReqJokerCancel(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        if (!userInfo.isJokerTry) return;

        userInfo.CancelUserJoker();

        //send SCNotiJokerInfo again
        userInfo.SetSendJokerInfo();//isTry, true->false, jokerFace(sometimes)

        //send SCNotiSpellList again  
        //userInfo.SetSendSpellIdxList(spellType);
        userInfo.SetSendSpellIdxList();
    }
}