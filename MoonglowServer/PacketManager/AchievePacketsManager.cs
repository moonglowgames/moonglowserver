﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class AchievePacketsManager : Singleton<AchievePacketsManager>
{
    internal void GetCSReqAchieveList(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }
        
        userInfo.sceneType = SceneType.Achievement;
        List<Achievement> achieveList = null;
        userInfo.GetAchieveList(out achieveList);
        if (achieveList != null)
        {
            PacketsHandlerServer.SendSCNotiAchieveList(serverSession, new AchieveList(PacketTypeList.SCNotiAchieveList.ToString(), achieveList));
        }
    }
}
