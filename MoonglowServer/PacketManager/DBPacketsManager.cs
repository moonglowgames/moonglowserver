﻿using MoonGlow;
using MoonGlow.Data;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public partial class InnerPacketsManager : Singleton<InnerPacketsManager>
{
    internal void GetSDReqReadUserData(string serverSession, string userSession, string userID)
    {
        //string nick = string.Empty;
        //int level = 0;
        //int faceIdx = 0;
        //int friendCount = 0;
        //int rankPoint = 0;
        //int checkInDays = 0;
        //string lastLoginDate = string.Empty;
        string userInfoJson = string.Empty;
        //if(GameServer.SqlType == SQLType.MySQL) MySqlConnect.Instance.GetUserInfo(userID, out nick, out level, out faceIdx, out rankPoint, out lastLoginDate, out friendCount, out checkInDays, out userInfoJson);
        //else MSSqlConnect.Instance.GetUserInfo(userID, out nick, out level, out faceIdx, out rankPoint, out lastLoginDate, out friendCount, out checkInDays, out userInfoJson);

        GameServer.documentDBConnect.GetUserInfo(userID, out userInfoJson);

        bool result = false;
        //if (userInfoDataBaseManager != null ) result = true;
        if (userInfoJson != null && userInfoJson != string.Empty)
        {
            result = true;

            //if (GameServer._isCloud == true)
            //{
            //    //SessionLogin Log
            //    LogAccountData data = null;
            //    data = new LogAccountData(userID, nick, DateTime.Now.ToString(), LogAccountType.SessionLogin);
            //    PacketsHandlerLog.SLogReqAccountInsert(serverSession, new PCKAccountLog(PacketTypeList.SLogReqAccountInsert.ToString(), data));
            //}
        }

        PacketsHandlerInner.DSRspReadUserData(serverSession, result, userSession, /*nick, level, faceIdx, rankPoint, lastLoginDate, friendCount, checkInDays,*/ userInfoJson);
    }

    internal void GetSDReqUpdateUserInfo(string serverSession, UserData userData)
    {
        //bool result = UserInfoDataBaseManager.Instance.SaveToDataBase(/*pckUserData.nick, pckUserData.level, pckUserData.faceIdx, pckUserData.rankPoint, */pckUserData.userData);

        bool result = GameServer.documentDBConnect.UpdateUserInfo(userData);

        if (!result) Console.WriteLine("*** DB: Update UserInfo Failed");

        // PacketsHandlerInner.DSRspUpdateUserInfo(serverSession, result);
    }

    internal void GetSDReqGetMailList(string serverSession, string userSession)
    {
        bool getResult = false;
        List<MailData> mailList = null;
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            GameServer.msSqlConnect.GetMailList(userInfo.userID, out mailList);

            if (mailList != null)
            {
                userInfo._mailList = mailList;
                getResult = true;

                //send SCNotiMenuAlarm - mailbox
                bool allowZero = false;
                userInfo.SetSendMenuAlarmMail(allowZero);
            }
        }

        if (getResult == false)
        {
            Console.WriteLine("*** DB: Get MailList from DB Failed");
        }
    }

    internal void GetSDReqUpdateMail(string serverSession, string userSession, int mailDBIdx)
    {
        bool updateResult = false;
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            if (userInfo._mailList != null)
            {
                foreach (var mailData in userInfo._mailList)
                {
                    if (mailData.idx == mailDBIdx)
                    {
                        updateResult = GameServer.msSqlConnect.MailUpdate(mailData);

                        break;
                    }
                }
            }
        }

        if (!updateResult) Console.WriteLine("*** DB: Update Mail from DB Failed");

        //make DSRspUpdateMail
        //PacketsHandlerInner.DSRspUpdateMail(serverSession, updateResult);
    }

    internal void GetSDReqUserFriendCount(string serverSession, string userSession, string friendID, int friendIdx, PacketTypeList sender)
    {
        int count = 0;
        bool getResult = GameServer.msSqlConnect.GetFriendCount(friendID, out count);

        //make DSRspUserFriendCount
        PacketsHandlerInner.DSRspUserFriendCount(serverSession, getResult, userSession, friendIdx, count, sender);
    }

    internal void GetSDReqUpdateGetFriendItem(string serverSession, string userSession, string friendID)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        bool updateResult = false;
        if (userInfo != null)
        {
            updateResult = GameServer.msSqlConnect.UpdateFriendGetGift(userInfo.userID, friendID);
        }

        if (!updateResult) Console.WriteLine("*** DB: Update get friend send item from DB Failed");

        //PacketsHandlerInner.DSRspUpdateGetFriendItem(serverSession, updateResult);
    }

    internal void GetSDReqGiftSendDate(string serverSession, string userSession, string friendID, string sendDate)
    {
        bool updateResult = false;
        DateTime _sendDate = new DateTime();
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            if (GlobalManager.Instance.ConvertToDateTime(sendDate, out _sendDate))
            {
                updateResult = GameServer.msSqlConnect.UpdateGiftSendDate(userInfo.userID, friendID, _sendDate);
            }
        }

        if (!updateResult) Console.WriteLine("*** DB: Update giftSendDate from DB Failed");

        //PacketsHandlerInner.DSRspGiftSendDate(serverSession, updateResult);
    }

    internal void GetSDReqRankHistory(string serverSession, string userSession)
    {
        List<PCKRankInfo> rankHisList = null;
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            GameServer.msSqlConnect.GetMyRankHis(userInfo.userID, out rankHisList);

            PacketsHandlerInner.DSRspRankHistory(serverSession, new PCKRankHisList(PacketTypeList.DSRspRankHistory.ToString(), userSession, rankHisList));

        }
    }

    internal void GetSDReqNewRankHis(string serverSession, string userSession, string userID, string chanelName, int currentCount, int currentPercent, /*float*/int currentPoint, int currentRankWin, int currentRankLose, int currentRankDraw, bool itemExist)
    {
        bool addResult = GameServer.msSqlConnect.AddNewRankHis(userID, chanelName, currentCount, currentPercent, currentPoint, currentRankWin, currentRankLose, currentRankDraw, itemExist);

        PacketsHandlerInner.DSRspNewRankHis(serverSession, addResult, userSession);
    }

    internal void GetSDReqUpdateChanelInfo(string serverSession, int chanelIdx)
    {
        bool updateResult = GameServer.msSqlConnect.UpdateChannelInfo(chanelIdx, false);

        if (!updateResult) Console.WriteLine("*** DB: Update ChanelUser to DB Failed.");

        //PacketsHandlerInner.DSRspUpdateChanelInfo(serverSession, updateResult);
    }

    internal void GetSDReqRemoveChanelUser(string serverSession, string chanelName)
    {
        bool removeResult = GameServer.msSqlConnect.RemoveChannelUser(chanelName);

        if (!removeResult) Console.WriteLine("*** DB: Update ChanelUser from DB Failed.");
        //PacketsHandlerInner.DSRspRemoveChanelUser(serverSession, removeResult);
    }

    internal void GetSDReqUpdateChanelInfoDate(string serverSession, int chanelIdx, string dateTime)
    {
        bool updateResult = false;
        DateTime _dateTime = new DateTime();
        if (GlobalManager.Instance.ConvertToDateTime(dateTime, out _dateTime))
        {
            updateResult = GameServer.msSqlConnect.UpdateChannelInfoDate(chanelIdx, _dateTime);
        }
    }

    internal void GetSDReqSaveToSql(string serverSession, string userID, string nickName, int faceIdx, int level, int rankPoint, string lastLoginDate)
    {
        bool saveResult = false;
        string jsonstring = string.Empty;//test first

        if (GameServer.msSqlConnect.UpdateUserData(userID, nickName, level, faceIdx, rankPoint, lastLoginDate))
        {
            saveResult = true;
        }

        if (!saveResult)
        {
            //Console.WriteLine("!!!Error: save userData to SQL failed.");
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "GetSDReqSaveToSql error";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
    }

    internal void GetSDReqLoginCheck(string serverSession_, AccountType accountType_, string account_, string password_, string deviceID_, Language language)
    {
        LoginState state = LoginState.Unknown;

        bool sameDevice = false;
        string userID = string.Empty;

        string _deviceID = string.Empty;
        GameServer.msSqlConnect.LoginCheck(accountType_.ToString(), account_, password_, out userID, out _deviceID, out state);

        sameDevice = (deviceID_ == _deviceID) ? true : false;

        string nick = string.Empty;
        if (!string.IsNullOrEmpty(userID)) GameServer.documentDBConnect.GetUserNick(userID, out nick);

        UserType userType = UserType.User;
        GlobalManager.Instance.GetUserType(account_, out userType);

        if (state == LoginState.Ban)
        {
            PacketsHandlerInner.DSRspLoginCheck(serverSession_, userID, userType, nick, sameDevice, deviceID_, state, language);
        }
        else
        {
            //check ban _redis
            PacketsHandlerInner.SMReqCheckUserBan(serverSession_, userID, userType, nick, sameDevice, deviceID_, state, language);
        }

        //PacketsHandlerInner.DSRspLoginCheck(serverSession_, userID, userType, nick, sameDevice, deviceID_, state, language);
    }

    internal void GetSDReqRegisterCheck(string serverSession, AccountType accountType, string account, string password, string nick, int faceIdx, string deviceID)
    {
        LoginState state = LoginState.Unknown;

        GameServer.msSqlConnect.RegisterCheck(accountType.ToString(), account, nick, out state);

        PacketsHandlerInner.DSRspRegisterCheck(serverSession, state, accountType, account, password, nick, faceIdx, deviceID);
    }

    internal void GetSDReqRepairChanel(string serverSession)
    {
        List<ChanelUser> chanelUserList = new List<ChanelUser>();
        foreach (var chanel in ChannelManager._items)
        {
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();
            if (GlobalManager.Instance.ConvertToDateTime(chanel.startDate, out startDate) && GlobalManager.Instance.ConvertToDateTime(chanel.endDate, out endDate))
            {
                GameServer.msSqlConnect.GetChannelUserList(chanel.name, chanelUserList);
            }
        }

        if (chanelUserList.Count > 0)
        {
            ChanelUserList _chanelUserList = new ChanelUserList(PacketTypeList.DMReqRepairChanel.ToString(), chanelUserList);
            PacketsHandlerInner.DMReqRepairChanel(serverSession, _chanelUserList);
        }
        else
        {
            string msg = "No channel list needs to repair";
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }

    }

    internal void GetSDReqAddChanelUser(string serverSession, string userID, string chanelName)
    {
        bool addResult = GameServer.msSqlConnect.AddChannelUser(chanelName, userID);

        if (!addResult) Console.WriteLine("*** DS: Add ChanelUser to DB Failed.");
    }

    internal void GetSDReqUpdateRankHis(string serverSession, string userID, string chanelName)
    {
        bool itemGot = true;
        DateTime itemGotDate = DateTime.Now;
        bool updateResult = GameServer.msSqlConnect.UpdateRank(userID, chanelName, itemGot, itemGotDate);

        if (!updateResult) Console.WriteLine("*** DB: Update rank to DB Failed.");
        //PacketsHandlerInner.DSRspUpdateRankHis(serverSession, updateResult);
    }

    internal void GetMDReqRankRange(string serverSession, RankRange rankRange)
    {
        UserInfo userInfo = GameServer.GetUserInfo(rankRange.userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        List<Rank> _rankList = null;
        if (rankRange.rangeEntry != null)
        {
            int userLevel = 0;
            string userNick = string.Empty;
            int rankWin = 0;
            int rankLose = 0;
            int rankDraw = 0;
            decimal winRate = 0;
            int totalRankFight = 0;

            _rankList = new List<Rank>();
            int entryCount = rankRange.rangeEntry.Count<SortedSetEntry>();
            int rankPercent = 0;
            for (int i = 0; i < entryCount; i++)
            {
                if (rankRange.rangeEntry[i].Element == userInfo.userID)
                {
                    userLevel = userInfo.userDataBase.Level;
                    userNick = userInfo.userDataBase.nickName;
                    rankWin = userInfo.userDataBase.userProfile.rankWin;
                    rankLose = userInfo.userDataBase.userProfile.rankLose;
                    rankDraw = userInfo.userDataBase.userProfile.rankDraw;
                }
                else
                {
                    GameServer.documentDBConnect.GetUserLevel(rankRange.rangeEntry[i].Element, out userLevel);
                    GameServer.documentDBConnect.GetUserNick(rankRange.rangeEntry[i].Element, out userNick);
                    GameServer.documentDBConnect.GetUserRankInfo(rankRange.rangeEntry[i].Element, out rankWin, out rankLose, out rankDraw);
                }

                if (userLevel != 0 && !string.IsNullOrEmpty(userNick))
                {
                    if (rankRange.currentCount != 0) { rankPercent = (i + 1) * 100 / rankRange.currentCount; }

                    winRate = 0;
                    totalRankFight = rankWin + rankLose /*+ rankDraw*/;
                    if (totalRankFight != 0) winRate = (decimal)rankWin / totalRankFight;
                    string winRateStr = winRate.ToString("#0.##%", CultureInfo.InvariantCulture);
                    _rankList.Add(new Rank(userLevel, userNick, (int)rankRange.rangeEntry[i].Score, rankWin, rankLose, winRateStr, rankPercent));
                }
            }
        }

        RankList rankList = new RankList(PacketTypeList.SCNotiRankList.ToString(), rankRange.chanelName, _rankList);

        PacketsHandlerInner.DSRspRankRange(serverSession, rankList);
    }

    internal void GetSDReqUpdateSendFriendItem(string serverSession, string userSession, string friendID, string sendDate)
    {
        DateTime _sendDate = new DateTime();
        bool updateResult = false;
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            if (GlobalManager.Instance.ConvertToDateTime(sendDate, out _sendDate))
            {
                updateResult = GameServer.msSqlConnect.UpdateFriendSendGift(userInfo.userID, friendID, _sendDate);
            }
        }

        if (!updateResult)
        {
            Console.WriteLine("*** DB: Update send friend send item from DB Failed");
        }
        else
        {
            //DMReqBCNewFriendGift
            PacketsHandlerInner.DMRepBCNewFriendGift(serverSession, friendID);
        }
    }

    internal void GetSDReqDeleteFriend(string serverSession, string userSession, string friendID, FriendType friendType)
    {
        bool delResult = false;
        int otherFriendCount = 0;
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            if (friendType == FriendType.Friend)
            {
                GameServer.msSqlConnect.GetFriendCount(friendID, out otherFriendCount);
                if (otherFriendCount != 0)
                {
                    delResult = GameServer.msSqlConnect.DeleteFriend(userInfo.userID, friendID);
                }
            }
            else if (friendType == FriendType.Accepting || friendType == FriendType.Requesting)
            {
                delResult = GameServer.msSqlConnect.DeleteFriend(userInfo.userID, friendID);
            }

            PacketsHandlerInner.DSRspDeleteFriend(serverSession, delResult, userSession, otherFriendCount, friendID, friendType);

        }
    }

    internal void GetSDReqUpdateFriendType(string serverSession, string userSession, string userID, string friendID, int otherFriendCount)
    {
        bool updateResult = GameServer.msSqlConnect.UpdateFriendType(userID, friendID);

        //make DSRspUpdateFriendType
        PacketsHandlerInner.DSRspUpdateFriendType(serverSession, updateResult, userSession, friendID, otherFriendCount);
    }

    internal void GetSDReqNewFriend(string serverSession, string userID, /*string userNick,*/ string friendID/*, string friendNick*/)
    {
        bool addResult = GameServer.msSqlConnect.NewFriend(userID, /*userNick,*/ friendID/*, friendNick*/);

        //make DSRspNewFriend
        PacketsHandlerInner.DSRspNewFriend(serverSession, addResult, userID, friendID);
    }

    internal void GetSDReqGetRequestFriends(string serverSession, string userSession)
    {
        bool getResult = false;
        List<FriendData> requestFriends = null;
        List<FriendData> delFriends = null;
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            GameServer.msSqlConnect.GetRequestFriends(userInfo.userID, out requestFriends, out delFriends);

            if (userInfo._requestFriends == null) userInfo._requestFriends = new List<FriendData>();
            if (userInfo._delRequestFriends == null) userInfo._delRequestFriends = new List<FriendData>();

            userInfo._requestFriends = requestFriends;
            userInfo._delRequestFriends = delFriends;
            getResult = true;
        }

        PacketsHandlerInner.DSRspGetRequestFriends(serverSession, getResult, userSession);

    }

    internal void GetSDReqGetMyFriends(string serverSession, string userSession)
    {
        bool getResult = false;
        List<FriendData> myFriends = null;
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            GameServer.msSqlConnect.GetMyFriends(userInfo.userID, out myFriends);

            if (myFriends != null)
            {
                userInfo._myFriends = myFriends;
                getResult = true;
            }
        }

        PacketsHandlerInner.DSRspGetMyFriends(serverSession, getResult, userSession);
    }

    internal void GetSDReqGetFriendList(string serverSession, string userSession)
    {
        bool getResult = false;
        List<FriendData> myFriends = null;
        List<FriendData> requestFriends = null;
        List<FriendData> delFriends = null;
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo != null)
        {
            GameServer.msSqlConnect.GetMyFriends(userInfo.userID, out myFriends);
            GameServer.msSqlConnect.GetRequestFriends(userInfo.userID, out requestFriends, out delFriends);

            if (myFriends != null && requestFriends != null)
            {
                userInfo._myFriends = new List<FriendData>();
                userInfo._requestFriends = new List<FriendData>();
                userInfo._delRequestFriends = new List<FriendData>();

                userInfo._myFriends = myFriends;
                userInfo._requestFriends = requestFriends;
                userInfo._delRequestFriends = delFriends;
                getResult = true;
            }

            PacketsHandlerInner.DSRspGetFriendList(serverSession, getResult, userSession);

            //send SCNotiMenuAlarm - friend
            int alarmCount = 0;
            bool allowZero = false;
            userInfo.SetSendMenuAlarmFriends(alarmCount, allowZero);
        }
    }

    internal void GetSDReqNewMail(string serverSession, MailData mailData)
    {
        bool addResult = false;
        UserInfo userInfo = GameServer.GetUserInfo(mailData.userSession);
        if (userInfo != null)
        {
            addResult = GameServer.msSqlConnect.NewMail(userInfo.userID, mailData);
        }

        PacketsHandlerInner.DSRspNewMail(serverSession, addResult, mailData.userSession, mailData.sendID);
    }

    internal void GetSDReqNewUserData(string serverSession, AccountType accountType, string account, string password, string nick, int faceIdx, string deviceID)
    {
        bool isExist = true;
        string userID = string.Empty;
        while (isExist)
        {
            userID = Guid.NewGuid().ToString("N");//"CLI" + Random.Range(1000, 100000).ToString("D6");
            if (GameServer.documentDBConnect.EnableUserID(userID)) isExist = false;
        }

        bool result = GameServer.msSqlConnect.Register(userID, accountType.ToString(), account, password, nick, faceIdx, deviceID);

        if (result == true)
        {
            //result = GameServer.documentDBConnect.Register(userID, nick, password);
            result = GameServer.documentDBConnect.Register(userID, account, nick, faceIdx);
        }

        // make DSRepMakeuserData packet
        PacketsHandlerInner.DSRspNewUserData(serverSession, accountType, result, userID, nick);
    }

    internal void GetSDReqSetDeveloper(string serverSession, string userNick, UserType userType)
    {
        string account = string.Empty;
        bool setResult = GameServer.msSqlConnect.SetDeveloper(userNick, userType, out account);

        PacketsHandlerInner.DSRspSetDeveloper(serverSession, userNick, userType, setResult);
    }

    internal /*async*/ void GetSDReqDevList(UserType userType)
    {
        switch (userType)
        {
            case UserType.Developer:
                GameServer.msSqlConnect.GetDeveloperList();
                break;
            case UserType.DeveloperManager:
                GameServer.msSqlConnect.GetDeveloperManagerList();
                break;
            case UserType.Operator:
                GameServer.msSqlConnect.GetOperatorList();
                break;
            case UserType.OperatorManager:
                GameServer.msSqlConnect.GetOperatorManagerList();
                break;
            case UserType.All:
                GameServer.msSqlConnect.GetAllDevList();
                break;
            default: break;
        }
    }

    internal void GetSDReqUpdatedeviceID(string userID, /*string account,*/ string deviceID)
    {
        GameServer.msSqlConnect.UpdatedeviceID(userID, /*account,*/ deviceID);
    }

    internal void GetSDReqUpdatePassword(string serverSession, string account, string oldPassword, string newPassword, bool isFind)
    {
        LoginState state = LoginState.Unknown;
        string userID = string.Empty;
        string userNick = string.Empty;

        if (isFind) GameServer.msSqlConnect.UpdatePassword(account, oldPassword, newPassword.EncryptDES(), isFind, out state, out userID);
        else GameServer.msSqlConnect.UpdatePassword(account, oldPassword, newPassword, isFind, out state, out userID);

        if (userID != string.Empty)
        {
            GameServer.documentDBConnect.GetUserNick(userID, out userNick);
        }

        if (state == LoginState.OK && isFind)
        {
            //send email to email address: account
            GlobalManager.Instance.SendEmail(account, userNick, newPassword);
        }

        PacketsHandlerInner.DSRspUpdatePassword(serverSession, isFind ? PacketTypeList.CSReqFindPassword : PacketTypeList.CSReqChangePassword, state);
    }

    internal void GetSDReqUpdateFriendFight(string userID, string friendID, Result result_)
    {
        string _result = string.Format("[{0}]", result_.ToString().ToLower());

        if (!GameServer.msSqlConnect.UpdateFriendFight(userID, friendID, _result))
        {
            Console.WriteLine("Update Friend Fight Record Fail!");
        }
    }

    internal void GetSDReqChangeNick(string serverSession, string userSession, string nick)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        ChangeNickResult result = ChangeNickResult.Unknown;
        GameServer.msSqlConnect.ChangeNick(userInfo.userID, nick, out result);

        PacketsHandlerServer.SendSCRspChangeNick(serverSession, result);
        if (result == ChangeNickResult.OK)
        {
            userInfo.userDataBase.userItem.saqqaraTablet -= 1;
            userInfo.userDataBase.nickName = nick;
            //userInfo.userDataBase.userProfile.saqqaraTabletUseCount++;

            //send myinfo again
            userInfo.SetSendMyInfo();
            //send inventory again
            List<InventoryInfo> inventoryList = null;
            userInfo.SetInvItems(out inventoryList);
            PacketsHandlerServer.SendSCNotiInventory(serverSession, new InventoryList(PacketTypeList.SCNotiInventory.ToString(), TabType.Items, inventoryList));

            //send userinfo again
            int upLevelExp = MoonGlow.Data.LevelExpManager.Instance.GetUpLevelExp(userInfo.userDataBase.Level);
            PacketsHandlerServer.SendSCNotiUserInfo(serverSession, userInfo.userDataBase.FaceIdx, userInfo.userDataBase.nickName, userInfo.badgeIdx, userInfo.userDataBase.Level, userInfo.userDataBase.Exp, upLevelExp);

            //send iteminfo again
            //userInfo.SetSendItemInfoList();
            //save userinfo 
            userInfo.SaveUserInfo();
        }
    }

    //MDNotiBCChannelInfo
    internal void GetMDNotiRefreshChannelInfo(string serverSession)
    {
        if (!GameServer._isLoginServer) ChannelManager.Instance.ReadChannelInfo();

        if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
        {
            string msg = "League List Refreshed!";
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }
    }

    internal void GetMDNotiRefreshClientVersion(string serverSession)
    {
        if (GameServer._isLoginServer) GameServer.GetClientVersions();

        if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
        {
            string msg = "Client Version Refreshed!";
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }
    }

    internal void GetSDReqUpdateUserBan(string serverSession, string userNick, bool ban, int minutes)
    {
        string msg = string.Empty;
        string userID = string.Empty;

        if (minutes != 0) GameServer.msSqlConnect.UpdateUserBan(userNick, false, out userID);
        else GameServer.msSqlConnect.UpdateUserBan(userNick, ban, out userID);

        if (userID != string.Empty)
        {
            //PacketsHandlerInner.SMReqRemoveRedis
            if (ban == true) PacketsHandlerInner.DMRepBCBanUser(serverSession, userID, userNick, ban, minutes);
            else
            {
                msg = string.Format("Release User: {0} successfully.", userNick);
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }
        else if (userID == string.Empty)
        {
            //nick not exist
            msg = string.Format("User: {0} not exist!", userNick);
            PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
        }
    }

    internal void GetSDReqAddIAP(string serverSession, string userID, string userNick, IAPType iAPType, int itemIdx)
    {
        string _guid = Guid.NewGuid().ToString("N");
        bool addResult = GameServer.msSqlConnect.AddIAP(userID, userNick, iAPType, _guid, itemIdx);
        if (addResult)
        {
            PacketsHandlerServer.SendSCRspIAP(serverSession, _guid, itemIdx);//do something
        }
        else
        {
            //Console.WriteLine("!!!Error: Add IAP to DB error!!!");
            if (GameServer._logger.IsErrorEnabled)
            {
                string log = "GetSDReqAddIAP error";
                /*Logger*/
                GameServer._logger.Error(log);
            }
        }
    }

    internal void GetSDReqIAPResult(string serverSession, string userSession, string guid, IAPResult result)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        int elementIdx = 0;
        bool alreadyDone = false;
        bool updateResult = GameServer.msSqlConnect.UpdateIAPState(userInfo.userID, guid, result, out elementIdx, out alreadyDone);

        ShopResult shopResult = ShopResult.Unknown;
        if (alreadyDone)
        {
            shopResult = ShopResult.AlreadyExist;
        }
        else
        {
            if (updateResult)
            {
                if (result == IAPResult.OK)
                {
                    //add ruby or gold
                    //saveuserinfo
                    ItemElementInfo info = ItemManager.Instance.Get(elementIdx);
                    if (info != null)
                    {
                        int _itemIdx = 0;
                        int _itemCount = info.quantity;
                        if (info.tabType == TabType.Gold)
                        {
                            _itemIdx = (int)ElementType.Gold;
                        }
                        else if (info.tabType == TabType.Ruby)
                        {
                            _itemIdx = (int)ElementType.MoonRuby;
                        }
                        else return;//not ruby and gold

                        userInfo.AddUserItem(_itemIdx, _itemCount);
                        //userInfo.SaveUserInfo();
                        UserData userData = null;
                        userInfo.SaveUserData(out userData);
                        if (GameServer.documentDBConnect.UpdateUserInfo(userData))
                        {
                            //need update done
                            if (GameServer.msSqlConnect.UpdateIAPDone(userInfo.userID, guid))
                            {
                                shopResult = ShopResult.OK;
                            }
                            else
                            {
                                //Console.WriteLine("!!!Error: UpdateIAPDone to DB error!!!");
                                if (GameServer._logger.IsErrorEnabled)
                                {
                                    string log = "UpdateIAPDone error";
                                    /*Logger*/
                                    GameServer._logger.Error(log);
                                }
                                return;
                            }
                        }
                        else
                        {
                            //Console.WriteLine("!!!Error: UpdateUserInfo to DocumentDB error!!!");
                            if (/*Logger*/GameServer._logger.IsErrorEnabled)
                            {
                                /*Logger*/
                                GameServer._logger.Error("UpdateUserInfo to DocumentDB error");
                            }
                            return;
                        }
                    }
                }
                else if (result == IAPResult.Cancel)
                {
                    shopResult = ShopResult.CancelPayment;
                }
                else if (result == IAPResult.Fail)
                {
                    shopResult = ShopResult.Fail;
                }
                else
                {
                    //Console.WriteLine("!!!Error: IAPResult format error!!!");
                    if (/*Logger*/GameServer._logger.IsErrorEnabled)
                    {
                        /*Logger*/
                        GameServer._logger.Error("IAPResult format error");
                    }
                }
            }
            else // if( updateresult...
            {
                //Console.WriteLine("!!!Error: Update IAP to DB error!!!");
                if (/*Logger*/GameServer._logger.IsErrorEnabled)
                {
                    /*Logger*/
                    GameServer._logger.Error("Update IAP to DB error");
                }
                return;
            }
        }

        //PacketsHandlerServer.SendSCRspBuyElement(serverSession, shopResult);
        PacketsHandlerServer.SendSCRspIAPResult(serverSession, guid, shopResult);
        userInfo.IAPDoing = false;
    }

    //GetSDReqGetPlayerProfile
    internal void GetSDReqGetPlayerProfile(string serverSession, string playerNick)
    {
        int faceIdx = 0;
        int lvl = 0;
        int winCount = 0;
        int loseCount = 0;
        List<Badge> badgeList = null;
        List<Pet> petList = null;
        GameServer.documentDBConnect.GetPlayerProfile(playerNick, out faceIdx, out lvl, out winCount, out loseCount, out badgeList, out petList);

        int badgeIdx = 0;
        List<int> petIdxList = new List<int>();
        foreach (var badge in badgeList)
        {
            if (badge.isSelect)
            {
                badgeIdx = badge.badgeIdx;
                break;
            }
        }
        foreach (var pet in petList)
        {
            if (pet.isSelect)
            {
                petIdxList.Add(pet.petIdx);
            }
        }

        PlayerProfile playerProfile = new PlayerProfile(PacketTypeList.SCNotiPlayerProfile.ToString(), faceIdx, lvl, playerNick, winCount, loseCount, badgeIdx, petIdxList);
        PacketsHandlerServer.SendSCNotiPlayerProfile(serverSession, playerProfile);
    }

    //GetMDNotiUpdateLocalization
    internal void GetMDNotiUpdateLocalization(string serverSession)
    {
        if (!GameServer._isLoginServer)
        {
            LocalizationManager.Instance.ReadLocalization();
            if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
            {
                //send cmd
                string msg = "Localization Data Refreshed!";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }
    }

    //GetMDNotiUpdateNews
    internal void GetMDNotiUpdateNews(string serverSession)
    {
        if (!GameServer._isLoginServer)
        {
            NewsManager.Instance.ReadNews();
            if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
            {
                //send cmd
                string msg = "News Data Refreshed!";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }
    }

    //GetMDNotiUpdateMission
    internal void GetMDNotiUpdateMission(string serverSession)
    {
        if (!GameServer._isLoginServer)
        {
            MissionManager.Instance.ReadMission();
            if (SessionIdManager.Instance._sessionDic.ContainsKey(serverSession))
            {
                //send cmd
                string msg = "Mission Data Refreshed!";
                PacketsHandlerServer.SendSCNotiCMD(serverSession, msg);
            }
        }
    }

}
