﻿using MoonGlow;
using MoonGlow.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class RankPacketsManager : Singleton<RankPacketsManager>
{
    internal void GetCSReqChanelList(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.sceneType = SceneType.Chanel;
        ChannelManager.Instance.CheckChanelDate(serverSession, userSession, userInfo.userDataBase.userProfile.chanelIdx);
        ChannelManager.Instance.GetChanelList(serverSession, userSession);
    }

    internal void GetCSReqJoinChanel(string serverSession, string userSession, int iDx)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        ChanelResult result = ChanelResult.Unknown;

        if (userInfo._chanelDic == null || iDx <= 0 || iDx > userInfo._chanelDic.Count)
        {
            result = ChanelResult.Fail;
            PacketsHandlerServer.SendSCRspRankResult(serverSession, result);
            return;
        }

        DateTime startDate = new DateTime();
        DateTime endDate = new DateTime();
        //if already joined this chanel..
        if (userInfo.userDataBase.userProfile.chanelIdx == userInfo._chanelDic[iDx].chanelIdx)
        {
            result = ChanelResult.Fail;
            PacketsHandlerServer.SendSCRspRankResult(serverSession, result);
            return;
        }

        if (GlobalManager.Instance.ConvertToDateTime(userInfo._chanelDic[iDx].startDate, out startDate) && GlobalManager.Instance.ConvertToDateTime(userInfo._chanelDic[iDx].endDate, out endDate))
        {
            if (DateTime.Now < startDate || DateTime.Now >= endDate)
            {
                result = ChanelResult.NotAccept;
                PacketsHandlerServer.SendSCRspRankResult(serverSession, result);
                return;
            }
        }

        if (userInfo.userDataBase.Level < userInfo._chanelDic[iDx].minLevel || userInfo.userDataBase.Level > userInfo._chanelDic[iDx].maxLevel)
        {
            result = ChanelResult.LevelLimit;
            PacketsHandlerServer.SendSCRspRankResult(serverSession, result);
            return;
        }

        PacketsHandlerInner.SMReqAddRank(serverSession, /*true,*/ userInfo._chanelDic[iDx].chanelIdx, userInfo._chanelDic[iDx].name, userInfo.session, 0);

    }

    internal void GetCSReqGetRankItem(string serverSession, string userSession, int rankIdx)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        ChanelResult result = ChanelResult.Unknown;
        if (rankIdx <= 0 || rankIdx > userInfo._rankHistroy.Count)
        {
            result = ChanelResult.Fail;
            PacketsHandlerServer.SendSCRspRankResult(serverSession, result);
            return;
        }

        if (userInfo._rankHistroy[rankIdx - 1].itemExist == true && userInfo._rankHistroy[rankIdx - 1].itemGot == false)
        {
            int chanelIdx = 0;
            string chanelName = userInfo._rankHistroy[rankIdx - 1].chanelName;
            int rankCount = userInfo._rankHistroy[rankIdx - 1].rankCount;
            int rankPercent = userInfo._rankHistroy[rankIdx - 1].rankPercent;

            string origChannelName = string.Empty;
            string infoName = string.Empty;
            foreach (var info in ChannelManager._items)
            {
                origChannelName = info.name;
                infoName = (LocalizationManager._dict.ContainsKey(info.name))?
                            LocalizationManager.Instance.Get(info.name, userInfo.language) : info.name;
                if (chanelName == infoName)
                {
                    chanelIdx = info.chanelIdx;
                    break;
                }
            }
            List<ItemInfo> chanelItemList = null;
            ChannelRewardManager.Instance.GetItemList(chanelIdx, rankCount, rankPercent, out chanelItemList);
            result = ChanelResult.OK;
            userInfo._rankHistroy[rankIdx - 1].itemGot = true;

            if (chanelItemList.Count > 0)
            {
                foreach (var info in chanelItemList)
                {
                    int itemIdx = info.itemIndex;
                    int itemCount = info.count;

                    //add new mail
                    string subject = "Rank Reward";
                    string content = string.Empty;
                    userInfo.SetContent(out content, itemIdx);
                    userInfo.SetSendNewMail("SYSTEM", /*"SYSTEM",*/ subject, content, itemIdx, itemCount);
                }

                //save to DB
                PacketsHandlerInner.SDReqUpdateRankHis(serverSession, userInfo.userID, origChannelName);
            }
        }
        else
        {
            result = ChanelResult.Fail;
        }

        PacketsHandlerServer.SendSCRspRankResult(serverSession, result);

        if (result == ChanelResult.OK)
        {
            //send scnotiMyRankInfo again
            List<ChanelInfo> chanelInfoList = null;
            ChannelManager.Instance.GetChanelInfoList(out chanelInfoList);

            if (userInfo.userDataBase.userProfile.chanelIdx != 0)
            {
                foreach (var info in chanelInfoList)
                {
                    if (userInfo.userDataBase.userProfile.chanelIdx == info.chanelIdx)
                    {
                        PacketsHandlerInner.SMReqCurrentRank(serverSession, userSession, userInfo.userID, info.chanelIdx, info.name, userInfo.userDataBase.userProfile.rankPoint, userInfo.userDataBase.userProfile.rankWin, userInfo.userDataBase.userProfile.rankLose, userInfo.userDataBase.userProfile.rankDraw, false);

                        break;
                    }
                }
            }
            else
            {
                PacketsHandlerServer.SendSCNotiMyRankInfo(serverSession, new RankHistory(PacketTypeList.SCNotiMyRankInfo.ToString(), string.Empty, 0, 0, 0, 0, 0, "0%", userInfo._rankHistroy));
            }


        }
    }

    internal void GetCSReqMyRankInfo(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.sceneType = SceneType.RankHistory;
        //if not joined any chanel, get history list, 
        //else check date and 
        //if chanel closed, update DB and load history list then.. 
        if (userInfo.userDataBase.userProfile.chanelIdx != 0)
        {
            ChannelManager.Instance.CheckChanelDate(serverSession, userInfo.session, userInfo.userDataBase.userProfile.chanelIdx);
            //if (userInfo.userDataBase.userProfile.chanelIdx == 0) { return; }
        }

        //get rankHis from DB...
        PacketsHandlerInner.SDReqRankHistory(serverSession, userInfo.session);
    }

    internal void GetCSReqRewardList(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        ChannelManager.Instance.CheckChanelDate(serverSession, userInfo.session, userInfo.userDataBase.userProfile.chanelIdx);
        if (userInfo.userDataBase.userProfile.chanelIdx == 0) { return; }

        List<Reward> rewardList = null;
        ChannelRewardManager.Instance.GetRewardList(userInfo.userDataBase.userProfile.chanelIdx, out rewardList);

        //Send SCNotiRewardList
        PacketsHandlerServer.SendSCNotiRewardList(serverSession, new RewardList(PacketTypeList.SCNotiRewardList.ToString(), rewardList));

    }

    internal void GetCSReqRankList(string serverSession, string userSession)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.sceneType = SceneType.Rank;
        ChannelManager.Instance.CheckChanelDate(serverSession, userInfo.session, userInfo.userDataBase.userProfile.chanelIdx);

        if (userInfo.userDataBase.userProfile.chanelIdx == 0) { return; }

        List<ChanelInfo> chanelInfoList = null;
        ChannelManager.Instance.GetChanelInfoList(out chanelInfoList);
        foreach (var info in chanelInfoList)
        {
            if (userInfo.userDataBase.userProfile.chanelIdx == info.chanelIdx)
            {
                PacketsHandlerInner.SMReqRankRange(serverSession, userSession, info.name);

                break;
            }
        }
    }
}
