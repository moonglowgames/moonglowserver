﻿using MoonGlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class FriendPacketsManager : Singleton<FriendPacketsManager>
{
    internal void GetCSReqFriendList(string serverSession, string userSession, FriendTabType tabType)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        userInfo.sceneType = SceneType.Friend;
        switch (tabType)
        {
            case FriendTabType.MyFriends:
                userInfo.GetMyFriends();

                break;
            case FriendTabType.RequestFriends:
                userInfo.GetRequestFriends();

                break;
            case FriendTabType.RecommendFriends:
                List<Friend> friendList = null;
                userInfo.GetRecommendFriends(out friendList);
                PacketsHandlerServer.SendSCNotiFriendList(serverSession, new FriendList(PacketTypeList.SCNotiFriendList.ToString(), FriendTabType.RecommendFriends, 20, friendList));

                break;
            default: break;
        }
    }

    internal void GetCSReqAcceptFriend(string serverSession, string userSession, FriendTabType tabType, int friendIdx)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        FriendResult friendResult = FriendResult.Unknown;
        if (tabType == FriendTabType.RequestFriends)
        {
            //user self not accept any more..   
            if (userInfo._myFriends.Count >= GlobalManager.MAX_FRIEND)
            {
                friendResult = FriendResult.NotAccept;

                //Send SCRspFriendResut
                PacketsHandlerServer.SendSCRspFriendResut(serverSession, PacketTypeList.CSReqAcceptFriend, friendResult);
            }
            else if (friendIdx < 1 || friendIdx > userInfo._requestFriends.Count)
            {
                friendResult = FriendResult.NotExist;

                //Send SCRspFriendResut
                PacketsHandlerServer.SendSCRspFriendResut(serverSession, PacketTypeList.CSReqAcceptFriend, friendResult);
            }
            else
            {
                PacketsHandlerInner.SDReqUserFriendCount(serverSession, userInfo.session, userInfo._requestFriends[friendIdx - 1].friendID, friendIdx, PacketTypeList.CSReqAcceptFriend);
            }
        }
        else
        {
            friendResult = FriendResult.Fail;

            //Send SCRspFriendResut
            PacketsHandlerServer.SendSCRspFriendResut(serverSession, PacketTypeList.CSReqAcceptFriend, friendResult);
        }
    }

    internal void GetCSReqGetAllGifts(string serverSession, string userSession, FriendTabType tabType)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        FriendResult friendResult = FriendResult.Unknown;
        if (tabType == FriendTabType.MyFriends)
        {
            userInfo.GetAllGifts(out friendResult);
        }
        else { friendResult = FriendResult.Fail; }

        //Send SCRspFriendResut
        PacketsHandlerServer.SendSCRspFriendResut(serverSession, PacketTypeList.CSReqGetAllGifts, friendResult);
    }

    internal void GetCSReqSendAllGifts(string serverSession, string userSession, FriendTabType tabType)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        FriendResult friendResult = FriendResult.Unknown;
        if (tabType == FriendTabType.MyFriends)
        {
           userInfo.SendAllGifts(out friendResult);
        }
        else { friendResult = FriendResult.Fail; }

        //Send SCRspFriendResut
        PacketsHandlerServer.SendSCRspFriendResut(serverSession, PacketTypeList.CSReqSendAllGifts, friendResult);
    }

    internal void GetCSReqGetGift(string serverSession, string userSession, FriendTabType tabType, int friendIdx)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        FriendResult friendResult = FriendResult.Unknown;
        if (tabType == FriendTabType.MyFriends)
        {
            if (friendIdx < 1 || friendIdx > userInfo._myFriends.Count)
            {
                friendResult = FriendResult.NotExist;
            }
            else
            {
                userInfo.GetGift(friendIdx, out friendResult);
            }
        }
        else { friendResult = FriendResult.Fail; }

        //Send SCRspFriendResut
        PacketsHandlerServer.SendSCRspFriendResut(serverSession, PacketTypeList.CSReqGetGift, friendResult);
    }

    internal void GetCSReqSendGift(string serverSession, string userSession, FriendTabType tabType, int friendIdx)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        FriendResult friendResult = FriendResult.Unknown;
        if (tabType == FriendTabType.MyFriends)
        {            
            if (friendIdx < 1 || friendIdx > userInfo._myFriends.Count)
            {
                friendResult = FriendResult.NotExist;
            }
            else
            {
                userInfo.SendGift(friendIdx, out friendResult);
            }           
        }
        else { friendResult = FriendResult.Fail; }

        //Send SCRspFriendResut
        PacketsHandlerServer.SendSCRspFriendResut(serverSession, PacketTypeList.CSReqSendGift, friendResult);
    }

    internal void GetCSReqDeleteFriend(string serverSession, string userSession, FriendTabType tabType, int friendIdx)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        FriendResult friendResult = FriendResult.Unknown;
        if (tabType == FriendTabType.MyFriends)
        {
           if (friendIdx < 1 || friendIdx > userInfo._myFriends.Count)
            {
                friendResult = FriendResult.NotExist;
            }
            else
            {
                friendResult = FriendResult.OK;
                userInfo.DeleteFriend(friendIdx);
            }            
        }
        else { friendResult = FriendResult.Fail; }

        //Send SCRspFriendResut
        PacketsHandlerServer.SendSCRspFriendResut(serverSession, PacketTypeList.CSReqDeleteFriend, friendResult);
    }

    internal void GetCSReqRequestFriend(string serverSession, string userSession, FriendTabType tabType, int friendIdx)
    {
        UserInfo userInfo = GameServer.GetUserInfo(userSession);
        if (userInfo == null)
        {
            GlobalManager.Instance.NullUserInfo(serverSession);
            return;
        }

        FriendResult friendResult = FriendResult.Unknown;
        if (tabType == FriendTabType.RecommendFriends)
        {
            //user self not accept any more..
            if (userInfo._myFriends.Count >= GlobalManager.MAX_FRIEND)
            {
                friendResult = FriendResult.NotAccept;

                //Send SCRspFriendResut
                PacketsHandlerServer.SendSCRspFriendResut(serverSession, PacketTypeList.CSReqRequestFriend, friendResult);

                //refresh recommend friend list
                userInfo._recommendFriends.RemoveAt(friendIdx - 1);
                List<Friend> friendList = new List<Friend>();
                for (int i = 0; i < userInfo._recommendFriends.Count; i++)
                {
                    friendList.Add(new Friend(i + 1, userInfo._recommendFriends[i].friendNick, userInfo._recommendFriends[i].faceIdx, userInfo._recommendFriends[i].level, userInfo._recommendFriends[i].friendLastLoginDate, userInfo._recommendFriends[i].friendType, false, false, 0, 0, 0));
                }

                //if (userInfo.sceneType == SceneType.Room)
                //{
                    //send scnotigame result again
                    //think make a new packets..maybe not 
                //}
                //send SCNotifriend list again-recommend
                PacketsHandlerServer.SendSCNotiFriendList(serverSession, new FriendList(PacketTypeList.SCNotiFriendList.ToString(), FriendTabType.RecommendFriends, 20, friendList));
            }
            else if (friendIdx < 1 || friendIdx > userInfo._recommendFriends.Count)
            {
                friendResult = FriendResult.NotExist;

                //Send SCRspFriendResut
                PacketsHandlerServer.SendSCRspFriendResut(serverSession, PacketTypeList.CSReqRequestFriend, friendResult);

                //refresh recommend friend list
                userInfo._recommendFriends.RemoveAt(friendIdx - 1);
                List<Friend> friendList = new List<Friend>();
                for (int i = 0; i < userInfo._recommendFriends.Count; i++)
                {
                    friendList.Add(new Friend(i + 1, userInfo._recommendFriends[i].friendNick, userInfo._recommendFriends[i].faceIdx, userInfo._recommendFriends[i].level, userInfo._recommendFriends[i].friendLastLoginDate, userInfo._recommendFriends[i].friendType, false, false, 0, 0, 0));
                }
                //send SCNotifriend list again-recommend
                PacketsHandlerServer.SendSCNotiFriendList(serverSession, new FriendList(PacketTypeList.SCNotiFriendList.ToString(), FriendTabType.RecommendFriends, 20, friendList));
            }
            else
            {
                PacketsHandlerInner.SDReqUserFriendCount(serverSession, userInfo.session, userInfo._recommendFriends[friendIdx - 1].friendID, friendIdx, PacketTypeList.CSReqRequestFriend);
            }
        }
           
    }
}
